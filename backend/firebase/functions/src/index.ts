import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

admin.initializeApp({
    credential: admin.credential.applicationDefault(),
    databaseURL: "https://well-playd.firebaseio.com"
});

const btoa = require("btoa");
const axios = require("axios"); // http library
const qs = require("qs"); // querystring parsing

let config = functions.config();

let clientId = config.spotify.client_id;
let clientSecret = config.spotify.client_secret;
let redirectUri = config.spotify.ios.redirect_uri;

// accept form-urlencoded submissions
axios.interceptors.request.use((request:any) => {
    if (request.data && request.headers['Content-Type'] === 'application/x-www-form-urlencoded') {
        request.data = qs.stringify(request.data);
    }
    return request;
});

export const api_token = functions.https.onRequest((req, res) => {
    console.log(req.query);
    console.log(req.body);

    axios({
        method: "POST",
        url: "https://accounts.spotify.com/api/token",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Basic " + btoa(clientId + ":" + clientSecret)
        },
        data: {
            grant_type: "authorization_code",
            redirect_uri: redirectUri,
            code: req.body.code
        }
    }).then((response: any) => {
        res.set("Content-Type", "text/json").status(response.status).send(response.data);
    }).catch((err: any) => {
        console.log(err);
        res.set("Content-Type", "text/json").status(err.response.status).send(err.response.data);
    });
});

export const refresh_token = functions.https.onRequest((req, res) => {
    axios({
        method: "POST",
        url: "https://accounts.spotify.com/api/token",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Basic " + btoa(clientId + ":" + clientSecret)
        },
        data: {
            grant_type: "refresh_token",
            refresh_token: req.body.refresh_token
        }
    }).then((response: any) => {
        res.set("Content-Type", "text/json").status(response.status).send(response.data);
    }).catch((err: any) => {
        res.set("Content-Type", "text/json").status(err.response.status).send(err.response.data);
    });
});
