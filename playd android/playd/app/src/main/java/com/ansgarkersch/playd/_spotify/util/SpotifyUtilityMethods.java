package com.ansgarkersch.playd._spotify.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

import java.util.List;

public class SpotifyUtilityMethods {

    /**
     * checks if spotify application is installed on the device
     * @param context
     * @return
     */
    public static boolean isSpotifyInstalled(Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo("com.spotify.music", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    /**
     * opens the given URI in spotify
     * @param context
     * @param uri
     */
    public static void openContentInSpotify(Context context, String uri) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(uri));
        intent.putExtra(Intent.EXTRA_REFERRER,
                Uri.parse("android-app://" + context.getPackageName()));
        context.startActivity(intent);

    }

    /**
     * redirects to the playStore site where spotfiy can be downloaded
     * @param context
     */
    public static void installSpotify(Context context) {
        final String appPackageName = "com.spotify.music";
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException ignored) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }

    }

    /**
     * removes the 'spotify:track:' from the trackID
     * @param trackId
     * @return
     */
    public static String trimTrackId(String trackId) {

        if (trackId.contains(":")) {
            return trackId.substring(trackId.lastIndexOf(":") + 1);
        }
        return trackId;
    }

    /**
     * adds the 'spotify:track:' to the trackID
     * @param trackId
     * @return
     */
    public static String completeTrackId(String trackId) {

        if (!trackId.contains(":")) {
            return "spotify:track:".concat(trackId);
        }
        return trackId;
    }

    public static String prepareSeveralTrackRequest(List<String> trackIds) {
        StringBuilder stringBuilder = new StringBuilder("");
        for (String s : trackIds) {
            String trimmed = SpotifyUtilityMethods.trimTrackId(s); // trim id if necessary
            stringBuilder.append(trimmed).append(",");
        }
        return stringBuilder.toString().substring(0, stringBuilder.length() - 1);
    }

}
