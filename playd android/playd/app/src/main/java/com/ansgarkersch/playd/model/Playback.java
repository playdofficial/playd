package com.ansgarkersch.playd.model;

import android.support.annotation.NonNull;

import com.ansgarkersch.playd._spotify.model.Track;
import com.ansgarkersch.playd.globals.GlobalVariables;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Playback implements Serializable, Comparable<Playback> {

    private String userId = "";
    private String trackId = "";
    private long trackStartedTimestamp = 0L;
    private long spotStartedTimestamp = 0L;
    private long currentMS = 0L;
    private long trackDuration = 0L;
    private String spotName = "Hello Playd";
    private double priority = 1.0; // BASIC = 1.0 ; GOLD = 2.0 ; BOOST = >2.0 ; ISFAMOUS = 3.0 ... TBD

    private Map<String, Boolean> currentSpotter = new HashMap<>();
    private Map<String, Boolean> trackGenres = new HashMap<>();
    private String trackName = "";
    private String trackArtistName = "";
    private String trackArtistId = "";
    private String albumArt_640 = ""; // 640x640
    private String albumArt_300 = ""; // 300x400

    private String actionCode = "";

    // Necessary for firebase jackson
    public Playback() { }

    public Playback(String id) {
        userId = id;
    }

    public static Playback getLocalPlayback(Track track) {
        Playback playback = new Playback();
        playback.setUserId(GlobalVariables.LOCAL_PLAYBACK);
        playback.setTrackId(track.getId());
        playback.setTrackStartedTimestamp(0);
        playback.setCurrentMS(0);
        // TODO missing: spotName & distance
        playback.setTrackDuration(track.getDuration_ms());
        playback.setTrackName(track.getName());
        playback.setTrackArtistId(track.getArtists().get(0).getId());
        playback.setTrackArtistName(track.getArtists().get(0).getName());
        playback.setAlbumArt_640(track.getAlbum().getImages().get(0).getUrl());
        playback.setAlbumArt_300(track.getAlbum().getImages().get(1).getUrl());
        return playback;
    }
    
    @Override
    public boolean equals(Object obj) {
        return obj != null && obj.getClass() == Playback.class && getUserId().equals(((Playback) obj).getUserId()) && getTrackId().equals(((Playback) obj).getTrackId());

    }

    @Override
    public int compareTo(@NonNull Playback o) {
        return Double.compare(getPriority(), o.getPriority());
    }


    /*
    generated methods
    */

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSpotName() {
        return spotName;
    }

    public void setSpotName(String spotName) {
        this.spotName = spotName;
    }

    public Map<String, Boolean> getCurrentSpotter() {
        return currentSpotter;
    }

    public void setCurrentSpotter(Map<String, Boolean> currentSpotter) {
        this.currentSpotter = currentSpotter;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getTrackArtistName() {
        return trackArtistName;
    }

    public void setTrackArtistName(String trackArtistName) {
        this.trackArtistName = trackArtistName;
    }

    public String getAlbumArt_640() {
        return albumArt_640;
    }

    public void setAlbumArt_640(String albumArt_640) {
        this.albumArt_640 = albumArt_640;
    }

    public String getAlbumArt_300() {
        return albumArt_300;
    }

    public void setAlbumArt_300(String albumArt_300) {
        this.albumArt_300 = albumArt_300;
    }

    public long getTrackStartedTimestamp() {
        return trackStartedTimestamp;
    }

    public void setTrackStartedTimestamp(long trackStartedTimestamp) {
        this.trackStartedTimestamp = trackStartedTimestamp;
    }

    public long getSpotStartedTimestamp() {
        return spotStartedTimestamp;
    }

    public void setSpotStartedTimestamp(long spotStartedTimestamp) {
        this.spotStartedTimestamp = spotStartedTimestamp;
    }

    public long getTrackDuration() {
        return trackDuration;
    }

    public void setTrackDuration(long trackDuration) {
        this.trackDuration = trackDuration;
    }

    public long getCurrentMS() {
        return currentMS;
    }

    public void setCurrentMS(long currentMS) {
        this.currentMS = currentMS;
    }

    public String getTrackArtistId() {
        return trackArtistId;
    }

    public void setTrackArtistId(String trackArtistId) {
        this.trackArtistId = trackArtistId;
    }

    public Map<String, Boolean> getTrackGenres() {
        return trackGenres;
    }

    public void setTrackGenres(Map<String, Boolean> trackGenres) {
        this.trackGenres = trackGenres;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public double getPriority() {
        return priority;
    }

    public void setPriority(double priority) {
        this.priority = priority;
    }
}
