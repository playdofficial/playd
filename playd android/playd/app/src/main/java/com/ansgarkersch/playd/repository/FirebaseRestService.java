package com.ansgarkersch.playd.repository;

import com.ansgarkersch.playd.model.Playback;
import com.ansgarkersch.playd.model.Spot;
import com.ansgarkersch.playd.model.User;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface FirebaseRestService {

    @GET("loginWithSpotify")
    Call<String> loginWithSpotify(@Header("AccessToken") String accessToken, @Header("SpotifyUserId") String spotifyUserId);

    @GET("getUserById")
    Call<User> getUserById(@Header("Authorization") String authorization, @Query("userId") String userId);

    @GET("getAllFollowedCurrentSpots")
    Call<List<Spot>> getAllFollowedCurrentSpots(@Header("Authorization") String authorization,
                                                @Query("limit") int limit,
                                                @Query("offsetTimestamp") long offsetTimestamp,
                                                @Query("updateFlag") int updateFlag,
                                                @Query("alreadyFetched") String alreadyFetched);

    @GET("getSingleUserOverview")
    Call<Spot> getSingleCurrentSpot(@Header("Authorization") String authorization,
                                    @Query("foreignUserId") String foreignUserId);

    @GET("getAllCurrentlyPlayingGenres")
    Call<List<String>> getAllCurrentlyPlayingGenres(@Header("Authorization") String authorization);

    @GET("getAllCurrentPlaybacksByGenre")
    Call<List<Playback>> getAllCurrentPlaybacksByGenre(@Header("Authorization") String authorization,
                                                       @Query("genre") String genre);

    @POST("sendMessageToDevice")
    Call<Void> sendMessageToDevice(@Header("Authorization") String authorization,
                                   @Body Map<String, String> data);

    /*
    The following methods are deprecated
     */

    @GET("getAllFollowedUIds")
    Call<List<String>> getAllFollowedUIds(@Header("Authorization") String authorization, @Query("userId") String userId);

    @GET("getAllFollowersUIds")
    Call<List<String>> getAllFollowersUIds(@Header("Authorization") String authorization, @Query("userId") String userId);

    @GET("getAllFollowedCurrentPlaybacks")
    Call<List<Playback>> getAllFollowedCurrentPlaybacks(@Header("Authorization") String authorization);

    @GET("getAllFollowedCurrentUsers")
    Call<List<User>> getAllFollowedCurrentUsers(
            @Header("Authorization") String authorization,
            @Query("limit") int limit,
            @Query("offset") int offset,
            @Query("currentPlayingSpotId") String currentPlayingSpotId);

    @GET("updatePlaybackStatus")
    Call<Void> updatePlaybackStatus(@Header("Authorization") String authorization, @Query("status") String status);

    @POST("updateCurrentPlayback")
    Call<Void> updateCurrentPlayback(@Header("Authorization") String authorization, @Body Map<String, Object> data);

    @DELETE("deleteCurrentPlayback")
    Call<Void> deleteCurrentPlayback(@Header("Authorization") String authorization);

    @POST("updateRegistrationToken")
    Call<Void> updateRegistrationToken(@Header("Authorization") String authorization, @Header("RegistrationToken") String userId);

    @POST("notifyFirebaseAboutSubscription")
    Call<Void> subscribeToUser(@Header("Authorization") String authorization,
                               @Header("RegistrationToken") String registrationToken,
                               @Body Map<String, String> data);

    @POST("unsubscribeFromUser")
    Call<Void> unsubscribeFromUser(@Header("Authorization") String authorization,
                                   @Header("RegistrationToken") String registrationToken,
                                   @Body Map<String, String> data);

    @GET("resubscribeToAllFollowedTopics")
    Call<Void> resubscribeToAllFollowedTopics(@Header("Authorization") String authorization,
                                              @Header("RegistrationToken") String registrationToken);

}
