package com.ansgarkersch.playd.dialogfragments;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.util.SquareImageView;
import com.bumptech.glide.Glide;


public class DialogFragmentSingleImage extends android.support.v4.app.DialogFragment {

    private SquareImageView image;

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.dialogfragment_galleryimage, null);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        image = view.findViewById(R.id.image_fragmentdialog_gallery);

        Bundle bundle = getArguments();
        if (bundle != null) {
            final String thumbnailUrl = getArguments().getString("thumbnailUrl");
            final String imageUrl = getArguments().getString("profileImageUrl");

            Glide.with(view.getContext()).load(
                    GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE.equals(thumbnailUrl)
                            ?
                            R.mipmap.no_profile_image
                            :
                            imageUrl).into(image);

            /*
            if (GlobalVariables.DIALOG_GALLERY_IMAGE_INTENT_EXTRA_PLACEHOLDER.equals(thumbnailUrl)) {
                // thumbnailUrl is not availibe, just load the imageUrl
                Picasso.get().load(imageUrl).into(image);
            } else {
                // thumbnailUrl is availibe, first load the thumbnailUrl (again) and then "lazy-load" the imageUrl afterwards
                Picasso.get().load(thumbnailUrl).into(image);
                Picasso.get().load(imageUrl).fetch(new Callback() {
                    @Override
                    public void onSuccess() {
                        Picasso.get().load(imageUrl).into(image);
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                });
            }
            */

        } else {
            dismiss();
            Toast.makeText(view.getContext(), view.getResources().getString(R.string.toast_unexpected_error_occured), Toast.LENGTH_SHORT).show();
        }

        return view;
    }

    /**
     * Dialog Fragment Factory
     * @param profileImageUrl
     * @return
     */
    public static DialogFragmentSingleImage newInstance(String thumbnailUrl, String profileImageUrl) {
        DialogFragmentSingleImage ret = new DialogFragmentSingleImage();

        Bundle args = new Bundle();
        args.putString("thumbnailUrl", thumbnailUrl);
        args.putString("profileImageUrl", profileImageUrl);
        ret.setArguments(args);

        return ret;
    }

}
