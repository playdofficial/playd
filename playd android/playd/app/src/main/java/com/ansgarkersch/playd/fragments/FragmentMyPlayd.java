package com.ansgarkersch.playd.fragments;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd._spotify.repository.SpotifyRepository;
import com.ansgarkersch.playd.activities.ActivityMain;
import com.ansgarkersch.playd.activities.ActivityWatchProfile;
import com.ansgarkersch.playd.activities.FragmentLoadingCallback;
import com.ansgarkersch.playd.adapter.AdapterRecyclerMyPlaydFeed;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.Advertisement;
import com.ansgarkersch.playd.model.RowMyPlaydSpotExplorer;
import com.ansgarkersch.playd.model.Spot;
import com.ansgarkersch.playd.model.User;
import com.ansgarkersch.playd.model.UserOverview;
import com.ansgarkersch.playd.repository.PlaybackRepository;
import com.ansgarkersch.playd.repository.SpotRepository;
import com.ansgarkersch.playd.repository.UserRepository;
import com.ansgarkersch.playd.repository.ValueCallback;
import com.ansgarkersch.playd.services.ForegroundServiceSpotListener;
import com.ansgarkersch.playd.viewModel.MySpotsViewModel;
import com.ansgarkersch.playd.viewModel.MyUserViewModel;
import com.ansgarkersch.playd.viewModel.MyUserViewModelFactory;
import com.dinuscxj.progressbar.CircleProgressBar;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import me.everything.android.ui.overscroll.IOverScrollDecor;
import me.everything.android.ui.overscroll.IOverScrollUpdateListener;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;
import me.everything.android.ui.overscroll.VerticalOverScrollBounceEffectDecorator;
import me.everything.android.ui.overscroll.adapters.RecyclerViewOverScrollDecorAdapter;

public class FragmentMyPlayd extends Fragment {

    // UI elements
    private View view;
    private RecyclerView recyclerViewMyPlaydFeed;

    // Recycler
    private AdapterRecyclerMyPlaydFeed adapter;
    private LinearLayoutManager linearLayoutManager;

    // Repositories
    private UserRepository userRepository;
    private SpotifyRepository spotifyRepository;
    private SpotRepository spotRepository;
    private PlaybackRepository playbackRepository;

    // View Models
    private MySpotsViewModel mySpotsViewModel;
    private MyUserViewModel myUserViewModel;

    // Callback
    private FragmentLoadingCallback fragmentLoadingCallback;

    private boolean isFetching = false;
    private boolean scrollingFinished = false;
    private int visibleItemCount;
    private int totalItemCount;
    private int pastVisiblesItems;

    // Util
    private boolean initialized = false;
    private String TAG = getClass().getName() + " - ";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_myplayd,
                container, false);

        return view;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() == null) {
            return;
        }

        // UI elements
        recyclerViewMyPlaydFeed = view.findViewById(R.id.myplayd_spots_recyclerView);

        // Repositories
        userRepository = new UserRepository();
        spotifyRepository = new SpotifyRepository(getActivity());
        spotRepository = new SpotRepository(userRepository);
        playbackRepository = new PlaybackRepository(getActivity());

        // View Models
        mySpotsViewModel = ViewModelProviders.of(getActivity()).get(MySpotsViewModel.class);
        myUserViewModel = ViewModelProviders.of(getActivity(), new MyUserViewModelFactory(getActivity())).get(MyUserViewModel.class);

        // Init Current followed currently playing UserOverviews
        myUserViewModel.getCurrentUser().observe(getActivity(), new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                if (user == null) {
                    return;
                }

                // Init MySpots RecyclerView
                initRecyclerView();

                // Init MySpotsViewModel
                mySpotsViewModel.init(user, userRepository, spotifyRepository, spotRepository, playbackRepository);

                // Init MyPlaydFeed
                initMyPlaydFeed();

            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Callback
        if (context instanceof FragmentLoadingCallback) {
            fragmentLoadingCallback = (FragmentLoadingCallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement FragmentLoadingCallback");
        }
    }

    @Override
    public void onDetach() {
        fragmentLoadingCallback = null;
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister the spot listener
        mySpotsViewModel.unregisterAllListeners();
    }


    private void initMyPlaydFeed() {
        // Observe followed current recyclerViewMyPlaydFeed
        mySpotsViewModel.getFollowedSpots().observe(this,
                new Observer<Map<String, Spot>>() {
                    @Override
                    public void onChanged(@Nullable Map<String, Spot> stringSpotMap) {
                        if (stringSpotMap == null) {
                            return;
                        }

                        // Notify the Parent Activity (in this case ActivityMain)
                        fragmentLoadingCallback.notifyFinishedLoading(stringSpotMap.size());

                        // Notify the adapter
                        // !! Beware of duplicates !!
                        adapter.updateMyPlaydFeed(stringSpotMap);
                    }
                });
    }

    /**
     * Like pull-to-refresh
     */
    private void refreshList() {
        mySpotsViewModel.fetchLatestSpots(new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                // Returns the size of the fresh fetched list
                // If the value is 0, hide the refresh-loading-view
                // Otherwise, the refresh-loading-view will be hidden when the viewModel observer recieves the spot objects
                if ((int) value == 0) {
                    // Hide the refresh-loading-view
                    // TODO
                }
            }

            @Override
            public void onFailure(Object value) {
                // Do something
                Log.d(TAG, "refreshList() - onFailure: " + value);
            }
        });
    }

    public boolean isFeedScrolled() {
        return false; // todo dummy
        /*
        if (recyclerViewMyPlaydFeed == null) {
            return false;
        }
        if (totalItemCount <= 3) {
            return false;
        }
        if (linearLayoutManager != null && linearLayoutManager.findFirstVisibleItemPosition() == 0) {
            return false;
        }
        return pastVisiblesItems >= 2;
        */
    }

    public void scrollToTop() {
        if (recyclerViewMyPlaydFeed == null) {
            return;
        }
        recyclerViewMyPlaydFeed.scrollToPosition(0);
    }

    private void play(Spot spot) {
        // Prevent UI glitch
        if (getActivity() != null) {
            getActivity().findViewById(R.id.activity_main_circleprogressbar_bottombar_spotprogress).setVisibility(View.GONE);
        }
        Intent serviceIntent = new Intent(getActivity(), ForegroundServiceSpotListener.class);
        serviceIntent.addCategory(GlobalVariables.SPOTIFY_PLAY);
        serviceIntent.putExtra(GlobalVariables.INTENT_SPOT, spot);
        getActivity().startService(serviceIntent);
    }

    private void pause() {
        Intent serviceIntent = new Intent(getActivity(), ForegroundServiceSpotListener.class);
        serviceIntent.addCategory(GlobalVariables.SPOTIFY_PAUSE);
        getActivity().startService(serviceIntent);
    }

    private void initRecyclerView() {

        // Set up the adapter
        adapter = new AdapterRecyclerMyPlaydFeed(view.getContext(), 4); // TODO set adFrequency

        // Init SpotExplorer View
        adapter.addToSpotList(new RowMyPlaydSpotExplorer());

        // todo just testing
        // Init ads
        List<Advertisement> ads = new ArrayList<>();
        Advertisement ad1 = new Advertisement();
        Advertisement ad2 = new Advertisement();
        Advertisement ad3 = new Advertisement();
        ads.add(ad1);
        ads.add(ad2);
        ads.add(ad3);
        adapter.setAds(ads);
        // todo just testing

        // Init UserSuggestions View


        // CurrentPlaybackListener
        adapter.setCurrentPlaybackListener(new AdapterRecyclerMyPlaydFeed.ItemClickListener() {
            @Override
            public void onClickPlayback(View view, Spot spot, boolean isPlaying) {
                if (isPlaying) { // 'play' is clicked

                    ((ActivityMain) getActivity()).bindForegroundServiceSpotListener();
                    // mark the track content as loading
                    adapter.markSelectedSpotAsLoading(spot.getId());
                    play(spot);
                } else { // 'pause' is clicked
                    // mark the track content as loading
                    adapter.markSelectedSpotAsPlaying("");
                    pause();
                }
            }

            @Override
            public void onClickOpenAccountWindow(View view, UserOverview user) {
                Intent intent = new Intent(getActivity(), ActivityWatchProfile.class);
                intent.putExtra(GlobalVariables.INTENT_PROFILE_ID, user.getId());
                intent.putExtra(GlobalVariables.INTENT_PROFILE_USERNAME, user.getU());
                intent.putExtra(GlobalVariables.INTENT_PROFILE_CURRENT_USER, myUserViewModel.getCurrentUser().getValue());

                /*
                Put the ArrayList of Currently Followed Spots - Users and put them into the
                cachedUsers-Map in ActivityWatchProfile to minimize the duplicate account-fetching
                 */
                /*
                if (spotViewModel.getFollowedSpots().getValue().values() != null) {
                    ArrayList<User> cachedUsers = new ArrayList<>();
                    for (Spot spot : spotViewModel.getFollowedSpots().getValue().values()) {
                        if (cachedUsers.size() < 10) {
                            // don't put more than 10 entries in the list to prevent an 'intent overflow'
                            cachedUsers.add(spot.getUser());
                        }
                    }
                    intent.putExtra(GlobalVariables.INTENT_CACHED_USERS, cachedUsers);
                }
                */

                getActivity().startActivity(intent);
                getActivity().overridePendingTransition(R.anim.activity_slide_in_up, android.R.anim.fade_out);
            }

            @Override
            public void onClickSpotExplorer(View view, int actionCode) {

                /*
                User currentLoggedInUser = myUserViewModel.getCurrentUser().getValue();
                if (currentLoggedInUser == null) {
                    return;
                }

                ((ActivityMain) getActivity()).unbindForegroundServiceSpotListener();
                Intent intentSpotExplorer = new Intent(getActivity(), ActivitySpotExplorer.class);
                ArrayList<Spot> data = new ArrayList<>(spotViewModel.getFollowedSpots().getValue().values());
                intentSpotExplorer.putExtra(GlobalVariables.INTENT_SPOT, data);
                intentSpotExplorer.putExtra(GlobalVariables.INTENT_PROFILE_CURRENT_USER, currentLoggedInUser);
                startActivity(intentSpotExplorer);
                getActivity().overridePendingTransition(R.anim.spotexplorer_slide_in_left_to_right, android.R.anim.fade_out);
                 */

            }

        });

        /*
        TODO Erklären !!
         */
        myUserViewModel.getCurrentlyPlayingSpot().observe(this, currentSpot -> {

            if (currentSpot == null) {
                Log.d(getClass().getName(), "CurrentSpot is null");
                adapter.markSelectedSpotAsPlaying("");

                return;
            }

            if (mySpotsViewModel.getFollowedSpots().getValue() == null) {
                Log.d(getClass().getName(), "getFollowedSpots() is null");
                adapter.markSelectedSpotAsPlaying("");

                return;
            }

            Log.d(getClass().getName(), "CurrentSpot from userId "
                    + currentSpot.getId() + " and trackName "
                    + currentSpot.getPlayback().getTrackName());

            if (mySpotsViewModel.getFollowedSpots().getValue().containsKey(currentSpot.getId())) {
                // switch from loadingIcon to playIcon
                adapter.markSelectedSpotAsPlaying(currentSpot.getId());
            } else {
                adapter.markSelectedSpotAsPlaying("");
            }

            // Set Bottom Playing Indicator
            //Glide.with(getActivity()).load(currentSpot.getPlayback().getAlbumArt_300()).into(bottomPlayingIndicatorAlbumArt);


        });

        linearLayoutManager = new LinearLayoutManager(view.getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewMyPlaydFeed.setLayoutManager(linearLayoutManager);
        recyclerViewMyPlaydFeed.setAdapter(adapter);
        recyclerViewMyPlaydFeed.setNestedScrollingEnabled(false);

        // Performance Tweaks
        recyclerViewMyPlaydFeed.setItemViewCacheSize(20);
        recyclerViewMyPlaydFeed.setDrawingCacheEnabled(true);
        recyclerViewMyPlaydFeed.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        // Set up OverScrollDecoratorHelper
        OverScrollDecoratorHelper.setUpOverScroll(recyclerViewMyPlaydFeed, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);
        VerticalOverScrollBounceEffectDecorator decor = new VerticalOverScrollBounceEffectDecorator(new RecyclerViewOverScrollDecorAdapter(recyclerViewMyPlaydFeed));

        CircleProgressBar recyclerViewProgress = view.findViewById(R.id.myplayd_spots_circleprogressbar_recyclerProgressbar);
        recyclerViewProgress.setMax(100);
        final boolean[] finished = {false};

        decor.setOverScrollUpdateListener(new IOverScrollUpdateListener() {
            @Override
            public void onOverScrollUpdate(IOverScrollDecor decor, int state, float offset) {
                final View view = decor.getView();
                if (offset > 0) {
                    Log.d("_________", "___________Offset: " + offset);
                    int progress = (int) (offset * 100) / 150;
                    if (!finished[0]) {
                        recyclerViewProgress.setProgress(progress);
                    }
                    Log.d("_________", "___________Progress: " + progress);
                    if (progress >= 100) {
                        finished[0] = true;
                        recyclerViewProgress.setProgress(100);
                        /*
                        decor.detach();
                        new CountDownTimer(1500,1000) {

                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                VerticalOverScrollBounceEffectDecorator decor = new VerticalOverScrollBounceEffectDecorator(new RecyclerViewOverScrollDecorAdapter(recyclerViewMyPlaydFeed));
                                //OverScrollDecoratorHelper.setUpOverScroll(recyclerViewMyPlaydFeed, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);
                            }
                        }.start();
                        */

                    }
                    // 'view' is currently being over-scrolled from the top.
                } else if (offset < 0) {
                    // 'view' is currently being over-scrolled from the bottom.
                } else {
                    // No over-scroll is in-effect.
                    // This is synonymous with having (state == STATE_IDLE).
                }
            }
        });

    }


}
