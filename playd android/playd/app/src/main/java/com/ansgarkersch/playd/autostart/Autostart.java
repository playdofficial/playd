package com.ansgarkersch.playd.autostart;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

public class Autostart extends BroadcastReceiver
{
    public void onReceive(Context context, Intent i)
    {
        if (i.getAction() != null && i.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED)) {
            //Intent intent = new Intent(context, activity);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                //context.startForegroundService(intent);
            } else {
                //context.startService(intent);
            }
        }
    }
}