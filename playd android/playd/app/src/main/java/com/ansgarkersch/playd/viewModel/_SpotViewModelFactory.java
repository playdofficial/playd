package com.ansgarkersch.playd.viewModel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;

@Deprecated
public class _SpotViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private Context mContext;

    public _SpotViewModelFactory(Context context) {
        mContext = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new _SpotViewModel(mContext);
    }
}

