package com.ansgarkersch.playd._spotify.model;

import java.io.Serializable;

public class User implements Serializable {

    private String id;
    private String country;
    private String product;

    /**
     * Checks if user has premium access to spotify
     * @return true if the user is a spotify premium user
     */
    public boolean isPremium() {
        return product.equals("premium");
    }

    public User() { }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }
}
