package com.ansgarkersch.playd.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.transition.Slide;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd._spotify.SpotifyGlobalVariables;
import com.ansgarkersch.playd._spotify.model.Token;
import com.ansgarkersch.playd._spotify.repository.SpotifyRepository;
import com.ansgarkersch.playd.dialogfragments.DialogFragmentDecisionDialog;
import com.ansgarkersch.playd.fragments.FragmentCreateAccountFirst;
import com.ansgarkersch.playd.fragments.FragmentCreateAccountLast;
import com.ansgarkersch.playd.fragments.FragmentCreateAccountSecond;
import com.ansgarkersch.playd.fragments.FragmentCreateAccountThird;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.globals.UserRolesDialogFragment;
import com.ansgarkersch.playd.model.Gender;
import com.ansgarkersch.playd.model.Nationality;
import com.ansgarkersch.playd.model.RelationshipStatus;
import com.ansgarkersch.playd.model.User;
import com.ansgarkersch.playd.repository.AuthCallback;
import com.ansgarkersch.playd.repository.AuthRepository;
import com.ansgarkersch.playd.repository.FirebaseRepository;
import com.ansgarkersch.playd.repository.UserRepository;
import com.ansgarkersch.playd.repository.ValueCallback;
import com.ansgarkersch.playd.util.UtilityMethods;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;

import java.io.IOException;

public class ActivityCreateAccount extends AppCompatActivity {

    private AuthRepository authRepository;
    private FirebaseRepository firebaseRepository;
    private UserRepository userRepository;
    private SpotifyRepository spotifyRepository;

    private SharedPreferences sharedPreferences;
    private User userToBeCreated;
    private Bitmap userProfilePicToBeCreated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        sharedPreferences = this.getSharedPreferences(GlobalVariables.PACKAGE_NAME, Context.MODE_PRIVATE);

        authRepository = new AuthRepository();
        firebaseRepository = new FirebaseRepository();
        spotifyRepository = new SpotifyRepository(this);
        userRepository = new UserRepository();

        userToBeCreated = new User(firebaseRepository.getCurrentUser().getUid()); // TODO

        // setUser with first fragment
        changeFragment(new FragmentCreateAccountFirst());

    }

    private void changeFragment(android.support.v4.app.Fragment fragment) {
        fragment.setEnterTransition(new Slide(Gravity.END).setDuration(150));
        fragment.setExitTransition(new Slide(Gravity.START).setDuration(150));
        FragmentTransaction manager = getSupportFragmentManager().beginTransaction();
        manager.replace(R.id.activity_create_account_frame, fragment);
        manager.commit();

        ProgressBar progressBar = findViewById(R.id.activity_create_account_progressBar);
        if (fragment instanceof FragmentCreateAccountFirst) {
            progressBar.setProgress(25);
        } else if (fragment instanceof FragmentCreateAccountSecond) {
            progressBar.setProgress(50);
        } else if (fragment instanceof FragmentCreateAccountThird) {
            progressBar.setProgress(75);
        } else {
            progressBar.setProgress(100);
        }
    }

    public void setUserProfilePicToBeCreated(Uri uri) {

        try {
            userProfilePicToBeCreated = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showAbortDialog() {
        Bundle bundle = new Bundle();
        bundle.putString(GlobalVariables.ALERTBOX_USERROLE, UserRolesDialogFragment.DECISION_ABORT_CREATING_ACCOUNT.toString());
        DialogFragmentDecisionDialog alert = new DialogFragmentDecisionDialog();
        alert.setArguments(bundle);
        alert.show(getFragmentManager(),
                getResources().getString(R.string.abort));
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fragment_create_account_first_confirm:
                EditText editText_name = findViewById(R.id.fragment_create_account_first_name);
                EditText editText_birthdate_day = findViewById(R.id.fragment_create_account_first_birthdate_day);
                EditText editText_birthdate_year = findViewById(R.id.fragment_create_account_first_birthdate_year);
                Spinner relationshipStatus = findViewById(R.id.fragment_create_account_first_spinner_relationship);
                Spinner birthdayMonth = findViewById(R.id.fragment_create_account_first_birthdate_spinner_month);
                int relationshipSpinnerSelected = relationshipStatus.getSelectedItemPosition();
                int birthdaySpinnerSelected = birthdayMonth.getSelectedItemPosition() + 1; // month 0 does not exist

                String name = editText_name.getText().toString().trim();
                String birthdate = editText_birthdate_day.getText().toString().trim() + "/" +
                        String.valueOf(birthdaySpinnerSelected) + "/" +
                        editText_birthdate_year.getText().toString().trim();

                if (name.isEmpty()) {
                    editText_name.setError(getResources().getString(R.string.error_enterName));
                    return;
                }

                if (!UtilityMethods.isThisDateValid(birthdate)) {
                    editText_birthdate_day.setError(getResources().getString(R.string.error_enterValidBirthdate));
                    return;
                }

                if (!UtilityMethods.isThisAgeValid(birthdate)) {
                    editText_birthdate_year.setError(getResources().getString(R.string.error_enterValidAge));
                    return;
                }

                userToBeCreated.setName(name);
                userToBeCreated.setBirthday(birthdate);
                userToBeCreated.setRelationshipStatus(RelationshipStatus.getRelationshipStatusBySpinnerPosition(relationshipSpinnerSelected));

                changeFragment(new FragmentCreateAccountSecond());
                break;
            case R.id.fragment_create_account_first_abort:
                showAbortDialog();
                break;
            case R.id.fragment_create_account_second_confirm:
                changeFragment(new FragmentCreateAccountThird());
                break;
            case R.id.fragment_create_account_second_abort:
                changeFragment(new FragmentCreateAccountFirst());
                break;
            case R.id.fragment_create_account_third_abort:
                changeFragment(new FragmentCreateAccountSecond());
                break;
            case R.id.fragment_create_account_last_confirm:

                final LinearLayout content = findViewById(R.id.fragment_create_account_last_linearLayout);
                final LinearLayout progressBar = findViewById(R.id.fragment_create_account_last_progressBarLinearLayout);

                final EditText editText_email = findViewById(R.id.fragment_create_account_last_email);
                final EditText editText_password = findViewById(R.id.fragment_create_account_last_password);
                final String email = editText_email.getText().toString().trim();
                String password = editText_password.getText().toString().trim();

                if (email.isEmpty()) {
                    editText_email.setError(getResources().getString(R.string.error_enterEmail));
                    return;
                }
                if (password.isEmpty()) {
                    editText_password.setError(getResources().getString(R.string.error_enterPassword));
                    return;
                }
                if (!UtilityMethods.isPasswordSecureEnough(password)) {
                    editText_password.setError(getResources().getString(R.string.error_unsecurePassword));
                    return;
                }

                if (firebaseRepository.getCurrentUser() == null) {
                    Toast.makeText(ActivityCreateAccount.this,
                            getResources().getString(R.string.toast_unexpected_error_occured), Toast.LENGTH_LONG).show();
                    Log.e(GlobalVariables.EXCEPTIONTAG_FIREBASE_AUTH, "currentUser is null");
                    return;
                }

                // everything's fine yet
                content.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);

                authRepository.linkCredentials(firebaseRepository.getCurrentUser(), email, password, new AuthCallback() {
                    @Override
                    public void onCallback(int resultCode) {
                        if (resultCode == GlobalVariables.RESULT_OK) {

                            // TODO in GUI definieren
                            userToBeCreated.setGender(Gender.MALE);
                            userToBeCreated.setNationality(Nationality.DE);

                            userRepository.registerUser(userToBeCreated, userProfilePicToBeCreated, new ValueCallback() {
                                @Override
                                public void onSuccess(Object value) {
                                    firebaseRepository.loginWithPlayd(email, password, new AuthCallback() {
                                        @Override
                                        public void onCallback(int resultCode) {
                                            if (resultCode == GlobalVariables.RESULT_OK) {
                                                // create own Topic

                                                /*
                                                firebaseRepository.subscribeToTopic(userRepository.getItemByPosition().getUid(), new ValueCallback() {
                                                    @Override
                                                    public void onSuccess(Object value) {
                                                        // start Activity
                                                        Intent intentSpotExplorer = new Intent(ActivityCreateAccount.this, ActivityMain.class);
                                                        startActivity(intentSpotExplorer);
                                                        overridePendingTransition(R.anim.dialog_slide_up, android.R.anim.fade_out);
                                                    }

                                                    @Override
                                                    public void onFailure(Object value) {
                                                        Toast.makeText(getApplication(), R.string.toast_unexpected_error_occured, Toast.LENGTH_LONG).show();
                                                        changeFragment(new FragmentCreateAccountThird());
                                                    }
                                                });

                                                */

                                                // create followed-entry

                                                // create follower-entry
                                                finish();
                                            } else {
                                                Toast.makeText(getApplication(), R.string.toast_unexpected_error_occured, Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    });
                                }

                                @Override
                                public void onFailure(Object value) {
                                    progressBar.setVisibility(View.GONE);
                                    content.setVisibility(View.VISIBLE);
                                }
                            });

                        } else {

                            switch (resultCode) {
                                case GlobalVariables.RESULT_CREDENTIALS_FAILED_WEAK_PASSWORD:
                                    editText_password.setError(getResources().getString(R.string.error_unsecurePassword));
                                    editText_password.requestFocus();
                                    break;
                                case GlobalVariables.RESULT_CREDENTIALS_FAILED_USER_COLLISION:
                                    editText_email.setError(getResources().getString(R.string.error_userAlreadyExists));
                                    editText_email.requestFocus();
                                    break;
                                case GlobalVariables.RESULT_CREDENTIALS_FAILED_INVALID_CREDENTIALS:
                                    editText_email.setError(getResources().getString(R.string.error_enterValidEmail));
                                    editText_email.requestFocus();
                                    break;
                                default:
                                    Toast.makeText(ActivityCreateAccount.this,
                                            getResources().getString(R.string.toast_unexpected_error_occured), Toast.LENGTH_LONG).show();
                                    break;
                            }

                        }

                    }
                });

                break;
            case R.id.fragment_create_account_last_abort:
                changeFragment(new FragmentCreateAccountThird());
                break;
        }
    }

    /**
     * Spotify Methods
     */
    public void getSpotifyPermissionCode() {
        AuthenticationRequest authenticationRequest = spotifyRepository.getSpotifyAuthenticationRequest();
        AuthenticationClient.openLoginActivity(this, SpotifyGlobalVariables.AUTH_REQUEST_CODE, authenticationRequest);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        // TODO überprüfe ob user bei den scopes wirklich auf JA gefrückt hat (resultCode? error?)
        if (requestCode == SpotifyGlobalVariables.AUTH_REQUEST_CODE) {
            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, intent);
            spotifyRepository.getAllTokens(response.getCode(), new ValueCallback() {
                @Override
                public void onSuccess(Object value) {
                    Token token = (Token) value;
                    String accessToken = token.getAccess_token();
                    String refreshToken = token.getRefresh_token();
                    sharedPreferences.edit().putString(SpotifyGlobalVariables.REFRESH_TOKEN, refreshToken).apply();
                    changeFragment(new FragmentCreateAccountLast());
                }

                @Override
                public void onFailure(Object value) {
                    changeFragment(new FragmentCreateAccountSecond());
                    Toast.makeText(ActivityCreateAccount.this,
                            getResources().getString(R.string.toast_unexpected_error_occured), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        // Nothing happens
    }

    public void closeWindow() {
        //startActivity(new Intent(ActivityCreateAccount.this, _ActivityWelcomeScreen.class));
        finish();
    }


}