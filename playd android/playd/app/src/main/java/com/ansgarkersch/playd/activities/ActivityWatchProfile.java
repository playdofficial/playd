package com.ansgarkersch.playd.activities;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.fragments.FragmentWatchProfilePlaydlist;
import com.ansgarkersch.playd.fragments.FragmentWatchProfileSubscriptions;
import com.ansgarkersch.playd.fragments.FragmentWatchProfileUserCard;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.User;
import com.ansgarkersch.playd.recievers.NotificationReciever;
import com.ansgarkersch.playd.repository.UserRepository;
import com.ansgarkersch.playd.repository.ValueCallback;
import com.ansgarkersch.playd.util.OnSlidingTouchListener;
import com.ansgarkersch.playd.viewModel.WatchProfileViewModel;
import com.bumptech.glide.manager.SupportRequestManagerFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class ActivityWatchProfile extends AppCompatActivity implements java.util.Observer {

    // UI elements
    private FrameLayout frameLayoutFrame;
    //private ProgressBar progressBarFrame;

    // View Model
    private WatchProfileViewModel watchProfileViewModel;

    // Repositories
    private UserRepository userRepository;

    // Optionally Values
    private int distanceValue; // delivered in bundle if origin is the currentSpot-Timeline
    private TextView toolbarName;

    // Optionally Values in case the own user's profile should be shown
    private int ACTION_FLAG;
    private ArrayList<String> NEW_FOLLOWERS_SINCE_LAST_LOGIN;

    // Bundle
    private String currentOpenedUserId, currentOpenedUserName;
    private User currentLoggedInUser;

    // Recievers
    private NotificationReciever notificationReciever;

    @Override
    public void update(Observable observable, Object o) {
        Toast.makeText(this, "bububub", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watchprofile);

        // Init View Model
        watchProfileViewModel = ViewModelProviders.of(this).get(WatchProfileViewModel.class);

        // Repositories
        userRepository = new UserRepository();

        // UI elements
        frameLayoutFrame = findViewById(R.id.activity_watchprofile_frame);
        //progressBarFrame = findViewById(R.id.activity_watchprofile_frame_progressBar);

        // Check if there is a bundle delivered
        if (getIntent() != null) {
            currentOpenedUserId = getIntent().getStringExtra(GlobalVariables.INTENT_PROFILE_ID);
            currentOpenedUserName = getIntent().getStringExtra(GlobalVariables.INTENT_PROFILE_USERNAME);
            currentLoggedInUser = (User) getIntent().getSerializableExtra(GlobalVariables.INTENT_PROFILE_CURRENT_USER);
            watchProfileViewModel.setCurrentLoggedInUser(currentLoggedInUser);
            if (currentLoggedInUser.getId().equals(currentOpenedUserId)) {
                // The requested user is the current-logged-in user
                watchProfileViewModel.cacheUser(currentLoggedInUser);
            }

            // Cache the currentOpenedUser
            // Cache all the recieved (by intent) users
            //watchProfileViewModel.cacheMultipleUsers((ArrayList<User>) getIntent().getSerializableExtra(GlobalVariables.INTENT_CACHED_USERS));


            distanceValue = getIntent().getIntExtra(GlobalVariables.DB_DISTANCE, -1);

            // flag that indicates if the subscriptionFragment has to be shown immediately
            ACTION_FLAG = getIntent().getIntExtra(GlobalVariables.INTENT_ACTION_FLAG, GlobalVariables.INTENT_ACTION_FLAG_USERCARD);
            NEW_FOLLOWERS_SINCE_LAST_LOGIN = getIntent().getStringArrayListExtra(GlobalVariables.INTENT_NEW_FOLLOWERS_SINCE_LAST_LOGIN);

        } else {
            Toast.makeText(this, getResources().getString(R.string.toast_unexpected_error_occured), Toast.LENGTH_SHORT).show();
            finish();
        }

        // Recievers
        notificationReciever = new NotificationReciever();

        // Get CurrentLoggedIn user
        watchProfileViewModel.getCurrentLoggedInUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {

                FragmentManager fragmentManager = getSupportFragmentManager();
                Fragment fragmentUserCard = new FragmentWatchProfileUserCard();
                Bundle bundleUserCard = new Bundle();

                // Optionally, depends on the ACTION_FLAG
                Fragment fragmentSubscriptions = new FragmentWatchProfileSubscriptions();
                Bundle bundleSubscriptions = new Bundle();
                bundleSubscriptions.putString(GlobalVariables.CURRENT_OPENED_USER_ID, currentOpenedUserId);

                // If the activity is opened to show the own users followed/followers, this ACTION_FLAG can be used
                if (ACTION_FLAG == GlobalVariables.INTENT_ACTION_FLAG_FOLLOWED) {
                    // Set ACTION_FLAG
                    bundleUserCard.putInt(GlobalVariables.INTENT_ACTION_FLAG, GlobalVariables.INTENT_ACTION_FLAG_FOLLOWED);
                    bundleSubscriptions.putString(GlobalVariables.INTENT_SUBSCRIPTION_TYPE, GlobalVariables.SUBSCRIPTION_TYPE_FOLLOWED);
                } else if (ACTION_FLAG == GlobalVariables.INTENT_ACTION_FLAG_FOLLOWERS) {
                    // Set ACTION_FLAG
                    bundleUserCard.putInt(GlobalVariables.INTENT_ACTION_FLAG, GlobalVariables.INTENT_ACTION_FLAG_FOLLOWERS);
                    bundleSubscriptions.putString(GlobalVariables.INTENT_SUBSCRIPTION_TYPE, GlobalVariables.SUBSCRIPTION_TYPE_FOLLOWERS);
                    bundleSubscriptions.putStringArrayList(GlobalVariables.INTENT_NEW_FOLLOWERS_SINCE_LAST_LOGIN, NEW_FOLLOWERS_SINCE_LAST_LOGIN);
                }

                // send currentOpenedUserId
                bundleUserCard.putString(GlobalVariables.CURRENT_OPENED_USER_ID, currentOpenedUserId);
                if (distanceValue != -1) {
                    bundleUserCard.putInt(GlobalVariables.DB_DISTANCE, distanceValue);
                }
                // wait until this line to set the arguments to catch the optional case ACTION_FLAG
                fragmentUserCard.setArguments(bundleUserCard);

                initLayoutElements();

                // Hide Loading Progressbar and show FrameLayout
                //frameLayoutFrame.setVisibility(View.VISIBLE);
                //progressBarFrame.setVisibility(View.GONE);

                if (ACTION_FLAG == GlobalVariables.INTENT_ACTION_FLAG_USERCARD) {
                    // default
                    fragmentManager.beginTransaction()
                            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                            .replace(R.id.activity_watchprofile_frame, fragmentUserCard, currentOpenedUserName)
                            .commit();
                    setToolbarInformation(fragmentUserCard.getClass().getName(), fragmentUserCard);
                } else {
                    // Show FragmentSubscriptions
                    fragmentSubscriptions.setArguments(bundleSubscriptions);
                    fragmentManager.beginTransaction()
                            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                            .replace(R.id.activity_watchprofile_frame, fragmentSubscriptions, "")
                            .commit();
                    // Create & Hide FragmentUserCard to fetch all the neccesary followed or followers user data
                    fragmentManager.beginTransaction()
                            .disallowAddToBackStack()
                            .add(R.id.activity_watchprofile_frame, fragmentUserCard,
                                    "")
                            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                            .hide(fragmentUserCard)
                            .commit();
                    setToolbarInformation(fragmentSubscriptions.getClass().getName(), fragmentSubscriptions);
                }

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        notificationReciever.registerReciever(this);
        notificationReciever.addObserver(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        notificationReciever.unregisterReciever(this);
        notificationReciever.deleteObserver(this);
    }

    public void setToolbarInformation(String tag, Fragment fragment) {

        if (tag.equals(FragmentWatchProfileUserCard.class.getName())) { // User card
            toolbarName.setText(fragment.getTag());
            //toolbarProfileImage.setImageDrawable(null);
        } else if (tag.equals(FragmentWatchProfileSubscriptions.class.getName())) { // Subscriptions
            String subscriptionType = fragment.getArguments().getString(GlobalVariables.INTENT_SUBSCRIPTION_TYPE, "");

            String title;
            switch (subscriptionType) {
                case GlobalVariables.SUBSCRIPTION_TYPE_FOLLOWED:
                    title = getResources().getString(R.string.followed);
                    break;
                case GlobalVariables.SUBSCRIPTION_TYPE_FOLLOWERS:
                    title = getResources().getString(R.string.followers);
                    break;
                default:
                    title = "";
                    break;
            }
            toolbarName.setText(title);
            //Picasso.get().load(currentOpenedUser.getProfilePicThumbnailUrl()).into(toolbarProfileImage);
        } else { // Playdlist(s)
            /*
            user = (User) fragment.getArguments().getSerializable(GlobalVariables.INTENT_PROFILE);
            toolbarName.setText("Playdlist"); // TODO set title: "Playdlist"
            Picasso.get().load(user.getProfilePicThumbnailUrl()).into(toolbarProfileImage);
            */
        }

    }

    /*
    public void addUserIdToCurrentLoggedInUsersFollowedUIds(String userId) {
        HashMap<String, Boolean> tempMap =
                watchProfileViewModel.getCurrentLoggedInUser().getValue().getFollowedIds();
        tempMap.put(userId, true);
        watchProfileViewModel.getCurrentLoggedInUser().getValue().setFollowedIds(tempMap);
    }
    */

    public int getDistanceValue() {
        return distanceValue;
    }

    public void addFragmentOnTop(Fragment fragment, String username) {

        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .add(R.id.activity_watchprofile_frame, fragment, username)
                .show(fragment)
                .addToBackStack(fragment.getClass().getName())
                .commit();

        setToolbarInformation(fragment.getClass().getName(), fragment);

    }

    @SuppressLint("ClickableViewAccessibility")
    private void initLayoutElements() {

        toolbarName = findViewById(R.id.watchProfile_toolbarName);
        //toolbarProfileImage = findViewById(R.id.watchProfile_toolbarProfileImage);

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.watchProfile_toolbar);
        toolbar.setOnTouchListener(new OnSlidingTouchListener(this) {
            @Override
            public boolean onSlideDown() {
                closeWindow();
                return true;
            }
        });
        toolbarName = findViewById(R.id.watchProfile_toolbarName);
        //toolbarProfileImage = findViewById(R.id.watchProfile_toolbarProfileImage);
    }


    public void closeWindow() {
        finish();
        overridePendingTransition(android.R.anim.fade_in, R.anim.activity_slide_out_down);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        List<Fragment> fragmentBackstack = getSupportFragmentManager().getFragments();

        Fragment fragment = fragmentBackstack.get(fragmentBackstack.size() - 1);
        if (fragment instanceof SupportRequestManagerFragment
                && fragmentBackstack.size() >= 3) {
            fragment = fragmentBackstack.get(fragmentBackstack.size() - 3);
        }

        /*
        if (ACTION_FLAG != GlobalVariables.INTENT_ACTION_FLAG_USERCARD) {
            if (fragmentBackstack.size() == 2) {
                if (fragment.isHidden()) {
                    fragment = fragmentBackstack.get(0);
                }
            }
        }
        */

        if (fragmentBackstack.size() == 1) {
            overridePendingTransition(android.R.anim.fade_in, R.anim.activity_slide_out_down);
        } else {
            overridePendingTransition(R.anim.no_animation, R.anim.no_animation);
        }
        setToolbarInformation(fragment.getClass().getName(), fragment);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.watchProfile_backButton:
                onBackPressed();
                break;
        }
    }

    /*
    public void onClickWatchProfileOptions(View view) {
        switch (view.getId()) {
            case R.id.fragment_watchProfile_userCard_button_likeUnlike:
                break;
            case R.id.fragment_watchProfile_userCard_button_message:
                // todo
                break;
            case R.id.fragment_watchProfile_userCard_button_playdlist:
                addFragmentOnTop(new FragmentWatchProfilePlaydlist(), "");
                break;
            case R.id.fragment_watchProfile_userCard_button_more:
                // todo
                break;
        }
    }
    */

}
