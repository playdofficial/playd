package com.ansgarkersch.playd.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.activities.ActivityMain;
import com.ansgarkersch.playd.events.PlaybackStateChangedEvent;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.events.SubscriptionEvent;
import com.ansgarkersch.playd.model.UserOverview;
import com.ansgarkersch.playd.recievers.SpotifyActiveReciever;
import com.ansgarkersch.playd.repository.FirebaseRepository;
import com.ansgarkersch.playd.repository.UserRepository;
import com.ansgarkersch.playd.repository.ValueCallback;
import com.ansgarkersch.playd.util.UtilityMethods;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

public class ServiceFirebaseTopic extends FirebaseMessagingService {

    // Repositories
    private FirebaseRepository firebaseRepository;
    private UserRepository userRepository;

    // Notification
    private NotificationManager mNotificationManager;
    private final int NOTIFICATION_ID_PLAYBACK_STARTED = 1212;

    // SpotifyActiveBroadcastReciever
    private BroadcastReceiver broadcastReceiverSpotifyActive;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(getClass().getName(), getClass().getName() + " - onCreate() called");

        // Repositories
        firebaseRepository = new FirebaseRepository();
        userRepository = new UserRepository();

        // Notification
        mNotificationManager = (NotificationManager) getApplicationContext()
                .getSystemService(Context.NOTIFICATION_SERVICE);

        // Initialize and register SpotifyActiveBroadcastReciever
        broadcastReceiverSpotifyActive = new SpotifyActiveReciever();
        registerReceiver(broadcastReceiverSpotifyActive, new IntentFilter("com.spotify.music.active"));

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister SpotifyActiveBroadcastReciever
        if (broadcastReceiverSpotifyActive != null) {
            unregisterReceiver(broadcastReceiverSpotifyActive);
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(getClass().getName(), "onMessageRecieved() triggered: " + remoteMessage.toString());

        // TODO TEST
        notifyComponentsAboutPlaybackStateChanges(GlobalVariables.FCM_ACTIONCODE_PLAYBACK_TERMINATED, "wly1337");
        // TODO TEST


        if (firebaseRepository.getCurrentUser() == null) {
            return;
        }

        if (remoteMessage.getFrom() == null) {
            return;
        }

        String originUserId = remoteMessage.getFrom().substring(remoteMessage.getFrom().lastIndexOf("/") + 1);
        Log.d(getClass().getName(), "OriginUID: " + originUserId);

        if (firebaseRepository.getCurrentUser().getUid().equals(originUserId)) {
            // it's the user's own message
            Log.d(getClass().getName(), "Own Message!");
            return;
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {

            Map<String, String> data = remoteMessage.getData();
            String actionCode = data.get("actionCode");
            String category = data.get("category");

            if (category == null || actionCode == null) {
                return;
            }


            switch (category) {
                case GlobalVariables.FCM_CATEGORY_PLAYBACK:
                    String userId = data.get("userId");
                    Log.d(getClass().getName(), "Category: " + category + " | ActionCode: " + actionCode + " | userId: " + userId);

                    // TODO IN FUTURE:
                    // Check if the current logged-in user enabled live-notifications for this user
                    // f.ex. own ROOM Table with the live-notification-users

                    switch (actionCode) {
                        case GlobalVariables.FCM_ACTIONCODE_PLAYBACK_STARTED:
                            // 1) Detect if Notification with the channelId 'userId' exists
                            //    If yes: do nothing

                            //    If no: Build new Notification with ChannelId

                            // Fetch the userOverview first to get the username and image
                            userRepository.getUserOverViewById(userId, new ValueCallback() {
                                @Override
                                public void onSuccess(Object value) {
                                    UserOverview userOverview = (UserOverview) value;

                                    // Check if App is in background
                                    // If yes: Show an notification outside the app
                                    // If no: Show an in-app notification
                                    if (UtilityMethods
                                            .isPlaydInBackground(ServiceFirebaseTopic.this)) {
                                        // Show notification outside the app
                                        mNotificationManager.notify(
                                                userId,
                                                NOTIFICATION_ID_PLAYBACK_STARTED,
                                                buildPlaybackStartedNotification(userOverview));
                                    } else {
                                        // Show an in-app notification
                                        // TODO
                                    }

                                }

                                @Override
                                public void onFailure(Object value) {

                                }
                            });

                            // 2) Notify the app components, especially the main feed, about new started playback
                            notifyComponentsAboutPlaybackStateChanges(actionCode, userId);

                            break;
                        case GlobalVariables.FCM_ACTIONCODE_PLAYBACK_TERMINATED:
                            // 1) If a Notification with the channelId 'userId' exists, kill this
                            mNotificationManager.cancel(
                                    userId,
                                    NOTIFICATION_ID_PLAYBACK_STARTED);

                            // 2) Notify the app components, especially the main feed, about terminated playback
                            notifyComponentsAboutPlaybackStateChanges(actionCode, userId);

                            break;
                    }

                    break;
                case GlobalVariables.FCM_CATEGORY_USERRELATIONSHIP:

                    // Check if App is in background
                    // If yes: Just show an notification outside the app
                    // If no: notifyComponentsAboutSubscriptionEvent
                    if (UtilityMethods
                            .isPlaydInBackground(ServiceFirebaseTopic.this)) {
                        // Show an notification outside the app
                        // TODO
                    } else {
                        Gson gson = new Gson();
                        // TODO
                        notifyComponentsAboutSubscriptionEvent(gson.fromJson(gson.toJson(data), SubscriptionEvent.class));
                    }

                    break;
            }
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(getClass().getName(), "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

    }


    private void notifyComponentsAboutPlaybackStateChanges(String actionCode, String userId) {
        EventBus.getDefault().postSticky(new PlaybackStateChangedEvent(userId, actionCode));
        Log.d(getClass().getName(),
                "A PlaybackStateChangedEvent with the id "
                        + userId
                        + " and actionCode "
                        + actionCode
                        + " was broadcasted by Greenrobot");
    }

    private void notifyComponentsAboutSubscriptionEvent(SubscriptionEvent subscriptionEvent) {
        EventBus.getDefault().post(subscriptionEvent);
        Log.d(getClass().getName(),
                "A SubscriptionEvent with the actionCode "
                + subscriptionEvent.getActionCode()
                + " was broadcasted by Greenrobot. Active: "
                + subscriptionEvent.getActiveParticipant()
                + " | Passive: "
                + subscriptionEvent.getPassiveParticipant());
    }

    private void sendNotificationToAppComponents(com.ansgarkersch.playd.model.Notification notification) {
        EventBus.getDefault().post(notification);
        Log.d(getClass().getName(),
                "A Notification with the notificationType "
                        + notification.getNotificationType()
                        + " was broadcasted by Greenrobot. UserId: "
                        + notification.getUserId()
                        + " | Message: "
                        + notification.getMessage());
    }

    private NotificationCompat.Builder getSilentNotificationBuilder(String channelId) {
        // Build Notification
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext(), channelId);
        mBuilder.setOnlyAlertOnce(true);
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setSound(null);
        return mBuilder;
    }

    private Notification buildPlaybackStartedNotification(UserOverview user) {

        NotificationCompat.Builder mBuilder = getSilentNotificationBuilder("notify_1212");

        Intent ii = new Intent(getApplicationContext(), ActivityMain.class);
        // TODO put Extra Code
        // Ziel: Wenn man auf die Notification drückt, wird automatisch der Spot gestartet
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, ii, 0);

        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.drawable.ic_playd_actionbar);
        mBuilder.setContentTitle(user.getU() + " hat angefangen Musik zu hören. Höre jetzt mit!");

        mNotificationManager =
                (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        // Handle newer Android Versions
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("notify_1212",
                    getClass().getName(),
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setSound(null, null);
            mNotificationManager.createNotificationChannel(channel);
        }

        return mBuilder.build();
    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
        // TODO
    }


    @Override
    public void onMessageSent(String s) {
        super.onMessageSent(s);
    }

    @Override
    public void onSendError(String s, Exception e) {
        super.onSendError(s, e);
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.d(getClass().getName(), "New Registration Token recieved");
        // Update the realtimeDatabase entry
        if (firebaseRepository.getCurrentUser() != null) {
            firebaseRepository.updateRegistrationToken();
        }
    }

}
