package com.ansgarkersch.playd._spotify;

public class SpotifyGlobalVariables {

    public static final String CLIENT_ID = "6a1ef849d7514f2cbc6009be4c61159c";
    public static final String CLIENT_SECRET = "980bfd8339994d1f935b2f4c1ea659f7";
    public static final String GRANT_TYPE_REFRESH_TOKEN = "refresh_token";
    public static final String GRANT_TYPE_AUTH_CODE = "authorization_code";
    public static final String REDIRECT_URI = "com.ansgarkersch.playd://callback";
    public static final int AUTH_REQUEST_CODE = 1337;

    public static final String[] SCOPE = new String[]{
            "app-remote-control",
            "playlist-read-private",
            "playlist-modify-private",
            "playlist-modify-public",
            "user-read-private",
            "user-read-email",
            "streaming",
            "user-top-read",
            "user-modify-playback-state",
            "user-read-currently-playing",
            "user-read-playback-state"};

    // Shared Preferences
    public static final String REFRESH_TOKEN = "SPOTIFY_REFRESH_TOKEN";

    // Response Codes
    public static final int RESPONSE_CODE_OK = 200;

    // Playlist parameters
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String IS_PUBLIC = "public";


}
