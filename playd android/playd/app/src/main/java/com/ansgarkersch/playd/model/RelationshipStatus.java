package com.ansgarkersch.playd.model;

import android.content.Context;

import com.ansgarkersch.playd.R;

public enum RelationshipStatus {
    SINGLE, IN_A_RELATIONSHIP, NOT_YOUR_BUSINESS;

    public static RelationshipStatus getRelationshipStatusBySpinnerPosition(int position) {
        return RelationshipStatus.values()[position];
    }

    public static String getRelationshipStatusInLanguage(Context context, RelationshipStatus relationshipStatus) {
        switch (relationshipStatus) {
            case SINGLE:
                return context.getResources().getString(R.string.relationship_single);
            case IN_A_RELATIONSHIP:
                return context.getResources().getString(R.string.relationship_inARelationship);
            case NOT_YOUR_BUSINESS:
                return context.getResources().getString(R.string.notYourBusiness);
        }
        return "";
    }
}
