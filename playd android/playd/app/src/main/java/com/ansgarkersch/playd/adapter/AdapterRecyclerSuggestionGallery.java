package com.ansgarkersch.playd.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.User;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterRecyclerSuggestionGallery extends RecyclerView.Adapter<AdapterRecyclerSuggestionGallery.ViewHolder> {

    private List<User> userData;
    private LayoutInflater mInflater;
    private AdapterRecyclerSuggestionGallery.ItemClickListener mClickListener;
    private Context context;

    // data is passed into the constructor
    public AdapterRecyclerSuggestionGallery(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        userData = new ArrayList<>();
    }

    public void setUserData(List<User> userData) {
        this.userData = userData;
        notifyDataSetChanged();
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public AdapterRecyclerSuggestionGallery.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row_subscription_gallery, parent, false);
        return new AdapterRecyclerSuggestionGallery.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(@NonNull AdapterRecyclerSuggestionGallery.ViewHolder holder, int position) {
        User currentUser = userData.get(position);

        if (currentUser == null) {
            return;
        }

        // Profile Image
        Glide.with(context).load(
                GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE.equals(currentUser.getProfilePicThumbnailUrl())
                        ?
                        R.mipmap.no_profile_image
                        :
                        currentUser.getProfilePicThumbnailUrl()).into(holder.profilePic);

        // Name
        holder.name.setText(userData.get(position).getUsername());

        // Handle onClick
        holder.content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onItemClick(view, currentUser);
                }
            }
        });

        //holder.name.setTextColor(context.getResources().getColor(R.color.colorPrimaryNew));

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return userData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CircleImageView profilePic;
        TextView name;
        LinearLayout content;

        ViewHolder(View itemView) {
            super(itemView);
            profilePic = itemView.findViewById(R.id.row_subscriptionGallery_image);
            name = itemView.findViewById(R.id.row_subscriptionGallery_name);
            content = itemView.findViewById(R.id.row_subscriptionGallery_content);
            name.setSelected(true);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            //if (mClickListener != null) mClickListener.onClickPlayback(view, getAdapterPosition());
        }
    }

    // allows clicks events to be caught
    public void setClickListener(AdapterRecyclerSuggestionGallery.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, User user);
    }

}
