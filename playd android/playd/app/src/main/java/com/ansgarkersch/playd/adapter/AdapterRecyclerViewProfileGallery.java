package com.ansgarkersch.playd.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ansgarkersch.playd.R;
import com.bumptech.glide.Glide;
import java.util.List;

public class AdapterRecyclerViewProfileGallery extends RecyclerView.Adapter<AdapterRecyclerViewProfileGallery.ViewHolder> {

    private List<String> profileImages;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private View view;

    // data is passed into the constructor
    public AdapterRecyclerViewProfileGallery(Context context, List<String> profileImages) {
        this.mInflater = LayoutInflater.from(context);
        this.profileImages = profileImages;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = mInflater.inflate(R.layout.row_watchprofile_imagegallery, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Glide.with(view.getContext()).load(profileImages.get(position)).into(holder.squareImageView);
        //Picasso.get().load(profileImages.get(position)).into(holder.squareImageView);
        if (position == getItemCount() - 1) {
            //holder.divider.setVisibility(View.GONE);
        } else {
            //holder.divider.setVisibility(View.VISIBLE);
        }
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return profileImages.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView squareImageView;
        //View divider;

        ViewHolder(View itemView) {
            super(itemView);
            squareImageView = itemView.findViewById(R.id.watchProfile_imageGallery_image);
            //divider = itemView.findViewById(R.id.watchProfile_imageGallery_divider);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

}
