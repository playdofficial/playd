package com.ansgarkersch.playd.globals;

public enum UserRolesDialogFragment {
    DECISION_UNFOLLOW_SPOT,
    DECISION_ABORT_CREATING_ACCOUNT,
    INFO_RECONNECTING_TO_SPOTIFY;
    // open spotexplorer while listening to music
}
