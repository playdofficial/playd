package com.ansgarkersch.playd.fragments;

public interface FragmentSearchCallback {
    void onTextChanged(String s);
    void onOpenSearchView();
    void onCloseSearchView();
}
