package com.ansgarkersch.playd._spotify.repository;

import android.content.Context;
import android.media.AudioManager;
import android.os.CountDownTimer;

import com.ansgarkersch.playd._spotify.util.SpotifyUtilityMethods;
import com.ansgarkersch.playd.repository.ValueCallback;
import com.spotify.android.appremote.api.SpotifyAppRemote;
import com.spotify.protocol.client.CallResult;
import com.spotify.protocol.client.ErrorCallback;
import com.spotify.protocol.types.Empty;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PlayTrackRepository {

    private SpotifyAppRemote mSpotifyAppRemote;
    private AudioManager mAudioManager;
    private int volume;
    private ExecutorService executorServiceFirst = Executors.newSingleThreadExecutor();

    public PlayTrackRepository(Context context, SpotifyAppRemote mSpotifyAppRemote) {
        this.mSpotifyAppRemote = mSpotifyAppRemote;
        mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        setCurrentVolume();
    }

    public void playTrackAtPosition(final String trackId, final long position, final ValueCallback callback) {
        String playbackId = SpotifyUtilityMethods.completeTrackId(trackId);
        executorServiceFirst.submit(new Thread ( new Runnable() {
            @Override
            public void run() {
                if (position == 0L) {
                    mSpotifyAppRemote.getPlayerApi().play(playbackId).setResultCallback(new CallResult.ResultCallback<Empty>() {
                        @Override
                        public void onResult(Empty empty) {
                            callback.onSuccess("");
                        }
                    }).setErrorCallback(new ErrorCallback() {
                        @Override
                        public void onError(Throwable throwable) {
                            callback.onFailure(throwable.getMessage());
                        }
                    });
                } else {
                    disableSound();
                    mSpotifyAppRemote.getPlayerApi().play(playbackId).setResultCallback(new CallResult.ResultCallback<Empty>() {
                        @Override
                        public void onResult(Empty empty) {
                            mSpotifyAppRemote.getPlayerApi().seekTo(position).setResultCallback(new CallResult.ResultCallback<Empty>() {
                                @Override
                                public void onResult(Empty empty) {
                                    new CountDownTimer(333, 1000) {
                                        public void onTick(long millisUntilFinished) { }
                                        public void onFinish() {
                                            enableSound(); // playback has started
                                            callback.onSuccess("");
                                        }
                                    }.start();
                                }
                            });
                        }
                    }).setErrorCallback(new ErrorCallback() {
                        @Override
                        public void onError(Throwable throwable) {
                            enableSound(); // playback has started
                            callback.onFailure(throwable.getMessage());
                        }
                    });
                }
            }
        }));
    }

    public void pausePlayback(final ValueCallback callback) {
        mSpotifyAppRemote.getPlayerApi().pause().setResultCallback(new CallResult.ResultCallback<Empty>() {
            @Override
            public void onResult(Empty empty) {
                callback.onSuccess("");
            }
        }).setErrorCallback(new ErrorCallback() {
            @Override
            public void onError(Throwable throwable) {
                callback.onFailure(throwable.getMessage());
            }
        });
    }



    public void setCurrentVolume() {
        if (mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC) != 0) {
            volume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        }
    }

    private void disableSound() {
        mAudioManager.setStreamVolume(
                AudioManager.STREAM_MUSIC,
                0, // Index
                0 // Flags
        );

    }

    private void enableSound() {
        mAudioManager.setStreamVolume(
                AudioManager.STREAM_MUSIC,
                volume, // Index
                0 // Flags
        );
    }


}
