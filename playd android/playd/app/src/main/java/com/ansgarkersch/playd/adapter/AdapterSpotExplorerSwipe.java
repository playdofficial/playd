package com.ansgarkersch.playd.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.activities.ActivitySpotExplorer;
import com.ansgarkersch.playd.activities.ActivityWatchProfile;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.Advertisement;
import com.ansgarkersch.playd.model.IScrollable;
import com.ansgarkersch.playd.model.Playback;
import com.ansgarkersch.playd.model.Spot;
import com.ansgarkersch.playd.model.User;
import com.ansgarkersch.playd.model.UserOverview;
import com.ansgarkersch.playd.services.ForegroundServiceSpotListener;
import com.ansgarkersch.playd.util.BlurBuilder;
import com.ansgarkersch.playd.util.UtilityMethods;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.lorentzos.flingswipe.SwipeFlingAdapterView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterSpotExplorerSwipe extends BaseAdapter {

    private CoordinatorLayout coordinatorLayout;
    private List<IScrollable> spotData;
    private Context context;

    private User currentLoggedInUser;

    private boolean isPlaying = false;
    private boolean liked = false;

    public AdapterSpotExplorerSwipe(Context context,
                                    CoordinatorLayout coordinatorLayout,
                                    User currentLoggedInUser,
                                    List<IScrollable> playbackList) {
        this.spotData = playbackList;
        this.context = context;
        this.coordinatorLayout = coordinatorLayout;
        this.currentLoggedInUser =currentLoggedInUser;
    }

    public void addToPlaylist(List<IScrollable> data) {
        spotData.addAll(data);
        notifyDataSetChanged();
    }

    /*
    public void notifyPlayback(boolean isPlaying) {
        this.isPlaying = isPlaying;
        notifyDataSetChanged();
    }
    */

    private void play(Spot spot) {
        Intent serviceIntent = new Intent(context, ForegroundServiceSpotListener.class);
        serviceIntent.addCategory(GlobalVariables.SPOTIFY_PLAY);
        serviceIntent.putExtra(GlobalVariables.INTENT_SPOT, spot);
        context.startService(serviceIntent);
        Log.d(getClass().getName(), "Play() was triggered. Song: " + spot.getPlayback().getTrackName());
    }

    public void like() {
        if (spotData.get(0) instanceof Spot) {

            liked = true;
            notifyDataSetChanged();

            Snackbar snackbarLike = Snackbar
                    .make(coordinatorLayout, context.getResources().getString(R.string.songAddedToPlaydlist), Snackbar.LENGTH_LONG)
                    .setAction(context.getResources().getString(R.string.undo), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Snackbar snackbarDismiss = Snackbar.make(coordinatorLayout, context.getResources().getString(R.string.songRemovedFromPlaydlist), Snackbar.LENGTH_SHORT);
                            snackbarDismiss.show();
                        }
                    });
            snackbarLike.show();
            snackbarLike.addCallback(new Snackbar.Callback() {
                @Override
                public void onDismissed(Snackbar snackbar, int event) {
                    Playback likedPlayback = ((Spot) spotData.get(0)).getPlayback();
                /*
                TODO push the required information to the user's playdlist (maybe observer/eventBus?)
                 */
                }

                @Override
                public void onShown(Snackbar snackbar) {
                }
            });
        }
    }

    public void next() {
        try {
            if (spotData.size() > 1) {
                spotData.remove(0);
            }
        } catch (Exception ex) {
            Log.e(getClass().getName(), ex.getMessage(), ex.getCause());
        } finally {
            notifyDataSetChanged();
        }
    }

    @Override
    public int getCount() {
        return spotData.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        final ViewHolder viewHolder;
        if (rowView == null) {

            LayoutInflater inflater = LayoutInflater.from(context);
            rowView = inflater.inflate(R.layout.row_spotexplorer_card, parent, false);
            rowView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.spotexplorer_slide_in_right_to_left));
            // configure view holder
            viewHolder = new ViewHolder();

            viewHolder.cardImage = rowView.findViewById(R.id.row_spotexplorer_albumart);
            viewHolder.background = ((ActivitySpotExplorer) context).findViewById(R.id.activtiy_spotexplorer_swipeView);

            // User-related data in activitySpotExplorer
            viewHolder.frameProfilePic = ((ActivitySpotExplorer) context).findViewById(R.id.activity_spotexplorer_frameLayoutButton_showProfile);
            viewHolder.profilePic = ((ActivitySpotExplorer) context).findViewById(R.id.activity_spotexplorer_profilePic);
            viewHolder.username = ((ActivitySpotExplorer) context).findViewById(R.id.activity_spotexplorer_username);
            viewHolder.status = ((ActivitySpotExplorer) context).findViewById(R.id.activity_spotexplorer_status);
            viewHolder.distance = ((ActivitySpotExplorer) context).findViewById(R.id.activity_spotexplorer_distance);

            // Bottom View CurrentSpotter
            viewHolder.textCurrentSpotter = ((ActivitySpotExplorer) context).findViewById(R.id.activtiy_spotexplorer_text_currentSpotter);
            viewHolder.itemCurrentSpotter = ((ActivitySpotExplorer) context).findViewById(R.id.activtiy_spotexplorer_image_itemCurrentSpotter);

            // Resume Indicator
            //viewHolder.resumeIndicator = rowView.findViewById(R.id.row_spotexplorer_image_resumeIndicator);

            rowView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (spotData.get(position) instanceof Spot) {
            Spot currentSpot = (Spot) spotData.get(position);
            UserOverview currentUser = currentSpot.getUser();
            Playback currentPlayback = currentSpot.getPlayback();

            if (!liked) {
                play(currentSpot);
            }
            liked = false;

            Glide.with(context).load(currentPlayback.getAlbumArt_640()).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    Bitmap bitmap = ((BitmapDrawable)resource).getBitmap();
                    viewHolder.cardImage.setImageBitmap(bitmap);
                    viewHolder.background.setBackground(new BitmapDrawable(context.getResources(), BlurBuilder.blur(context, bitmap)));

                    // Set bottom Text color depending on the album art's brightness
                    if (UtilityMethods.isBitmapWhite(bitmap)) {
                        // Color the items black
                        viewHolder.textCurrentSpotter.setTextColor(context.getResources().getColor(android.R.color.black));
                        viewHolder.itemCurrentSpotter.setColorFilter(ContextCompat.getColor(context, android.R.color.black),
                                android.graphics.PorterDuff.Mode.MULTIPLY);
                    }
                    return true;
                }
            }).preload();


            Glide.with(context).load(
                    GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE.equals(currentUser.getProfilePicUrl())
                            ?
                            R.mipmap.no_profile_image
                            :
                            currentUser.getProfilePicUrl()).into(viewHolder.profilePic);

            /*
            Click Listener
             */
            viewHolder.frameProfilePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openAccountWindow(currentUser);
                }
            });



            /*
            // Resume Indicator Visibility
            if (isPlaying) {
                viewHolder.resumeIndicator.setVisibility(View.GONE);
            } else {
                viewHolder.resumeIndicator.setVisibility(View.VISIBLE);
            }

            // Resume Indicator Click Listener
            viewHolder.resumeIndicator.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    play(currentSpot);
                }
            });
            */

            /*
            // User Stuff
            viewHolder.profilePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openAccountWindow(currentUser);
                }
            });

            viewHolder.profileHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openAccountWindow(currentUser);
                }
            });
            */

            viewHolder.username.setText(currentUser.getU());
            viewHolder.status.setText(currentPlayback.getSpotName());
            //viewHolder.distance.setText(currentPlayback.getDistance());

        } else if (spotData.get(position) instanceof Advertisement) {
            Advertisement advertisement = (Advertisement) spotData.get(position);
            // do sth with advertisement
        }


        return rowView;
    }

    private void openAccountWindow(UserOverview user) {
        Intent intent = new Intent(context, ActivityWatchProfile.class);
        intent.putExtra(GlobalVariables.INTENT_PROFILE_ID, user.getId());
        intent.putExtra(GlobalVariables.INTENT_PROFILE_USERNAME, user.getU());
        intent.putExtra(GlobalVariables.INTENT_PROFILE_CURRENT_USER, currentLoggedInUser);
        context.startActivity(intent);
        ((ActivitySpotExplorer)context).overridePendingTransition(R.anim.activity_slide_in_up, android.R.anim.fade_out);
    }

    public class ViewHolder {
        FrameLayout frameProfilePic;
        ImageView cardImage;
        SwipeFlingAdapterView background;
        CircleImageView profilePic;
        TextView username, status, distance;
        LinearLayout profileHeader;
        //ImageButton resumeIndicator;

        // Bottom View: Current Spotter
        TextView textCurrentSpotter;
        ImageView itemCurrentSpotter;

    }

}