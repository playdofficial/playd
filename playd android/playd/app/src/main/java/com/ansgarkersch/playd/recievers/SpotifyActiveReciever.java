package com.ansgarkersch.playd.recievers;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.activities.ActivityMain;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.services.ForegroundServiceSpotListener;
import com.ansgarkersch.playd.util.UtilityMethods;

public class SpotifyActiveReciever extends BroadcastReceiver {

    private long WAITING_TIME_FOR_THE_NEXT_NOTIFICATION = 86400000; // 24 hours in milliseconds
    private String TAG = getClass().getName();

    @Override
    public void onReceive(Context context, Intent intent) {

        // If ForegroundServiceSpotListener is not running, return
        if (UtilityMethods.isMyServiceRunning(context, ForegroundServiceSpotListener.class)) {
            return;
        }

        // Before doing anything. Check if it's time for a new notification
        // The user should be notified maximum every 24 hours
        // Check the specific sharedPreferences entry for the last notification time
        SharedPreferences prefs = context.getSharedPreferences(
                "com.ansgarkersch.playd", Context.MODE_PRIVATE);

        long lastNotificationTimestamp = prefs.getLong(GlobalVariables.LAST_SPOTIFY_ACTIVE_NOTIFICATION_TIMESTAMP, 0);
        Log.d(TAG, "onReceive: lastNotificationTimestamp: " + lastNotificationTimestamp);
        if (lastNotificationTimestamp != 0) {
            if ((UtilityMethods.getCurrentTime() - lastNotificationTimestamp) <= WAITING_TIME_FOR_THE_NEXT_NOTIFICATION) {
                Log.d(TAG, "onReceive: differenceTimestamp: " + (UtilityMethods.getCurrentTime() - lastNotificationTimestamp));
                return;
            }
        }
        Log.d(TAG, "onReceive: It's time for a new notification! differenceTimestamp: " + (UtilityMethods.getCurrentTime() - lastNotificationTimestamp));
        // It's time for a new notification!

        // Initialize NotificationManager
        NotificationManager mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        // Build Notification
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context, "notify_8888");
        mBuilder.setOnlyAlertOnce(true);
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setSound(null);

        // Optional
        Intent ii = new Intent(context, ActivityMain.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, ii, 0);
        mBuilder.setContentIntent(pendingIntent);

        mBuilder.setSmallIcon(R.drawable.ic_playd_actionbar);
        mBuilder.setContentTitle("Guten Musikgeschmack hast du - Zeig es deinen Fans auf playd!");

        // Handle newer Android Versions
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("notify_8888",
                    getClass().getName(),
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setSound(null, null);
            mNotificationManager.createNotificationChannel(channel);
        }

        prefs.edit().putLong(GlobalVariables.LAST_SPOTIFY_ACTIVE_NOTIFICATION_TIMESTAMP, UtilityMethods.getCurrentTime()).apply();

        mNotificationManager.notify(8888, mBuilder.build());

    }

}
