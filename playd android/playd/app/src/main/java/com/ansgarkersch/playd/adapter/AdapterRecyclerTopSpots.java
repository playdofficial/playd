package com.ansgarkersch.playd.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.Spot;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class AdapterRecyclerTopSpots extends RecyclerView.Adapter<AdapterRecyclerTopSpots.ViewHolder>  {

    private LayoutInflater mInflater;
    private Context context;
    private AdapterRecyclerTopSpots.ItemClickListener mClickListener;
    private List<Spot> topSpots = new ArrayList<>();

    // data is passed into the constructor
    public AdapterRecyclerTopSpots(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void setTopSpots(List<Spot> data) {
        topSpots = data;
        notifyDataSetChanged();
    }

    // Inflates the row layout from xml when needed
    @NonNull
    @Override
    public AdapterRecyclerTopSpots.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row_topspots, parent, false);
        return new AdapterRecyclerTopSpots.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterRecyclerTopSpots.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        Spot currentSpot = topSpots.get(position);

        Glide.with(context).load(
                GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE.equals(currentSpot.getUser().getProfilePicUrl())
                        ?
                        R.mipmap.no_profile_image
                        :
                        currentSpot.getUser().getProfilePicUrl()).into(holder.profilePicBackground);

        holder.spotName.setText(currentSpot.getPlayback().getSpotName());
        holder.spotterCountWithText.setText("15 hören zu"); // TODO
        holder.username.setText(currentSpot.getUser().getU());

    }

    @Override
    public int getItemCount() {
        return topSpots.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView profilePicBackground;
        TextView spotName, spotterCountWithText, username;

        ViewHolder(View itemView) {
            super(itemView);
            profilePicBackground = itemView.findViewById(R.id.row_topspots_imageview_profilePicBackground);
            spotName = itemView.findViewById(R.id.row_topspots_textview_spotName);
            spotterCountWithText = itemView.findViewById(R.id.row_topspots_textview_spotterCountWithText);
            username = itemView.findViewById(R.id.row_topspots_textview_username);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            //if (mClickListener != null) mClickListener.onClickPlayback(view, getAdapterPosition());
        }
    }

    // allows clicks events to be caught
    public void setClickListener(AdapterRecyclerTopSpots.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, Spot spot);
    }
}
