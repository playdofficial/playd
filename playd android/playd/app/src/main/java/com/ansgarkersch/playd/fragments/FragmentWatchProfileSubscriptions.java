package com.ansgarkersch.playd.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.activities.ActivityWatchProfile;
import com.ansgarkersch.playd.adapter.AdapterSubscriptions;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.IScrollable;
import com.ansgarkersch.playd.model.User;
import com.ansgarkersch.playd.model.UserOverview;
import com.ansgarkersch.playd.repository.SubscriptionRepository;
import com.ansgarkersch.playd.repository.UserRepository;
import com.ansgarkersch.playd.repository.ValueCallback;
import com.ansgarkersch.playd.util.UtilityMethods;
import com.ansgarkersch.playd.viewModel.WatchProfileViewModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class FragmentWatchProfileSubscriptions extends Fragment {

    // View Models
    private WatchProfileViewModel watchProfileViewModel;

    // Repositories
    private SubscriptionRepository subscriptionRepository;

    // UI elements
    private View view;
    private AdapterSubscriptions adapter;
    private RecyclerView subscriptionsRecycler;
    private LinearLayout subscriptionsRecyclerProgress;

    // Subscriptions Type
    private String subscriptionType;

    // Delivered Bundle
    private String currentOpenedUserId;

    // Optionally Values
    private ArrayList<String> newFollowers;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_watchprofile_subscriptions,
                container, false);

        // Repositories
        subscriptionRepository = new SubscriptionRepository(getActivity());

        // init ViewModel
        watchProfileViewModel = ViewModelProviders.of(getActivity()).get(WatchProfileViewModel.class);

        //currentLoggedInUsersFollowedUIds = ((ActivityWatchProfile)getActivity()).getCurrentLoggedInUsersFollowedUIds();
        //currentLoggedInUser = ((ActivityWatchProfile)getActivity()).getCurrentLoggedInUser();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            //currentWatchedUser = (User) bundle.getSerializable(GlobalVariables.INTENT_PROFILE);
            //currentUsersSubscriptionUIds = (List<String>) bundle.getSerializable(GlobalVariables.INTENT_SUBSCRIPTION_UIDS);
            subscriptionType = bundle.getString(GlobalVariables.INTENT_SUBSCRIPTION_TYPE);
            currentOpenedUserId = bundle.getString(GlobalVariables.CURRENT_OPENED_USER_ID);
            newFollowers = bundle.getStringArrayList(GlobalVariables.INTENT_NEW_FOLLOWERS_SINCE_LAST_LOGIN);
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // UI elements
        subscriptionsRecycler = view.findViewById(R.id.fragment_watchProfile_subscriptions_recycler);
        subscriptionsRecyclerProgress = view.findViewById(R.id.fragment_watchProfile_subscriptions_linearLayout_progress);

        initViewModels();
    }


    private void fetchAllUsers(List<String> userIds, final ValueCallback callback) {
        if (getActivity() == null) {
            return;
        }
        UserRepository userRepository = new UserRepository();
        List<UserOverview> promises = Collections.synchronizedList(new ArrayList<>());
        for (String id : userIds) {
            userRepository.getUserOverViewById(id, new ValueCallback() {
                @Override
                public void onSuccess(Object value) {
                    UserOverview user = (UserOverview) value;
                    promises.add(user);
                    if (user == null) {
                        Log.d("________", "_______: " + id);
                    }
                    if (!GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE.equals(user.getProfilePicUrl())) {
                        Glide.with(getActivity()).load(user.getProfilePicUrl()).preload();
                    }
                    callback.onSuccess(promises);
                }

                @Override
                public void onFailure(Object value) {

                }
            });
        }

    }

    private void initViewModels() {
        if (getActivity() == null) {
            return;
        }
        // wait for currentLoggedInUser getting ready
        watchProfileViewModel.getCurrentLoggedInUser().observe(getActivity(), new Observer<User>() {
            @Override
            public void onChanged(@Nullable User currentLoggedInUser) {
                if (currentLoggedInUser == null) {
                    return;
                }
                List<UserOverview> subscriptionList = new ArrayList<>();
                User currentOpenedUser = watchProfileViewModel.getCachedUserById(currentOpenedUserId);

                final List<String> userIds = new ArrayList<>();
                switch (subscriptionType) {
                    case GlobalVariables.SUBSCRIPTION_TYPE_FOLLOWED:

                        List<Map.Entry<String, Long>> followedList = new ArrayList<>(currentOpenedUser.getFollowedIds().entrySet());
                        Collections.sort(followedList, new Comparator<Map.Entry<String, Long>>() {
                            @Override
                            public int compare(Map.Entry<String, Long> t1, Map.Entry<String, Long> t2) {
                                if (t1.getValue() > t2.getValue()) {
                                    return -1;
                                } else if (t2.getValue() > t1.getValue()) {
                                    return 1;
                                } else {
                                    return 0;
                                }
                            }
                        });

                        for (Map.Entry<String, Long> entry : followedList) {
                            userIds.add(entry.getKey());
                        }

                        if (userIds.indexOf(currentLoggedInUser.getId()) > -1) {
                            userIds.remove(currentLoggedInUser.getId());
                            subscriptionList.add(0, currentLoggedInUser.getOverview());
                        }

                        /*
                        Check if a user is already fetched
                         */
                        Iterator<String> followedIterator = userIds.iterator();
                        while (followedIterator.hasNext()) {
                            String userId = followedIterator.next();
                            if (watchProfileViewModel.isUserAlreadyCached(userId)) {
                                subscriptionList.add(watchProfileViewModel.getCachedUserById(userId).getOverview());
                                followedIterator.remove();
                            }
                        }

                        /*
                        Show Recycler if all the users have been fetched already
                         */
                        if (userIds.size() == 0) {
                            initRecyclerView();
                            adapter.setUserData(subscriptionList, GlobalVariables.SUBSCRIPTION_TYPE_FOLLOWED);
                            subscriptionsRecyclerProgress.setVisibility(View.GONE);
                            subscriptionsRecycler.setVisibility(View.VISIBLE);
                            return;
                        }

                        /*
                        Fetch all not-cached users
                         */
                        fetchAllUsers(userIds, new ValueCallback() {
                            @Override
                            public void onSuccess(Object value) {
                                List<UserOverview> userList = (List<UserOverview>) value;
                                if (userList.size() >= userIds.size()) {
                                    initRecyclerView();
                                    subscriptionList.addAll(userList);
                                    adapter.setUserData(subscriptionList, GlobalVariables.SUBSCRIPTION_TYPE_FOLLOWED);
                                    subscriptionsRecyclerProgress.setVisibility(View.GONE);
                                    subscriptionsRecycler.setVisibility(View.VISIBLE);
                                }
                            }

                            @Override
                            public void onFailure(Object value) {

                            }
                        });

                        break;
                    case GlobalVariables.SUBSCRIPTION_TYPE_FOLLOWERS:

                        List<Map.Entry<String, Long>> followersList = new ArrayList<>(currentOpenedUser.getFollowersIds().entrySet());
                        Collections.sort(followersList, new Comparator<Map.Entry<String, Long>>() {
                            @Override
                            public int compare(Map.Entry<String, Long> t1, Map.Entry<String, Long> t2) {
                                if (t1.getValue() > t2.getValue()) {
                                    return -1;
                                } else if (t2.getValue() > t1.getValue()) {
                                    return 1;
                                } else {
                                    return 0;
                                }
                            }
                        });

                        for (Map.Entry<String, Long> entry : followersList) {
                            userIds.add(entry.getKey());
                        }

                        if (userIds.indexOf(currentLoggedInUser.getId()) > -1) {
                            userIds.remove(currentLoggedInUser.getId());
                            subscriptionList.add(0, currentLoggedInUser.getOverview());
                        }

                        /*
                        Check if a user is already fetched
                         */
                        Iterator<String> followersIterator = userIds.iterator();
                        while (followersIterator.hasNext()) {
                            String userId = followersIterator.next();
                            if (watchProfileViewModel.isUserAlreadyCached(userId)) {
                                subscriptionList.add(watchProfileViewModel.getCachedUserById(userId).getOverview());
                                followersIterator.remove();
                            }
                        }

                        /*
                        Show Recycler if all the users have been fetched already
                         */
                        if (userIds.size() == 0) {
                            initRecyclerView();
                            adapter.setUserData(subscriptionList, GlobalVariables.SUBSCRIPTION_TYPE_FOLLOWERS);
                            subscriptionsRecyclerProgress.setVisibility(View.GONE);
                            subscriptionsRecycler.setVisibility(View.VISIBLE);
                            return;
                        }
                        /*
                        Fetch all not-cached users
                         */
                        fetchAllUsers(userIds, new ValueCallback() {
                            @Override
                            public void onSuccess(Object value) {
                                List<UserOverview> userList = (List<UserOverview>) value;
                                if (userList.size() >= userIds.size()) {
                                    initRecyclerView();
                                    subscriptionList.addAll(userList);
                                    if (currentOpenedUser.getFollowersIds().containsKey(currentLoggedInUser.getId())) {
                                        subscriptionList.add(0, currentLoggedInUser.getOverview());
                                    }
                                    adapter.setUserData(subscriptionList, GlobalVariables.SUBSCRIPTION_TYPE_FOLLOWERS);
                                    subscriptionsRecyclerProgress.setVisibility(View.GONE);
                                    subscriptionsRecycler.setVisibility(View.VISIBLE);
                                }
                            }

                            @Override
                            public void onFailure(Object value) {

                            }
                        });

                        break;
                }

            }
        });

    }


            /*
        TODO
        1) check ob currentOpenedUser.getId = currentLoggedInUser.getId
        WENN JA: Übergebe diese Informationen and Adapter, damit der alle neu hinzugekommenen Accounts anhand des Timestamps markieren kann (Neu)
        2) Der Adapter muss in Zukunft HashMap<String,Long> statt List<String> annehmen!
         */

    /**
     * initializes the recylcer view
     */
    private void initRecyclerView() {
        User currentLoggedInUser = watchProfileViewModel.getCurrentLoggedInUser().getValue();
        if (currentLoggedInUser == null) {
            return;
        }

        // set up the RecyclerView
        final RecyclerView spots = view.findViewById(R.id.fragment_watchProfile_subscriptions_recycler);
        adapter = new AdapterSubscriptions(view.getContext(), currentLoggedInUser);
        adapter.setClickListener(new AdapterSubscriptions.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                UserOverview userToOpen = (UserOverview) adapter.getItemByPosition(position);
                Fragment watchProfile = new FragmentWatchProfileUserCard();
                Bundle bundle = new Bundle();
                // Send currentOpenedUserId
                bundle.putString(GlobalVariables.CURRENT_OPENED_USER_ID, userToOpen.getId());
                watchProfile.setArguments(bundle);
                ((ActivityWatchProfile) getActivity()).addFragmentOnTop(watchProfile, userToOpen.getU());
            }

            @Override
            public void onSubscribeClick(View view, String userId) {

                subscriptionRepository.subscribeToUser(userId, new ValueCallback() {
                    @Override
                    public void onSuccess(Object value) {
                        // Update the WatchProfile ViewModel
                        User user = watchProfileViewModel.getCurrentLoggedInUser().getValue();
                        if (user == null) {
                            return;
                        }
                        HashMap<String, Long> map = user.getFollowedIds();
                        long currentTime = UtilityMethods.getCurrentTime();
                        map.put(userId, currentTime);
                        user.setFollowedIds(map);
                        //watchProfileViewModel.setCurrentLoggedInUser(user); ??????

                        // TODO show Subscribed!-Dialog
                    }

                    @Override
                    public void onFailure(Object value) {

                    }
                });

            }

            @Override
            public void onLoadMore(View view) {

            }
        });

        LinearLayoutManager llm = new LinearLayoutManager(view.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        spots.setLayoutManager(llm);
        spots.setAdapter(adapter);
    }

}
