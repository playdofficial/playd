package com.ansgarkersch.playd.repository;

import android.support.annotation.NonNull;
import android.util.Log;

import com.ansgarkersch.playd.globals.FirebaseEndpoints;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.UserOverview;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthEmailException;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FirebaseRepository {

    private String TAG = "Class: FirebaseRepository - ";
    private FirebaseAuth firebaseAuth;
    private FirebaseRestService firebaseRestService;
    private DatabaseReference firebaseDatabase;

    public FirebaseRepository() {
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance().getReference();

        // Firebase REST Service
        // OkHttpClient
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();

        // Retrofit
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(FirebaseEndpoints.GLOBAL_ENDPOINT)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        firebaseRestService = retrofit.create(FirebaseRestService.class);


        long currentTimestamp = 0L;
        firebaseDatabase.child("messages").child("userA").child("userB").orderByChild("timestamp").startAt(currentTimestamp);

        firebaseDatabase.child("messages").child("userA").child("userB");

    }

    /**
     * Returns the currently logged-in user
     * @return
     */
    public FirebaseUser getCurrentUser() {
        return firebaseAuth.getCurrentUser();
    }

    /**
     * Returns the user token of the currently logged-in user
     * @param callback
     */
    public void getUserToken(final ValueCallback callback) {
        getCurrentUser().getIdToken(true).addOnSuccessListener(new OnSuccessListener<GetTokenResult>() {
            @Override
            public void onSuccess(GetTokenResult getTokenResult) {
                callback.onSuccess(getTokenResult.getToken());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onFailure(GlobalVariables.RESULT_FAILURE);
                Log.d(TAG, e.getMessage());
            }
        });
    }

    /**
     *
     * @param callback
     */
    public void loginAnonymously(ValueCallback callback) {
        firebaseAuth.signInAnonymously().
                addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        Log.d(TAG, "signInAnonymously:success");
                        FirebaseUser user = authResult.getUser();
                        callback.onSuccess(user);
                    }
                }).
                addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.w(TAG, e.getMessage());
                callback.onFailure(GlobalVariables.RESULT_FAILURE);
            }
        });
    }

    /**
     *
     * @param username
     * @param callback
     */
    public void isUsernameAlreadyGiven(String username, ValueCallback callback) {

        firebaseDatabase
                .child(GlobalVariables.DB_USEROVERVIEW)
                .orderByChild(GlobalVariables.DB_USEROVERVIEW_USERNAME)
                .equalTo(username)
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    // The username already exists
                    callback.onSuccess(true);
                } else {
                    // The username does not exist
                    callback.onSuccess(false);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                callback.onFailure(databaseError.getMessage());
            }
        });

    }


    /**
     * Login with Spotify
     * Fetches the customToken and log in with this token
     * Returns the current-logged-in userId
     * @param accessToken
     * @param spotifyId
     * @param callback
     */
    public void loginWithSpotify(final String accessToken, final String spotifyId, final ValueCallback callback) {
        firebaseRestService.loginWithSpotify(accessToken, spotifyId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                if (response.code() == GlobalVariables.HTTP_OK) {
                    String customToken = response.body();
                    loginWithCustomToken(customToken, new ValueCallback() {
                        @Override
                        public void onSuccess(Object value) {
                            String userId = value.toString();
                            callback.onSuccess(userId);
                        }

                        @Override
                        public void onFailure(Object value) {
                            callback.onFailure(GlobalVariables.RESULT_FAILURE);
                        }
                    });
                } else {
                    callback.onFailure(GlobalVariables.RESULT_FAILURE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                if (t.getMessage() != null) {
                    Log.d(TAG, t.getMessage());
                }
                if(t instanceof SocketTimeoutException){
                    callback.onFailure(GlobalVariables.RESULT_TIMEOUT);
                } else {
                    callback.onFailure(GlobalVariables.RESULT_FAILURE);
                }
            }
        });
    }

    private void loginWithCustomToken(final String customToken, final ValueCallback callback) {

        firebaseAuth.signInWithCustomToken(customToken)
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                Log.d(TAG, "Successfully signed up with customToken");
                callback.onSuccess(authResult.getUser().getUid());
            }
        })
                .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, e.getMessage());
                callback.onFailure(GlobalVariables.RESULT_FAILURE);
            }
        }).addOnCanceledListener(new OnCanceledListener() {
            @Override
            public void onCanceled() {
                Log.d(TAG, "Canceled");
                callback.onFailure(GlobalVariables.RESULT_CANCELED);
            }
        });

    }


    /**
     * Login with the given credentials: Email and Password
     * @param email
     * @param password
     * @param callback
     */
    public void loginWithPlayd(final String email, final String password, final AuthCallback callback) {

        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        FirebaseUser s = authResult.getUser();
                        callback.onCallback(GlobalVariables.RESULT_OK);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(getClass().getName(), e.getMessage());
                if (e instanceof FirebaseAuthEmailException) {
                    callback.onCallback(GlobalVariables.RESULT_CREDENTIALS_FAILED_AUTH_INVALID_EMAIL);
                } else if (e instanceof FirebaseAuthInvalidUserException) {
                    callback.onCallback(GlobalVariables.RESULT_CREDENTIALS_FAILED_AUTH_INVALID_USER);
                } else if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    callback.onCallback(GlobalVariables.RESULT_CREDENTIALS_FAILED_INVALID_CREDENTIALS);
                } else {
                    callback.onCallback(GlobalVariables.RESULT_FAILURE);
                }
            }
        }).addOnCanceledListener(new OnCanceledListener() {
            @Override
            public void onCanceled() {
                Log.d(getClass().getName(), "Login was canceled");
                callback.onCallback(GlobalVariables.RESULT_CANCELED);
            }
        });

    }

    /**
     * Logout
     * @param callback
     */
    public void logoutUser(final ValueCallback callback) {
        firebaseAuth.signOut();
        callback.onSuccess(GlobalVariables.RESULT_OK);
    }

    /**
     * updates the registrationToken in the firebase realtimeDatabase
     */
    public void updateRegistrationToken() {

        getRegistrationToken(new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                String registrationToken = value.toString();
                Map<String, Object> registrationTokenData = new HashMap<>();
                registrationTokenData.put(getCurrentUser().getUid(), registrationToken);
                firebaseDatabase.child(GlobalVariables.DB_REGISTRATION_TOKENS).updateChildren(registrationTokenData);
            }

            @Override
            public void onFailure(Object value) {

            }
        });

    }

    private void getRegistrationToken(final ValueCallback callback) {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String registrationToken = instanceIdResult.getToken();
                callback.onSuccess(registrationToken);
            }
        });
    }

    public void sendMessageToDevice(final String foreignUserId, final String category, final String actionCode, final ValueCallback callback) {

        getUserToken(new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                String userToken = value.toString();

                Map<String, String> data = new HashMap<>();
                data.put(GlobalVariables.FOREIGN_USER_ID, foreignUserId);
                data.put(GlobalVariables.CATEGORY, category);
                data.put(GlobalVariables.ACTION_CODE, actionCode);

                firebaseRestService.sendMessageToDevice(userToken, data).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                        if (response.code() == GlobalVariables.HTTP_OK) {
                            callback.onSuccess(GlobalVariables.RESULT_OK);
                        } else {
                            callback.onSuccess(GlobalVariables.RESULT_FAILURE);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                        if (t.getMessage() != null) {
                            Log.d(TAG, t.getMessage());
                        }
                        if(t instanceof SocketTimeoutException){
                            callback.onFailure(GlobalVariables.RESULT_TIMEOUT);
                        } else {
                            callback.onFailure(GlobalVariables.RESULT_FAILURE);
                        }
                    }
                });
            }

            @Override
            public void onFailure(Object value) {

            }
        });

    }

}
