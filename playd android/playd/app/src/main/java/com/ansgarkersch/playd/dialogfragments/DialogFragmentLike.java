package com.ansgarkersch.playd.dialogfragments;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.globals.GlobalVariables;

public class DialogFragmentLike extends DialogFragment {

    private View view;
    private ImageView heart;

    @SuppressLint("InflateParams")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialogfragment_like, null);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        setCancelable(false);

        heart = view.findViewById(R.id.dialogfragment_like_imageview_heart);
        heart.animate().alpha(1.0f).setStartDelay(200).setDuration(400).start();

        new CountDownTimer(1000, 600) {
            @Override
            public void onTick(long l) { }
            @Override
            public void onFinish() {
                dismiss();
            }
        }.start();

        return view;
    }

}
