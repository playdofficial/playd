package com.ansgarkersch.playd.viewModel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.ansgarkersch.playd.activities.ActivityMain;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.Notification;
import com.ansgarkersch.playd.model.Spot;
import com.ansgarkersch.playd.events.SubscriptionEvent;
import com.ansgarkersch.playd.model.User;
import com.ansgarkersch.playd.repository.FirebaseRepository;
import com.ansgarkersch.playd.recievers.SubscriptionReciever;
import com.ansgarkersch.playd.repository.SubscriptionRepository;
import com.ansgarkersch.playd.repository.UserRepository;
import com.ansgarkersch.playd.repository.ValueCallback;
import com.ansgarkersch.playd.util.UtilityMethods;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class MyUserViewModel extends ViewModel {

    // Live Data
    private MutableLiveData<Spot> currentlyPlayingSpot;
    private MutableLiveData<User> currentUser;
    private MutableLiveData<Map<String, String>> subscriptionsWithRelationships;

    // Repositories
    private UserRepository userRepository;
    private FirebaseRepository firebaseRepository;
    private SubscriptionRepository subscriptionRepository;

    // Context
    private WeakReference<Context> weakContext;

    // Util
    private String TAG = getClass().getName();

    /**
     * todo erklären
     * IMPORTANT: Should only called once, in ActivityMain at startup.
     *
     * @param mContext
     */
    MyUserViewModel(Context mContext) {
        weakContext = new WeakReference<>(mContext);
        firebaseRepository = new FirebaseRepository();
        userRepository = new UserRepository();
        subscriptionRepository = new SubscriptionRepository(mContext);
    }

    public void init() {

        // Init with current logged-in user
        userRepository.getUserById(firebaseRepository.getCurrentUser().getUid(), new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                User user = (User) value;
                setCurrentUser(user);
                initRelationships();

                // Validate lastLoggedInTimestamp: If the last login has been too long (2 weeks), resubscribe all followed user topics
                long twoWeeksInMS = 1209600000L;
                if (UtilityMethods.getCurrentTime() - user.getLastLoggedInTimestamp() > twoWeeksInMS) {
                    // It's been to long!
                    Log.d(getClass().getName(), "The last login has been too long. Resubscribe all followed user topics...");
                    subscriptionRepository.resubscribeToAllFollowedUsers(new ArrayList<>(user.getFollowedIds().keySet()),
                            new ValueCallback() {
                                @Override
                                public void onSuccess(Object value) {
                                }

                                @Override
                                public void onFailure(Object value) {
                                }
                            });
                }

                // Validate the lastLoggedInTimestamp with the Followed-ValueTimestamps
                if (user.getFollowersIds().size() > 0
                        && Collections.max(user.getFollowersIds().values())
                        > user.getLastLoggedInTimestamp()) {
                    // there is a user which subscribed to the current-logged-in user without his knowledge
                    if (weakContext != null) {
                        ((ActivityMain) weakContext.get()).setAccountNotificationBadge(1);
                        Log.d(getClass().getName(), "Increment AccountNotificationBadge Count");
                    }
                }

                // Update current logged-in user lastLoggedInTimestamp
                userRepository.updateUsersLastLoggedInTimestamp(new ValueCallback() {
                    @Override
                    public void onSuccess(Object value) {
                        Log.d(getClass().getName(), "Successfully updated current users lastLoggedInTimestamp");
                    }

                    @Override
                    public void onFailure(Object value) {
                        Log.d(getClass().getName(), "Updating current users lastLoggedInTimestamp failed. Reason: " + value);
                    }
                });

                registerEventBus();
            }

            @Override
            public void onFailure(Object value) {
                Log.d(getClass().getName(), "Error fetching current logged-in user: " + value.toString());
            }
        });

    }

    /**
     * This listener recieves SubscriptionEvents, usually triggered by an FCM from the Server or from the app itself,
     * f.ex. when the current logged-in user subscribes to or unsubscribes from another user inside the app
     * @param subscriptionEvent the SubscriptionEvent, containing all the necessary information (Active/Passive Participant & ActionCode)
     */
    @Subscribe
    public void onSubscriptionEvent(SubscriptionEvent subscriptionEvent) {
        if (subscriptionEvent == null) {
            return;
        }
        String actionCode = subscriptionEvent.getActionCode();
        String activeParticipant = subscriptionEvent.getActiveParticipant();
        String passiveParticipant = subscriptionEvent.getPassiveParticipant();
        String myUserId = firebaseRepository.getCurrentUser().getUid();

        switch (actionCode) {
            case GlobalVariables.FCM_ACTIONCODE_SUBSCRIBED:
                Notification notification;
                if (myUserId.equals(activeParticipant)) { // current logged-in user is the active Participant
                    addUserIdToFollowed(passiveParticipant);
                    initRelationships();
                    if (isFriend(passiveParticipant)) {
                        // A friendship is born
                        notification = new Notification(GlobalVariables.ACTIONCODE_NOTIFICATION_FRIENDSHIP);
                        notification.setUserId(passiveParticipant);
                        sendNotificationToAppComponents(notification);
                    } else {
                        // The current logged-in user followed someone
                    }
                    return;
                } else { // current logged-in user is the passive Participant
                    // Add userId to 'followers'
                    addUserIdToFollowers(activeParticipant);
                    initRelationships();
                    if (isFriend(activeParticipant)) {
                        // A friendship is born
                        notification = new Notification(GlobalVariables.ACTIONCODE_NOTIFICATION_FRIENDSHIP);
                        notification.setUserId(activeParticipant);
                        sendNotificationToAppComponents(notification);
                    } else {
                        notification = new Notification(GlobalVariables.ACTIONCODE_NOTIFICATION_NEW_FOLLOWER);
                        notification.setUserId(activeParticipant);
                        sendNotificationToAppComponents(notification);
                    }
                }

                break;
            case GlobalVariables.FCM_ACTIONCODE_UNSUBSCRIBED:
                if (myUserId.equals(activeParticipant)) { // current logged-in user is the active Participant
                    removeUserIdFromFollowed(passiveParticipant);
                    return;
                } else { // current logged-in user is the passive Participant
                    // Remove userId from 'followers'
                    removeUserIdFromFollowers(activeParticipant);
                }
                break;
        }

        initRelationships();
    }

    /*
    Usually called in the onStart()-Method of the Parent - Activity/Fragment
     */
    public void registerEventBus() {
        EventBus.getDefault().register(this);
        Log.d(TAG, "registerEventBus");
    }

    /*
    Usually called in the onStop()-Method of the Parent - Activity/Fragment
     */
    public void unregisterEventBus() {
        EventBus.getDefault().unregister(this);
        Log.d(TAG, "unregisterEventBus");
    }

    private void sendNotificationToAppComponents(Notification notification) {
        EventBus.getDefault().post(notification);
        Log.d(getClass().getName(),
                "A Notification with the notificationType "
                        + notification.getNotificationType()
                        + " was broadcasted by Greenrobot. UserId: "
                        + notification.getUserId()
                        + " | Message: "
                        + notification.getMessage());
    }

    /**
     * todo erklären
     */
    private void initRelationships() {
        Map<String, String> subscriptionsWithRelationship = getSubscriptionsWithRelationship().getValue();
        User user = getCurrentUser().getValue();

        if (user == null) {
            return;
        }

        List<String> followedUIds = new ArrayList<>(user.getFollowedIds().keySet());
        List<String> followersUIds = new ArrayList<>(user.getFollowersIds().keySet());

        if (subscriptionsWithRelationship == null) {
            subscriptionsWithRelationship = new HashMap<>();
        }

        for (String s : followedUIds) {
            subscriptionsWithRelationship.put(s, GlobalVariables.RELATIONSHIP_FOLLOWED);
        }

        for (String s : followersUIds) {
            if (subscriptionsWithRelationship.containsKey(s)) { // friends!
                subscriptionsWithRelationship.put(s, GlobalVariables.RELATIONSHIP_FRIEND);
            } else {
                subscriptionsWithRelationship.put(s, GlobalVariables.RELATIONSHIP_FOLLOWERS);
            }
        }
        setSubscriptionsWithRelationship(subscriptionsWithRelationship);

    }

    /**
     * Get a Map with friends and the friendshipDate
     *
     * @return
     */
    public Map<String, Long> getFriends() {
        Map<String, Long> result = new HashMap<>();
        User user = getCurrentUser().getValue();
        Map<String, String> subscriptionsWithRelationship = getSubscriptionsWithRelationship().getValue();
        if (user == null || subscriptionsWithRelationship == null) {
            return result;
        }

        for (Map.Entry<String, String> entry : subscriptionsWithRelationship.entrySet()) {
            if (entry.getValue().equals(GlobalVariables.RELATIONSHIP_FRIEND)) {
                long followedTimeStamp = user.getFollowedIds().get(entry.getKey());
                long followerTimeStamp = user.getFollowersIds().get(entry.getKey());
                if (followedTimeStamp >= followerTimeStamp) {
                    result.put(entry.getKey(), followedTimeStamp);
                } else {
                    result.put(entry.getKey(), followerTimeStamp);
                }
            }
        }

        return result;
    }

    public boolean isFriend(String foreignUserId) {
        return getUserIdsByRelationship(GlobalVariables.RELATIONSHIP_FRIEND).contains(foreignUserId);
    }

    public List<String> getUserIdsByRelationship(String relationship) {
        Map<String, String> subscriptionsWithRelationship = getSubscriptionsWithRelationship().getValue();
        List<String> result = new ArrayList<>();

        if (subscriptionsWithRelationship == null) {
            return result;
        }

        for (Map.Entry<String, String> entry : subscriptionsWithRelationship.entrySet()) {
            if (entry.getValue().equals(relationship)) {
                result.add(entry.getKey());
            }
        }
        return result;
    }

    private void addUserIdToFollowers(String followersUserIdToAdd) {
        User user = getCurrentUser().getValue();
        if (user == null) {
            return;
        }
        HashMap<String, Long> followersIds = getCurrentUser().getValue().getFollowersIds();
        followersIds.put(followersUserIdToAdd, UtilityMethods.getCurrentTime());
        user.setFollowersIds(followersIds);
        setCurrentUser(user);
    }

    private void removeUserIdFromFollowers(String followersUserIdToRemove) {
        User user = getCurrentUser().getValue();
        if (user == null) {
            return;
        }
        HashMap<String, Long> followersIds = getCurrentUser().getValue().getFollowersIds();
        followersIds.remove(followersUserIdToRemove);
        user.setFollowersIds(followersIds);
        setCurrentUser(user);
    }

    private void addUserIdToFollowed(String followedUserIdToAdd) {
        User user = getCurrentUser().getValue();
        if (user == null) {
            return;
        }
        HashMap<String, Long> followedIds = getCurrentUser().getValue().getFollowedIds();
        followedIds.put(followedUserIdToAdd, UtilityMethods.getCurrentTime());
        user.setFollowedIds(followedIds);
        setCurrentUser(user);
    }

    private void removeUserIdFromFollowed(String followedUserIdToRemove) {
        User user = getCurrentUser().getValue();
        if (user == null) {
            return;
        }
        HashMap<String, Long> followedIds = getCurrentUser().getValue().getFollowedIds();
        followedIds.remove(followedUserIdToRemove);
        user.setFollowedIds(followedIds);
        setCurrentUser(user);
    }


    public MutableLiveData<Spot> getCurrentlyPlayingSpot() {
        if (currentlyPlayingSpot == null) {
            currentlyPlayingSpot = new MutableLiveData<>();
        }
        return currentlyPlayingSpot;
    }

    public void setCurrentlyPlayingSpot(Spot currentlyPlayingSpot) {
        if (this.currentlyPlayingSpot == null) {
            this.currentlyPlayingSpot = new MutableLiveData<>();
        }
        getCurrentlyPlayingSpot().postValue(currentlyPlayingSpot);
    }

    public void setCurrentUser(User user) {
        if (currentUser == null) {
            currentUser = new MutableLiveData<>();
        }
        getCurrentUser().postValue(user);
    }

    public MutableLiveData<User> getCurrentUser() {
        if (currentUser == null) {
            currentUser = new MutableLiveData<>();
        }
        return currentUser;
    }

    /******************* Subscription Relationships **********************/
    public void setSubscriptionsWithRelationship(Map<String, String> subscriptionsWithRelationship) {
        if (subscriptionsWithRelationship == null) {
            this.subscriptionsWithRelationships = new MutableLiveData<>();
        }
        getSubscriptionsWithRelationship().setValue(subscriptionsWithRelationship);
    }

    public MutableLiveData<Map<String, String>> getSubscriptionsWithRelationship() {
        if (subscriptionsWithRelationships == null) {
            subscriptionsWithRelationships = new MutableLiveData<>();
        }
        return subscriptionsWithRelationships;
    }

}
