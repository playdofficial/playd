package com.ansgarkersch.playd.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.adapter.AdapterSpotExplorerSwipe;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.IScrollable;
import com.ansgarkersch.playd.model.Spot;
import com.ansgarkersch.playd.model.User;
import com.ansgarkersch.playd.repository.SpotRepository;
import com.ansgarkersch.playd.repository.UserRepository;
import com.ansgarkersch.playd.services.ForegroundServiceSpotListener;
import com.ansgarkersch.playd.services.ForegroundServiceSpotListenerCallbacks;
import com.ansgarkersch.playd.util.UtilityMethods;
import com.lorentzos.flingswipe.SwipeFlingAdapterView;

import java.util.ArrayList;
import java.util.List;

public class ActivitySpotExplorer extends AppCompatActivity implements ForegroundServiceSpotListenerCallbacks {

    // Services
    ForegroundServiceSpotListener mForegroundServiceSpotListener;
    boolean mBoundSpotListener = false;

    // Repositories
    private SpotRepository spotRepository;

    // UI elements
    private AdapterSpotExplorerSwipe adapterSpotExplorerSwipe;

    // todo category
    private ArrayList<Spot> spotData;
    private User currentLoggedInUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spotexplorer);

        // Prevent errors on some api versions
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        // Check if there is a bundle delivered
        if (getIntent() != null) {
            // todo Bundle? Get first spots from activityMain to prevent loading?
            spotData = (ArrayList<Spot>) getIntent().getSerializableExtra(GlobalVariables.INTENT_SPOT);
            currentLoggedInUser = (User) getIntent().getSerializableExtra(GlobalVariables.INTENT_PROFILE_CURRENT_USER);
        }

        // Repositories
        spotRepository = new SpotRepository(new UserRepository());

        initSwipeView();

        // Check if ForegroundServiceSpotListener is already running
        if (UtilityMethods.isMyServiceRunning(this, ForegroundServiceSpotListener.class)) {
            bindForegroundServiceSpotListener();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindForegroundServiceSpotListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Check if ForegroundServiceSpotListener is already running
        if (UtilityMethods.isMyServiceRunning(this, ForegroundServiceSpotListener.class)) {
            if (!mBoundSpotListener) {
                bindForegroundServiceSpotListener();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        overridePendingTransition(android.R.anim.fade_in, R.anim.spotexplorer_slide_out_left_to_right);

        // Unbind Service
        unbindForegroundServiceSpotListener();
    }

    @Override
    public void onBackPressed() {
        closeWindow();
    }

    private void closeWindow() {
        finish();
        overridePendingTransition(android.R.anim.fade_in, R.anim.spotexplorer_slide_out_left_to_right);
    }

    /**
     * initializes the spotExplorer SwipeView
     */
    private void initSwipeView() {

        final SwipeFlingAdapterView flingContainer = findViewById(R.id.activtiy_spotexplorer_swipeView);

        List<IScrollable> data = new ArrayList<>(spotData);

        adapterSpotExplorerSwipe = new AdapterSpotExplorerSwipe(
                this,
                findViewById(R.id.activtiy_spotexplorer_coordinatorLayout),
                currentLoggedInUser,
                data);
        flingContainer.setAdapter(adapterSpotExplorerSwipe);
        flingContainer.setMaxVisible(1);
        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {

            }

            @Override
            public void onLeftCardExit(Object dataObject) {
                adapterSpotExplorerSwipe.next();
            }

            @Override
            public void onRightCardExit(Object dataObject) {
                adapterSpotExplorerSwipe.like();
            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {
                // todo fetch more
            }

            @Override
            public void onScroll(float scrollProgressPercent) {
                View view = flingContainer.getSelectedView();
                try {
                    view.findViewById(R.id.row_spotexplorer_frameLayout_swipeIndicator).setAlpha(0);
                    view.findViewById(R.id.row_spotexplorer_item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                    view.findViewById(R.id.row_spotexplorer_item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
                } catch (Exception ex) {
                    Log.e(getClass().getName(), ex.getMessage(), ex.getCause());
                }
            }
        });
    }

    @Override
    public void notifyPlaying(Spot spot) {
        // adapterSpotExplorerSwipe.notifyPlayback(true);
    }

    @Override
    public void notifyPaused() {
        // adapterSpotExplorerSwipe.notifyPlayback(false);
    }

    /**
     * Init Binding to ForegroundServiceSpotListener
     */
    private ServiceConnection mConnectionSpotListener = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ForegroundServiceSpotListener.LocalBinder binder = (ForegroundServiceSpotListener.LocalBinder) service;
            mForegroundServiceSpotListener = binder.getService();
            mBoundSpotListener = true;

            if (mForegroundServiceSpotListener == null) {
                return;
            }

            mForegroundServiceSpotListener.setCallbacks(ActivitySpotExplorer.this);

            // is called only once, when the service is fresh binded to this activity
            if (mForegroundServiceSpotListener.getCurrentSpot() == null) {
                return;
            }
            notifyPlaying(mForegroundServiceSpotListener.getCurrentSpot());

        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBoundSpotListener = false;
        }
    };

    public void bindForegroundServiceSpotListener() {
        if (!mBoundSpotListener) {
            Intent intentForegroundServiceSpotListener = new Intent(this, ForegroundServiceSpotListener.class);
            bindService(intentForegroundServiceSpotListener, mConnectionSpotListener, Context.BIND_AUTO_CREATE);
            Log.e(getClass().getName(), "ForegroundServiceSpotListener is binded to ActivitySpotExplorer");
        }
    }

    public void unbindForegroundServiceSpotListener() {
        if (mBoundSpotListener) {
            unbindService(mConnectionSpotListener);
            mBoundSpotListener = false;
            if (mForegroundServiceSpotListener != null) {
                mForegroundServiceSpotListener.setCallbacks(null);
            }
            Log.e(getClass().getName(), "ForegroundServiceSpotListener is unbinded by ActivitySpotExplorer");
        }
    }

}
