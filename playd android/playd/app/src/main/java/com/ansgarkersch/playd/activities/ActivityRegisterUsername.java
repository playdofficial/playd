package com.ansgarkersch.playd.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.util.UtilityMethods;

public class ActivityRegisterUsername extends AppCompatActivity {

    // UI elements
    private EditText editText_username;
    private ImageView image_errorIndicator;
    private Button button_submit;

    // User-specific values
    private String spotifyUserId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_username);

        // Get recieved Spotify userId from Bundle
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            finish();
            Toast.makeText(this, R.string.toast_unexpected_error_occured, Toast.LENGTH_LONG).show();
            return;
        } else {
            spotifyUserId = getIntent().getStringExtra(GlobalVariables.INTENT_SPOTIFY_USER_ID);
        }

        // UI elements
        editText_username = findViewById(R.id.activtiy_register_username_editText_username);
        image_errorIndicator = findViewById(R.id.activtiy_register_username_image_errorIndicator);
        button_submit = findViewById(R.id.activtiy_register_username_button_submit);

        initSubmitButton();

    }

    private void initSubmitButton() {
        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button_submit.setOnClickListener(null);
                String username = editText_username.getText().toString().trim();

                if (username.isEmpty()) {
                    image_errorIndicator.setVisibility(View.VISIBLE);
                    editText_username.setError(getResources().getString(R.string.error_enterUsername));
                    initSubmitButton();
                    return;
                }

                if (username.length() < 5) {
                    image_errorIndicator.setVisibility(View.VISIBLE);
                    editText_username.setError(getResources().getString(R.string.error_minFiveLetters));
                    initSubmitButton();
                    return;
                }

                if (username.length() > 10) {
                    image_errorIndicator.setVisibility(View.VISIBLE);
                    editText_username.setError(getResources().getString(R.string.error_maxTenLetters));
                    initSubmitButton();
                    return;
                }

                if (!UtilityMethods.isFirebaseReferenceAllowed(username)) {
                    image_errorIndicator.setVisibility(View.VISIBLE);
                    editText_username.setError(getResources().getString(R.string.error_lettersNotAllowed));
                    initSubmitButton();
                    return;
                }

                // Go on with enterin the credentials
                Intent intentRegisterCredentials = new Intent(ActivityRegisterUsername.this, ActivityRegisterCredentials.class);
                // Put username and spotifyUserId in the Bundle
                intentRegisterCredentials.putExtra(GlobalVariables.INTENT_USERNAME, username);
                intentRegisterCredentials.putExtra(GlobalVariables.INTENT_SPOTIFY_USER_ID, spotifyUserId);
                startActivity(intentRegisterCredentials);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

            }
        });
    }

}
