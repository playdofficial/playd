package com.ansgarkersch.playd.events;

import com.ansgarkersch.playd.globals.GlobalVariables;

import java.io.Serializable;

public class SubscriptionEvent implements Serializable {

    private String activeParticipant;
    private String passiveParticipant;
    private String actionCode;

    public SubscriptionEvent() { }

    public String getActiveParticipant() {
        return activeParticipant;
    }

    public void setActiveParticipant(String activeParticipant) {
        this.activeParticipant = activeParticipant;
    }

    public String getPassiveParticipant() {
        return passiveParticipant;
    }

    public void setPassiveParticipant(String passiveParticipant) {
        this.passiveParticipant = passiveParticipant;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }
}
