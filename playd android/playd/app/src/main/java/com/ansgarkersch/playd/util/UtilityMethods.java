package com.ansgarkersch.playd.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.ansgarkersch.playd.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class UtilityMethods {

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static boolean isFirebaseReferenceAllowed(String text) {
        if (text.contains(".")) {
            return false;
        }
        if (text.contains("$")) {
            return false;
        }
        if (text.contains("[")) {
            return false;
        }
        if (text.contains("]")) {
            return false;
        }
        if (text.contains("#")) {
            return false;
        }
        if (text.contains("/")) {
            return false;
        }
        return true;
    }


    public static boolean isPasswordSecureEnough(String password) {
        return password.length() >= 6 && password.matches(".*\\d+.*");
    }

    public static boolean isThisAgeValid(String dateToValidate) {
        if (getUserAge(dateToValidate) < 13) {
            return false;
        }
        return true;
    }

    public static long getCurrentTime() {
        return System.currentTimeMillis();
    }

    public static boolean isThisDateValid(String dateToValidate){
        if(dateToValidate == null){
            return false;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setLenient(false);

        try {
            //if not valid, it will throw ParseException
            sdf.parse(dateToValidate);
        } catch (ParseException e) {
            return false;
        }

        if (getUserAge(dateToValidate) > 123) {
            return false;
        }

        return true;
    }

    public static byte[] uriToByteArray(Context context, Uri uri) throws IOException {
        InputStream inputStream = context.getContentResolver().openInputStream(uri);
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        if (inputStream != null) {
            while ((len = inputStream.read(buffer)) != -1) {
                byteBuffer.write(buffer, 0, len);
            }
        }
        return byteBuffer.toByteArray();
    }

    public static byte[] bitmapToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        return baos.toByteArray();
    }

    public static boolean isSpotifyInstalled(Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo("com.spotify.music", PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            // do nothing
        }

        return false;
    }

    public static int getUserAge(String input){
        if (input == null) {
            return 0;
        }
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date birthdate;
        try {
            birthdate = df.parse(input);
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
        Calendar dateOfBirth = Calendar.getInstance();
        dateOfBirth.setTime(birthdate);
        Calendar today = Calendar.getInstance();
        int age = today.get(Calendar.YEAR) - dateOfBirth.get(Calendar.YEAR);
        dateOfBirth.add(Calendar.YEAR, age);
        if (today.before(dateOfBirth)) {
            age--;
        }
        return age;
    }

    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isBitmapWhite(Bitmap image) {
        Bitmap bitmap = Bitmap.createScaledBitmap(image, 50, 50, false);
        int pixelSpacing = 1;
        int R = 0; int G = 0; int B = 0;
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        int n = 0;
        int[] pixels = new int[width * height];
        bitmap.getPixels(pixels, 0, width, 0, 0, width, height);
        for (int i = 0; i < pixels.length; i += pixelSpacing) {
            int color = pixels[i];
            R += Color.red(color);
            G += Color.green(color);
            B += Color.blue(color);
            n++;
        }
        // Range between 0 and 255, 0 is dark, 255 is bright
        int brightness = (R + B + G) / (n * 3);
        return brightness > 230;
    }

    public static int getDominantColorFromBitmap(Bitmap input) {
        Bitmap bitmap = Bitmap.createScaledBitmap(input, 1, 1, true);
        final int dominantColor = bitmap.getPixel(0, 0);
        bitmap.recycle();
        return dominantColor;
    }

    /**
     * TODO erklären
     * @param spotStartedTimestamp
     * @return
     */
    public static long calculateSpotStartedTimestampInMinutes(long spotStartedTimestamp) {
        long now = UtilityMethods.getCurrentTime();
        long differenceSeconds = (now - spotStartedTimestamp) / 1000;
        if (differenceSeconds < 60) {
            // Print in UI a text like "Started right now"
            return -1;
        }
        return Math.round(differenceSeconds / 60);
    }

    public static String getCalculatedSpotStartedTimestampInMinutesAsText(Context context, long minutes) {
        String spotStartedTimeStampInMinutesText;
        if (minutes == -1) {
            // Spot started less a minute ago
            return context.getResources().getString(R.string.rightNow);
        } else if (minutes == 1) {
            return context.getResources().getString(R.string.since) + " " +
                            context.getResources().getString(R.string.one) + " " +
                            context.getResources().getString(R.string.minute);
        } else if (minutes == 60){
            return context.getResources().getString(R.string.since) + " " +
                    context.getResources().getString(R.string.one) + " " +
                    context.getResources().getString(R.string.hour);
        } else if (minutes > 60){
            return context.getResources().getString(R.string.since) + " " +
                    String.valueOf(Math.round(minutes / 60)) + " " +
                    context.getResources().getString(R.string.hours);
        } else {
            return context.getResources().getString(R.string.since) + " " +
                    String.valueOf(minutes) + " " +
                    context.getResources().getString(R.string.minutes);
        }
    }

    public static long calculateDistance(double longitude, double latitude) {
        return 0L; // TODO
    }


    public static boolean isPlaydInBackground(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
            if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                for (String activeProcess : processInfo.pkgList) {
                    if (activeProcess.equals(context.getPackageName())) {
                        // Your app is the process in foreground,
                        // return false beacuse it's not running in background
                        return false;
                    }
                }
            }
        }


        return true;
    }

}
