package com.ansgarkersch.playd.util;

import com.ansgarkersch.playd.model.Spot;
import com.ansgarkersch.playd.model.UserOverview;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class SpotUtilities {

    /**
     * Helper Method, Removes null values a & sort by spotStartedTimestamp
     * @param data List of UserOverview-Objects
     * @return
     */
    public static List<UserOverview> removeNullValuesFromUserOverviewsAndSortBySpotStartedTimestamp(List<UserOverview> data) {
        Iterator<UserOverview> iterator = data.iterator();
        while (iterator.hasNext()) {
            UserOverview o = iterator.next();
            if (o == null) {
                iterator.remove();
            }
        }
        Collections.sort(data, new Comparator<UserOverview>() {
            @Override
            public int compare(UserOverview t1, UserOverview t2) {
                return Long.compare(t2.getS(), t1.getS());
            }
        });
        return data;
    }


    /**
     * Helper Method, Removes null values a & sort by spotStartedTimestamp
     * @param data List of UserOverview-Objects
     * @return
     */
    public static List<Spot> removeNullValuesFromSpotsAndSortBySpotStartedTimestamp(List<Spot> data) {
        Iterator<Spot> iterator = data.iterator();
        while (iterator.hasNext()) {
            Spot o = iterator.next();
            if (o == null) {
                iterator.remove();
            }
        }
        Collections.sort(data, new Comparator<Spot>() {
            @Override
            public int compare(Spot t1, Spot t2) {
                return Long.compare(t2.getPlayback().getSpotStartedTimestamp(), t1.getPlayback().getSpotStartedTimestamp());
            }
        });
        return data;
    }


}
