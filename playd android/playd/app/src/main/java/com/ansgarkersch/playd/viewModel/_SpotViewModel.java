package com.ansgarkersch.playd.viewModel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;

import com.ansgarkersch.playd._spotify.repository.SpotifyRepository;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.Spot;
import com.ansgarkersch.playd.events.SubscriptionEvent;
import com.ansgarkersch.playd.repository.FirebaseRepository;
import com.ansgarkersch.playd.recievers.PlaybackReciever;
import com.ansgarkersch.playd.repository.SpotRepository;
import com.ansgarkersch.playd.recievers.SubscriptionReciever;
import com.ansgarkersch.playd.repository.UserRepository;
import com.ansgarkersch.playd.repository.ValueCallback;
import com.google.firebase.firestore.ListenerRegistration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

@Deprecated
public class _SpotViewModel extends ViewModel implements Observer {

    private MutableLiveData<List<Spot>> followedSpotList;
    private MutableLiveData<Map<String, Spot>> followedSpots;

    // Repositories
    private SpotifyRepository spotifyRepository;
    private SpotRepository spotRepository;
    private FirebaseRepository firebaseRepository;

    // Reciever
    private SubscriptionReciever subscriptionReciever;
    private PlaybackReciever playbackReciever;

    _SpotViewModel(Context mContext) {
        // Repositories
        spotRepository = new SpotRepository(new UserRepository());
        firebaseRepository = new FirebaseRepository();

        // Reciever
        subscriptionReciever = new SubscriptionReciever();
        subscriptionReciever.addObserver(this);
        playbackReciever = new PlaybackReciever();
        playbackReciever.addObserver(this);
    }

    @Override
    public void update(Observable o, Object recievedObject) {
        if (recievedObject instanceof String) {
            // Subscription-Event
            final String playbackId = recievedObject.toString();
            spotRepository.getSingleUserOverview(playbackId, new ValueCallback() {
                @Override
                public void onSuccess(Object value) {
                    Spot spot = (Spot) value;
                    if (spot == null) {
                        return;
                    }
                    addToFollowedSpots(spot);
                }

                @Override
                public void onFailure(Object value) {

                }
            });
        } else if (recievedObject instanceof SubscriptionEvent) {
            // Playback-Event
            final SubscriptionEvent subscriptionEvent = (SubscriptionEvent) recievedObject;
            if (subscriptionEvent == null) {
                return;
            }
            String actionCode = subscriptionEvent.getActionCode();
            String activeParticipant = subscriptionEvent.getActiveParticipant();
            String passiveParticipant = subscriptionEvent.getPassiveParticipant();
            String myUserId = firebaseRepository.getCurrentUser().getUid();

            switch (actionCode) {
                case GlobalVariables.FCM_ACTIONCODE_SUBSCRIBED:
                    if (myUserId.equals(activeParticipant)) { // current logged-in user is the active Participant
                        // Check if freshly subscribed user is a spot at the moment
                        spotRepository.getSingleUserOverview(passiveParticipant, new ValueCallback() {
                            @Override
                            public void onSuccess(Object value) {
                                Spot spot = (Spot) value;
                                if (spot == null) {
                                    return;
                                }
                                addToFollowedSpots(spot);
                            }

                            @Override
                            public void onFailure(Object value) {

                            }
                        });
                    } else { // current logged-in user is the passive Participant
                        // do nothing yet
                    }
                    break;
                case GlobalVariables.FCM_ACTIONCODE_UNSUBSCRIBED:
                    if (myUserId.equals(activeParticipant)) { // current logged-in user is the active Participant
                        // Remove the spot from the feed
                        removeSpotFromFollowedSpots(passiveParticipant);
                        // todo
                    } else { // current logged-in user is the passive Participant
                        // do nothing yet
                    }
                    break;
            }
        }



    }

    /**
     * todo erklären
     * @param context
     */
    public void registerReciever(Context context) {
        subscriptionReciever.registerReciever(context);
        playbackReciever.registerReciever(context); // todo in seperate methode?
    }

    /**
     * todo erklären
     * @param context
     */
    public void unregisterReciever(Context context) {
        subscriptionReciever.unregisterReciever(context);
        playbackReciever.unregisterReciever(context); // todo in seperate methode?
    }

    public void removeSpotFromFollowedSpots(String spotId) {
        Map<String, Spot> currentSpots = this.followedSpots.getValue();
        if (currentSpots == null) {
            currentSpots = new HashMap<>();
        }
        currentSpots.remove(spotId);
        setFollowedSpots(currentSpots);
    }

    public void addToFollowedSpots(Spot spot) {
        Map<String, Spot> data = new HashMap<>();
        data.put(spot.getId(), spot);
        addToFollowedSpots(data);
    }

    public void addToFollowedSpots(Map<String, Spot> followedSpots) {
        if (followedSpots == null) {
            this.followedSpots = new MutableLiveData<>();
            return;
        }
        Map<String, Spot> currentSpots = this.followedSpots.getValue();
        if (currentSpots == null) {
            currentSpots = new HashMap<>();
        }
        currentSpots.putAll(followedSpots);
        setFollowedSpots(currentSpots);
    }

    public void addToFollowedSpots(List<Spot> followedSpots) {
        if (followedSpots == null) {
            this.followedSpots = new MutableLiveData<>();
            return;
        }
        Map<String, Spot> currentSpots = this.followedSpots.getValue();
        if (currentSpots == null) {
            currentSpots = new HashMap<>();
        }
        for (Spot s : followedSpots) {
            currentSpots.put(s.getId(), s);
        }
        setFollowedSpots(currentSpots);
    }



    /*
    Following: THE MYPLAYD FEED
     */

    public void setFollowedSpots(Map<String, Spot> followedSpots) {
        if (followedSpots == null) {
            this.followedSpots = new MutableLiveData<>();
        }
        getFollowedSpots().postValue(followedSpots);
    }

    public MutableLiveData<Map<String, Spot>> getFollowedSpots() {
        if (followedSpots == null) {
            followedSpots = new MutableLiveData<>();
        }
        return followedSpots;
    }




    // Firestore Listener
    private Map<String, ListenerRegistration> listenerRegistrationMap;


    public void init() {

    }


















}
