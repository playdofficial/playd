package com.ansgarkersch.playd._spotify.model;

import java.util.List;

public class Track {

    private Album album;
    private List<Artist> artists;
    private String id;
    private String name;
    private long duration_ms;
    private int popularity;

    public Track() {
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj.getClass() == Track.class && this.id.equals(((Track) obj).getId());
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public List<Artist> getArtists() {
        return artists;
    }

    public void setArtists(List<Artist> artists) {
        this.artists = artists;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDuration_ms() {
        return duration_ms;
    }

    public void setDuration_ms(long duration_ms) {
        this.duration_ms = duration_ms;
    }

    public int getPopularity() {
        return popularity;
    }

    public void setPopularity(int popularity) {
        this.popularity = popularity;
    }
}
