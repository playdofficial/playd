package com.ansgarkersch.playd.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd._spotify.SpotifyGlobalVariables;
import com.ansgarkersch.playd._spotify.model.Token;
import com.ansgarkersch.playd._spotify.model.User;
import com.ansgarkersch.playd._spotify.repository.SpotifyRepository;
import com.ansgarkersch.playd._spotify.util.SpotifyUtilityMethods;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.repository.ValueCallback;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;

/**
 * IMPORTANT: Only use this Activity in an "activity-for-result" - context
 * Connects with Spotify and fetches the accessToken/refreshToken
 * Can return 3 resultCodes:
 * - SPOTIFY_RESULT_NOT_INSTALLED returns value null
 * - SPOTIFY_RESULT_NOT_PREMIUM returns value null
 * - RESULT_OK returns an Intent with SpotifyUser-Object, AccessToken and RefreshToken
 */
public class ActivityConnectWithSpotify extends AppCompatActivity {

    // UI elements
    private ImageButton back;

    // Shared Preferences
    private SharedPreferences sharedPreferences;

    // Repositories
    private SpotifyRepository spotifyRepository;

    // Util
    private boolean isAuthenticating = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connectwithspotify);

        // UI elements
        back = findViewById(R.id.activity_connectwithspotify_imagebutton_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        // Shared Preferences
        sharedPreferences = this.getSharedPreferences(
                "com.ansgarkersch.playd", Context.MODE_PRIVATE);

        // Repositories
        spotifyRepository = new SpotifyRepository(this);

        // Check if Spotify is installed
        boolean isSpotifyInstalled = SpotifyUtilityMethods.isSpotifyInstalled(ActivityConnectWithSpotify.this);
        if (isSpotifyInstalled) {
            // Go on
            initSpotifyAuth();
        } else {
            setResult(GlobalVariables.SPOTIFY_RESULT_NOT_INSTALLED, null);
            finish();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        setResult(GlobalVariables.RESULT_FAILURE, null);
        finish();
    }

    private void initSpotifyAuth() {

        spotifyRepository.getCurrentUser(new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                User user = (User) value;
                spotifyRepository.getRefreshedAccessToken(new ValueCallback() {
                    @Override
                    public void onSuccess(Object value) {
                        String accessToken = value.toString();
                        if (accessToken == null || accessToken.isEmpty()) {
                            setResult(GlobalVariables.RESULT_FAILURE, null);
                            finish();
                        }
                        if (user.isPremium()) {
                            Intent tokenIntent = new Intent();
                            tokenIntent.putExtra(GlobalVariables.INTENT_SPOTIFY_USER, user);
                            tokenIntent.putExtra(GlobalVariables.INTENT_SPOTIFY_ACCESS_TOKEN, accessToken);
                            setResult(GlobalVariables.RESULT_OK, tokenIntent);
                            finish();
                        } else {
                            setResult(GlobalVariables.SPOTIFY_RESULT_NOT_PREMIUM, null);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Object value) {
                        setResult(GlobalVariables.RESULT_FAILURE, null);
                        finish();
                    }
                });

            }

            @Override
            public void onFailure(Object value) {
                if (isAuthenticating) {
                    return;
                }
                isAuthenticating = true;
                AuthenticationRequest authenticationRequest = spotifyRepository.getSpotifyAuthenticationRequest();
                AuthenticationClient.openLoginActivity(
                        ActivityConnectWithSpotify.this,
                        SpotifyGlobalVariables.AUTH_REQUEST_CODE,
                        authenticationRequest);
            }
        });

    }

    private void getAllTokens(String code) {
        spotifyRepository.getAllTokens(code, new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                Token token = (Token) value;
                String refreshToken = token.getRefresh_token();
                String accessToken = token.getAccess_token();
                // Save refreshToken in Shared Preferences
                sharedPreferences.edit().putString(SpotifyGlobalVariables.REFRESH_TOKEN, refreshToken).apply();
                spotifyRepository.getCurrentUser(new ValueCallback() {
                    @Override
                    public void onSuccess(Object value) {
                        User user = (User) value;
                        if (user.isPremium()) {
                            Intent tokenIntent = new Intent();
                            tokenIntent.putExtra(GlobalVariables.INTENT_SPOTIFY_USER, user);
                            tokenIntent.putExtra(GlobalVariables.INTENT_SPOTIFY_ACCESS_TOKEN, accessToken);
                            setResult(GlobalVariables.RESULT_OK, tokenIntent);
                            finish();
                        } else {
                            setResult(GlobalVariables.SPOTIFY_RESULT_NOT_PREMIUM, null);
                            finish();
                        }

                    }

                    @Override
                    public void onFailure(Object value) {
                        setResult(GlobalVariables.RESULT_FAILURE, null);
                        finish();
                    }
                });

            }

            @Override
            public void onFailure(Object value) {
                setResult(GlobalVariables.RESULT_FAILURE, null);
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case SpotifyGlobalVariables.AUTH_REQUEST_CODE:
                AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, data);
                String code = response.getCode();

                if (code == null) {
                    AuthenticationRequest.Builder builder =
                            new AuthenticationRequest.Builder(
                                    SpotifyGlobalVariables.CLIENT_ID,
                                    AuthenticationResponse.Type.CODE,
                                    SpotifyGlobalVariables.REDIRECT_URI);

                    builder.setScopes(SpotifyGlobalVariables.SCOPE);
                    AuthenticationRequest request = builder.build();

                    AuthenticationClient.openLoginInBrowser(this, request);
                    return;
                } else {
                    getAllTokens(code);
                }
                break;

        }
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Uri uri = intent.getData();
        if (uri == null) {
            setResult(GlobalVariables.RESULT_FAILURE, null);
            finish();
        } else {
            AuthenticationResponse response = AuthenticationResponse.fromUri(uri);
            getAllTokens(response.getCode());
        }

    }
}
