package com.ansgarkersch.playd.repository;

public interface ValueCallback {
    void onSuccess(Object value);
    void onFailure(Object value);
}
