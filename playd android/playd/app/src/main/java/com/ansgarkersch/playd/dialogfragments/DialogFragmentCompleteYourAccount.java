package com.ansgarkersch.playd.dialogfragments;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.globals.GlobalVariables;

public class DialogFragmentCompleteYourAccount extends DialogFragment {

    // UI elements
    private View view;
    private TextView dontShowMeThisAgain, gotIt;

    @SuppressLint("InflateParams")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialogfragment_completeyouraccount, null);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);

        // UI elements
        dontShowMeThisAgain = view.findViewById(R.id.dialogfragment_completeyouraccount_textViewButton_dontShowMeThisAgain);
        gotIt = view.findViewById(R.id.dialogfragment_completeyouraccount_textViewButton_ok);

        dontShowMeThisAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // todo edit sharedPrefs
                dismiss();
            }
        });

        gotIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        return view;
    }

}