package com.ansgarkersch.playd.repository;

import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.ansgarkersch.playd.globals.FirebaseEndpoints;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.User;
import com.ansgarkersch.playd.model.UserOverview;
import com.ansgarkersch.playd.util.UtilityMethods;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * GOOGLE FIREBASE METHODS
 */
public class UserRepository extends Observable {

    // Firebase REST Service
    private FirebaseRestService firebaseRestService;

    // Firebase Services
    private FirebaseFirestore firestoreDatabase;
    DatabaseReference firebaseRealtimeDatabase;
    private FirebaseStorage firebaseStorage;
    private FirebaseMessaging firebaseMessaging;

    // Repositories
    private FirebaseRepository firebaseRepository;

    // Util
    private String TAG = "Class: UserRepository - ";

    public UserRepository() {
        // Firebase Services
        firestoreDatabase = FirebaseFirestore.getInstance();
        firebaseStorage = FirebaseStorage.getInstance();
        firebaseMessaging = FirebaseMessaging.getInstance();
        firebaseRealtimeDatabase = FirebaseDatabase.getInstance().getReference();

        // Repositories
        firebaseRepository = new FirebaseRepository();

        // Firebase REST Service
        // OkHttpClient
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .build();

        // Retrofit
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(FirebaseEndpoints.GLOBAL_ENDPOINT)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        firebaseRestService = retrofit.create(FirebaseRestService.class);

    }

    /**
     * todo erklären
     * @param userId
     * @param callback
     */
    public void getUserById(String userId, final ValueCallback callback) {
        List<Boolean> promises = Collections.synchronizedList(new ArrayList<>());
        User user = new User(userId);
        synchronized (user) {
            firestoreDatabase
                    .collection(GlobalVariables.DB_USERS)
                    .document(userId)
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            Gson gson = new Gson();
                            String json = gson.toJson(documentSnapshot.getData());
                            User jsonUser = gson.fromJson(json, User.class);
                            user.setUsername(jsonUser.getUsername());
                            user.setSpotifyUserId(jsonUser.getSpotifyUserId());
                            user.setName(jsonUser.getName());
                            user.setUserSubtitle(jsonUser.getUserSubtitle());
                            user.setBirthday(jsonUser.getBirthday());
                            user.setRelationshipStatus(jsonUser.getRelationshipStatus());
                            user.setGender(jsonUser.getGender());
                            user.setNationality(jsonUser.getNationality());
                            user.setDescription(jsonUser.getDescription());
                            user.setSpotifyPlaydlistId(jsonUser.getSpotifyPlaydlistId());
                            user.setPremiumLevel(jsonUser.getPremiumLevel());
                            user.setLastLoggedInTimestamp(jsonUser.getLastLoggedInTimestamp());
                            user.setPreferredMusicGenres(jsonUser.getPreferredMusicGenres());
                            user.setProfileImages(jsonUser.getProfileImages());
                            promises.add(true);
                            if (promises.size() == 3) {
                                callback.onSuccess(user);
                            }
                        }
                    });
            firestoreDatabase
                    .collection(GlobalVariables.DB_FOLLOWED)
                    .document(userId)
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if (documentSnapshot == null || documentSnapshot.getData() == null) {
                                promises.add(true);
                                return;
                            }
                            user.setFollowedIds(convertMap(documentSnapshot.getData()));
                            promises.add(true);
                            if (promises.size() == 3) {
                                callback.onSuccess(user);
                            }
                        }
                    });
            firestoreDatabase
                    .collection(GlobalVariables.DB_FOLLOWERS)
                    .document(userId)
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if (documentSnapshot == null || documentSnapshot.getData() == null) {
                                promises.add(true);
                                return;
                            }
                            user.setFollowersIds(convertMap(documentSnapshot.getData()));
                            promises.add(true);
                            if (promises.size() == 3) {
                                callback.onSuccess(user);
                            }
                        }
                    });
        }

    }


    /**
     * Fetches a single userOverView from the database
     * It does not matter if the user is currently a spot
     * @param userId
     * @param callback
     */
    public void getUserOverViewById(String userId, final ValueCallback callback) {

        firebaseRealtimeDatabase
                .child(GlobalVariables.DB_USEROVERVIEW)
                .child(userId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {
                    firebaseStorage.getReference()
                            .child(userId)
                            .child(GlobalVariables.DB_STORAGE_THUMB_ID)
                            .getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            UserOverview userOverview = dataSnapshot.getValue(UserOverview.class);
                            userOverview.setId(dataSnapshot.getKey());
                            userOverview.setProfilePicUrl(uri.toString());
                            callback.onSuccess(userOverview);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            Log.d(getClass().getName(), exception.getMessage());
                            UserOverview userOverview = dataSnapshot.getValue(UserOverview.class);
                            userOverview.setId(dataSnapshot.getKey());
                            userOverview.setProfilePicUrl(GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE);
                            callback.onSuccess(userOverview);
                        }
                    });
                } else {
                    callback.onSuccess(null);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                callback.onFailure(databaseError.getMessage());
            }
        });
    }

    /**
     * Fetches a userOverView from the database but only if this user is also currently a spot
     * @param userId
     * @param callback
     */
    public void getCurrentlyPlayingUserOverViewById(String userId, final ValueCallback callback) {
        firebaseRealtimeDatabase
                .child(GlobalVariables.DB_USEROVERVIEW)
                .orderByChild(GlobalVariables.DB_USEROVERVIEW_ISPLAYING)
                .equalTo(true, userId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {
                            firebaseStorage.getReference()
                                    .child(userId)
                                    .child(GlobalVariables.DB_STORAGE_THUMB_ID)
                                    .getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    UserOverview userOverview = dataSnapshot.getValue(UserOverview.class);
                                    userOverview.setId(dataSnapshot.getKey());
                                    userOverview.setProfilePicUrl(uri.toString());
                                    callback.onSuccess(userOverview);
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    Log.d(getClass().getName(), exception.getMessage());
                                    UserOverview userOverview = dataSnapshot.getValue(UserOverview.class);
                                    userOverview.setId(dataSnapshot.getKey());
                                    userOverview.setProfilePicUrl(GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE);
                                    callback.onSuccess(userOverview);
                                }
                            });
                        } else {
                            callback.onSuccess(null);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        callback.onFailure(databaseError.getMessage());
                    }
                });
    }



    /**
     * Creates a new User in the FirestoreDatabase
     * @param user the user to be created
     * @param callback RESULT_OK, RESULT_FAILURE or RESULT_CANCELED
     */
    public void createUserInDatabase(final User user, final ValueCallback callback) {

        /*
        Register the user in firestoreDatabase
         */
        firestoreDatabase.collection(GlobalVariables.DB_USERS).
                document(firebaseRepository.getCurrentUser().getUid()).
                set(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        callback.onSuccess(GlobalVariables.RESULT_OK);
                        Log.e(getClass().getName(), " - " + "New User with the Id " + user.getId() + " successfully created.");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        callback.onFailure(GlobalVariables.RESULT_FAILURE);
                        Log.e(getClass().getName(), " - " + e.getMessage());
                    }
                })
                .addOnCanceledListener(new OnCanceledListener() {
                    @Override
                    public void onCanceled() {
                        callback.onFailure(GlobalVariables.RESULT_CANCELED);
                        Log.e(getClass().getName(), " - Canceled");
                    }
                });

        /*
        Create an usersOverview entry
         */
        createUserOverview(user.getUsername());
    }


    /**
     * todo erklären
     * @param username
     */
    private void createUserOverview(String username) {
        Map<String, Object> data = new HashMap<>();
        data.put(GlobalVariables.DB_USEROVERVIEW_USERNAME, username);
        firebaseRealtimeDatabase
                .child(GlobalVariables.DB_USEROVERVIEW)
                .child(firebaseRepository.getCurrentUser().getUid())
                .setValue(data);
    }


    /**
     * todo erklären
     * @param callback
     */
    public void updateUsersLastLoggedInTimestamp(final ValueCallback callback) {
        HashMap<String, Object> data = new HashMap<>();
        data.put(GlobalVariables.DB_LASTLOGGEDINTIMESTAMP, UtilityMethods.getCurrentTime());
        updateUser(data, new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                callback.onSuccess(value);
            }

            @Override
            public void onFailure(Object value) {
                callback.onFailure(value);
            }
        });
    }

    /**
     * uploads an image
     * @param image
     * @param imagePath
     * @param callback
     */
    public void uploadImage(byte[] image, String imagePath, final ValueCallback callback) {
        StorageReference storageRef = firebaseStorage.getReference().child(firebaseRepository.getCurrentUser().getUid() + "/" + imagePath + ".png");
        UploadTask uploadTask = storageRef.putBytes(image);
        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    callback.onFailure(task.getException());
                }
                // Continue with the task to get the download URL
                return storageRef.getDownloadUrl();
            }
        }).addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                callback.onSuccess(uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onFailure(e.getMessage());
            }
        }).addOnCanceledListener(new OnCanceledListener() {
            @Override
            public void onCanceled() {
                callback.onFailure(GlobalVariables.RESULT_CANCELED);
            }
        });
    }


    public void deleteImage(String imagePath, final ValueCallback callback) {
        StorageReference storageRef = firebaseStorage.getReference();
        StorageReference desertRef = storageRef.child(firebaseRepository.getCurrentUser().getUid() + "/" + imagePath + ".png");
        desertRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                callback.onSuccess("");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                callback.onFailure(exception.getMessage());
            }
        });
    }


    /**
     * Search User by given substring user name
     * @param substring
     * @param limit
     * @param callback
     */
    public void searchUsersByGivenSubstringUserName(String substring, int limit, final ValueCallback callback) {

        if (substring.isEmpty()) {
            callback.onSuccess(new ArrayList<>());
            return;
        }

        firebaseRealtimeDatabase
                .child(GlobalVariables.DB_USEROVERVIEW)
                .orderByChild(GlobalVariables.DB_USEROVERVIEW_USERNAME)
                .limitToFirst(limit)
                .startAt(substring)
                .endAt(substring + "\uf8ff")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists() && dataSnapshot.getKey() != null && dataSnapshot.getValue() != null) {
                            List<UserOverview> userOverviews = Collections.synchronizedList(new ArrayList<>());
                            for (DataSnapshot children : dataSnapshot.getChildren()) {
                                if (children == null || children.getKey().equals(firebaseRepository.getCurrentUser().getUid())) {
                                    userOverviews.add(null);
                                } else {
                                    getUserOverViewById(children.getKey(), new ValueCallback() {
                                        @Override
                                        public void onSuccess(Object value) {
                                            UserOverview user = (UserOverview) value;
                                            userOverviews.add(user);
                                            if (userOverviews.size() == dataSnapshot.getChildrenCount()) {
                                                callback.onSuccess(userOverviews);
                                            }
                                        }

                                        @Override
                                        public void onFailure(Object value) {
                                            userOverviews.add(null);
                                            if (userOverviews.size() == dataSnapshot.getChildrenCount()) {
                                                callback.onSuccess(userOverviews);
                                            }
                                        }
                                    });
                                }
                            }

                        } else {
                            callback.onSuccess(new ArrayList<>());
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        callback.onFailure(databaseError.getMessage());
                    }
                });
    }


    public void updateUser(HashMap<String, Object> data, final ValueCallback callback) {
        DocumentReference docRef = firestoreDatabase.collection(GlobalVariables.DB_USERS).document(firebaseRepository.getCurrentUser().getUid());
        docRef.update(data).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                callback.onSuccess("");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onFailure(e.getMessage());
            }
        }).addOnCanceledListener(new OnCanceledListener() {
            @Override
            public void onCanceled() {
                callback.onFailure(GlobalVariables.RESULT_CANCELED);
            }
        });
    }

    private HashMap<String, Long> convertMap(Map<String, Object> data) {
        HashMap<String, Long> result = new HashMap<>();
        for (Map.Entry<String, Object> entry : data.entrySet()) {
            try {
                result.put(entry.getKey(), (Long) entry.getValue());
            } catch (ClassCastException ex) {
                Log.d(getClass().getName(),
                        "Method: convertMap - Class Cast Exception catched! Bad UserId + "
                                + entry.getKey() + ", " + entry.getValue());
            }
        }
        return result;
    }








    /*
     * ************************************************ DEPRECATED *******************************************************
     */


    /**
     * Register a new User
     * Returns an Integer by Callback with STATUS_OK or STATUS_FAILURE
     */
    @Deprecated
    public void registerUser(final User user, final Bitmap profileImage, final ValueCallback callback) {

        final String imageId = String.valueOf(UtilityMethods.getCurrentTime() / 1000);
        HashMap<String, String> profileImages = new HashMap<>(); // to be filled with profileImage and thumbnail

        uploadImage(UtilityMethods.bitmapToByteArray(profileImage), imageId, new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                String downloadUrl = value.toString();
                profileImages.put(imageId, downloadUrl);
                user.setProfileImages(profileImages);

                final int THUMBSIZE = 128;
                Bitmap thumbnail = ThumbnailUtils.extractThumbnail(profileImage, THUMBSIZE, THUMBSIZE);

                uploadImage(UtilityMethods.bitmapToByteArray(thumbnail), imageId + "_thumb", new ValueCallback() {
                    @Override
                    public void onSuccess(Object value) {
                        String thumbnailDownloadUrl = value.toString();
                        profileImages.put(GlobalVariables.DB_THUMBNAIL, thumbnailDownloadUrl);
                        user.setProfileImages(profileImages);

                        HashMap<String, Object> userData = new HashMap<>();
                        userData.put(GlobalVariables.DB_NAME, user.getName());
                        userData.put(GlobalVariables.DB_BIRTHDAY, user.getBirthday());
                        userData.put(GlobalVariables.DB_GENDER, user.getGender().toString());
                        userData.put(GlobalVariables.DB_IS_FAMOUS, false);
                        userData.put(GlobalVariables.DB_DESCRIPTION, "");
                        userData.put(GlobalVariables.DB_NATIONALITY, user.getNationality().toString());
                        userData.put(GlobalVariables.DB_RELATIONSHIP_STATUS, user.getRelationshipStatus().toString());
                        userData.put(GlobalVariables.DB_SPOTIFY_PLAYDLIST_ID, user.getSpotifyPlaydlistId());
                        userData.put(GlobalVariables.DB_PROFILE_IMAGES, user.getProfileImages());
                        userData.put(GlobalVariables.DB_PREFERRED_MUSIC_GENRES, null);

                        // register the user in firestoreDatabase
                        firestoreDatabase.collection(GlobalVariables.DB_USERS).
                                document(firebaseRepository.getCurrentUser().getUid()).
                                set(userData).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                callback.onSuccess("");
                                Log.e(getClass().getName(), " - " + "New User with the Id " + user.getId() + " successfully created.");
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                callback.onFailure(e.getMessage());
                                Log.e(getClass().getName(), " - " + e.getMessage());
                            }
                        });

                    }

                    @Override
                    public void onFailure(Object value) {
                        callback.onFailure(value.toString());
                    }
                });

            }

            @Override
            public void onFailure(Object value) {
                callback.onFailure(value.toString());
            }
        });
    }

}
