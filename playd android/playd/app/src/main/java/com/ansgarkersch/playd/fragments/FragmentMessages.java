package com.ansgarkersch.playd.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ansgarkersch.playd.R;

public class FragmentMessages extends Fragment {

    private View view;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_messages,
                container, false);
        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // setUser UI elements
        //initImageGallery();
    }


    /*
    private void initImageGallery() {
        final List<User> userList = new ArrayList<>();



        User testUser1 = new User("", "");
        testUser1.setName("Ansgar");
        testUser1.setProfileImages(new String[]{"R.mipmap.sample_imagegallery_3"});
        testUser1.setActive(true);
        testUser1.setSpotName("Imagine Life without Music");
        testUser1.setDistance(2);
        userList.add(testUser1);

        User testUser2 = new User("", "");
        testUser2.setName("Kathi");
        testUser2.setProfileImages(new String[]{"R.mipmap.sample_imagegallery_5"});
        testUser2.setActive(true);
        testUser2.setSpotName("Fake it until you make it");
        testUser2.setDistance(7);
        userList.add(testUser2);

        User testUser3 = new User("", "");
        testUser3.setName("Manuel");
        testUser3.setProfileImages(new String[]{"R.mipmap.sample_imagegallery_1"});
        testUser3.setActive(true);
        testUser3.setSpotName("I love playd");
        testUser3.setDistance(12);
        userList.add(testUser3);

        User testUser4 = new User("", "");
        testUser4.setName("Regina");
        testUser4.setProfileImages(new String[]{"R.mipmap.sample_imagegallery_2"});
        testUser4.setActive(true);
        testUser4.setSpotName("Wiener Blut");
        testUser4.setDistance(14);
        userList.add(testUser4);

        User testUser5 = new User("", "");
        testUser5.setName("Hannah");
        testUser5.setProfileImages(new String[]{"R.mipmap.sample_imagegallery_3"});
        testUser5.setActive(true);
        testUser5.setSpotName("Back to the 80ts");
        testUser5.setDistance(56);
        userList.add(testUser5);

        // set up the RecyclerView

        final RecyclerView imageGalleryView = view.findViewById(R.id.messages_friendlist_imageGallery);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(view.getContext(),
                LinearLayoutManager.HORIZONTAL, false);
        imageGalleryView.setLayoutManager(horizontalLayoutManager);

        AdapterRecyclerFollowedGallery adapter = new AdapterRecyclerFollowedGallery(view.getContext(), userList);

        adapter.setClickListener(new AdapterRecyclerFollowedGallery.ItemClickListener() {
            @Override
            public void onClickPlayback(View view, int position) {
                // TODO
            }
        });

        imageGalleryView.setAdapter(adapter);

    }
    */
}
