package com.ansgarkersch.playd.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.User;
import com.ansgarkersch.playd.repository.AuthCallback;
import com.ansgarkersch.playd.repository.AuthRepository;
import com.ansgarkersch.playd.repository.UserRepository;
import com.ansgarkersch.playd.repository.ValueCallback;
import com.ansgarkersch.playd.util.UtilityMethods;

public class ActivityRegisterCredentials extends AppCompatActivity {

    // Repositories
    AuthRepository authRepository;
    UserRepository userRepository;

    // UI elements
    private EditText editText_email;
    private EditText editText_passwordFirst;
    private EditText editText_passwordSecond;
    private Button button_register;
    private ImageView errorIndicatorEmail, errorIndicatorPassword;

    // User-specific values
    private String username;
    private String spotifyUserId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_credentials);

        // Get recieved Spotify userId and username from Bundle
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            finish();
            Toast.makeText(this, R.string.toast_unexpected_error_occured, Toast.LENGTH_LONG).show();
            return;
        } else {
            spotifyUserId = getIntent().getStringExtra(GlobalVariables.INTENT_SPOTIFY_USER_ID);
            username = getIntent().getStringExtra(GlobalVariables.INTENT_USERNAME);
        }

        // Repositories
        authRepository = new AuthRepository();
        userRepository = new UserRepository();

        // UI elements
        editText_email = findViewById(R.id.activity_register_credentials_editText_email);
        editText_passwordFirst = findViewById(R.id.activity_register_credentials_editText_passwordFirst);
        editText_passwordSecond = findViewById(R.id.activity_register_credentials_editText_passwordSecond);
        button_register = findViewById(R.id.activity_register_credentials_button_register);
        errorIndicatorEmail = findViewById(R.id.activity_register_credentials_image_errorIndicatorEmail);
        errorIndicatorPassword = findViewById(R.id.activity_register_credentials_image_errorIndicatorPassword);

        initRegisterButton();

    }

    private void initRegisterButton() {
        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button_register.setOnClickListener(null);
                String email = editText_email.getText().toString().trim();
                String passwordFirst = editText_passwordFirst.getText().toString().trim();
                String passwordSecond = editText_passwordSecond.getText().toString().trim();

                if (email.isEmpty()) {
                    errorIndicatorEmail.setVisibility(View.VISIBLE);
                    editText_email.setError(getResources().getString(R.string.error_enterEmail));
                    initRegisterButton();
                    return;
                }

                if (passwordFirst.isEmpty() || passwordSecond.isEmpty()) {
                    errorIndicatorPassword.setVisibility(View.VISIBLE);
                    editText_passwordFirst.setError(getResources().getString(R.string.error_enterPassword));
                    initRegisterButton();
                    return;
                }

                if (!passwordFirst.equals(passwordSecond)) {
                    errorIndicatorPassword.setVisibility(View.VISIBLE);
                    editText_passwordFirst.setError(getResources().getString(R.string.error_enterSamePasswordTwice));
                    initRegisterButton();
                    return;
                }

                authRepository.registerUserWithEmail(email, passwordFirst, new ValueCallback() {
                    @Override
                    public void onSuccess(Object value) {
                        String userId = value.toString();
                        // Check if Firebase userId is valid
                        if (userId == null || userId.isEmpty()) {
                            Toast.makeText(ActivityRegisterCredentials.this,
                                    R.string.toast_unexpected_error_occured,
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }
                        // Create user object
                        User user = new User(userId);
                        user.setUsername(username);
                        user.setSpotifyUserId(spotifyUserId);
                        // Create Database Entry for user
                        userRepository.createUserInDatabase(user, new ValueCallback() {
                            @Override
                            public void onSuccess(Object value) {
                                // The Registration was successful. Open ActivityMain
                                Intent intentRegister = new Intent(ActivityRegisterCredentials.this, ActivityMain.class);
                                startActivity(intentRegister);
                                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                                finish();
                            }
                            @Override
                            public void onFailure(Object value) {
                                Toast.makeText(ActivityRegisterCredentials.this,
                                        R.string.toast_unexpected_error_occured,
                                        Toast.LENGTH_SHORT).show();
                                initRegisterButton();
                            }
                        });
                    }

                    @Override
                    public void onFailure(Object value) {
                        int resultCode = (int) value;
                        switch (resultCode) {
                            case GlobalVariables.RESULT_CREDENTIALS_FAILED_WEAK_PASSWORD:
                                editText_passwordFirst.setError(getResources().getString(R.string.error_unsecurePassword));
                                editText_passwordFirst.requestFocus();
                                break;
                            case GlobalVariables.RESULT_CREDENTIALS_FAILED_USER_COLLISION:
                                editText_email.setError(getResources().getString(R.string.error_userAlreadyExists));
                                editText_email.requestFocus();
                                break;
                            case GlobalVariables.RESULT_CREDENTIALS_FAILED_INVALID_CREDENTIALS:
                                editText_email.setError(getResources().getString(R.string.error_enterValidEmail));
                                editText_email.requestFocus();
                                break;
                            default:
                                Toast.makeText(ActivityRegisterCredentials.this,
                                        R.string.toast_unexpected_error_occured,
                                        Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

}
