package com.ansgarkersch.playd.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.Gender;
import com.ansgarkersch.playd.model.Nationality;
import com.ansgarkersch.playd.model.RelationshipStatus;
import com.ansgarkersch.playd.model.User;
import com.ansgarkersch.playd.repository.UserRepository;
import com.ansgarkersch.playd.repository.ValueCallback;
import com.bumptech.glide.Glide;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class ActivityAccountEditProfile extends AppCompatActivity {

    private User userToBeEdited;
    private EditText nameEditText, descriptionEditText;
    private Spinner relationshipStatus, gender, nationality;
    private CircleImageView profilePic;

    private UserRepository userRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_editprofile);
        userRepository = new UserRepository();

        if( getIntent() != null )
        {
            userToBeEdited = (User) getIntent().getSerializableExtra(GlobalVariables.INTENT_PROFILE);
            initUIElements();
        } else {
            Toast.makeText(this, getResources().getString(R.string.toast_unexpected_error_occured), Toast.LENGTH_SHORT).show();
            finish();
        }

    }

    private void initImages() {
        Glide.with(this).load(GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE.equals(userToBeEdited.getProfilePicThumbnailUrl())
                ?
                R.mipmap.no_profile_image
                :
                userToBeEdited.getProfilePicThumbnailUrl()).into(profilePic);
    }

    private void initUIElements() {
        // elements
        nameEditText = findViewById(R.id.activity_account_editProfile_editText_name);
        descriptionEditText = findViewById(R.id.activity_account_editProfile_editText_description);
        profilePic = findViewById(R.id.activity_account_editProfile_circleImage_profilePic);
        relationshipStatus = findViewById(R.id.activity_account_editProfile_spinner_relationshipStatus);
        gender = findViewById(R.id.activity_account_editProfile_spinner_gender);
        nationality = findViewById(R.id.activity_account_editProfile_spinner_nationality);

        // Profile Image
        initImages();
        // nameEditText
        if (userToBeEdited.getName().isEmpty()) {
            nameEditText.setText(userToBeEdited.getUsername());
        } else {
            nameEditText.setText(userToBeEdited.getName());
        }
        // descriptionEditText
        if (userToBeEdited.getDescription() != null) {
            descriptionEditText.setText(userToBeEdited.getDescription());
        }
        // nationality
        Nationality[] nationalityArray = Nationality.values();
        String[] nationalityStates = new String[nationalityArray.length];
        for (int i = 0; i < nationalityStates.length; i++) {
            nationalityStates[i] = Nationality.getNationalityNameInLanguage(this, nationalityArray[i]);
        }
        nationality.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, nationalityStates));
        nationality.setSelection(Nationality.valueOf(userToBeEdited.getNationality().name()).ordinal());

        // gender
        Gender[] genderArray = Gender.values();
        String[] genderStates = new String[genderArray.length];
        for (int i = 0; i < genderStates.length; i++) {
            genderStates[i] = Gender.getGenderInLanguage(this, genderArray[i]);
        }
        gender.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, genderStates));
        gender.setSelection(Gender.valueOf(userToBeEdited.getGender().name()).ordinal());

        // relationshipStatus
        RelationshipStatus[] relationshipStatusArray = RelationshipStatus.values();
        String[] relationShipStates = new String[relationshipStatusArray.length];
        for (int i = 0; i < relationShipStates.length; i++) {
            relationShipStates[i] = RelationshipStatus.getRelationshipStatusInLanguage(this, relationshipStatusArray[i]);
        }
        relationshipStatus.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, relationShipStates));
        relationshipStatus.setSelection(RelationshipStatus.valueOf(userToBeEdited.getRelationshipStatus().name()).ordinal());

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_account_editProfile_button_editImages:
                Intent editImages = new Intent(ActivityAccountEditProfile.this, ActivityAccountEditProfileEditImages.class);
                editImages.putExtra(GlobalVariables.INTENT_PROFILE_IMAGES, userToBeEdited.getProfileImages());
                startActivityForResult(editImages, GlobalVariables.REQUESTCODE_GET_PROFILE_IMAGES);
                overridePendingTransition(R.anim.fade_in, android.R.anim.fade_out);
                break;
            case R.id.activity_account_editProfile_button_confirm:
                String name = nameEditText.getText().toString();
                String description = descriptionEditText.getText().toString();

                int relationshipSpinnerSelected = relationshipStatus.getSelectedItemPosition();
                int genderSpinnerSelected = gender.getSelectedItemPosition();
                int nationalitySpinnerSelected = nationality.getSelectedItemPosition();

                // check if nameEditText is empty
                if (name.isEmpty()) {
                    nameEditText.setError(getResources().getString(R.string.error_enterName));
                }

                userToBeEdited.setDescription(description);
                userToBeEdited.setRelationshipStatus(RelationshipStatus.
                        getRelationshipStatusBySpinnerPosition(relationshipSpinnerSelected));
                userToBeEdited.setGender(Gender.
                        getGenderBySpinnerPosition(genderSpinnerSelected));
                userToBeEdited.setNationality(Nationality.
                        getNationalityBySpinnerPosition(nationalitySpinnerSelected));

                uploadChanges();
                break;
            case R.id.activity_account_editProfile_button_abort:
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
                break;
        }
    }

    private void uploadChanges() {
        // update firebase database
        HashMap<String, Object> data = new HashMap<>();
        data.put(GlobalVariables.DB_DESCRIPTION, userToBeEdited.getDescription());
        data.put(GlobalVariables.DB_NAME, userToBeEdited.getName());
        data.put(GlobalVariables.DB_GENDER, userToBeEdited.getGender().toString());
        data.put(GlobalVariables.DB_RELATIONSHIP_STATUS, userToBeEdited.getRelationshipStatus().toString());
        data.put(GlobalVariables.DB_NATIONALITY, userToBeEdited.getNationality().toString());
        data.put(GlobalVariables.DB_PROFILE_IMAGES, userToBeEdited.getProfileImages());
        userRepository.updateUser(data, new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                confirmChanges();
            }

            @Override
            public void onFailure(Object value) {
                Toast.makeText(
                        getApplication(),
                        getResources().getString(R.string.toast_unexpected_error_occured),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private void confirmChanges() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(GlobalVariables.INTENT_PROFILE, userToBeEdited);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case GlobalVariables.REQUESTCODE_GET_USER:
                if (resultCode == Activity.RESULT_OK){
                    // update edited user locally
                    userToBeEdited.setProfileImages((HashMap) data.getSerializableExtra(GlobalVariables.INTENT_PROFILE_IMAGES));
                    initImages();
                }
                break;
        }
    }
}
