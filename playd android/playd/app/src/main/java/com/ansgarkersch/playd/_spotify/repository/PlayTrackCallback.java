package com.ansgarkersch.playd._spotify.repository;

public interface PlayTrackCallback {
    void onSuccess();
}
