package com.ansgarkersch.playd.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.repository.UserRepository;
import com.ansgarkersch.playd.repository.ValueCallback;
import com.ansgarkersch.playd.util.SquareImageView;
import com.ansgarkersch.playd.util.UtilityMethods;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

public class ActivityAccountEditProfileEditImages extends AppCompatActivity {

    private UserRepository userRepository;
    private TreeMap<String, String> profileImages;
    private List<Integer> imageIds = Arrays.asList(
            R.id.activity_account_editprofile_editImages_image_1,
            R.id.activity_account_editprofile_editImages_image_2,
            R.id.activity_account_editprofile_editImages_image_3,
            R.id.activity_account_editprofile_editImages_image_4,
            R.id.activity_account_editprofile_editImages_image_5,
            R.id.activity_account_editprofile_editImages_image_6,
            R.id.activity_account_editprofile_editImages_image_7);
    private List<Integer> addButtonsIds = Arrays.asList(
            R.id.activity_account_editprofile_editImages_image_1_button_add,
            R.id.activity_account_editprofile_editImages_image_2_button_add,
            R.id.activity_account_editprofile_editImages_image_3_button_add,
            R.id.activity_account_editprofile_editImages_image_4_button_add,
            R.id.activity_account_editprofile_editImages_image_5_button_add,
            R.id.activity_account_editprofile_editImages_image_6_button_add,
            R.id.activity_account_editprofile_editImages_image_7_button_add);
    private List<Integer> removeButtonsIds = Arrays.asList(
            R.id.activity_account_editprofile_editImages_image_1_button_remove,
            R.id.activity_account_editprofile_editImages_image_2_button_remove,
            R.id.activity_account_editprofile_editImages_image_3_button_remove,
            R.id.activity_account_editprofile_editImages_image_4_button_remove,
            R.id.activity_account_editprofile_editImages_image_5_button_remove,
            R.id.activity_account_editprofile_editImages_image_6_button_remove,
            R.id.activity_account_editprofile_editImages_image_7_button_remove);

    private String cachedFirstImageKey; // to prevent useless thumbnail recreating

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_editprofile_editimages);

        userRepository = new UserRepository();

        if( getIntent() != null )
        {
            profileImages = new TreeMap<>((HashMap<String, String>) getIntent().getSerializableExtra(GlobalVariables.INTENT_PROFILE_IMAGES));
            if (profileImages.size() == 0) {
                return;
            }
            cachedFirstImageKey = Collections.min(profileImages.keySet());
        } else {
            Toast.makeText(this, getResources().getString(R.string.toast_unexpected_error_occured), Toast.LENGTH_SHORT).show();
            finish();
        }

        initImages();

    }

    private void initImages() {
        for (int i = 0; i < imageIds.size(); i++) {
            SquareImageView image = findViewById(imageIds.get(i));
            image.setImageDrawable(null);
            findViewById(removeButtonsIds.get(i)).setVisibility(View.GONE);
            findViewById(addButtonsIds.get(i)).setVisibility(View.VISIBLE);
        }
        List<String> images = new ArrayList<>(profileImages.values());
        // REMOVE THUMBNAIL
        /*
        if (images.size() > 1) {
            images.remove(images.size() - 1);
        }
        */
        for (int i = 0; i < images.size(); i++) {
            findViewById(addButtonsIds.get(i)).setVisibility(View.GONE);
            findViewById(removeButtonsIds.get(i)).setVisibility(View.VISIBLE);
            SquareImageView image = findViewById(imageIds.get(i));
            Glide.with(this).load(images.get(i)).into(image);
        }
    }

    private void confirmChanges(TreeMap<String, String> profileImages) {
        Intent intent = new Intent();
        intent.putExtra(GlobalVariables.INTENT_PROFILE_IMAGES, profileImages);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_account_editprofile_buttons_confirm:
                // generate thumbnail
                if (profileImages.size() == 0) {
                    confirmChanges(profileImages);
                    return;
                }
                String firstImageKey = Collections.min(profileImages.keySet());
                if (firstImageKey.equals(cachedFirstImageKey)) { // creating thumbnail is useless
                    confirmChanges(profileImages);
                    return;
                }
                String firstImageUrl = profileImages.get(firstImageKey);

                Glide.with(this).load(firstImageUrl).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        final int THUMBSIZE = 256;
                        Bitmap thumbnail = ThumbnailUtils.extractThumbnail(((BitmapDrawable)resource).getBitmap(), THUMBSIZE, THUMBSIZE);
                        userRepository.uploadImage(
                                UtilityMethods.bitmapToByteArray(thumbnail),
                                "thumb",
                                new ValueCallback() {
                                    @Override
                                    public void onSuccess(Object value) {
                                        //profileImages.put(GlobalVariables.DB_THUMBNAIL, value.toString());
                                        confirmChanges(profileImages);
                                    }

                                    @Override
                                    public void onFailure(Object value) {

                                    }
                                });

                        return true;
                    }
                });
                break;
            case R.id.activity_account_editprofile_buttons_abort:
                Intent intent = new Intent();
                setResult(Activity.RESULT_CANCELED, intent);
                finish();
                break;
        }
    }

    public void onClickAdd(View view) {
        Intent cropImageIntent = new Intent(view.getContext(), ActivityCropImage.class);
        startActivityForResult(cropImageIntent, GlobalVariables.REQUESTCODE_PICKIMAGE);
    }

    public void onClickRemove(View view) {
        int position = removeButtonsIds.indexOf(view.getId());
        String keyToRemove = new ArrayList<>(profileImages.keySet()).get(position);
        userRepository.deleteImage(keyToRemove, new ValueCallback() {
            @Override
            public void onSuccess(Object value) {

            }

            @Override
            public void onFailure(Object value) {

            }
        });

        if (position == 0) { // delete thumbnail too
            profileImages.remove(GlobalVariables.DB_THUMBNAIL); // delete locally
            userRepository.deleteImage(keyToRemove + "_thumb", new ValueCallback() { // delete in firebase storage
                @Override
                public void onSuccess(Object value) {

                }

                @Override
                public void onFailure(Object value) {

                }
            });
        }
        profileImages.remove(keyToRemove);
        initImages();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == GlobalVariables.REQUESTCODE_PICKIMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                String imageId = String.valueOf(UtilityMethods.getCurrentTime() / 1000);
                Uri uri = Uri.parse(data.getStringExtra(GlobalVariables.URI_IMAGE));
                try {
                    userRepository.uploadImage(UtilityMethods.uriToByteArray(this, uri), imageId, new ValueCallback() {
                        @Override
                        public void onSuccess(Object value) {
                            profileImages.put(imageId, value.toString());
                            initImages();
                        }

                        @Override
                        public void onFailure(Object value) {

                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                // Who care's
            }
        }

        // TODO delete image
    }
}