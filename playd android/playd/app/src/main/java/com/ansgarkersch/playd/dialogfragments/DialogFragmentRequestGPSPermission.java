package com.ansgarkersch.playd.dialogfragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.ansgarkersch.playd.R;

public class DialogFragmentRequestGPSPermission extends DialogFragment {

    // UI elements
    private View view;
    private TextView dontWantToGiveAccessToGPS, confirm;

    // Callback
    private DialogFragmentCallback dialogFragmentCallback;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialogfragment_requestgpspermission, null);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);

        // UI elements
        dontWantToGiveAccessToGPS = view.findViewById(R.id.dialogfragment_requestGPSPermission_textViewButton_iDontWantThis);
        confirm = view.findViewById(R.id.dialogfragment_requestGPSPermission_textViewButton_ok);

        initButtons();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DialogFragmentCallback) {
            dialogFragmentCallback = (DialogFragmentCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement DialogFragmentCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        dialogFragmentCallback = null;
    }

    private void initButtons() {
        dontWantToGiveAccessToGPS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogFragmentCallback.onDialogCallback(false);
                dismiss();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogFragmentCallback.onDialogCallback(true);
                dismiss();
            }
        });
    }

}
