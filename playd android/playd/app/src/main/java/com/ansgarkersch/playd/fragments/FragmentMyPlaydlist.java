package com.ansgarkersch.playd.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd._spotify.model.Playlist;
import com.ansgarkersch.playd._spotify.repository.SpotifyRepository;
import com.ansgarkersch.playd.adapter.AdapterRecyclerFollowedGallery;
import com.ansgarkersch.playd.adapter.AdapterRecyclerPlaydlist;
import com.ansgarkersch.playd.model.Playback;
import com.ansgarkersch.playd.model.User;
import com.ansgarkersch.playd.repository.ValueCallback;
import com.ansgarkersch.playd.viewModel.MyUserViewModel;
import com.ansgarkersch.playd.viewModel.MyUserViewModelFactory;

import java.util.ArrayList;
import java.util.List;

public class FragmentMyPlaydlist extends Fragment {

    private View view;
    private CoordinatorLayout coordinatorLayout;

    // repository
    private SpotifyRepository spotifyRepository;

    // View Models
    private MyUserViewModel myUserViewModel;

    // UI elements
    private RecyclerView currentPlaydlist;
    private ProgressBar currentPlaydlist_loading;

    // adapter
    private AdapterRecyclerPlaydlist playdlistAdapter;
    private AdapterRecyclerFollowedGallery galleryAdapter;

    // current playdlist
    List<Playback> result = new ArrayList<>();

    // recyclerview endless loading variables
    final int limit = 15;
    int currentOffset = 0;
    boolean loading = true;
    int visibleItemCount;
    int totalItemCount;
    int pastVisiblesItems;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_playdlist,
                container, false);
        spotifyRepository = new SpotifyRepository(getActivity());
        coordinatorLayout = view.findViewById(R.id.myplaydlist_coordinatorLayout);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // View model
        myUserViewModel = ViewModelProviders.of(getActivity(), new MyUserViewModelFactory(getActivity())).get(MyUserViewModel.class);

        // View Model Observer
        // observe current logged-in user
        myUserViewModel.getCurrentUser().observe(this, currentUser -> {

            if (currentUser == null) {
                return;
            }
            initFriendsGallery(currentUser);
            fetchPlaylist();

            // TODO wenn sich der logged-in user ändert, aktualisiere die followedGallery

        });

        // observer followed user
        try {
            myUserViewModel.getSubscriptionsWithRelationship().observe(this, subscriptions -> {

                if (subscriptions == null) {
                    return;
                }

                // TODO wenn sich followed ändert, aktualisiere die 'followedGallery'

            });
        } catch (IllegalArgumentException ex) {
            Log.d(getClass().getName(), "Catched IllegalArgumentException: " + ex.getMessage());
        }


        // UI elements
        currentPlaydlist = view.findViewById(R.id.myplaydlist_currentPlaydlist);
        currentPlaydlist_loading = view.findViewById(R.id.myplaydlist_currentPlaydlist_loading);
        //galleryAdapter.setCurrentlyVisibleUser(currentUserViewModel.getUser().getValue());
        initRecyclerView();
        //String accessToken = SpotifyUtilityMethods.getAccessToken(getActivity());

        /*
        String playdlistId = currentUserViewModel.getUser().getValue().getSpotifyPlaydlistId();
        if (playdlistId == null || "".equals(playdlistId)) {
            // show "no items screen"
            currentPlaydlist_loading.setVisibility(View.GONE);
            return;
        }
                executorService.submit(fetchInBackground(accessToken, playdlistId));
        */
    }

    private void fetchPlaylist() {
        String testPlaydlistId = "3W2TbHHUHKDOdS3p71wazI";
        spotifyRepository.getPlaylistById(testPlaydlistId, limit, currentOffset, new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                Playlist playlist = (Playlist) value;
                currentOffset += 10;
                playdlistAdapter.addToPlaylist(playlist.getItems());
                loading = true;
            }

            @Override
            public void onFailure(Object value) {

            }
        });
    }

    private void initFriendsGallery(User currentUser) {

        final RecyclerView imageGalleryView = view.findViewById(R.id.myplaydlist_foreignPlaydlists_gallery);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(view.getContext(),
                LinearLayoutManager.HORIZONTAL, false);
        imageGalleryView.setLayoutManager(horizontalLayoutManager);

        galleryAdapter = new AdapterRecyclerFollowedGallery(view.getContext(), currentUser.getId());
        //galleryAdapter.setUser(currentUserViewModel.getUser().getValue());

        /*
        spotsViewModel.getSpots().observe(this, spot -> {
            if (spot != null) {
                List<Playback> userList = new ArrayList<>(spot.values());
                galleryAdapter.setUserData(userList);
                galleryAdapter.setCurrentlyVisibleUser(currentUserViewModel.getUser().getValue());
            } else {
                galleryAdapter.setUserData(new ArrayList<>());
            }
        });
        */

        galleryAdapter.setClickListener(new AdapterRecyclerFollowedGallery.ItemClickListener() {
            @Override
            public void onItemClick(View view, User user) {
                //String accessToken = SpotifyUtilityMethods.getAccessToken(getActivity());
                //executorService.submit(fetchInBackground(accessToken, user.getSpotifyPlaydlistId()));
                galleryAdapter.setCurrentlyVisibleUser(user);
            }
        });

        imageGalleryView.setAdapter(galleryAdapter);

    }

    private void initRecyclerView() {

        // init AppBarLayout
        AppBarLayout appBarLayout = view.findViewById(R.id.myplaydlist_appbarLayout);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
        AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) params.getBehavior();
        if (behavior != null) {
            behavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
                @Override
                public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
                    return false;
                }
            });
        } else {
            params.setBehavior(new AppBarLayout.Behavior());
            behavior = (AppBarLayout.Behavior) params.getBehavior();
            behavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
                @Override
                public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
                    return false;
                }
            });
        }

        // set up the RecyclerView
        playdlistAdapter = new AdapterRecyclerPlaydlist(view.getContext(), currentPlaydlist);

        LinearLayoutManager llm = new LinearLayoutManager(view.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        currentPlaydlist.setLayoutManager(llm);
        currentPlaydlist.setAdapter(playdlistAdapter);
        currentPlaydlist.setNestedScrollingEnabled(false);

        currentPlaydlist.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy)
            {
                if(dy > 0) // scroll down
                {
                    visibleItemCount = llm.getChildCount();
                    totalItemCount = llm.getItemCount();
                    pastVisiblesItems = llm.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        String s = "";
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount * 0.75) {
                            String s2 = "";
                            loading = false;
                            fetchPlaylist();
                        }

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            fetchPlaylist();
                        }
                    }
                }
            }
        });

        // currentPlabackListener
        playdlistAdapter.setCurrentPlaybackListener(new AdapterRecyclerPlaydlist.ItemClickListener() {
            @Override
            public void onItemClick(View view, Playback playback, boolean isPlaying) {
                if (isPlaying) {
                    //myUserViewModel.setCurrentPlayback(playback);
                } else {
                    //myUserViewModel.setCurrentPlayback(null);
                }
            }
        });


        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new
                ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(
                            final RecyclerView recyclerView,
                            final RecyclerView.ViewHolder viewHolder,
                            final RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                        final int dragFlags = 0;
                        final int swipeFlags = ItemTouchHelper.END;
                        return makeMovementFlags(dragFlags, swipeFlags);
                    }

                    @Override
                    public float getSwipeThreshold(RecyclerView.ViewHolder viewHolder) {
                        return 0.25f;
                    }

                    @Override
                    public float getSwipeEscapeVelocity(float defaultValue) {
                        return 0.6f;
                    }

                    @Override
                    public float getSwipeVelocityThreshold(float defaultValue) {
                        return 0.95f;
                    }

                    @Override
                    public void onChildDraw (Canvas c, RecyclerView recyclerView,
                                             RecyclerView.ViewHolder viewHolder,
                                             float dX, float dY, int actionState,
                                             boolean isCurrentlyActive){
                        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

                        Drawable background = ContextCompat.getDrawable(view.getContext(), R.drawable.gradient_like_spot);

                        try {
                            background.setBounds(0, viewHolder.itemView.getTop(),
                                    (int) (viewHolder.itemView.getLeft() + dX),
                                    viewHolder.itemView.getBottom());
                            background.draw(c);
                        } catch (NullPointerException ex) {
                            Log.d("NPE", ex.getMessage());
                        }

                    }

                    @Override
                    public void onSwiped(
                            final RecyclerView.ViewHolder viewHolder,
                            final int swipeDir) {
                        playdlistAdapter.notifyItemChanged(viewHolder.getAdapterPosition());
                        like(viewHolder.getAdapterPosition());
                    }
                };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(
                simpleItemTouchCallback
        );
        itemTouchHelper.attachToRecyclerView(currentPlaydlist);

    }

    private void like(int position) {
        Snackbar snackbarLike = Snackbar
                .make(coordinatorLayout, view.getResources().getString(R.string.songAddedToPlaydlist), Snackbar.LENGTH_LONG)
                .setAction(view.getResources().getString(R.string.undo), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Snackbar snackbarDismiss = Snackbar.make(coordinatorLayout, view.getResources().getString(R.string.songRemovedFromPlaydlist), Snackbar.LENGTH_SHORT);
                        snackbarDismiss.show();
                    }
                });
        snackbarLike.show();
        snackbarLike.addCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar snackbar, int event) {
                /*
                TODO push the required information to the user's playdlist (maybe observer/eventBus?)
                 */
            }

            @Override
            public void onShown(Snackbar snackbar) {
            }
        });
    }


}
