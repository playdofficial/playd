package com.ansgarkersch.playd.model;

public interface IScrollable {
    int getType();
}
