package com.ansgarkersch.playd._spotify.model;

import java.util.List;

public class Album {

    private String id;
    private String name;
    private List<Image> images; // positions: 0 = 640px, 1 = 300px, 2 = 64px

    public Album() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }
}
