package com.ansgarkersch.playd.services;

public interface ServiceConnectionListenerCallbacks {
    void isInternetAccessible(boolean isAccessible);
    void isGPSAccessible(boolean isAccessible);
}
