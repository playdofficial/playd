package com.ansgarkersch.playd.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.repository.AuthCallback;
import com.ansgarkersch.playd.repository.AuthRepository;

public class ActivityPhoneAuthVerify extends AppCompatActivity {

    private EditText editText;
    private AuthRepository authRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_auth_verify);
        authRepository = new AuthRepository();
        editText = findViewById(R.id.activity_phone_auth_verify_edittext_verificationCode);

        Bundle extras = getIntent().getExtras();
        if (extras == null)
        {
            Toast.makeText(this, getResources().getString(R.string.toast_unexpected_error_occured), Toast.LENGTH_LONG).show();
            startActivity(new Intent(ActivityPhoneAuthVerify.this, ActivityPhoneAuthRequest.class));
            finish();
        } else {
            authRepository.sendVerificationCode(extras.getString(GlobalVariables.TELEPHONE_NUMBER), new AuthCallback() {
                @Override
                public void onCallback(int resultCode) {
                    switch (resultCode) {
                        case GlobalVariables.RESULT_VERIFICATION_COMPLETED:
                            signIn(editText.getText().toString().trim());
                            break;
                        case GlobalVariables.RESULT_VERIFICATION_FAILED_INVALID_REQUEST:
                            Toast.makeText(ActivityPhoneAuthVerify.this, getResources().getString(R.string.toast_unexpected_error_occured), Toast.LENGTH_LONG).show();
                            break;
                        case GlobalVariables.RESULT_VERIFICATION_FAILED_TOO_MANY_REQUESTS:
                            Toast.makeText(ActivityPhoneAuthVerify.this, getResources().getString(R.string.error_maximumSMSSent), Toast.LENGTH_LONG).show();
                            //startActivity(new Intent(ActivityPhoneAuthVerify.this, _ActivityWelcomeScreen.class));
                            finish();
                            break;
                    }
                }
            });
        }

    }

    private void signIn(String code) {
        authRepository.signIn(code, new AuthCallback() {
            @Override
            public void onCallback(int resultCode) {
                if (resultCode == GlobalVariables.RESULT_OK) {
                    startActivity(new Intent(ActivityPhoneAuthVerify.this, ActivityCreateAccount.class));
                    finish();
                } else {
                    findViewById(R.id.activity_phone_auth_verify_progressBar_confirmCode).setVisibility(View.GONE);
                    findViewById(R.id.activity_phone_auth_verify_button_verify).setVisibility(View.VISIBLE);
                    findViewById(R.id.activity_phone_auth_verify_button_sendSMSAgain).setVisibility(View.VISIBLE);
                    editText.setError(getResources().getString(R.string.error_enterValidVerificationCode));
                    editText.requestFocus();
                }
            }
        });
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_phone_auth_verify_button_verify:
                String code = editText.getText().toString().trim();
                if (code.isEmpty() || code.length() < 6) {
                    editText.setError(getResources().getString(R.string.error_enterValidVerificationCode));
                    editText.requestFocus();
                    return;
                } else {
                    findViewById(R.id.activity_phone_auth_verify_button_verify).setVisibility(View.GONE);
                    findViewById(R.id.activity_phone_auth_verify_progressBar_confirmCode).setVisibility(View.VISIBLE);
                    findViewById(R.id.activity_phone_auth_verify_button_sendSMSAgain).setVisibility(View.GONE);
                    signIn(code);
                }
                break;
            case R.id.activity_phone_auth_verify_button_sendSMSAgain:
                closeWindow();
                break;
            case R.id.activity_phone_auth_verify_button_goBack:
                closeWindow();
                break;
        }
    }

    private void closeWindow() {
        startActivity(new Intent(ActivityPhoneAuthVerify.this, ActivityPhoneAuthRequest.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        closeWindow();
    }

}
