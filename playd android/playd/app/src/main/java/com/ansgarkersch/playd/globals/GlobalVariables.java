package com.ansgarkersch.playd.globals;

public class GlobalVariables {

    // TODO zuweisen:
    public static final String LAST_SPOTIFY_ACTIVE_NOTIFICATION_TIMESTAMP = "LAST_SPOTIFY_ACTIVE_NOTIFICATION_TIMESTAMP";



    // ViewTypes
    public static final int VIEWTYPE_LOADING_ICON = -1;
    public static final int VIEWTYPE_SPOT = 0;
    public static final int VIEWTYPE_SPOTEXPLORER = 1;
    public static final int VIEWTYPE_USERSUGGESTIONS = 2;
    public static final int VIEWTYPE_ALLSPOTSSEEN = 3;
    public static final int VIEWTYPE_ADVERTISEMENT = 4;

    // Adapter Subscriptions
    public static final int VIEWTYPE_SHOWALLRESULTS = 5;
    public static final int VIEWTYPE_USEROVERVIEW = 6;
    public static final int VIEWTYPE_HEADERTITLE = 7;


    public static final String INTENT_SPOTIFY_USER = "INTENT_SPOTIFY_USER";
    public static final String INTENT_SPOTIFY_ACCESS_TOKEN = "INTENT_SPOTIFY_ACCESS_TOKEN";
    public static final String INTENT_SPOTIFY_REFRESH_TOKEN = "INTENT_SPOTIFY_REFRESH_TOKEN";

    public static final int HTTP_OK = 200;
    public static final int HTTP_FAILURE = 400;

    public static final String CURRENT_OPENED_USER_ID = "CURRENT_OPENED_USER_ID";

    // GetAllFollowedCurrentSpots
    public static final int UPDATE_FEED_SCROLL = 1;
    public static final int UPDATE_FEED_PULL_TO_REFRESH = 2;

    // limiters
    public static final int MAX_CHARS_USERNAME = 15;

    // packages name
    public static final String PACKAGE_NAME = "com.ansgarkersch.playd";

    // DialogFragmentSingleImage Intent Extra Placeholder
    public static final String DIALOG_GALLERY_IMAGE_INTENT_EXTRA_PLACEHOLDER = "DIALOG_GALLERY_IMAGE_INTENT_EXTRA_PLACEHOLDER";

    // Categories
    public static final String FCM_CATEGORY_PLAYBACK = "FCM_CATEGORY_PLAYBACK";
    public static final String FCM_CATEGORY_USERRELATIONSHIP = "FCM_CATEGORY_USERRELATIONSHIP";
    public static final String CATEGORY_NOTIFICATION = "CATEGORY_NOTIFICATION";

    // FCM ActionCodes Playback
    public static final String FCM_ACTIONCODE_PLAYBACK_STARTED = "FCM_ACTIONCODE_PLAYBACK_STARTED"; // a playback has started
    public static final String FCM_ACTIONCODE_PLAYBACK_TERMINATED = "FCM_ACTIONCODE_PLAYBACK_TERMINATED"; // a playback was terminated

    // ActionCodes Notification
    public static final String ACTIONCODE_NOTIFICATION_FRIENDSHIP = "ACTIONCODE_NOTIFICATION_FRIENDSHIP";
    public static final String ACTIONCODE_NOTIFICATION_NEW_FOLLOWER = "ACTIONCODE_NOTIFICATION_NEW_FOLLOWER";
    public static final String ACTIONCODE_NOTIFICATION_MESSAGE = "ACTIONCODE_NOTIFICATION_MESSAGE";
    public static final String ACTIONCODE_NOTIFICATION_PLAYBACK_SPOT_STARTED_PLAYBACK = "ACTIONCODE_NOTIFICATION_PLAYBACK_SPOT_STARTED_PLAYBACK";
    public static final String ACTIONCODE_NOTIFICATION_PLAYBACK_SPOT_STOPPED_PLAYBACK = "ACTIONCODE_NOTIFICATION_PLAYBACK_SPOT_STOPPED_PLAYBACK";

    // FCM ActionCodes UserRelationship
    public static final String FCM_ACTIONCODE_SUBSCRIBED = "FCM_ACTIONCODE_SUBSCRIBED";
    public static final String FCM_ACTIONCODE_UNSUBSCRIBED = "FCM_ACTIONCODE_UNSUBSCRIBED";
    public static final String FCM_ACTIONCODE_MATCH = "FCM_ACTIONCODE_MATCH";

    // Subscription Relationships
    public static final String RELATIONSHIP_FOLLOWED = "RELATIONSHIP_FOLLOWED";
    public static final String RELATIONSHIP_FOLLOWERS = "RELATIONSHIP_FOLLOWERS";
    public static final String RELATIONSHIP_FRIEND = "RELATIONSHIP_FRIEND";

    // playd intern stuff
    public static final String FOREIGN_USER_ID = "foreignUserId";
    public static final String CATEGORY = "category";
    public static final String ACTION_CODE = "actionCode";

    public static final String INTENT_NOTIFICATION = "INTENT_NOTIFICATION";

    public static final String FIREBASE_USER_TOKEN = "FIREBASE_USER_TOKEN";
    public static final String INTENT_PLAYBACK = "INTENT_PLAYBACK";
    public static final String INTENT_PLAYBACK_ID = "INTENT_PLAYBACK_ID";
    public static final String INTENT_USERNAME = "INTENT_USERNAME";
    public static final String INTENT_SPOT = "INTENT_SPOT";
    public static final String INTENT_PROFILE = "INTENT_PROFILE";
    public static final String INTENT_PROFILE_ID = "INTENT_PROFILE_ID";
    public static final String INTENT_PROFILE_USERNAME = "INTENT_PROFILE_USERNAME";
    public static final String INTENT_PROFILE_CURRENT_USER = "INTENT_PROFILE_CURRENT_USER";
    public static final String INTENT_SUBSCRIPTIONEVENT = "INTENT_SUBSCRIPTIONEVENT";
    public static final String INTENT_SUBSCRIPTION_UIDS = "INTENT_SUBSCRIPTION_UIDS";
    public static final String INTENT_SUBSCRIPTION_TYPE = "INTENT_SUBSCRIPTION_TYPE";
    public static final String INTENT_CACHED_USERS = "INTENT_CACHED_USERS";
    public static final String SUBSCRIPTION_TYPE_FOLLOWED = "SUBSCRIPTION_TYPE_FOLLOWED";
    public static final String SUBSCRIPTION_TYPE_FOLLOWERS = "SUBSCRIPTION_TYPE_FOLLOWERS";
    public static final String INTENT_NEW_FOLLOWERS_SINCE_LAST_LOGIN = "INTENT_NEW_FOLLOWERS_SINCE_LAST_LOGIN";

    public static final String INTENT_ACTION_FLAG = "INTENT_ACTION_FLAG";
    public static final int INTENT_ACTION_FLAG_USERCARD = -1;
    public static final int INTENT_ACTION_FLAG_FOLLOWED = 0;
    public static final int INTENT_ACTION_FLAG_FOLLOWERS = 1;

    public static final String INTENT_SPOTIFY_USER_ID = "INTENT_SPOTIFY_USER_ID";
    public static final int SPOTIFY_RESULT_NOT_INSTALLED = -100;
    public static final int SPOTIFY_RESULT_NOT_PREMIUM = -200;



    public static final String INTENT_SONG_QUEUE = "INTENT_SONG_QUEUE";
    public static final String INTENT_PROFILE_IMAGES = "INTENT_PROFILE_IMAGES";
    public static final String DB_THUMBNAIL = "thumbnail";
    public static final String PLACEHOLDER_NO_PROFILEIMAGE = "PLACEHOLDER_NO_PROFILEIMAGE";
    public static final String LOCAL_PLAYBACK = "LOCAL_PLAYBACK";
    public static final String STOP_SERVICE = "STOP_SERVICE";
    public static final String START_SERVICE = "START_SERVICE";

    // firebase DB titles
    public static final String DB_REGISTRATION_TOKENS = "registrationTokens";

    // header
    public static final String DB_USERS = "users";
    public static final String DB_USEROVERVIEW = "userOverview";
    public static final String DB_PLAYBACKOVERVIEW = "playbackOverview";
    public static final String DB_PLAYBACK = "playback";
    public static final String DB_TRACKID = "trackId";
    public static final String DB_USERID = "userId";

        // playback
    public static final String DB_TRACKSTARTEDTIMESTAMP = "trackStartedTimestamp";
    public static final String DB_SPOTSTARTEDTIMESTAMP = "spotStartedTimestamp";
    public static final String DB_CURRENTSPOTTER = "currentSpotter";
    public static final String DB_CURRENTSPOTTER_COUNT = "currentSpotterCount";
    public static final String DB_CURRENTMS = "currentMS";
    public static final String DB_GENRES = "trackGenres";
    public static final String DB_PRIORITY = "priority";
    public static final String DB_STATUS = "status";
    public static final String DB_DISTANCE = "distance";

    // firebase database DB titles
    public static final String DB_CHATROOMS = "chatrooms";
    public static final String DB_CHATROOMIDS = "chatroomIds";

    // firestore DB titles

        // follower
    public static final String DB_FOLLOWERS = "followers"; // /followers/{followedUid}/{followerUid} - the current users followers

        // followed
    public static final String DB_FOLLOWED = "followed"; // /followed/{followerUid}/{followedUid} - the current users subscriptions

        // users
    public static final String DB_NAME = "name";
    public static final String DB_USERNAME = "username";
    public static final String DB_USEROVERVIEW_USERNAME = "u";
    public static final String DB_USEROVERVIEW_ISPLAYING = "t";
    public static final String DB_USEROVERVIEW_SPOTSTARTEDTIMESTAMP = "s";
    public static final String DB_BIRTHDAY = "birthday";
    public static final String DB_RELATIONSHIP_STATUS = "relationshipStatus";
    public static final String DB_GENDER = "gender";
    public static final String DB_NATIONALITY = "nationality";
    public static final String DB_DESCRIPTION = "description";
    public static final String DB_LASTLOGGEDINTIMESTAMP = "lastLoggedInTimestamp";
    public static final String DB_SPOTIFY_PLAYDLIST_ID = "spotifyPlaydlistId";
    public static final String DB_IS_FAMOUS = "isFamous";
    public static final String DB_PROFILE_PICTURE = "profilePicture";
    public static final String DB_PROFILE_IMAGES = "profileImages";
    public static final String DB_PROFILE_IMAGE = "profileImage";
    public static final String DB_PREFERRED_MUSIC_GENRES = "preferredMusicGenres";

    public static final String DB_STORAGE_THUMB_ID = "thumb.png";

    public static final String DB_PROFILEPIC = "profilePic";
    public static final String DB_PROFILEPIC_THUMB = "profilePic_thumb";

    // firebase DB child titles
    public static final String DB_PHONE = "phone";

    // Firebase AuthRepository RESULTCODES
    public static final int RESULT_TIMEOUT = -1337;
    public static final int RESULT_VERIFICATION_COMPLETED = 1;
    public static final int RESULT_VERIFICATION_FAILED_INVALID_REQUEST = 2;
    public static final int RESULT_VERIFICATION_FAILED_TOO_MANY_REQUESTS = 3;
    public static final int RESULT_CREDENTIALS_FAILED_WEAK_PASSWORD = 4;
    public static final int RESULT_CREDENTIALS_FAILED_INVALID_CREDENTIALS = 5; // wrong password or wrong email
    public static final int RESULT_CREDENTIALS_FAILED_USER_COLLISION = 6;
    public static final int RESULT_CREDENTIALS_FAILED = 7;
    public static final int RESULT_CREDENTIALS_FAILED_AUTH_INVALID_EMAIL = 8;
    public static final int RESULT_CREDENTIALS_FAILED_AUTH_INVALID_USER = 9;

    // general RESULTCODES
    public static final int RESULT_OK = 0;
    public static final int RESULT_FAILURE = -1;
    public static final int RESULT_CANCELED = -2;

    // Spotify Remote App SDK Playd-Codes
    public static final String SPOTIFY_PLAY = "SPOTIFY_PLAY";
    public static final String SPOTIFY_PAUSE = "SPOTIFY_PAUSE";
    public static final String SPOTIFY_ABORT = "SPOTIFY_ABORT";
    public static final String SPOTIFY_SKIPPED = "SPOTIFY_SKIPPED";

    public static final String SPOTIFY_CONNECTED = "SPOTIFY_CONNECTED";

    public static final String SPOTIFY_OBSERVE_CURRENTUSERS_PLAYBACK = "SPOTIFY_OBSERVE_CURRENTUSERS_PLAYBACK";
    public static final String SPOTIFY_REMOVE_CURRENTUSERS_PLAYBACK_OBSERVER = "SPOTIFY_REMOVE_CURRENTUSERS_PLAYBACK_OBSERVER";


    // Error Tags
    public static final String EXCEPTIONTAG_NPE = "NPE";
    public static final String EXCEPTIONTAG_CROP_IMAGE = "EXCEPTIONTAG_CROP_IMAGE";
    public static final String EXCEPTIONTAG_FIREBASE_AUTH = "EX_FIREBASE_AUTH";
    public static final String EXCEPTIONTAG_FIREBASE_CREATE_USER = "EX_FIREBASE_C_USER";
    public static final String ERROR_NO_INTERNET = "ERROR_NO_INTERNET_666";


    // Success or Information Tags
    public static final String TAG_FIREBASE_AUTH = "TAG_FIREBASE_AUTH";
    public static final String TAG_FIREBASE_CREATE_USER = "TAG_FIREBASE_C_USER";
    public static final String TAG_DOWNLOAD_IMAGE = "TAG_DOWNLOAD_IMAGE";

    // ActivityForResult RequestCodes and ResultCodes
    public static final int REQUESTCODE_PICKIMAGE = 0;
    public static final int RESULTCODE_EMPTY = -1;
    public static final int REQUESTCODE_GET_USER = 0;
    public static final int REQUESTCODE_GET_PROFILE_IMAGES = 0;

    // Permission RequestCodes
    public static final int PERMISSION_READ_EXTERNAL_STORAGE = 1;
    public static final int PERMISSION_WRITE_EXTERNAL_STORAGE = 2;





    public static final String TELEPHONE_NUMBER = "TELEPHONE_NUMBER";

    public static final String ALERTBOX_USERROLE = "ALERTBOX_USERROLE";

    public static final String URI_IMAGE = "URI_IMAGE";








}
