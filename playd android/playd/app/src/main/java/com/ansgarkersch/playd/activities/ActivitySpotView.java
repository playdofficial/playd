package com.ansgarkersch.playd.activities;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.dialogfragments.DialogFragmentLike;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.Playback;
import com.ansgarkersch.playd.model.Spot;
import com.ansgarkersch.playd.model.User;
import com.ansgarkersch.playd.model.UserOverview;
import com.ansgarkersch.playd.services.ForegroundServiceSpotListener;
import com.ansgarkersch.playd.services.ForegroundServiceSpotListenerCallbacks;
import com.ansgarkersch.playd.util.UtilityMethods;
import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import de.hdodenhof.circleimageview.CircleImageView;

public class ActivitySpotView extends AppCompatActivity implements ForegroundServiceSpotListenerCallbacks {

    // Firebase Services
    private DatabaseReference firebaseRealtimeDatabase;
    private ValueEventListener currentSpotterCountListener;

    // UI elements
    private CircleImageView profileImage;
    private ImageView albumArt;
    private ImageButton back, like, message;
    private TextView username, spotName, spotterCount, distance, spotStartedTimeStampInMinutes, trackName, artistName;

    // Current logged-in User
    private User currentLoggedInUser;

    // (Current) Spot Data
    private MutableLiveData<Spot> currentSpot;

    // Spot Data
    private UserOverview user;
    private Playback playback;
    private long currentSpotterCount;

    // Services
    ForegroundServiceSpotListener mForegroundServiceSpotListener;
    boolean mBoundSpotListener = false;

    // Util
    private String TAG = getClass().getName().concat(" - ");
    private CountDownTimer spotProgressTimer = null;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spotview);

        // Firebase Services
        firebaseRealtimeDatabase = FirebaseDatabase.getInstance().getReference();

        // Get Spot from Bundle
        if (getIntent() == null) {
            Log.e(TAG, "Bundle = null!");
            finish();
        } else {
            setCurrentSpot((Spot) getIntent().getSerializableExtra(GlobalVariables.INTENT_SPOT));
            currentLoggedInUser = ((User) getIntent().getSerializableExtra(GlobalVariables.INTENT_PROFILE_CURRENT_USER));
        }

        // Initialize all UI elements
        initUIElements();
        initSpotInformation();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Unbind from service
        unbindForegroundServiceSpotListener();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // If ForegroundServiceSpotListener is not running, close the windows immediately
        if (!UtilityMethods.isMyServiceRunning(this, ForegroundServiceSpotListener.class)) {
            finish();
            return;
        }

        // Bind to service
        bindForegroundServiceSpotListener();

        // Reinitialize all Button click listener
        profileImage.setEnabled(true);
        username.setEnabled(true);

        // Reset TrackProgressTimer
        if (spotProgressTimer != null) {
            spotProgressTimer.cancel();
        }

        spotProgressTimer = new CountDownTimer(Integer.MAX_VALUE, 1000) {
            @Override
            public void onTick(long l) {
                //bottombarSpotProgress.setProgress(getCurrentTrackProgress());
            }

            @Override
            public void onFinish() { }
        }.start();

        // Enable Click Listener
        enableAllClickableElements();

    }

    /**
     * Triggered by ForegroundServiceSpotListener
     * @param spot the recieved spot object
     */
    @Override
    public void notifyPlaying(Spot spot) {
        if (spot == null || GlobalVariables.LOCAL_PLAYBACK.equals(spot.getId())) {
            // Spot is null
            // The spot probably stopped its playback
            finish();
            return;
        }
        setCurrentSpot(spot);
    }

    /**
     * Triggered by ForegroundServiceSpotListener
     */
    @Override
    public void notifyPaused() {
        // The spot stopped its playback
        finish();
    }

    /**
     * Init Binding to ForegroundServiceSpotListener
     */
    private ServiceConnection mConnectionSpotListener = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ForegroundServiceSpotListener.LocalBinder binder = (ForegroundServiceSpotListener.LocalBinder) service;
            mForegroundServiceSpotListener = binder.getService();
            mBoundSpotListener = true;

            if (mForegroundServiceSpotListener == null) {
                return;
            }

            mForegroundServiceSpotListener.setCallbacks(ActivitySpotView.this);

            // is called only once, when the service is fresh binded to this activity
            if (mForegroundServiceSpotListener.getCurrentSpot() == null) {
                return;
            }

            // Call notifyPlaying
            notifyPlaying(mForegroundServiceSpotListener.getCurrentSpot());

        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBoundSpotListener = false;
            unbindForegroundServiceSpotListener();
        }
    };


    public void bindForegroundServiceSpotListener() {
        if (!mBoundSpotListener) {
            Intent intentForegroundServiceSpotListener = new Intent(this, ForegroundServiceSpotListener.class);
            bindService(intentForegroundServiceSpotListener, mConnectionSpotListener, Context.BIND_AUTO_CREATE);
            Log.e(TAG, "ForegroundServiceSpotListener is binded to ActivityMain");
        }
    }

    public void unbindForegroundServiceSpotListener() {
        if (mBoundSpotListener) {
            unbindService(mConnectionSpotListener);
            mBoundSpotListener = false;
            if (mForegroundServiceSpotListener != null) {
                mForegroundServiceSpotListener.setCallbacks(null);
            }
            Log.e(TAG, "ForegroundServiceSpotListener is unbinded by ActivityMain");
        }
    }

    /**
     * Initialize all UI elements
     */
    private void initUIElements() {
        albumArt = findViewById(R.id.activity_spotview_imageview_albumArt);
        spotName = findViewById(R.id.activity_spotview_textview_spotName);
        spotterCount = findViewById(R.id.activity_spotview_textview_spotterCount);
        distance = findViewById(R.id.activity_spotview_textview_distance);
        spotStartedTimeStampInMinutes = findViewById(R.id.activity_spotview_textview_spotStartedTimestamp);
        trackName = findViewById(R.id.activity_spotview_textview_trackName);
        artistName = findViewById(R.id.activity_spotview_textview_artistName);

        profileImage = findViewById(R.id.activity_spotview_circleimageview_profileImage);
        username = findViewById(R.id.activity_spotview_textview_username);
        back = findViewById(R.id.activity_spotview_imagebutton_back);
        like = findViewById(R.id.activity_spotview_imagebutton_like);
        message = findViewById(R.id.activity_spotview_imagebutton_message);

        // Init Buttons
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Disable Click listener to prevent double clicks
                disableAllClickableElements();
                onClickAccount();
            }
        });
        username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Disable Click listener to prevent double clicks
                disableAllClickableElements();
                onClickAccount();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                disableAllClickableElements();
                finish();
            }
        });

        // Check if this song is already in the users playdlist
        // if yes -> color the heartIcon
        // if no -> set OnClickListener

        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                disableAllClickableElements();
                DialogFragmentLike dialogFragmentLike = new DialogFragmentLike();
                dialogFragmentLike.show(getSupportFragmentManager(), dialogFragmentLike.getClass().getName());

                // TODO Put this song in the playdlist or open dialog in which the user ca decive whoch playlist he wants to edit

            }
        });
        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO open Chat Activity
            }
        });

    }

    /**
     * Writes the information from the recieved spot object in the UI
     */
    private void initSpotInformation() {

        currentSpot.observe(ActivitySpotView.this, new Observer<Spot>() {
            @Override
            public void onChanged(@Nullable Spot spot) {
                if (spot == null) {
                    return;
                }

                // Init Objects
                user = spot.getUser();
                playback = spot.getPlayback();

                // User information
                Glide.with(ActivitySpotView.this).load(
                        GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE.equals(user.getProfilePicUrl())
                                ?
                                R.mipmap.no_profile_image
                                :
                                user.getProfilePicUrl()).into(profileImage);
                username.setText(user.getU());

                // Playback information
                Glide.with(ActivitySpotView.this).load(
                        GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE.equals(playback.getAlbumArt_640())
                                ?
                                R.mipmap.no_profile_image
                                :
                                playback.getAlbumArt_640()).into(albumArt);

                spotName.setText(playback.getSpotName());

                // TODO distance?
                //UtilityMethods.calculateDistance()

                // Calculate spotStartedTimeStampInMinutes
                spotStartedTimeStampInMinutes
                        .setText(UtilityMethods
                                .getCalculatedSpotStartedTimestampInMinutesAsText(
                                        getBaseContext(),
                                        UtilityMethods.calculateSpotStartedTimestampInMinutes
                                                (playback.getSpotStartedTimestamp())));

                trackName.setText(playback.getTrackName());
                artistName.setText(playback.getTrackArtistName());

                // Initialize SpotterCount Listener
                // 1) Remove Value Event Listener if it's already active
                if (currentSpotterCountListener != null) {
                    firebaseRealtimeDatabase.child(GlobalVariables.DB_CURRENTSPOTTER_COUNT)
                            .removeEventListener(currentSpotterCountListener);
                }
                // 1) Create new Value Event Listener
                currentSpotterCountListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() == null) {
                            currentSpotterCount = 0L;
                        } else {
                            currentSpotterCount = (long) dataSnapshot.getValue();
                        }
                        // Update UI spotterCount
                        spotterCount.setText(String.valueOf(currentSpotterCount));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e(TAG, databaseError.getMessage());
                        currentSpotterCount = 0L;
                        // Update UI spotterCount
                        spotterCount.setText(String.valueOf(currentSpotterCount));
                    }
                };
                firebaseRealtimeDatabase
                        .child(GlobalVariables.DB_CURRENTSPOTTER_COUNT)
                        .child(spot.getId())
                        .addValueEventListener(currentSpotterCountListener);

            }
        });

    }

    /**
     * When the user clicks on the account links (profile image & username)
     */
    private void onClickAccount() {
        Intent intent = new Intent(ActivitySpotView.this, ActivityWatchProfile.class);
        intent.putExtra(GlobalVariables.INTENT_PROFILE_ID, user.getId());
        intent.putExtra(GlobalVariables.INTENT_PROFILE_USERNAME, user.getU());
        intent.putExtra(GlobalVariables.INTENT_PROFILE_CURRENT_USER, currentLoggedInUser);
        startActivity(intent);
        overridePendingTransition(R.anim.activity_slide_in_up, android.R.anim.fade_out);
    }

    public int getCurrentTrackProgress() {
        return mForegroundServiceSpotListener.getCurrentTrackProgress();
    }

    private void disableAllClickableElements() {
        profileImage.setEnabled(false);
        username.setEnabled(false);
        back.setEnabled(false);
        like.setEnabled(false);
        message.setEnabled(false);
    }

    private void enableAllClickableElements() {
        profileImage.setEnabled(true);
        username.setEnabled(true);
        back.setEnabled(true);
        like.setEnabled(true);
        message.setEnabled(true);
    }

    /*
    GETTER & SETTER
     */

    public MutableLiveData<Spot> getCurrentSpot() {
        if (currentSpot == null) {
            currentSpot = new MutableLiveData<>();
        }
        return currentSpot;
    }

    public void setCurrentSpot(Spot currentSpot) {
        if (this.currentSpot == null) {
            this.currentSpot = new MutableLiveData<>();
        }
        getCurrentSpot().postValue(currentSpot);
    }

}
