package com.ansgarkersch.playd.model;

import com.ansgarkersch.playd.globals.GlobalVariables;

public class RowSearchHeader implements IScrollable {

    private String title;

    public RowSearchHeader(String title) {
        this.title = title;
    }

    @Override
    public int getType() {
        return GlobalVariables.VIEWTYPE_HEADERTITLE;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
