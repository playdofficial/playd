package com.ansgarkersch.playd.viewModel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.ansgarkersch.playd.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WatchProfileViewModel extends ViewModel {

    private MutableLiveData<User> currentLoggedInUser;
    private MutableLiveData<Map<String, User>> cachedUsers;

    public MutableLiveData<User> getCurrentLoggedInUser() {
        if (currentLoggedInUser == null) {
            currentLoggedInUser = new MutableLiveData<>();
        }
        return currentLoggedInUser;
    }

    public void setCurrentLoggedInUser(User currentLoggedInUser) {
        if (currentLoggedInUser == null) {
            return;
        }
        getCurrentLoggedInUser().postValue(currentLoggedInUser);
    }

    /*
     * Cached Users
     */

    /**
     * Returns a user from cache by id
     * @param userId
     * @return returns null if the user is not cached yet
     */
    public User getCachedUserById(String userId) {
        if (cachedUsers == null) {
            cachedUsers = new MutableLiveData<>();
            return null;
        }
        if (getCachedUsers().getValue() == null) {
            return null;
        }
        return getCachedUsers().getValue().get(userId);
    }

    /**
     * Adds a user to the cached users
     * @param user the new User to cache
     */
    public void cacheUser(User user) {
        Map<String, User> cachedUsers = getCachedUsers().getValue();
        if (cachedUsers == null) {
            cachedUsers = new HashMap<>();
        }
        if (user == null) {
            return;
        }
        cachedUsers.put(user.getId(), user);
        setCachedUsers(cachedUsers);
    }

    public boolean isUserAlreadyCached(String userId) {
        Map<String, User> cachedUsers = getCachedUsers().getValue();
        if (cachedUsers == null) {
            return false;
        }
        return cachedUsers.containsKey(userId);
    }

    /**
     * Adds multiple users to the cached users
     * @param users the ArrayList of users to cache
     */
    public void cacheMultipleUsers(ArrayList<User> users) {
        Map<String, User> cachedUsers = getCachedUsers().getValue();
        if (cachedUsers == null) {
            cachedUsers = new HashMap<>();
        }
        if (users == null) {
            return;
        }
        for (User user : users) {
            cachedUsers.put(user.getId(), user);
        }
        setCachedUsers(cachedUsers);
    }

    public MutableLiveData<Map<String, User>> getCachedUsers() {
        if (cachedUsers == null) {
            cachedUsers = new MutableLiveData<>();
        }
        return cachedUsers;
    }

    public void setCachedUsers(Map<String, User> cachedUsers) {
        if (cachedUsers == null) {
            return;
        }
        getCachedUsers().setValue(cachedUsers);
    }
}
