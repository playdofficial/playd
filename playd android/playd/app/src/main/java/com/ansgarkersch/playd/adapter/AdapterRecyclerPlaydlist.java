package com.ansgarkersch.playd.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd._spotify.model.PlaylistTrackObject;
import com.ansgarkersch.playd._spotify.model.Track;
import com.ansgarkersch.playd.model.Playback;

import java.util.ArrayList;
import java.util.List;

public class AdapterRecyclerPlaydlist extends RecyclerView.Adapter<AdapterRecyclerPlaydlist.ViewHolder> {

    private List<PlaylistTrackObject> playlist = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;
    private AdapterRecyclerPlaydlist.ItemClickListener mClickListener;
    private RecyclerView recyclerView;

    // Part of OnClick-Handling
    private Playback currentlyPlayingPlayback = null;

    // data is passed into the constructor
    public AdapterRecyclerPlaydlist(Context context, RecyclerView recyclerView) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.recyclerView = recyclerView;
    }

    public void setCurrentlyPlayingPlayback(Playback playback) {
        if (playback != null) {
            currentlyPlayingPlayback = playback;
        } else {
            currentlyPlayingPlayback = null;
        }
        notifyDataSetChanged();
    }

    /*
    public void scrollToPlayback(Playback playback) {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                if (playdlistItems.indexOf(playback) < 0) {
                    recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, new RecyclerView.State(), 0);
                } else {
                    int size = playdlistItems.size(); // 10  12 //
                    int scrollPosition = playdlistItems.indexOf(playback) == size ? playdlistItems.indexOf(playback) : playdlistItems.indexOf(playback) + 2;
                    recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, new RecyclerView.State(), scrollPosition);
                }
            }
        });
    }


    public void setPlaydlistItems(List<Playback> playdlistItems) {
        this.playdlistItems = playdlistItems;
        notifyDataSetChanged();
    }

    */

    public void addToPlaylist(List<PlaylistTrackObject> playlist) {
        this.playlist.addAll(playlist);
        notifyDataSetChanged();
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public AdapterRecyclerPlaydlist.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row_playdlist, parent, false);
        return new AdapterRecyclerPlaydlist.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(@NonNull final AdapterRecyclerPlaydlist.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        if (playlist == null) {
            return;
        }

        final Track currentTrack = playlist.get(position).getTrack();

        //Picasso.get().load(currentTrack.getAlbum().getImages().get(1).getUrl()).into(holder.albumArt);
        //Picasso.get().load(currentTrack.getAlbum().getImages().get(0).getUrl()).into(holder.albumArtAlphaBackground);

        holder.artistName.setText(currentTrack.getArtists().get(0).getName());
        holder.trackName.setText(currentTrack.getName());

        /*
        holder.trackContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (currentPlayback.equals(currentlyPlayingPlayback)) {
                    currentlyPlayingPlayback = null;
                    if (mClickListener != null) {
                        pause();
                        mClickListener.onItemClick(view, currentPlayback, false);
                        }
                } else {
                    currentlyPlayingPlayback = currentPlayback;
                    if (mClickListener != null) {
                        play(currentPlayback);
                        mClickListener.onItemClick(view, currentPlayback, true);
                    }
                }
                notifyDataSetChanged();

            }
        });


        if (currentPlayback.equals(currentlyPlayingPlayback)) {
            holder.albumArtAlphaBackground.setAlpha(0.20f);
            holder.musicPlayingIndicator.setVisibility(View.VISIBLE);
            holder.artistName.setTextColor(context.getResources().getColor(android.R.color.black));
            holder.trackName.setTextColor(context.getResources().getColor(android.R.color.black));
        } else {
            holder.albumArtAlphaBackground.setAlpha(0.08f);
            holder.musicPlayingIndicator.setVisibility(View.INVISIBLE);
            holder.artistName.setTextColor(context.getResources().getColor(R.color.colorBlack));
            holder.trackName.setTextColor(context.getResources().getColor(R.color.colorBlack));
        }

        */

    }

    // total number of rows
    @Override
    public int getItemCount() {
        if (playlist == null) {
            return 0;
        }
        return playlist.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView albumArtAlphaBackground, albumArt;
        TextView artistName, trackName;
        FrameLayout trackContent, musicPlayingIndicator;


        ViewHolder(View itemView) {
            super(itemView);
            albumArt = itemView.findViewById(R.id.row_playdlist_albumArt);
            albumArtAlphaBackground = itemView.findViewById(R.id.row_playdlist_albumArtAlphaBackground);
            artistName = itemView.findViewById(R.id.row_playdlist_artistName);
            trackName = itemView.findViewById(R.id.row_playdlist_trackName);
            trackContent = itemView.findViewById(R.id.row_playdlist_trackContent);
            musicPlayingIndicator = itemView.findViewById(R.id.row_playdlist_musicPlayingIndicator);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            //if (mClickListener != null) mClickListener.onClickPlayback(view, getAdapterPosition());
        }
    }

    // allows clicks events to be caught
    public void setCurrentPlaybackListener(AdapterRecyclerPlaydlist.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, Playback playback, boolean isPlaying);
    }

}
