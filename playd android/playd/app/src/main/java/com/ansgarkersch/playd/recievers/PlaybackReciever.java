package com.ansgarkersch.playd.recievers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.CountDownTimer;
import android.support.v4.content.LocalBroadcastManager;

import com.ansgarkersch.playd.globals.GlobalVariables;

import java.util.Observable;

public class PlaybackReciever extends Observable {

    private CountDownTimer broadcastRecieverTimer = null;

    public PlaybackReciever() { }

    public void registerReciever(Context context) {
        IntentFilter customIntentFilter = new IntentFilter();
        customIntentFilter.addAction(GlobalVariables.FCM_ACTIONCODE_PLAYBACK_STARTED);
        customIntentFilter.addCategory(GlobalVariables.FCM_CATEGORY_PLAYBACK);
        LocalBroadcastManager.getInstance(context).registerReceiver(bReciever, customIntentFilter);
    }

    public void unregisterReciever(Context context) {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(bReciever);
    }

    private BroadcastReceiver bReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null || intent.getAction() == null) {
                return;
            }

            if (broadcastRecieverTimer != null) {
                broadcastRecieverTimer.cancel();
            }
            broadcastRecieverTimer = new CountDownTimer(1000, 1000) {
                public void onTick(long millisUntilFinished) { }

                public void onFinish() {
                    String playbackId = intent.getStringExtra(GlobalVariables.INTENT_PLAYBACK_ID);
                    setChanged();
                    notifyObservers(playbackId);
                }
            }.start();

        }
    };


}
