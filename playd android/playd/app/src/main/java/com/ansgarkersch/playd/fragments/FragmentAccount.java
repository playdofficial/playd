package com.ansgarkersch.playd.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.activities.ActivityMain;
import com.ansgarkersch.playd.activities.ActivityWatchProfile;
import com.ansgarkersch.playd.adapter.AdapterRecyclerViewProfileGallery;
import com.ansgarkersch.playd.dialogfragments.DialogFragmentCompleteYourAccount;
import com.ansgarkersch.playd.dialogfragments.DialogFragmentSingleImage;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.Gender;
import com.ansgarkersch.playd.model.Nationality;
import com.ansgarkersch.playd.model.RelationshipStatus;
import com.ansgarkersch.playd.model.User;
import com.ansgarkersch.playd.viewModel.MyUserViewModel;
import com.ansgarkersch.playd.viewModel.MyUserViewModelFactory;
import com.ansgarkersch.playd.viewModel._SpotViewModel;
import com.ansgarkersch.playd.viewModel._SpotViewModelFactory;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class FragmentAccount extends Fragment {


    private View view;

    private User currentUser;
    private CircleImageView profilePic;
    private TextView name, nationality, relationshipStatus, description, gender, followedCount, followersCount, followersText, followersTextBadge;
    private LinearLayout mainContent, followers, followed;
    private ProgressBar mainContentProgress;

    // View Model
    private MyUserViewModel myUserViewModel;
    private _SpotViewModel spotViewModel;

    // Followers which subscribed the current-logged-in user since the last login
    private ArrayList<String> newFollowersIds;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_account,
                container, false);

        // View Model
        myUserViewModel = ViewModelProviders.of(getActivity(), new MyUserViewModelFactory(getActivity())).get(MyUserViewModel.class);
        spotViewModel = ViewModelProviders.of(getActivity(), new _SpotViewModelFactory(getActivity())).get(_SpotViewModel.class);

        // Followers which subscribed the current-logged-in user since the last login
        newFollowersIds = new ArrayList<>();
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // UI elements
        initLayoutElements();

        // observe current logged-in user
        myUserViewModel.getCurrentUser().observe(this, user -> {
            if (user == null) {
                return;
            }
            currentUser = user;
            updateUI();
            mainContentProgress.setVisibility(View.GONE);
            mainContent.setVisibility(View.VISIBLE);

            // Preload all images
            for (String imageUrl : user.getProfileImageList()) {
                Glide.with(this).load(imageUrl).preload();
            }
        });
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            // FragmentAccount is hidden
            // TODO
        } else {
            // FragmentAccount is visible
            // Check if account has to be completed
            if (currentUser == null) {
                return;
            }
            if (currentUser.getBirthday().isEmpty()
                    || currentUser.getProfileImages().size() == 0
                    || currentUser.getDescription().isEmpty()) {
                DialogFragmentCompleteYourAccount dialogFragmentCompleteYourAccount = new DialogFragmentCompleteYourAccount();
                dialogFragmentCompleteYourAccount
                        .show(getActivity().getFragmentManager(),
                                dialogFragmentCompleteYourAccount.getClass().getName());
            }
        }
    }

    private void updateUI() {
        initUserInformation();
        //initImages();
        initRecycler();
    }

    /**
     * initializes the "toolbar"
     */
    private void initLayoutElements() {
        // Main Layout
        mainContent = view.findViewById(R.id.fragment_account_mainContent);
        // Main Layout Progress Bar, visible by default
        mainContentProgress = view.findViewById(R.id.fragment_account_mainContent_progressBar);

        // TextViews
        name = view.findViewById(R.id.watchProfile_textview_name);
        nationality = view.findViewById(R.id.watchProfile_textview_nationality);
        relationshipStatus = view.findViewById(R.id.watchProfile_textview_relationshipStatus);
        description = view.findViewById(R.id.fragment_watchProfile_textview_description);
        gender = view.findViewById(R.id.watchProfile_textview_gender);
        followedCount = view.findViewById(R.id.watchProfile_textview_followedCount);
        followersCount = view.findViewById(R.id.watchProfile_textview_followersCount);
        followersText = view.findViewById(R.id.watchProfile_textview_followersText);
        followersTextBadge = view.findViewById(R.id.watchProfile_textview_followersCount_badge);

        followers = view.findViewById(R.id.watchProfile_button_followers);
        followed = view.findViewById(R.id.watchProfile_button_followed);
    }

    private void initUserInformation() {
        if (currentUser.getName().isEmpty()) {
            name.setText(currentUser.getUsername());
        } else {
            name.setText(currentUser.getName());
        }
        nationality.setText(Nationality.getNationalityNameInLanguage(getActivity(), currentUser.getNationality()));
        relationshipStatus.setText(RelationshipStatus.getRelationshipStatusInLanguage(getActivity(), currentUser.getRelationshipStatus()));
        description.setText(currentUser.getDescription());
        gender.setText(Gender.getGenderInLanguage(getActivity(), currentUser.getGender()));

        // Validate the lastLoggedInTimestamp with the Followed-ValueTimestamps
        for (Map.Entry<String, Long> entry : currentUser.getFollowersIds().entrySet()) {
            if (entry.getValue() > currentUser.getLastLoggedInTimestamp()) {
                newFollowersIds.add(entry.getKey());
            }
        }
        if (newFollowersIds.size() > 0) {
            if (newFollowersIds.size() >= 99) {
                followersTextBadge.setText("99");
            } else {
                followersTextBadge.setText(newFollowersIds.size() + "");
            }
            addFollowersTextBadge();
        }

        // followed
        followedCount.setText(String.valueOf(currentUser.getFollowedIds().size()));
        followed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentUser.getFollowedIds().size() == 0) {
                    return;
                }
                openAccountWindow(
                        GlobalVariables.INTENT_ACTION_FLAG_FOLLOWED);
            }
        });

        // followers
        followersCount.setText(String.valueOf(currentUser.getFollowersIds().size()));
        followers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentUser.getFollowersIds().size() == 0) {
                    return;
                }
                openAccountWindow(
                        GlobalVariables.INTENT_ACTION_FLAG_FOLLOWERS);
                // Remove Notification
                ((ActivityMain)getActivity()).setAccountNotificationBadge(-1);
                Log.d(getClass().getName(), "Decrement AccountNotificationBadge Count");
                removeFollowersTextBadge();
                newFollowersIds.clear();
            }
        });

    }

    private void openAccountWindow(int actionFlag) {
        Intent intent = new Intent(getActivity(), ActivityWatchProfile.class);
        intent.putExtra(GlobalVariables.INTENT_PROFILE_ID, currentUser.getId());
        intent.putExtra(GlobalVariables.INTENT_PROFILE_CURRENT_USER, currentUser);
        intent.putExtra(GlobalVariables.INTENT_ACTION_FLAG, actionFlag);
        /*
        Put the user's new followers since his last login in the bundle
         */
        if (newFollowersIds.size() > 0) {
            if (newFollowersIds.size() > 99) {
                intent.putStringArrayListExtra(GlobalVariables.INTENT_NEW_FOLLOWERS_SINCE_LAST_LOGIN,
                        new ArrayList<>(newFollowersIds.subList(0, 99)));
            } else {
                intent.putStringArrayListExtra(GlobalVariables.INTENT_NEW_FOLLOWERS_SINCE_LAST_LOGIN,
                        newFollowersIds);
            }
        }
        /*
        Put the ArrayList of Currently Followed Spots - Users and put them into the
        cachedUsers-Map in ActivityWatchProfile to minimize the duplicate account-fetching
        */
        /*
        if (spotViewModel.getFollowedSpots().getValue()!= null && spotViewModel.getFollowedSpots().getValue().values() != null) {
            ArrayList<User> cachedUsers = new ArrayList<>();
            for (Spot spot : spotViewModel.getFollowedSpots().getValue().values()) {
                if (cachedUsers.size() < 10) {
                    // don't put more than 10 entries in the list to prevent an 'intent overflow'
                    cachedUsers.add(spot.getUser());
                }
            }
            intent.putExtra(GlobalVariables.INTENT_CACHED_USERS, cachedUsers);
        }
        */
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.no_animation, R.anim.no_animation);
    }

    private void addFollowersTextBadge() {
        followersTextBadge.setVisibility(View.VISIBLE);
    }

    private void removeFollowersTextBadge() {
        followersTextBadge.setVisibility(View.GONE);
    }

    private void initRecycler() {

        if (currentUser.getProfileImages() == null) {
            return;
        }

        if (currentUser.getProfileImages().size() == 0) {
            return;
        }

        /*
        // set up the RecyclerView
        final RecyclerView imageGalleryView = view.findViewById(R.id.watchProfile_imageGallery);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);

        imageGalleryView.setLayoutManager(horizontalLayoutManager);
        AdapterRecyclerViewProfileGallery adapter = new AdapterRecyclerViewProfileGallery(getActivity(),
                currentUser.getProfileImageList());
                */

        final RecyclerView imageGalleryView = view.findViewById(R.id.watchProfile_imageGallery);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);

        // Remove first element (profile Image) from list
        //List<String> profileImages = currentUser.getProfileImageList();
        //profileImages.remove(0);

        imageGalleryView.setLayoutManager(horizontalLayoutManager);
        AdapterRecyclerViewProfileGallery adapter = new AdapterRecyclerViewProfileGallery(
                getActivity(),
                currentUser.getProfileImageList());


        //imageGalleryView.setHasFixedSize(true);
        //imageGalleryView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        //imageGalleryView.setNestedScrollingEnabled(false);

        adapter.setClickListener(new AdapterRecyclerViewProfileGallery.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                android.support.v4.app.DialogFragment image = DialogFragmentSingleImage.
                        newInstance(GlobalVariables.DIALOG_GALLERY_IMAGE_INTENT_EXTRA_PLACEHOLDER,
                                currentUser.getProfileImageList().get(position));
                image.show(getActivity().getSupportFragmentManager(), "");
            }
        });

        imageGalleryView.setAdapter(adapter);
    }



}
