package com.ansgarkersch.playd.events;

public class PlaybackStateChangedEvent {

    private String userId;
    private String actionCode;

    public PlaybackStateChangedEvent() {}

    public PlaybackStateChangedEvent(String userId, String actionCode) {
        this.userId = userId;
        this.actionCode = actionCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }
}
