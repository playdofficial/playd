package com.ansgarkersch.playd.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.model.RelationshipStatus;

public class FragmentCreateAccountFirst extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_account_first,
                container, false);

        // Relationship Spinner
        RelationshipStatus[] relationshipStatusArray = RelationshipStatus.values();
        String[] relationShipStates = new String[relationshipStatusArray.length];
        for (int i = 0; i < relationShipStates.length; i++) {
            relationShipStates[i] = RelationshipStatus.getRelationshipStatusInLanguage(view.getContext(), relationshipStatusArray[i]);
        }
        Spinner relationshipStatus = view.findViewById(R.id.fragment_create_account_first_spinner_relationship);
        relationshipStatus.setAdapter(new ArrayAdapter<>(view.getContext(),
                android.R.layout.simple_spinner_dropdown_item, relationShipStates));

        // Birthdate Month Spinner
        String[] monthNames = new String[]{
                view.getResources().getString(R.string.month_january),
                view.getResources().getString(R.string.month_february),
                view.getResources().getString(R.string.month_march),
                view.getResources().getString(R.string.month_april),
                view.getResources().getString(R.string.month_may),
                view.getResources().getString(R.string.month_june),
                view.getResources().getString(R.string.month_july),
                view.getResources().getString(R.string.month_august),
                view.getResources().getString(R.string.month_september),
                view.getResources().getString(R.string.month_october),
                view.getResources().getString(R.string.month_november),
                view.getResources().getString(R.string.month_december)};
        Spinner birthdateMonth = view.findViewById(R.id.fragment_create_account_first_birthdate_spinner_month);
        birthdateMonth.setAdapter(new ArrayAdapter<>(view.getContext(),
                android.R.layout.simple_spinner_dropdown_item, monthNames));

        return view;

    }
}
