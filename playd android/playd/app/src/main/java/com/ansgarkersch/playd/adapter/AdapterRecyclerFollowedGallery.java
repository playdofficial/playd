package com.ansgarkersch.playd.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.Playback;
import com.ansgarkersch.playd.model.User;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterRecyclerFollowedGallery extends RecyclerView.Adapter<AdapterRecyclerFollowedGallery.ViewHolder> {

    private List<User> userData;
    private LayoutInflater mInflater;
    private AdapterRecyclerFollowedGallery.ItemClickListener mClickListener;
    private Context context;
    private String currentUserId;

    private User currentlyVisibleUser = null;

    // data is passed into the constructor
    public AdapterRecyclerFollowedGallery(Context context, String currentUserId) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.currentUserId = currentUserId;
        userData = new ArrayList<>();
    }

    public void setUserData(Collection<Playback> playbacks) {
        //userData.addAll(playbacks);
        notifyDataSetChanged();
    }

    public void setUser(User user) {
        userData.add(user);
        notifyDataSetChanged();
    }

    public void setCurrentlyVisibleUser(Playback user) {
        //currentlyVisibleUser = user;
    }

    public void setCurrentlyVisibleUser(User user) {
        currentlyVisibleUser = user;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public AdapterRecyclerFollowedGallery.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row_subscription_gallery, parent, false);
        return new AdapterRecyclerFollowedGallery.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(@NonNull AdapterRecyclerFollowedGallery.ViewHolder holder, int position) {
        User currentUser = userData.get(position);

        // Profile Image
        Glide.with(context).load(
                GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE.equals(currentUser.getProfilePicThumbnailUrl())
                        ?
                        R.mipmap.no_profile_image
                        :
                        currentUser.getProfilePicThumbnailUrl()).into(holder.profilePic);

        // Name
        if (currentUser.getId().equals(currentUserId)) {
            holder.name.setText(context.getResources().getString(R.string.yourPlaydlist));
        } else {
            holder.name.setText(userData.get(position).getUsername());
        }

        // Handle onClick
        holder.content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!currentUser.equals(currentlyVisibleUser)) {
                    currentlyVisibleUser = currentUser;
                    mClickListener.onItemClick(view, currentUser);
                }
                notifyDataSetChanged();
            }
        });

        holder.profilePic.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
        holder.name.setTextColor(context.getResources().getColor(R.color.colorBlack));

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return userData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CircleImageView profilePic;
        TextView name;
        LinearLayout content;

        ViewHolder(View itemView) {
            super(itemView);
            profilePic = itemView.findViewById(R.id.row_subscriptionGallery_image);
            name = itemView.findViewById(R.id.row_subscriptionGallery_name);
            content = itemView.findViewById(R.id.row_subscriptionGallery_content);
            name.setSelected(true);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            //if (mClickListener != null) mClickListener.onClickPlayback(view, getAdapterPosition());
        }
    }

    // allows clicks events to be caught
    public void setClickListener(AdapterRecyclerFollowedGallery.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, User user);
    }

}
