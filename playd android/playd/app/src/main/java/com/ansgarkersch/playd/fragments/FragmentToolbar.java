package com.ansgarkersch.playd.fragments;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.User;
import com.ansgarkersch.playd.util.UtilityMethods;
import com.ansgarkersch.playd.viewModel.MyUserViewModel;
import com.ansgarkersch.playd.viewModel.MyUserViewModelFactory;
import com.ansgarkersch.playd.viewModel._SpotViewModel;
import com.ansgarkersch.playd.viewModel._SpotViewModelFactory;
import com.bumptech.glide.Glide;

import de.hdodenhof.circleimageview.CircleImageView;

public class FragmentToolbar extends Fragment {

    private View view;
    private User currentLoggedInUser;

    /*
    Global Elements
     */

    // Toolbars
    private Toolbar mainContent;
    private LinearLayout searchContent;
    private Toolbar accountContent;

    // View Models
    private _SpotViewModel spotViewModel;
    private MyUserViewModel myUserViewModel;

    /*
    Content MyPlayd
     */
    // UI Elements
    private CircleImageView profileImage, foreignSpotterProfileImage;
    private CircleImageView profileImageBadge, foreignSpotterProfileImageBadge;
    private TextView iconAccountBadge;

    /*
    Content Account
     */
    // AccountContent elements
    private TextView accountName;

    // Account Badge Count
    private int accountBadgeCount = 0;

    /*
    Content Search
     */
    // Callback Listener
    private FragmentSearchCallback fragmentSearchCallback;
    private EditText edittextSearchUsers;
    private ImageButton buttonSearchContentBack;
    private boolean searchViewVisible = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main_toolbar,
                container, false);

        /*
        Global Elements
         */
        // Toolbars
        mainContent = view.findViewById(R.id.fragment_main_toolbar_toolbar_mainContent);
        searchContent = view.findViewById(R.id.fragment_main_toolbar_linearlayoutToolbar_searchContent);
        accountContent = view.findViewById(R.id.fragment_main_toolbar_toolbar_accountContent);

        // View Models
        spotViewModel = ViewModelProviders.of(getActivity(), new _SpotViewModelFactory(getActivity())).get(_SpotViewModel.class);
        myUserViewModel = ViewModelProviders.of(getActivity(), new MyUserViewModelFactory(getActivity())).get(MyUserViewModel.class);

        return view;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /*
        Content MyPlayd
         */
        // UI elements
        profileImage = view.findViewById(R.id.activity_main_toolbar_myspot_profileImage);
        profileImageBadge = view.findViewById(R.id.activity_main_toolbar_myspot_badge);
        foreignSpotterProfileImage = view.findViewById(R.id.activity_main_toolbar_foreign_spotter_profileImage);
        foreignSpotterProfileImageBadge = view.findViewById(R.id.activity_main_toolbar_foreign_spotter_profileImage_badge);

        /*
        Content Account
         */
        accountName = view.findViewById(R.id.fragment_main_toolbar_toolbar_accountContent_name);
        iconAccountBadge = view.findViewById(R.id.customBottomBar_Icon_Account_Badge);

        /*
        Content Search
         */
        edittextSearchUsers = view.findViewById(R.id.fragment_main_toolbar_edittext_searchView);
        edittextSearchUsers.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (MotionEvent.ACTION_UP == motionEvent.getAction() && !searchViewVisible) {
                    buttonSearchContentBack.setVisibility(View.VISIBLE);
                    fragmentSearchCallback.onOpenSearchView();
                    fragmentSearchCallback.onTextChanged("");
                    searchViewVisible = true;
                }
                return false;
            }
        });
        edittextSearchUsers.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String substring = s.toString();
                fragmentSearchCallback.onTextChanged(substring);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        buttonSearchContentBack = view.findViewById(R.id.fragment_main_toolbar_imagebutton_cancelSearch);
        buttonSearchContentBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentSearchCallback.onCloseSearchView();
                edittextSearchUsers.setText("");
                edittextSearchUsers.clearFocus();
                UtilityMethods.hideKeyboard(getActivity());
                searchViewVisible = false;
                buttonSearchContentBack.setVisibility(View.GONE);
            }
        });



        // Observe current logged-in user
        myUserViewModel.getCurrentUser().observe(this, currentUser -> {
            if (currentUser == null) {
                return;
            }
            currentLoggedInUser = currentUser;

            /*
            Set Placeholder if the user does not have a profile picture
             */
            Glide.with(FragmentToolbar.this).load(
                    GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE.equals(currentUser.getProfilePicThumbnailUrl())
                    ?
                    R.mipmap.no_profile_image
                    :
                    currentUser.getProfilePicThumbnailUrl()).into(profileImage);

            /*
            Preload all profile images
             */
            for (String url : currentUser.getProfileImageList()) {
                Glide.with(FragmentToolbar.this).load(url).preload();
            }

            /*
            Set the account badge
             */

            // Show editYourProfile-badge when:
            // - Birthday is not set
            // - ProfilePicture is not set
            if (currentUser.getBirthday().isEmpty()
                    || currentUser.getProfileImages().size() == 0) {
                setAccountNotificationBadge(1);
                Log.d(getClass().getName(), "Increment AccountNotificationBadge Count");
            }

        });

        /*
        // observe current playback
        myUserViewModel.getCurrentlyPlayingSpot().observe(this, currentSpot -> {

            // current logged-in user
            if (currentSpot == null || currentSpot.getId().equals(GlobalVariables.LOCAL_PLAYBACK)) {
                view.findViewById(R.id.activity_main_toolbar_foreign_spotter_layout).setVisibility(View.GONE);
                profileImageBadge.setVisibility(View.INVISIBLE);
                return;
            }
            view.findViewById(R.id.activity_main_toolbar_foreign_spotter_layout).setVisibility(View.VISIBLE);

            Glide.with(FragmentToolbar.this).load(
                    GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE.equals(currentSpot.getUser().getProfilePicUrl())
                            ?
                            R.mipmap.no_profile_image
                            :
                            currentSpot.getUser().getProfilePicUrl()).into(foreignSpotterProfileImage);

            foreignSpotterProfileImageBadge.setBackground(getResources().getDrawable(R.drawable.circle_solid_playdgreen));
            foreignSpotterProfileImageBadge.setVisibility(View.VISIBLE);
            foreignSpotterProfileImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) { // show the spot's profile
                    Intent intent = new Intent(getActivity(), ActivityWatchProfile.class);
                    intent.putExtra(GlobalVariables.INTENT_PROFILE_ID, currentSpot.getUser().getId());
                    intent.putExtra(GlobalVariables.INTENT_PROFILE_USERNAME, currentSpot.getUser().getU());
                    intent.putExtra(GlobalVariables.INTENT_PROFILE_CURRENT_USER, currentLoggedInUser);
                    getActivity().startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.activity_slide_in_up, android.R.anim.fade_out);
                }
            });
        });
        */

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentSearchCallback) {
            fragmentSearchCallback = (FragmentSearchCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentSearchCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentSearchCallback = null;
    }

    /**
     * adds or removes badge digits
     * @param count can be positve or negative
     */
    public void setAccountNotificationBadge(int count) {
        accountBadgeCount += count;
        if (accountBadgeCount <= 0) {
            iconAccountBadge.setVisibility(View.GONE);
            return;
        }
        iconAccountBadge.setVisibility(View.VISIBLE);
        iconAccountBadge.setText(String.valueOf(accountBadgeCount));
    }

    public void enableSearchView() {
        searchContent.setVisibility(View.VISIBLE);
        mainContent.setVisibility(View.GONE);
        accountContent.setVisibility(View.GONE);
    }

    public void enableMainView() {
        mainContent.setVisibility(View.VISIBLE);
        searchContent.setVisibility(View.GONE);
        accountContent.setVisibility(View.GONE);
    }

    public void enableAccountView() {
        accountContent.setVisibility(View.VISIBLE);
        mainContent.setVisibility(View.GONE);
        searchContent.setVisibility(View.GONE);

        // init AccountContent
        if (currentLoggedInUser == null) {
            return;
        }
        accountName.setText(currentLoggedInUser.getUsername());
    }










}
