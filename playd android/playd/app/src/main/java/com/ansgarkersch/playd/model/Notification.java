package com.ansgarkersch.playd.model;

import com.ansgarkersch.playd.util.UtilityMethods;

import java.io.Serializable;

public class Notification implements Serializable {

    private final long id;
    private String notificationType;
    private String userId; // deprecated
    private UserOverview userOverview;
    private String message;

    public Notification(String notificationType, UserOverview fetchedUserOverview) {
        id = UtilityMethods.getCurrentTime();
        this.notificationType = notificationType;
        this.userOverview = fetchedUserOverview;
    }

    public Notification(String notificationType) {
        id = UtilityMethods.getCurrentTime();
        this.notificationType = notificationType;
    }

    public Notification(String notificationType, String userId, String message) {
        id = UtilityMethods.getCurrentTime();
        this.notificationType = notificationType;
        this.userId = userId;
        this.message = message;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getId() {
        return id;
    }

    public UserOverview getUserOverview() {
        return userOverview;
    }

    public void setUserOverview(UserOverview userOverview) {
        this.userOverview = userOverview;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
