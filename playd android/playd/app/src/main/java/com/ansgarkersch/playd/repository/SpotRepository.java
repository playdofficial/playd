package com.ansgarkersch.playd.repository;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.ansgarkersch.playd._spotify.repository.SpotifyRepository;
import com.ansgarkersch.playd.globals.FirebaseEndpoints;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.Playback;
import com.ansgarkersch.playd.model.Spot;
import com.ansgarkersch.playd.model.User;
import com.ansgarkersch.playd.model.UserOverview;
import com.ansgarkersch.playd.util.SpotUtilities;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;

import java.lang.ref.WeakReference;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SpotRepository {

    // Firebase Services
    private FirebaseFirestore firestoreDatabase;
    private DatabaseReference firebaseRealtimeDatabase;
    private FirebaseStorage firebaseStorage;

    // Util
    private String TAG = "Class: SpotRepository - ";

    // Repositories
    private UserRepository userRepository;

    public SpotRepository(UserRepository userRepository) {
        // Repositories
        this.userRepository = userRepository;
        // Firebase Services
        firestoreDatabase = FirebaseFirestore.getInstance();
        firebaseRealtimeDatabase = FirebaseDatabase.getInstance().getReference();
        firebaseStorage = FirebaseStorage.getInstance();
    }


    /**
     * Gets all followed current userOverviews
     * @param followedUIds the followedUids of the current logged-in users
     * @param alreadyPlayingUIds the already fetched user ids. Set to null if there's no need to prevent
     *                           duplicate API calls or if this method is triggered the first time
     *                           This value represents the usecase "Pull-to-refresh"
     * @param callback the callback, returns a list of userOverviews
     */
    public void getAllFollowedCurrentUserOverviews(final Set<String> followedUIds,
                                                   final Set<String> alreadyPlayingUIds,
                                                   final ValueCallback callback) {

        // Remove the already fetched userIds to get only the latest possible spots
        if (alreadyPlayingUIds != null) {
            Iterator<String> iterator = followedUIds.iterator();
            while (iterator.hasNext()) {
                String uid = iterator.next();
                if (alreadyPlayingUIds.contains(uid)) {
                    // Remove this userId because it was already fetched before
                    iterator.remove();

                    // TODO removeAll(var x) statt Iterator Lösung? -> Google!

                }
            }
        }

        List<UserOverview> currentFollowedUserOverviews
                = Collections.synchronizedList(new ArrayList<>());

        for (String followed : followedUIds) {

            firebaseRealtimeDatabase.child(GlobalVariables.DB_USEROVERVIEW)
                    .orderByChild(GlobalVariables.DB_USEROVERVIEW_ISPLAYING)
                    .equalTo(true, followed)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {
                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    if (snapshot == null) {
                                        currentFollowedUserOverviews.add(null);
                                        // Check if Async Loop has finished
                                        if (currentFollowedUserOverviews.size() == followedUIds.size()) {
                                            callback.onSuccess(SpotUtilities.
                                                    removeNullValuesFromUserOverviewsAndSortBySpotStartedTimestamp
                                                    (currentFollowedUserOverviews));
                                        }
                                    } else {
                                        firebaseStorage.getReference()
                                                .child(snapshot.getKey())
                                                .child(GlobalVariables.DB_STORAGE_THUMB_ID)
                                                .getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                            @Override
                                            public void onSuccess(Uri uri) {
                                                UserOverview userOverview = snapshot.getValue(UserOverview.class);
                                                userOverview.setId(snapshot.getKey());
                                                userOverview.setProfilePicUrl(uri.toString());
                                                currentFollowedUserOverviews.add(userOverview);
                                                // Check if Async Loop has finished
                                                if (currentFollowedUserOverviews.size() == followedUIds.size()) {
                                                    callback.onSuccess(SpotUtilities.
                                                            removeNullValuesFromUserOverviewsAndSortBySpotStartedTimestamp
                                                            (currentFollowedUserOverviews));
                                                }
                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception exception) {
                                                Log.d(getClass().getName(), exception.getMessage());
                                                UserOverview userOverview = snapshot.getValue(UserOverview.class);
                                                userOverview.setId(snapshot.getKey());
                                                userOverview.setProfilePicUrl(GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE);
                                                currentFollowedUserOverviews.add(userOverview);
                                                // Check if Async Loop has finished
                                                if (currentFollowedUserOverviews.size() == followedUIds.size()) {
                                                    callback.onSuccess(SpotUtilities.
                                                            removeNullValuesFromUserOverviewsAndSortBySpotStartedTimestamp
                                                                    (currentFollowedUserOverviews));
                                                }
                                            }
                                        });
                                    }
                                }
                            } else {
                                currentFollowedUserOverviews.add(null);
                                // Check if Async Loop has finished
                                if (currentFollowedUserOverviews.size() == followedUIds.size()) {
                                    callback.onSuccess(SpotUtilities.
                                            removeNullValuesFromUserOverviewsAndSortBySpotStartedTimestamp
                                                    (currentFollowedUserOverviews));
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            currentFollowedUserOverviews.add(null);
                            // Check if Async Loop has finished
                            if (currentFollowedUserOverviews.size() == followedUIds.size()) {
                                callback.onSuccess(SpotUtilities.removeNullValuesFromUserOverviewsAndSortBySpotStartedTimestamp(currentFollowedUserOverviews));
                            }
                        }
                    });

        }

    }


    /**
     * Fetches a single spot if the according user currently streams playback information
     * @param foreignUserId
     * @param callback
     */
    @Deprecated
    public void getSingleUserOverview(final String foreignUserId, final ValueCallback callback) {

        firebaseRealtimeDatabase
                .child(GlobalVariables.DB_USEROVERVIEW)
                .child(foreignUserId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() == null) {
                            callback.onFailure(GlobalVariables.RESULT_FAILURE);
                            return;
                        }

                        firebaseStorage.getReference()
                                .child(foreignUserId)
                                .child(GlobalVariables.DB_STORAGE_THUMB_ID)
                                .getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                UserOverview userOverview = dataSnapshot.getValue(UserOverview.class);
                                if (userOverview == null) {
                                    callback.onFailure(GlobalVariables.RESULT_FAILURE);
                                    return;
                                }
                                userOverview.setId(dataSnapshot.getKey());
                                userOverview.setProfilePicUrl(uri.toString());
                                callback.onSuccess(userOverview);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                Log.d(getClass().getName(), exception.getMessage());
                                UserOverview userOverview = dataSnapshot.getValue(UserOverview.class);
                                if (userOverview == null) {
                                    callback.onFailure(GlobalVariables.RESULT_FAILURE);
                                    return;
                                }
                                userOverview.setId(dataSnapshot.getKey());
                                userOverview.setProfilePicUrl(GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE);
                                callback.onSuccess(userOverview);
                            }
                        });

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e(getClass().getName(), databaseError.getMessage());
                        callback.onFailure(GlobalVariables.RESULT_FAILURE);
                    }
                });
    }

    /**
     * TODO Erklären
     * @param limit limits the count of recieved playback objects
     * @param offsetPriority set -1 if there is no offset needed
     * @param callback
     */
    public void getTopSpots(int limit, int offsetPriority, ValueCallback callback) {
        List<Spot> result = new ArrayList<>();
        firestoreDatabase
                .collection(GlobalVariables.DB_PLAYBACK)
                .orderBy(GlobalVariables.DB_PRIORITY, Query.Direction.DESCENDING)
                .startAt(offsetPriority == -1 ? 1000 : offsetPriority)
                .limit(limit)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (queryDocumentSnapshots.isEmpty()) {
                            callback.onSuccess(result);
                            return;
                        }
                        int size = queryDocumentSnapshots.size();
                        for (DocumentSnapshot doc : queryDocumentSnapshots.getDocuments()) {
                            final Playback playback = doc.toObject(Playback.class);
                            if (playback == null) {
                                size--;
                                if (result.size() == size) {
                                    // Finished
                                    callback.onSuccess(result);
                                }
                            } else {

                                int finalSize = size;
                                userRepository.getUserOverViewById(playback.getUserId(), new ValueCallback() {
                                    @Override
                                    public void onSuccess(Object value) {
                                        UserOverview user = (UserOverview) value;
                                        Spot spot = new Spot(user, playback);
                                        result.add(spot);
                                        if (result.size() == finalSize) {
                                            // Finished
                                            callback.onSuccess(result);
                                        }
                                    }

                                    @Override
                                    public void onFailure(Object value) {
                                        callback.onSuccess(result);
                                    }
                                });
                            }
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        callback.onFailure(e.getMessage());
                    }
                });

    }

}
