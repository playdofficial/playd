package com.ansgarkersch.playd.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.activities.ActivityWatchProfile;
import com.ansgarkersch.playd.adapter.AdapterRecyclerMusicGenres;
import com.ansgarkersch.playd.adapter.AdapterRecyclerSuggestionGallery;
import com.ansgarkersch.playd.adapter.AdapterRecyclerTopSpots;
import com.ansgarkersch.playd.adapter.AdapterSubscriptions;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.IScrollable;
import com.ansgarkersch.playd.model.RowSearchHeader;
import com.ansgarkersch.playd.model.Spot;
import com.ansgarkersch.playd.model.User;
import com.ansgarkersch.playd.model.UserOverview;
import com.ansgarkersch.playd.repository.FirebaseRepository;
import com.ansgarkersch.playd.repository.PlaybackRepository;
import com.ansgarkersch.playd.repository.SpotRepository;
import com.ansgarkersch.playd.repository.SubscriptionRepository;
import com.ansgarkersch.playd.repository.UserRepository;
import com.ansgarkersch.playd.repository.ValueCallback;
import com.ansgarkersch.playd.util.UtilityMethods;
import com.ansgarkersch.playd.viewModel.MyUserViewModel;
import com.ansgarkersch.playd.viewModel.MyUserViewModelFactory;
import com.ansgarkersch.playd.viewModel._SpotViewModel;
import com.ansgarkersch.playd.viewModel._SpotViewModelFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FragmentExplorePlayd extends Fragment {

    // Global elements
    private View view;
    private User currentLoggedInUser;

    // Repositories
    private UserRepository userRepository;
    private PlaybackRepository playbackRepository;
    private FirebaseRepository firebaseRepository;
    private SubscriptionRepository subscriptionRepository;

    // View Models
    private MyUserViewModel myUserViewModel;

    // UI Elements
    private LinearLayout exploreContent, searchContent;

    /*
    Explore Content
     */
    private AdapterRecyclerSuggestionGallery suggestionGalleryAdapter;
    private AdapterRecyclerMusicGenres musicGenresAdapter;
    private RecyclerView genreGallery, topspotGallery;

    /*
    Search Content
     */
    private RecyclerView recyclerSearchUsers;
    private AdapterSubscriptions adapterRecyclerSearchUsers;
    private LinearLayout progressBarLinearlayoutSearch;
    private final int LIMIT = 15;

    // Search Content: Prevent unnecessary api calls
    private CountDownTimer broadcastRecieverTimer = null;
    private Map<String, List<UserOverview>> searchCache = new HashMap<>(); // TODO überprüfen
    private String currentSubstring = "";

    private List<IScrollable> userSuggestions; // TODO überprüfen


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_exploreplayd,
                container, false);

        // UI Elements
        exploreContent = view.findViewById(R.id.fragment_explorePlayd_exploreContent);
        searchContent = view.findViewById(R.id.fragment_explorePlayd_searchContent);

        // View Model
        myUserViewModel = ViewModelProviders.of(getActivity(), new MyUserViewModelFactory(getActivity())).get(MyUserViewModel.class);

        // Repositories
        userRepository = new UserRepository();
        playbackRepository = new PlaybackRepository(getActivity());
        firebaseRepository = new FirebaseRepository();
        subscriptionRepository = new SubscriptionRepository(getActivity());

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // UI elements "ExplorePlayd"
        genreGallery = view.findViewById(R.id.fragment_exploreplayd_recycler_genreGallery);
        // UI elements "SearchContent"
        recyclerSearchUsers = view.findViewById(R.id.fragment_explorePlayd_searchContent_recyclerView);
        progressBarLinearlayoutSearch = view.findViewById(R.id.fragment_explorePlayd_searchContent_progressbarLinearlayout);
        // Search Content: Prevent unnecessary api calls
        userSuggestions = new ArrayList<>();
        userSuggestions.add(new RowSearchHeader(getResources().getString(R.string.userSuggestions)));

        // View Model Observer
        myUserViewModel.getCurrentUser().observe(this, user -> {
            if (user == null || user.getFollowedIds() == null) {
                return;
            }

            currentLoggedInUser = user;
            initTopspotGallery();
            initGenreGallery();
            initSearchRecyclerView();

            /*
            // TODO JUST TESTING
            for (String id : user.getFollowedIds().keySet()) {
                userRepository.getUserOverViewById(id, new ValueCallback() {
                    @Override
                    public void onSuccess(Object value) {
                        UserOverview userOverview = (UserOverview) value;
                        userSuggestions.add(userOverview);
                    }

                    @Override
                    public void onFailure(Object value) {
                        userSuggestions.add(null);
                    }
                });

            }
            // TODO JUST TESTING
            */

        });

    }

    /**
     * Triggered by ActivityMain
     * @param username
     */
    public void onTextChanged(String username) {
        currentSubstring = username;

        // If the search-substring is empty, show the suggestions
        if (username.isEmpty()) {
            adapterRecyclerSearchUsers.setIScrollableData(userSuggestions); // TODO Überprüfen
            recyclerSearchUsers.setVisibility(View.VISIBLE);
            return;
        }

        // Check if search operation is necessary
        if (searchCache.containsKey(username)) {
            // New Search operation is not necessary
            // Update RecyclerView
            adapterRecyclerSearchUsers.setUserData(searchCache.get(username));
            recyclerSearchUsers.setVisibility(View.VISIBLE);
            return;
        }

        progressBarLinearlayoutSearch.setVisibility(View.VISIBLE);
        recyclerSearchUsers.setVisibility(View.GONE);

        // Delay 500ms to prevent unnecessary api calls
        if (broadcastRecieverTimer != null) {
            broadcastRecieverTimer.cancel();
        }
        broadcastRecieverTimer = new CountDownTimer(500, 1000) {
            public void onTick(long millisUntilFinished) { }

            public void onFinish() {

                userRepository.searchUsersByGivenSubstringUserName(username, LIMIT, new ValueCallback() {
                    @Override
                    public void onSuccess(Object value) {
                        List<UserOverview> data = (List<UserOverview>) value;
                        // Cache this search operation
                        searchCache.put(username, data);
                        progressBarLinearlayoutSearch.setVisibility(View.GONE);
                        if (data.size() == 0) {
                            // Show "No-Results" - Screen
                        } else {
                            // Update RecyclerView
                            adapterRecyclerSearchUsers.setUserData(data);
                            if (data.size() == LIMIT - 1) {
                                adapterRecyclerSearchUsers.addItem(new IScrollable() {
                                    @Override
                                    public int getType() {
                                        return GlobalVariables.VIEWTYPE_SHOWALLRESULTS;
                                    }
                                });
                            }
                            recyclerSearchUsers.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(Object value) {
                        progressBarLinearlayoutSearch.setVisibility(View.GONE);
                        // Show "No-Results" - Screen
                    }
                });

            }
        }.start();
    }

    /**
     * Open searchView
     * Triggered by ActivityMain
     */
    public void onOpenSearchView() {
        exploreContent.setVisibility(View.GONE);
        searchContent.setVisibility(View.VISIBLE);
    }

    /**
     * Close searchView
     * Triggered by ActivityMain
     */
    public void onCloseSearchView() {
        exploreContent.setVisibility(View.VISIBLE);
        searchContent.setVisibility(View.GONE);
    }

    /**
     *
     */
    private void initGenreGallery() {
        final RecyclerView musicGenreGallery = view.findViewById(R.id.fragment_exploreplayd_recycler_genreGallery);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(view.getContext(),
                LinearLayoutManager.HORIZONTAL, false);
        musicGenreGallery.setLayoutManager(horizontalLayoutManager);
        musicGenresAdapter = new AdapterRecyclerMusicGenres(view.getContext());
        musicGenreGallery.setAdapter(musicGenresAdapter);
        musicGenreGallery.setNestedScrollingEnabled(false);

        playbackRepository.getAllCurrentlyPlayingGenres(new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                List<String> genreData = (List<String>) value;
                if (genreData == null) {
                    return;
                }
                musicGenresAdapter.setMusicGenres(genreData);
                if (genreData.size() > 0) {
                    genreGallery.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Object value) {

            }
        });
    }

    /**
     *
     */
    private void initTopspotGallery() {


        final RecyclerView recyclerViewTopSpots = view.findViewById(R.id.fragment_exploreplayd_recycler_topspotGallery);
        //recyclerViewTopSpots.setHasFixedSize(true);

        /*
        StaggeredGridLayoutManager gridLayoutManager = new StaggeredGridLayoutManager(2,
                StaggeredGridLayoutManager.VERTICAL);
        recyclerViewTopSpots.setLayoutManager(gridLayoutManager);
        */
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(view.getContext(),
                LinearLayoutManager.VERTICAL, false);
        recyclerViewTopSpots.setLayoutManager(horizontalLayoutManager);

        AdapterRecyclerTopSpots adapterRecyclerTopSpots = new AdapterRecyclerTopSpots(getActivity());

        SpotRepository spotRepository = new SpotRepository(new UserRepository());
        spotRepository.getTopSpots(10, -1, new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                adapterRecyclerTopSpots.setTopSpots((List<Spot>) value);
            }

            @Override
            public void onFailure(Object value) {
                String s = "";
            }
        });

        /*
        final RecyclerView suggestionGallery = view.findViewById(R.id.fragment_exploreplayd_recycler_topspotGallery);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(view.getContext(),
                LinearLayoutManager.VERTICAL, false);
        suggestionGallery.setLayoutManager(horizontalLayoutManager);
        suggestionGalleryAdapter = new AdapterRecyclerSuggestionGallery(view.getContext());
        suggestionGallery.setNestedScrollingEnabled(false);
        suggestionGallery.setHasFixedSize(true);
        suggestionGallery.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        suggestionGallery.setNestedScrollingEnabled(true);

        suggestionGalleryAdapter.setClickListener(new AdapterRecyclerSuggestionGallery.ItemClickListener() {
            @Override
            public void onItemClick(View view, User user) {

                // TODO

            }
        });

        suggestionGallery.setAdapter(suggestionGalleryAdapter);
        */
    }



    /**
     * Initializes the Search Recylcer view
     */
    private void initSearchRecyclerView() {
        // Set up the RecyclerView
        final RecyclerView spots = view.findViewById(R.id.fragment_explorePlayd_searchContent_recyclerView);
        spots.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    UtilityMethods.hideKeyboard(getActivity());
                }
            }
        });
        adapterRecyclerSearchUsers = new AdapterSubscriptions(view.getContext(), currentLoggedInUser);
        adapterRecyclerSearchUsers.setClickListener(new AdapterSubscriptions.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                UserOverview userToOpen = (UserOverview) adapterRecyclerSearchUsers.getItemByPosition(position);
                Intent intent = new Intent(getActivity(), ActivityWatchProfile.class);
                intent.putExtra(GlobalVariables.INTENT_PROFILE_ID, userToOpen.getId());
                intent.putExtra(GlobalVariables.INTENT_PROFILE_USERNAME, userToOpen.getU());
                intent.putExtra(GlobalVariables.INTENT_PROFILE_CURRENT_USER, myUserViewModel.getCurrentUser().getValue());
                getActivity().startActivity(intent);
                getActivity().overridePendingTransition(R.anim.activity_slide_in_up, android.R.anim.fade_out);
            }

            @Override
            public void onSubscribeClick(View view, String userId) {

                subscriptionRepository.subscribeToUser(userId, new ValueCallback() {
                    @Override
                    public void onSuccess(Object value) {
                        // TODO show Subscribed!-Dialog
                    }

                    @Override
                    public void onFailure(Object value) {

                    }
                });

            }

            @Override
            public void onLoadMore(View view) {

            }
        });

        LinearLayoutManager llm = new LinearLayoutManager(view.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        spots.setLayoutManager(llm);
        spots.setAdapter(adapterRecyclerSearchUsers);
    }


}