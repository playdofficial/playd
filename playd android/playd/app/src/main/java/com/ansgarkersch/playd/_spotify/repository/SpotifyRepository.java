package com.ansgarkersch.playd._spotify.repository;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.ansgarkersch.playd._spotify.SpotifyEndpoints;
import com.ansgarkersch.playd._spotify.SpotifyGlobalVariables;
import com.ansgarkersch.playd._spotify.model.Artist;
import com.ansgarkersch.playd._spotify.model.Playlist;
import com.ansgarkersch.playd._spotify.model.Token;
import com.ansgarkersch.playd._spotify.model.Track;
import com.ansgarkersch.playd._spotify.model.User;
import com.ansgarkersch.playd._spotify.util.SpotifyUtilityMethods;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.Playback;
import com.ansgarkersch.playd.repository.ValueCallback;
import com.bumptech.glide.Glide;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;

import java.lang.ref.WeakReference;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SpotifyRepository {

    private SpotifyRestService spotifyRestService;
    private String TAG = "Class: SpotifyRepository - ";
    private SharedPreferences sharedPreferences;
    private final OkHttpClient okHttpClient;

    // Context
    private WeakReference<Context> weakContext;

    public SpotifyRepository(Context mContext) {
        weakContext = new WeakReference<>(mContext);
        sharedPreferences = mContext.getSharedPreferences(GlobalVariables.PACKAGE_NAME, Context.MODE_PRIVATE);

        // OkHttpClient
        okHttpClient = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .build();

        // Retrofit
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SpotifyEndpoints.GLOBAL_ENDPOINT)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        spotifyRestService = retrofit.create(SpotifyRestService.class);
    }

    /**
     * returns a AuthenticationRequest object
     * sample in activity:
     * start with AuthenticationClient.openLoginActivity(this, SpotifyGlobalVariables.AUTH_REQUEST_CODE, request);
     * end in onActivityResult
     */
    public AuthenticationRequest getSpotifyAuthenticationRequest() {
        AuthenticationRequest.Builder builder = new AuthenticationRequest.Builder(
                SpotifyGlobalVariables.CLIENT_ID,
                AuthenticationResponse.Type.CODE,
                SpotifyGlobalVariables.REDIRECT_URI);
        builder.setScopes(SpotifyGlobalVariables.SCOPE);
        builder.setState("a23fsc3rxd3");
        return builder.build();
    }

    /**
     * returns a refreshed accessToken
     * @param callback
     */
    public void getRefreshedAccessToken(final ValueCallback callback) {
        // Retrofit
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SpotifyEndpoints.TOKEN_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SpotifyRestService service = retrofit.create(SpotifyRestService.class);
        String refreshToken = sharedPreferences.getString(SpotifyGlobalVariables.REFRESH_TOKEN, "");
        service.getRefreshedAccessToken(
                SpotifyGlobalVariables.CLIENT_ID,
                SpotifyGlobalVariables.CLIENT_SECRET,
                SpotifyGlobalVariables.GRANT_TYPE_REFRESH_TOKEN,
                refreshToken)
                .enqueue(new Callback<Token>() {
            @Override
            public void onResponse(@NonNull Call<Token> call, @NonNull Response<Token> response) {
                if (response.code() != 200) {
                    callback.onFailure(GlobalVariables.RESULT_FAILURE);
                }
                Token token = response.body();
                if (token == null) {
                    return;
                }
                callback.onSuccess(token.getAccess_token());
            }

            @Override
            public void onFailure(@NonNull Call<Token> call, @NonNull Throwable t) {
                if (t.getMessage() != null) {
                    Log.d(TAG, t.getMessage());
                }
                if (t instanceof SocketTimeoutException){
                    callback.onFailure(GlobalVariables.RESULT_TIMEOUT);
                } else {
                    callback.onFailure(GlobalVariables.RESULT_FAILURE);
                }
            }
        });
    }

    /**
     * returns a fresh accessToken & a refreshToken in a token object
     * @param code
     * @param callback
     */
    public void getAllTokens(String code, final ValueCallback callback) {
        // Retrofit
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SpotifyEndpoints.TOKEN_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SpotifyRestService service = retrofit.create(SpotifyRestService.class);
        service.getAllTokens(SpotifyGlobalVariables.CLIENT_ID,
                SpotifyGlobalVariables.CLIENT_SECRET,
                code,
                SpotifyGlobalVariables.GRANT_TYPE_AUTH_CODE,
                SpotifyGlobalVariables.REDIRECT_URI)
                .enqueue(new Callback<Token>() {
            @Override
            public void onResponse(@NonNull Call<Token> call, @NonNull Response<Token> response) {
                if (response.code() != 200) {
                    callback.onFailure(GlobalVariables.RESULT_FAILURE);
                }
                Token token = response.body();
                if (token == null) {
                    return;
                }
                sharedPreferences.edit().putString(SpotifyGlobalVariables.REFRESH_TOKEN, token.getRefresh_token()).apply();
                callback.onSuccess(token);
            }

            @Override
            public void onFailure(@NonNull Call<Token> call, @NonNull Throwable t) {
                if (t.getMessage() != null) {
                    Log.d(TAG, t.getMessage());
                }
                if (t instanceof SocketTimeoutException){
                    callback.onFailure(GlobalVariables.RESULT_TIMEOUT);
                } else {
                    callback.onFailure(GlobalVariables.RESULT_FAILURE);
                }
            }
        });
    }

    /**
     * creates a playlist with the given parameters
     * @param name
     * @param description
     * @param isPublic
     * @param callback
     */
    public void createPlaylist(String name, String description, boolean isPublic, final ValueCallback callback) {
        getRefreshedAccessToken(new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                String accessToken = "Bearer " + value.toString();
                Map<String, Object> data = new HashMap<>();
                data.put(SpotifyGlobalVariables.NAME, name);
                data.put(SpotifyGlobalVariables.DESCRIPTION, description);
                data.put(SpotifyGlobalVariables.IS_PUBLIC, isPublic);
                spotifyRestService.createPlaylist(accessToken, data)
                        .enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                        callback.onFailure(GlobalVariables.RESULT_OK);
                    }

                    @Override
                    public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                        if (t.getMessage() != null) {
                            Log.d(TAG, t.getMessage());
                        }
                        if (t instanceof SocketTimeoutException){
                            callback.onFailure(GlobalVariables.RESULT_TIMEOUT);
                        } else {
                            callback.onFailure(GlobalVariables.RESULT_FAILURE);
                        }
                    }
                });
            }

            @Override
            public void onFailure(Object value) {
                if ((int) value == GlobalVariables.RESULT_TIMEOUT) {
                    callback.onFailure(GlobalVariables.RESULT_TIMEOUT);
                } else {
                    callback.onFailure(GlobalVariables.RESULT_FAILURE);
                }
            }
        });
    }

    /**
     * gets a playlists tracks
     * @param playlistId
     * @param limit
     * @param offset
     * @param callback
     */
    public void getPlaylistById(String playlistId, int limit, int offset, final ValueCallback callback) {
        getRefreshedAccessToken(new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                String accessToken = "Bearer " + value.toString();
                final String fieldparameter = "items(track(id,album(images),artists))";
                spotifyRestService.getPlaylist(accessToken, playlistId, limit, offset)
                        .enqueue(new Callback<Playlist>() {
                    @Override
                    public void onResponse(@NonNull Call<Playlist> call, @NonNull Response<Playlist> response) {
                        Playlist playlist = response.body();
                        if (playlist == null) {
                            callback.onFailure(GlobalVariables.RESULT_FAILURE);
                            return;
                        }
                        callback.onSuccess(playlist);
                    }

                    @Override
                    public void onFailure(@NonNull Call<Playlist> call, @NonNull Throwable t) {
                        if (t.getMessage() != null) {
                            Log.d(TAG, t.getMessage());
                        }
                        if (t instanceof SocketTimeoutException){
                            callback.onFailure(GlobalVariables.RESULT_TIMEOUT);
                        } else {
                            callback.onFailure(GlobalVariables.RESULT_FAILURE);
                        }
                    }
                });
            }

            @Override
            public void onFailure(Object value) {
                if ((int) value == GlobalVariables.RESULT_TIMEOUT) {
                    callback.onFailure(GlobalVariables.RESULT_TIMEOUT);
                }
                callback.onFailure(GlobalVariables.RESULT_FAILURE);
            }
        });
    }




    public void updatePlaybackInformation(Playback playback, final ValueCallback callback) {
        getRefreshedAccessToken(new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                String accessToken = value.toString();
                spotifyRestService.getTrackById("Bearer " + accessToken, playback.getTrackId())
                        .enqueue(new Callback<Track>() {
                            @Override
                            public void onResponse(@NonNull Call<Track> call, @NonNull Response<Track> response) {
                                Track track = response.body();
                                if (track == null) {
                                    if (response.code() == 401) {
                                        // Token expired
                                    }
                                    return;
                                }
                                if (weakContext.get() == null) {
                                    return;
                                }
                                Glide.with(weakContext.get().getApplicationContext()).load(track.getAlbum().getImages().get(0).getUrl()).preload(); // preload the albumArts
                                Glide.with(weakContext.get().getApplicationContext()).load(track.getAlbum().getImages().get(1).getUrl()).preload(); // preload the albumArts
                                playback.setTrackName(track.getName());
                                StringBuilder trackArtistName = new StringBuilder();
                                for (Artist artist : track.getArtists()) {
                                    trackArtistName.append(artist.getName()).append(", ");
                                }
                                playback.setTrackArtistName(trackArtistName.toString().substring(0, trackArtistName.length() - 2));
                                playback.setTrackArtistId(track.getArtists().get(0).getId());
                                playback.setAlbumArt_640(track.getAlbum().getImages().get(0).getUrl());
                                playback.setAlbumArt_300(track.getAlbum().getImages().get(1).getUrl());
                                playback.setTrackDuration(response.body().getDuration_ms());

                                // get genre
                                getGenresByArtist(track.getArtists().get(0).getId(), new ValueCallback() {
                                    @Override
                                    public void onSuccess(Object value) {
                                        Map<String, Boolean> trackGenres = (Map<String, Boolean>) value;
                                        playback.setTrackGenres(trackGenres);
                                        callback.onSuccess(playback);
                                    }

                                    @Override
                                    public void onFailure(Object value) {
                                        if ((int) value == GlobalVariables.RESULT_TIMEOUT) {
                                            callback.onFailure(GlobalVariables.RESULT_TIMEOUT);
                                        } else {
                                            callback.onFailure(GlobalVariables.RESULT_FAILURE);
                                        }
                                    }
                                });

                            }

                            @Override
                            public void onFailure(@NonNull Call<Track> call, @NonNull Throwable t) {
                                if (t.getMessage() != null) {
                                    Log.d(TAG, t.getMessage());
                                }
                                if (t instanceof SocketTimeoutException){
                                    callback.onFailure(GlobalVariables.RESULT_TIMEOUT);
                                } else {
                                    callback.onFailure(GlobalVariables.RESULT_FAILURE);
                                }
                            }
                        });
            }

            @Override
            public void onFailure(Object value) {
                if ((int) value == GlobalVariables.RESULT_TIMEOUT) {
                    callback.onFailure(GlobalVariables.RESULT_TIMEOUT);
                }
                callback.onFailure(GlobalVariables.RESULT_FAILURE);
            }
        });
    }

    public void getGenresByArtist(String artistId, final ValueCallback callback) {
        getRefreshedAccessToken(new ValueCallback() {
                    @Override
                    public void onSuccess(Object value) {
                        String accessToken = value.toString();
                        spotifyRestService.getArtistById("Bearer " + accessToken, artistId)
                                .enqueue(new Callback<Artist>() {
                                    @Override
                                    public void onResponse(@NonNull Call<Artist> call, @NonNull Response<Artist> response) {
                                        Artist a = response.body();
                                        if (a == null) {
                                            callback.onFailure(response.code());
                                            if (response.code() == 401) {
                                                // Token expired
                                            }
                                            return;
                                        }
                                        Map<String, Boolean> trackGenres = new HashMap<>();
                                        for (String genre : a.getGenres()) {
                                            trackGenres.put(genre, true);
                                        }
                                        callback.onSuccess(trackGenres);
                                    }

                                    @Override
                                    public void onFailure(@NonNull Call<Artist> call, @NonNull Throwable t) {
                                        if (t.getMessage() != null) {
                                            Log.d(TAG, t.getMessage());
                                        }
                                        if (t instanceof SocketTimeoutException){
                                            callback.onFailure(GlobalVariables.RESULT_TIMEOUT);
                                        } else {
                                            callback.onFailure(GlobalVariables.RESULT_FAILURE);
                                        }
                                    }
                                });
                    }

                    @Override
                    public void onFailure(Object value) {
                        if ((int) value == GlobalVariables.RESULT_TIMEOUT) {
                            callback.onFailure(GlobalVariables.RESULT_TIMEOUT);
                        } else {
                            callback.onFailure(GlobalVariables.RESULT_FAILURE);
                        }
                    }
                });
    }


    /**
     *
     * @param callback returns the user or the errorCode (TIMEOUT, NOT_PREMIUM or general FAILURE)
     */
    public void getCurrentUser(final ValueCallback callback) {
        getRefreshedAccessToken(new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                String accessToken = value.toString();
                spotifyRestService.getCurrentUser("Bearer " + accessToken)
                        .enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                                User currentUser = response.body();
                                if (currentUser == null) {
                                    callback.onFailure(response.code());
                                    if (response.code() == 401) {
                                        // Token expired
                                    }
                                    return;
                                }
                                if (currentUser.isPremium()) {
                                    callback.onSuccess(currentUser);
                                } else {
                                    callback.onFailure(GlobalVariables.SPOTIFY_RESULT_NOT_PREMIUM);
                                }
                            }

                            @Override
                            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                                if (t.getMessage() != null) {
                                    Log.d(TAG, t.getMessage());
                                }
                                if (t instanceof SocketTimeoutException){
                                    callback.onFailure(GlobalVariables.RESULT_TIMEOUT);
                                } else {
                                    callback.onFailure(GlobalVariables.RESULT_FAILURE);
                                }
                            }
                        });
            }

            @Override
            public void onFailure(Object value) {
                if ((int) value == GlobalVariables.RESULT_TIMEOUT) {
                    callback.onFailure(GlobalVariables.RESULT_TIMEOUT);
                } else {
                    callback.onFailure(GlobalVariables.RESULT_FAILURE);
                }
            }
        });
    }


    /**
     * Gets the current users top genres
     * @param callback returns an ArrayList<String> of the current users top genres
     */
    public void getCurrentUsersTopGenres(ValueCallback callback) {
        getRefreshedAccessToken(new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                String accessToken = "Bearer " + value.toString();
                spotifyRestService
                        .getUsersTopArtists(accessToken)
                        .enqueue(new Callback<List<Artist>>() {
                    @Override
                    public void onResponse(@NonNull Call<List<Artist>> call, @NonNull Response<List<Artist>> response) {
                        List<Artist> data = response.body();
                        if (data == null) {
                            callback.onSuccess(GlobalVariables.RESULT_FAILURE);
                            return;
                        }
                        Map<String, Boolean> genreMap = new HashMap<>();
                        for (Artist artist : data) {
                            for (String genre : artist.getGenres()) {
                                genreMap.put(genre, true);
                            }
                        }
                        callback.onSuccess(new ArrayList<>(genreMap.keySet()));
                    }

                    @Override
                    public void onFailure(@NonNull Call<List<Artist>> call, @NonNull Throwable t) {
                        if (t.getMessage() != null) {
                            Log.d(TAG, t.getMessage());
                        }
                        if (t instanceof SocketTimeoutException){
                            callback.onFailure(GlobalVariables.RESULT_TIMEOUT);
                        } else {
                            callback.onFailure(GlobalVariables.RESULT_FAILURE);
                        }
                    }
                });
            }

            @Override
            public void onFailure(Object value) {
                if ((int) value == GlobalVariables.RESULT_TIMEOUT) {
                    callback.onFailure(GlobalVariables.RESULT_TIMEOUT);
                } else {
                    callback.onFailure(GlobalVariables.RESULT_FAILURE);
                }
            }
        });


    }




    public void playTrack(Playback playback, final ValueCallback callback) {
        getRefreshedAccessToken(new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                String accessToken = "Bearer " + value.toString();
                Map<String, Object> trackData = new HashMap<>();
                trackData.put("uris", new String[]{SpotifyUtilityMethods.completeTrackId(playback.getTrackId())});
                spotifyRestService.playTrack(accessToken, trackData).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                        callback.onSuccess(response.code());
                    }

                    @Override
                    public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                        if (t.getMessage() != null) {
                            Log.d(TAG, t.getMessage());
                        }
                        if (t instanceof SocketTimeoutException){
                            callback.onFailure(GlobalVariables.RESULT_TIMEOUT);
                        } else {
                            callback.onFailure(GlobalVariables.RESULT_FAILURE);
                        }
                    }
                });
            }

            @Override
            public void onFailure(Object value) {
                if ((int) value == GlobalVariables.RESULT_TIMEOUT) {
                    callback.onFailure(GlobalVariables.RESULT_TIMEOUT);
                } else {
                    callback.onFailure(GlobalVariables.RESULT_FAILURE);
                }
            }
        });
    }

}
