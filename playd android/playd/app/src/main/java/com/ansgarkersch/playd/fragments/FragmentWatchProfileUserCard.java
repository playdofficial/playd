package com.ansgarkersch.playd.fragments;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.activities.ActivityWatchProfile;
import com.ansgarkersch.playd.adapter.AdapterRecyclerViewProfileGallery;
import com.ansgarkersch.playd.dialogfragments.DialogFragmentSingleImage;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.Gender;
import com.ansgarkersch.playd.model.Nationality;
import com.ansgarkersch.playd.model.RelationshipStatus;
import com.ansgarkersch.playd.model.User;
import com.ansgarkersch.playd.model.UserOverview;
import com.ansgarkersch.playd.repository.UserRepository;
import com.ansgarkersch.playd.repository.ValueCallback;
import com.ansgarkersch.playd.util.BlurBuilder;
import com.ansgarkersch.playd.util.UtilityMethods;
import com.ansgarkersch.playd.viewModel.WatchProfileViewModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class FragmentWatchProfileUserCard extends Fragment {

    // UI elements
    private View view;
    private LinearLayout subscriptionContent, topContent, bottomContent, messageContent, followedContent, followerContent;
    private ScrollView root;
    private ProgressBar loading;
    private RecyclerView imageGallery;
    private ImageButton back, more, notificationSettings; // notificationSettings to enable spot-notification
    private Button subscribe, message;
    private CircleImageView profileImage;
    private ImageView blurredProfileImage;
    private TextView username, userSubtitle, followedCount, followerCount, description;

    // Optionally UI elements
    private LinearLayout spotifyLinking, instagramLinking;
    private LinearLayout relationshipConnection_content;
    private TextView relationshipConnection_username;
    private CircleImageView relationshipConnection_profileImage;

    /**
     * Current opened User
     * Fetches the user from viewModel's "cachedUsers" by the given currentOpenedUserId
     */
    private User currentOpenedUser;
    private String currentOpenedUserId = "";

    // View Model
    private WatchProfileViewModel watchProfileViewModel;

    // Repository
    UserRepository userRepository;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_watchprofile_usercard,
                container, false);

        // Repository
        userRepository = new UserRepository();

        // init View Model
        watchProfileViewModel = ViewModelProviders.of(getActivity()).get(WatchProfileViewModel.class);

        initUIElements();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            // We just get the id from the userOverView at
            currentOpenedUserId = bundle.getString(GlobalVariables.CURRENT_OPENED_USER_ID);
        }

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() == null) {
            return;
        }

        watchProfileViewModel.getCurrentLoggedInUser().observe(getActivity(), new Observer<User>() {
            @Override
            public void onChanged(@Nullable User currentLoggedInUser) {
                if (currentLoggedInUser == null) {
                    return;
                }

                if (watchProfileViewModel.isUserAlreadyCached(currentOpenedUserId)) {
                    // User is already cached
                    currentOpenedUser = watchProfileViewModel.getCachedUserById(currentOpenedUserId);
                    // Initialize optionally Spotify & Instagram linking
                    initThirdPartyLinkings(currentOpenedUser);
                    modifyUI(); // Hide ImageGallery if there is no image inside
                    checkForRelationshipConnection();
                } else {
                    // User is not cached yet
                    fetchOpenedUser(currentOpenedUserId, new ValueCallback() {
                        @Override
                        public void onSuccess(Object value) {
                            modifyUI(); // Hide ImageGallery if there is no image inside
                            checkForRelationshipConnection();
                            // Initialize optionally Spotify & Instagram linking
                            initThirdPartyLinkings(watchProfileViewModel.getCachedUserById(currentOpenedUserId));
                        }

                        @Override
                        public void onFailure(Object value) {
                            checkForRelationshipConnection();
                        }
                    });

                }


            }
        });

    }

    /**
     * Initialize optionally Spotify & Instagram linking
     * @param user
     */
    private void initThirdPartyLinkings(User user) {
        // Spotfiy Linking
        spotifyLinking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://open.spotify.com/user/" + user.getSpotifyUserId()));
                startActivity(browserIntent);
            }
        });
    }

    private void fetchOpenedUser(String userId, final ValueCallback callback) {
        List<Boolean> promises = Collections.synchronizedList(new ArrayList<>());
        userRepository.getUserById(userId, new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                User fetchedUser = (User) value;
                currentOpenedUser = fetchedUser;
                watchProfileViewModel.cacheUser(fetchedUser);
                modifyUI(); // Hide ImageGallery if there is no image inside
                if (fetchedUser.getProfileImageList().size() == 0) {
                    callback.onSuccess(true);
                    return;
                }
                callback.onSuccess(true);

                for (String url : fetchedUser.getProfileImageList()) {
                    if (getActivity() == null) {
                        callback.onSuccess(false);
                        return;
                    }
                    Glide.with(getActivity()).load(url).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            callback.onSuccess(false);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            promises.add(true);
                            if (promises.size() == fetchedUser.getProfileImageList().size()) {
                                callback.onSuccess(true);
                            }
                            return true;
                        }
                    }).preload();
                }



            }

            @Override
            public void onFailure(Object value) {

            }
        });
    }


    private void checkForRelationshipConnection() {
        if (watchProfileViewModel.getCurrentLoggedInUser().getValue().getId().equals(currentOpenedUser.getId())) {
            // Cancel if the opened user is the logged-in user
            return;
        }

        /*
        Check for relationship connection between the opened user and the current-logged-in user
        */
        String relationshipConnectionUserId = null;
        for (String followerId : currentOpenedUser.getFollowersIds().keySet()) {
            List<String> data = new ArrayList<>(watchProfileViewModel.getCurrentLoggedInUser().getValue().getFollowedIds().keySet());
            if (data.indexOf(followerId) > -1) {
                // There is a connection! $followerId$ already subscribed to the fetchedUser
                relationshipConnectionUserId = followerId;
            }
        }

        if (relationshipConnectionUserId == null) {
            initUIFunctions();
        } else {
            if (watchProfileViewModel.isUserAlreadyCached(relationshipConnectionUserId)) {
                UserOverview user = watchProfileViewModel.getCachedUserById(relationshipConnectionUserId).getOverview();
                // Now we have the userOverview of the relationshipConnection between the openedUser and the currentLoggedInUser
                relationshipConnection_username.setText(user.getU());

                if (getActivity() == null) {
                    return;
                }

                Glide.with(getActivity())
                        .load(user.getProfilePicUrl()).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        relationshipConnection_content.setVisibility(View.VISIBLE);
                        relationshipConnection_content.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Fragment watchProfile = new FragmentWatchProfileUserCard();
                                Bundle bundle = new Bundle();
                                // Send currentOpenedUserId
                                bundle.putString(GlobalVariables.CURRENT_OPENED_USER_ID, user.getId());
                                watchProfile.setArguments(bundle);
                                ((ActivityWatchProfile) getActivity()).addFragmentOnTop(watchProfile, user.getU());
                            }
                        });
                        initUIFunctions();
                        return true;
                    }
                }).preload();
            }


            userRepository.getUserOverViewById(relationshipConnectionUserId, new ValueCallback() {
                @Override
                public void onSuccess(Object value) {
                    UserOverview user = (UserOverview) value;
                    // Now we have the userOverview of the relationshipConnection between the openedUser and the currentLoggedInUser
                    relationshipConnection_username.setText(user.getU());

                    if (getActivity() == null) {
                        return;
                    }

                    Glide.with(getActivity().getApplicationContext())
                            .load(user.getProfilePicUrl()).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            relationshipConnection_content.setVisibility(View.VISIBLE);
                            relationshipConnection_content.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Fragment watchProfile = new FragmentWatchProfileUserCard();
                                    Bundle bundle = new Bundle();
                                    // Send currentOpenedUserId
                                    bundle.putString(GlobalVariables.CURRENT_OPENED_USER_ID, user.getId());
                                    watchProfile.setArguments(bundle);
                                    ((ActivityWatchProfile) getActivity()).addFragmentOnTop(watchProfile, user.getU());
                                }
                            });
                            initUIFunctions();
                            return true;
                        }
                    }).preload();

                }

                @Override
                public void onFailure(Object value) {
                    initUIFunctions();
                }
            });
        }

    }

    /**
     * Hide ImageGallery if there is no image inside
     */
    private void modifyUI() {
        if (currentOpenedUser.getProfileImageList().size() == 0) {
            imageGallery.setVisibility(View.GONE);
        }
        if (!currentOpenedUser.getDescription().isEmpty()) {
            description.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Initializes all UI elements
     */
    private void initUIElements() {
        // LinearLayout
        subscriptionContent = view.findViewById(R.id.fragment_usercard_linearlayout_subscriptionContent);
        topContent = view.findViewById(R.id.fragment_usercard_linearlayout_topContent);
        bottomContent = view.findViewById(R.id.fragment_usercard_linearlayout_bottomContent);
        messageContent = view.findViewById(R.id.fragment_usercard_linearlayout_messageContent);
        followedContent = view.findViewById(R.id.fragment_usercard_linearlayout_followedContent);
        followerContent = view.findViewById(R.id.fragment_usercard_linearlayout_followerContent);
        // ScrollView
        root = view.findViewById(R.id.fragment_usercard_scrollview_root);
        // ProgressBar
        loading = view.findViewById(R.id.fragment_usercard_progressBar_loading);
        // RecyclerView
        imageGallery = view.findViewById(R.id.fragment_usercard_recyclerview_imageGallery);
        // ImageButton
        back = view.findViewById(R.id.fragment_usercard_imagebutton_back);
        more = view.findViewById(R.id.fragment_usercard_imagebutton_more);
        notificationSettings = view.findViewById(R.id.fragment_usercard_imagebutton_notificationSettings);
        // Button
        subscribe = view.findViewById(R.id.fragment_usercard_button_subscribe);
        message = view.findViewById(R.id.fragment_usercard_button_message);
        // CircleImageView
        profileImage = view.findViewById(R.id.fragment_usercard_circleimageiew_profileImage);
        // ImageView
        blurredProfileImage = view.findViewById(R.id.fragment_usercard_imageview_blurredProfileImage);
        // TextView
        username = view.findViewById(R.id.fragment_usercard_textview_username);
        userSubtitle = view.findViewById(R.id.fragment_usercard_textview_userSubtitle);
        description = view.findViewById(R.id.fragment_usercard_textview_description);
        followerCount = view.findViewById(R.id.fragment_usercard_textview_followerCount);
        followedCount = view.findViewById(R.id.fragment_usercard_textview_followedCount);

        relationshipConnection_content = view.findViewById(R.id.fragment_watchprofile_usercard_linearlayout_relationshipConnectionContent);
        relationshipConnection_username = view.findViewById(R.id.fragment_watchprofile_usercard_textview_relationshipConnection);
        relationshipConnection_profileImage = view.findViewById(R.id.fragment_watchprofile_usercard_circleimage_relationshipConnection);

        spotifyLinking = view.findViewById(R.id.fragment_watchprofile_usercard_linearLayout_spotifyLinkingContent);
        instagramLinking = view.findViewById(R.id.fragment_watchprofile_usercard_linearLayout_instagramLinkingContent);
    }

    /**
     * initializes all UI elements
     */
    @SuppressLint("ClickableViewAccessibility")
    private void initUIFunctions() {

        username.setText(currentOpenedUser.getUsername());

        if (currentOpenedUser.getUserSubtitle().isEmpty()) {
            userSubtitle.setVisibility(View.INVISIBLE);
        } else {
            userSubtitle.setText(currentOpenedUser.getUserSubtitle());
        }

        description.setText(currentOpenedUser.getDescription());

        if (GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE.equals(currentOpenedUser.getProfilePicUrl())) {
            // Hide Loading Progressbar and show Linearlayout
            root.setVisibility(View.VISIBLE);
            subscriptionContent.setVisibility(View.VISIBLE);
            loading.setVisibility(View.GONE);
        } else {

            if (getActivity() == null) {
                return;
            }

            Glide.with(getActivity().getApplicationContext()).load(currentOpenedUser.getProfilePicUrl()).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    blurredProfileImage.setImageBitmap(BlurBuilder.blur(getActivity().getApplicationContext(), ((BitmapDrawable)resource).getBitmap()));

                    // Hide Loading Progressbar and show Linearlayout
                    root.setVisibility(View.VISIBLE);
                    subscriptionContent.setVisibility(View.VISIBLE);
                    loading.setVisibility(View.GONE);

                    return true;
                }
            }).preload();
        }

        if (getActivity() == null) {
            return;
        }

        Glide.with(getActivity()).load(
                GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE.equals(currentOpenedUser.getProfilePicUrl())
                        ?
                        R.mipmap.no_profile_image
                        :
                        currentOpenedUser.getProfilePicUrl()).into(profileImage);

        // Set up the RecyclerView
        final RecyclerView imageGalleryView = view.findViewById(R.id.fragment_usercard_recyclerview_imageGallery);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);
        imageGalleryView.setLayoutManager(horizontalLayoutManager);

        List<String> galleryData = currentOpenedUser.getProfileImageList();
        if (galleryData.size() > 0) {
            galleryData.remove(0);
        }
        AdapterRecyclerViewProfileGallery adapter = new AdapterRecyclerViewProfileGallery(getActivity(),
                galleryData);

        imageGalleryView.setNestedScrollingEnabled(false);
        imageGalleryView.setHasFixedSize(true);
        imageGalleryView.setLayoutManager(new GridLayoutManager(getActivity(), 3));

        adapter.setClickListener(new AdapterRecyclerViewProfileGallery.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                android.support.v4.app.DialogFragment image = DialogFragmentSingleImage.
                        newInstance(GlobalVariables.DIALOG_GALLERY_IMAGE_INTENT_EXTRA_PLACEHOLDER,
                                galleryData.get(position));
                image.show(getActivity().getSupportFragmentManager(), "");
            }
        });

        imageGalleryView.setAdapter(adapter);

        /*
        Set Followed and Followers
        */

        // Set Followed
        int followedSize = currentOpenedUser.getFollowedIds().size();
        if (followedSize <= 0) {
            followedCount.setText("0");
        } else {
            followedCount.setText(String.valueOf(followedSize));
        }
        // Set Followers
        int followerSize = currentOpenedUser.getFollowersIds().size();
        if (followerSize <= 0) {
            followerCount.setText("0");
        } else {
            followerCount.setText(String.valueOf(followerSize));
        }

        // Followed
        if (followedSize > 0) {
            followedContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    followedContent.setEnabled(false);
                    Fragment watchFollowed = new FragmentWatchProfileSubscriptions();
                    Bundle bundle = new Bundle();
                    bundle.putString(GlobalVariables.INTENT_SUBSCRIPTION_TYPE, GlobalVariables.SUBSCRIPTION_TYPE_FOLLOWED);
                    bundle.putString(GlobalVariables.CURRENT_OPENED_USER_ID, currentOpenedUser.getId());
                    watchFollowed.setArguments(bundle);
                    ((ActivityWatchProfile) getActivity()).addFragmentOnTop(watchFollowed, currentOpenedUser.getUsername());
                    followedContent.setEnabled(true);
                }
            });
        }

        // Follower
        if (followerSize > 0) {
            followerContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    followerContent.setEnabled(false);
                    Fragment watchFollowers = new FragmentWatchProfileSubscriptions();
                    Bundle bundle = new Bundle();
                    bundle.putString(GlobalVariables.INTENT_SUBSCRIPTION_TYPE, GlobalVariables.SUBSCRIPTION_TYPE_FOLLOWERS);
                    bundle.putString(GlobalVariables.CURRENT_OPENED_USER_ID, currentOpenedUser.getId());
                    watchFollowers.setArguments(bundle);
                    ((ActivityWatchProfile) getActivity()).addFragmentOnTop(watchFollowers, currentOpenedUser.getUsername());
                    followerContent.setEnabled(true);
                }
            });
        }

    }

}
