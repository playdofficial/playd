package com.ansgarkersch.playd.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.Advertisement;
import com.ansgarkersch.playd.model.IScrollable;
import com.ansgarkersch.playd.model.Playback;
import com.ansgarkersch.playd.model.RowMyPlaydFinish;
import com.ansgarkersch.playd.model.RowMyPlaydSpotExplorer;
import com.ansgarkersch.playd.model.RowMyPlaydUserSuggestions;
import com.ansgarkersch.playd.model.Spot;
import com.ansgarkersch.playd.model.User;
import com.ansgarkersch.playd.model.UserOverview;
import com.ansgarkersch.playd.util.UtilityMethods;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterRecyclerMyPlaydFeed extends RecyclerView.Adapter<AdapterRecyclerMyPlaydFeed.ViewHolder> {

    // List Data
    private Map<String, Spot> spotMap = new HashMap<>();
    private List<IScrollable> spotList = new ArrayList<>();

    // Advertisement
    private List<Advertisement> adList = new ArrayList<>();
    private int adFrequency;

    // User Suggestions
    private List<User> userSuggestions = new ArrayList<>();

    private LayoutInflater mInflater;
    private Context context;
    private AdapterRecyclerMyPlaydFeed.ItemClickListener mClickListener;

    private Playback currentlyPlayingPlayback = null;
    private String currentlyPlayingUserId;
    private boolean isPlaybackLoading = false; // is false if the playback is started, true if a playback is 'in the queue' but it has not started yet
    private boolean isFeedLoading = false;

    // data is passed into the constructor
    public AdapterRecyclerMyPlaydFeed(Context context, int adFrequency) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.adFrequency = adFrequency;
    }

    /**
     * todo erklären
     * @param currentlyPlayingUserId
     */
    public void markSelectedSpotAsPlaying(String currentlyPlayingUserId) {
        this.currentlyPlayingUserId = currentlyPlayingUserId;
        isPlaybackLoading = false;
        notifyDataSetChanged();
    }

    /**
     * todo erklären
     * @param currentlyPlayingUserId
     */
    public void markSelectedSpotAsLoading(String currentlyPlayingUserId) {
        this.currentlyPlayingUserId = currentlyPlayingUserId;
        isPlaybackLoading = true;
        notifyDataSetChanged();
    }

    /*
    public void notifyPlaybackChanged(Playback changedPlayback) {
        Spot spot = spotMap.get(changedPlayback.getUserId());
        if (spot == null) {
            return;
        }
        spot.setPlayback(changedPlayback);
        spotMap.put(spot.getId(), spot);
        int index = spotList.indexOf(spot);
        if (index == -1) {
            return;
        }
        spotList.set(index, spot);
        notifyItemChanged(index);
    }
    */

    public void notifyPlaybackDeleted(String deletedPlaybackId) {
        IScrollable cursor = new Spot(deletedPlaybackId);
        spotList.remove(cursor);
        notifyDataSetChanged();
    }

    public void setAds(List<Advertisement> adList) {
        this.adList = adList;
    }

    public void setUserSuggestions(List<User> userSuggestions) {
        this.userSuggestions = userSuggestions;
    }


    public void updateMyPlaydFeed(Map<String, Spot> data) {
        // Clear the spotlist to guarantee there are won't be duplicates
        spotList.clear();

        // Add all Spot, e.g. all IScrollable objects to the spotList
        spotList.addAll(data.values());
        // Sort the spotList
        Collections.sort(spotList, new Comparator<IScrollable>() {
            @Override
            public int compare(IScrollable a, IScrollable b) {
                if (a instanceof RowMyPlaydSpotExplorer) {
                    return 1;
                }
                Spot s1 = (Spot) a;
                Spot s2 = (Spot) b;
                return Long.compare(s2.getPlayback().getSpotStartedTimestamp(), s1.getPlayback().getSpotStartedTimestamp());
            }
        });
        // Insert ads or other items at the right places
        // TODO
        spotList.add(0, new RowMyPlaydSpotExplorer()); // todo - just testing

        // Finish the operation
        notifyDataSetChanged();
    }




    public void addToSpotList(IScrollable iScrollable) {
        if (spotList.indexOf(iScrollable) > -1) {
            spotList.set(spotList.indexOf(iScrollable), iScrollable);
        } else {
            spotList.add(iScrollable);
        }
        if (spotList.size() % adFrequency == 0) {
            spotList.add(adList.get(0)); // TODO
        }
        notifyDataSetChanged();
    }

    public void addItemsToSpotList(List<IScrollable> spots) {
        // Add all spots to the adapter's spot list
        spotList.addAll(spots);

        // Insert Ads
        int j = 0;
        for (int i = 0; i < spots.size(); i++) {
            if (i != 0 && i % adFrequency == 0) {
                j++;
                // Insert adds at this position (but in the adapters spot list)
                spotList.add(j + i, adList.get(0));
            }
        }

        //spotList.add(new RowMyPlaydFinish());

        notifyDataSetChanged();
    }

    private int getPosition(Spot spot) {
        return spotList.indexOf(spot);
    }

    private int getPosition(String userId) {
        return getPosition(new Spot(userId));
    }

    public void setLoadingIcon() {
        if (spotList.get(spotList.size() - 1) instanceof RowMyPlaydFinish) {
            return;
        }
        /*
        addToSpotList(new IScrollable() {
            @Override
            public int getType() {
                return GlobalVariables.VIEWTYPE_LOADING_ICON;
            }
        });
        */
    }

    public void hideLoadingIcon() {
        int listSize = spotList.size();
        if (listSize == 0) {
            return;
        }
        Iterator<IScrollable> iterator = spotList.iterator();
        while (iterator.hasNext()) {
            IScrollable iScrollable = iterator.next();
            if (iScrollable.getType() == GlobalVariables.VIEWTYPE_LOADING_ICON) {
                iterator.remove();
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return spotList.get(position).getType();
    }


    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public AdapterRecyclerMyPlaydFeed.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case GlobalVariables.VIEWTYPE_SPOT:
                view = mInflater.inflate(R.layout.row_myplayd_spot, parent, false);
                break;
            case GlobalVariables.VIEWTYPE_SPOTEXPLORER:
                view = mInflater.inflate(R.layout.row_myplayd_spotexplorer, parent, false);
                break;
            case GlobalVariables.VIEWTYPE_USERSUGGESTIONS:
                view = mInflater.inflate(R.layout.row_myplayd_usersuggestions, parent, false);
                break;
            case GlobalVariables.VIEWTYPE_ALLSPOTSSEEN:
                view = mInflater.inflate(R.layout.row_myplayd_allspotsseen, parent, false);
                break;
            case GlobalVariables.VIEWTYPE_ADVERTISEMENT:
                view = mInflater.inflate(R.layout.row_myplayd_advertisement, parent, false);
                break;
            default:
                view = mInflater.inflate(R.layout.row_myplayd_loading, parent, false);
                break;
        }

        return new AdapterRecyclerMyPlaydFeed.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(@NonNull final AdapterRecyclerMyPlaydFeed.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final IScrollable currentElement = spotList.get(position);

        isFeedLoading = false; // todo

        if (currentElement instanceof Spot) {

            holder.setSpotEntry(currentElement);

        } else if (currentElement instanceof RowMyPlaydSpotExplorer) {
            /*
            Element is a SpotExplorer View
             */
            final RowMyPlaydSpotExplorer rowMyPlaydSpotExplorer = (RowMyPlaydSpotExplorer) currentElement;
            holder.informationSpotExplorerContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mClickListener.onClickSpotExplorer(view, 0); // todo set actionCode
                }
            });

        } else if (currentElement instanceof RowMyPlaydUserSuggestions) {
            /*
            Element is an UserSuggestions View
             */
            final RowMyPlaydUserSuggestions rowMyPlaydUserSuggestions = (RowMyPlaydUserSuggestions) currentElement;

        } else if (currentElement instanceof RowMyPlaydFinish) {
            /*
            Element is an Information View (e.g. "All spots already seen - Message")
             */
            // no need to create object
            //final RowMyPlaydFinish rowMyPlaydFinish = (RowMyPlaydFinish) currentElement;

        } else if (currentElement instanceof Advertisement) {
            /*
            Element is an Advertisement
             */
            final Advertisement advertisement = (Advertisement) currentElement;

        } else {
            // Item is the Progress Icon
            isFeedLoading = true; // todo
        }

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return spotList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        /*
        Element Spot
         */
        ImageView albumArt, albumArtAlphaBackground, musicPlayingIndicatorPlayingIcon;
        CircleImageView profileImage;
        TextView userName, spotStartedTimestamp, distance, spotName, trackName, artistName, currentSpotterCountWithRelationship, suggestedForYou;
        CoordinatorLayout musicPlayingIndicator;
        ProgressBar musicPlayingIndicatorLoadingIcon;
        LinearLayout spotDetailContent;
        /*
        Element SpotExplorer
         */
        LinearLayout informationSpotExplorerContent;
        /*
        Element AllSpotsSeen
         */
        View dividerLeft, dividerRight;
        ImageView centerImageAllSpotsSeen;
        TextView informationAllSpotsSeen;

        ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            /*
            Element Spot
             */
            // ImageView
            albumArt = itemView.findViewById(R.id.myplayd_row_spots_imageview_albumArt);
            albumArtAlphaBackground = itemView.findViewById(R.id.myplayd_row_spots_imageview_albumArtAlphaBackground);
            musicPlayingIndicatorPlayingIcon = itemView.findViewById(R.id.myplayd_row_spots_imageview_musicPlayingIndicatorPlayingIcon);
            // CircleImageView
            profileImage = itemView.findViewById(R.id.myplayd_row_spots_circleimageview_profileImage);
            // TextView
            userName = itemView.findViewById(R.id.myplayd_row_spots_textview_username);
            spotName = itemView.findViewById(R.id.myplayd_row_spots_textview_spotName);
            spotStartedTimestamp = itemView.findViewById(R.id.myplayd_row_spots_textview_spotStartedTimestamp);
            distance = itemView.findViewById(R.id.myplayd_row_spots_textview_distance);
            trackName = itemView.findViewById(R.id.myplayd_row_spots_textview_trackName);
            artistName = itemView.findViewById(R.id.myplayd_row_spots_textview_artistName);
            currentSpotterCountWithRelationship = itemView.findViewById(R.id.myplayd_row_spots_textview_currentSpotterCountWithRelationship);
            suggestedForYou = itemView.findViewById(R.id.myplayd_row_spots_textview_suggestedForYou);
            // CoordinatorLayout
            musicPlayingIndicator = itemView.findViewById(R.id.myplayd_spots_musicPlayingIndicator);
            // ProgressBar
            musicPlayingIndicatorLoadingIcon = itemView.findViewById(R.id.myplayd_row_spots_textview_progressbar_musicPlayingIndicatorLoadingIcon);
            // LinearLayout
            spotDetailContent = itemView.findViewById(R.id.myplayd_row_spots_linearlayout_spotDetailContent);

            /*
            Element SpotExplorer
             */
            informationSpotExplorerContent = itemView.findViewById(R.id.row_myplayd_spotexplorer_content);

            /*
            Element UserSuggestions
             */
            // todo

            /*
            Element Information
             */
            // todo

            /*
            Element Advertisement
             */
            // todo

            /*
            Element All Spots Seen
             */
            dividerLeft = itemView.findViewById(R.id.row_myplayd_allspotsseen_divider_left);
            dividerRight = itemView.findViewById(R.id.row_myplayd_allspotsseen_divider_right);
            centerImageAllSpotsSeen = itemView.findViewById(R.id.row_myplayd_allspotsseen_centerImage);
            informationAllSpotsSeen = itemView.findViewById(R.id.row_myplayd_allspotsseen_text);

        }


        public void setSpotEntry(IScrollable entry) {
            final Spot currentSpot = (Spot) entry;
            final UserOverview currentUser = currentSpot.getUser();
            final Playback currentPlayback = currentSpot.getPlayback();

            Resources resources = context.getResources();




            Glide.with(context).load(
                    GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE.equals(currentUser.getProfilePicUrl())
                            ?
                            R.mipmap.no_profile_image
                            :
                            currentUser.getProfilePicUrl()).into(profileImage);

            userName.setText(currentUser.getU());

            if (currentPlayback.getSpotName() != null && !currentPlayback.getSpotName().equals("")) {
                spotName.setText(currentPlayback.getSpotName());
            } else {
                spotName.setText("");
            }

            /*
            TODO calculate distance with GeoFirestore (or GeoFirebase?)
             */
            String distanceText = "66" + " " +
                    resources.getString(R.string.kilometer) + " " +
                    resources.getString(R.string.away);

            //holder.distance.setText(distance);
            distance.setVisibility(View.INVISIBLE);

            /*
            TODO fetch spotterCounter from RealTimeDatabase
             */

            //String spotterCounter = currentPlayback.getSpotterCount() + " " + context.getResources().getString(R.string.spotter);
            //holder.spotterCounter.setText(spotterCounter);

            // Calculate spotStartedTimeStampInMinutes
            new Thread(new Runnable() {
                @Override
                public void run() {
                    spotStartedTimestamp
                            .setText(UtilityMethods
                                    .getCalculatedSpotStartedTimestampInMinutesAsText(
                                            context,
                                            UtilityMethods.calculateSpotStartedTimestampInMinutes
                                                    (currentPlayback.getSpotStartedTimestamp())));
                }
            }).start();


            /*
             ************** ONCLICK PROFILE PIC **************
             */
            profileImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mClickListener != null) {
                        //pause();
                        mClickListener.onClickOpenAccountWindow(view, currentUser);
                    }
                }
            });
            userName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mClickListener != null) {
                        //pause();
                        mClickListener.onClickOpenAccountWindow(view, currentUser);
                    }
                }
            });

            /*
             ************** TRACK INFORMATION **************
             */
            String albumArtUrl300 = currentPlayback.getAlbumArt_300();

            Glide.with(context).load(
                    "".equals(albumArtUrl300)
                            ?
                            R.mipmap.no_profile_image
                            :
                            albumArtUrl300).into(albumArt);

            Glide.with(context).load(
                    "".equals(albumArtUrl300)
                            ?
                            R.mipmap.no_profile_image
                            :
                            albumArtUrl300).into(albumArtAlphaBackground);

            artistName.setText(currentPlayback.getTrackArtistName());
            trackName.setText(currentPlayback.getTrackName());

            albumArt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (currentPlayback.getUserId().equals(currentlyPlayingUserId)) {
                        currentlyPlayingPlayback = null;
                        if (mClickListener != null) {
                            //pause();
                            mClickListener.onClickPlayback(view, currentSpot, false);
                        }
                        // stop the playback
                    } else {
                        currentlyPlayingPlayback = currentPlayback;
                        if (mClickListener != null) {
                            //play(currentPlayback);
                            mClickListener.onClickPlayback(view, currentSpot, true);
                        }
                    }
                    notifyDataSetChanged();

                }
            });

            if (isPlaybackLoading) {
                albumArt.setEnabled(false);
            } else {
                albumArt.setEnabled(true);
            }

            if (currentPlayback.getUserId().equals(currentlyPlayingUserId)) {
                if (isPlaybackLoading) {
                    // Show isPlaybackLoading icon
                    musicPlayingIndicator.setVisibility(View.VISIBLE);
                    musicPlayingIndicatorLoadingIcon.setVisibility(View.VISIBLE);
                    musicPlayingIndicatorPlayingIcon.setBackground(null);
                } else {
                    // Show play icon
                    musicPlayingIndicator.setVisibility(View.VISIBLE);
                    musicPlayingIndicatorLoadingIcon.setVisibility(View.GONE);
                    musicPlayingIndicatorPlayingIcon.setBackground(resources.getDrawable(R.drawable.circle_border_transparent_red));
                }
            } else {
                musicPlayingIndicator.setVisibility(View.GONE);
            }


            // TODO TEST begin !!!!!

            if (currentSpot.getId().equals("nh1v4mPIj7hTOVfDXpGoLU57sSw1") || currentSpot.getId().equals("fIDdQ23GEraJagmvFbI5tTAL2NK2")) {
                suggestedForYou.setVisibility(View.VISIBLE);
                spotDetailContent.setVisibility(View.GONE);
            } else {
                suggestedForYou.setVisibility(View.GONE);
                spotDetailContent.setVisibility(View.VISIBLE);
            }

            final int random = new Random().nextInt(61) + 20;

            if (currentSpot.getId().equals("wly1337")) {
                currentSpotterCountWithRelationship.setText("aliciaberger und " + random + " andere");
            } else {
                currentSpotterCountWithRelationship.setText(random + " hören zu");
            }

            if (currentSpot.getId().equals("nh1v4mPIj7hTOVfDXpGoLU57sSw1") || currentSpot.getId().equals("fIDdQ23GEraJagmvFbI5tTAL2NK2")) {
                suggestedForYou.setVisibility(View.VISIBLE);
                spotDetailContent.setVisibility(View.GONE);
            } else {
                suggestedForYou.setVisibility(View.GONE);
                spotDetailContent.setVisibility(View.VISIBLE);
            }

            // TODO TEST end !!!!!

        }

        @Override
        public void onClick(View view) { }
    }

    // allows clicks events to be caught
    public void setCurrentPlaybackListener(AdapterRecyclerMyPlaydFeed.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onClickPlayback(View view, Spot spot, boolean isPlaying);
        void onClickOpenAccountWindow(View view, UserOverview user);
        void onClickSpotExplorer(View view, int actionCode);
    }

}
