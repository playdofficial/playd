package com.ansgarkersch.playd.repository;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.events.SubscriptionEvent;
import com.ansgarkersch.playd.util.UtilityMethods;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessaging;

import org.greenrobot.eventbus.EventBus;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubscriptionRepository {

    // Repositories
    private FirebaseRepository firebaseRepository;

    // Firebase Services
    private FirebaseFirestore firestoreDatabase;
    private FirebaseMessaging firebaseMessaging;
    private DatabaseReference firebaseDatabase;

    // Util
    private String userId;

    // Context
    private WeakReference<Context> weakContext;

    public SubscriptionRepository(Context mContext) {
        weakContext = new WeakReference<>(mContext);

        // Repositories
        firebaseRepository = new FirebaseRepository();

        // Firebase Services
        firestoreDatabase = FirebaseFirestore.getInstance();
        firebaseMessaging = FirebaseMessaging.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance().getReference();

        // Util
        userId = firebaseRepository.getCurrentUser().getUid();
    }


    /**
     * subscribes to the given user
     * @param foreignUserId
     * @param callback
     */
    public void subscribeToUser(final String foreignUserId, final ValueCallback callback) {
        notifyFirebaseAboutSubscription(foreignUserId, new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                int promiseSize = (int) value;
                // Count of backend operations to be done by notifyFirebaseAboutSubscription()
                if (promiseSize >= 4) { // 4 if no friendship is born, 5 if there is a new friendship born
                    notifyComponentsAboutSubscription(foreignUserId);
                    callback.onSuccess(GlobalVariables.RESULT_OK);
                }
            }

            @Override
            public void onFailure(Object value) {
                callback.onSuccess(GlobalVariables.RESULT_FAILURE);
            }
        });

    }


    public void unsubscribeFromUser(final String foreignUserId, final ValueCallback callback) {
        notifyFirebaseAboutUnsubscription(foreignUserId, new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                int promiseSize = (int) value;
                // Count of backend operations to be done by notifyFirebaseAboutUnsubscription()
                if (promiseSize >= 4) {
                    notifyComponentsAboutUnsubscription(foreignUserId);
                    callback.onSuccess(GlobalVariables.RESULT_OK);
                }
            }

            @Override
            public void onFailure(Object value) {
                callback.onSuccess(GlobalVariables.RESULT_FAILURE);
            }
        });

    }

    /**
     * Subscribes to a user
     * Does the following things:
     * - Update followed entry
     * - Update followers entry
     * - Subscribe to foreign users topic
     * - Notify the foreign user about the subscription by FCM
     * -
     * @param foreignUserId
     * @param callback
     */
    private void notifyFirebaseAboutSubscription(String foreignUserId, final ValueCallback callback) {

        List<Boolean> promises = Collections.synchronizedList(new ArrayList<>());
        long subscriptionTimestamp = UtilityMethods.getCurrentTime();

        /*
        Update FOLLOWED entry
         */
        Map<String, Object> subscribeData = new HashMap<>();
        subscribeData.put(foreignUserId, subscriptionTimestamp);

        firestoreDatabase.collection(GlobalVariables.DB_FOLLOWED)
                .document(userId)
                .update(subscribeData)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d(getClass().getName(), "Successfully updated FOLLOWED entry with id " + userId);
                        promises.add(true);
                        callback.onSuccess(promises.size());
                    }
                }).addOnCanceledListener(new OnCanceledListener() {
            @Override
            public void onCanceled() {
                Log.d(getClass().getName(), "Updating FOLLOWED entry with id " + userId + " was canceled");
                callback.onFailure(GlobalVariables.RESULT_CANCELED);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(getClass().getName(), "Updating FOLLOWED entry with id " + userId + " failed: " + e.getMessage());
                callback.onFailure(GlobalVariables.RESULT_FAILURE);
            }
        });

        /*
        Update FOLLOWERS entry
         */
        subscribeData.clear();
        subscribeData.put(userId, subscriptionTimestamp);

        firestoreDatabase.collection(GlobalVariables.DB_FOLLOWERS)
                .document(foreignUserId)
                .update(subscribeData)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d(getClass().getName(), "Successfully updated FOLLOWERS entry with id " + foreignUserId);
                        promises.add(true);
                        callback.onSuccess(promises.size());
                    }
                }).addOnCanceledListener(new OnCanceledListener() {
            @Override
            public void onCanceled() {
                Log.d(getClass().getName(), "Updating FOLLOWERS entry with id " + foreignUserId + " was canceled");
                callback.onFailure(GlobalVariables.RESULT_CANCELED);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(getClass().getName(), "Updating FOLLOWERS entry with id " + foreignUserId + " failed: " + e.getMessage());
                callback.onFailure(GlobalVariables.RESULT_FAILURE);
            }
        });

        /*
        Subscribe to foreign users TOPIC
         */
        firebaseMessaging.subscribeToTopic(foreignUserId)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d(getClass().getName(), "Successfully subscribed to user with id " + foreignUserId);
                        promises.add(true);
                        callback.onSuccess(promises.size());
                    }
                }).addOnCanceledListener(new OnCanceledListener() {
            @Override
            public void onCanceled() {
                Log.d(getClass().getName(), "Subscribing to user with id " + foreignUserId + " was canceled");
                callback.onFailure(GlobalVariables.RESULT_FAILURE);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(getClass().getName(), "Subscribing to user with id " + foreignUserId + " failed: " + e.getMessage());
                callback.onFailure(GlobalVariables.RESULT_FAILURE);
            }
        });

        /*
        Notify the foreign user about the subscription by FCM
         */
        firebaseRepository.sendMessageToDevice(
                foreignUserId,
                GlobalVariables.FCM_CATEGORY_USERRELATIONSHIP,
                GlobalVariables.FCM_ACTIONCODE_SUBSCRIBED,
                new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                Log.d(getClass().getName(), "Successfully sent a message to user with id " + foreignUserId);
                promises.add(true);
                callback.onSuccess(promises.size());
            }

            @Override
            public void onFailure(Object value) {
                Log.d(getClass().getName(), "Send message to user with id " + foreignUserId + " failed");
                callback.onFailure(GlobalVariables.RESULT_FAILURE);
            }
        });

        /*
        If a friendship is born:
        1) Notify the UI
        2) Create a realtimeDatabase entry 'chats' and push a unique chatRoomKey in:
        - /chats/ownUserId/foreignUserId/$chatroomId$
        - /chats/foreignUserId/ownUserId/$chatroomId$
         */
        firestoreDatabase.collection(GlobalVariables.DB_FOLLOWERS).document(userId).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists() && documentSnapshot.getData() != null) {
                    if (documentSnapshot.getData().keySet().contains(foreignUserId)) {
                        // A friendship is born!
                        // Create Chatroom Ids for both users
                        String chatroomId = String.valueOf(UtilityMethods.getCurrentTime());
                        Map<String, Object> chatRoomIdData = new HashMap<>();
                        chatRoomIdData.put(foreignUserId, chatroomId);
                        // Chatroom entry /chatroomIds/currentUser/foreignuser:$chatroomId$
                        firebaseDatabase.child(GlobalVariables.DB_CHATROOMIDS).child(userId).updateChildren(chatRoomIdData);
                        chatRoomIdData.clear();
                        chatRoomIdData.put(userId, chatroomId);
                        // Chatroom entry /chatroomIds/foreignuser/currentUser:$chatroomId$
                        firebaseDatabase.child(GlobalVariables.DB_CHATROOMIDS).child(foreignUserId).updateChildren(chatRoomIdData);
                    }
                    promises.add(true);
                    callback.onSuccess(promises.size());
                } else {
                    promises.add(true);
                    callback.onSuccess(promises.size());
                }
            }
        }).addOnCanceledListener(new OnCanceledListener() {
            @Override
            public void onCanceled() {
                callback.onFailure(GlobalVariables.RESULT_FAILURE);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onFailure(GlobalVariables.RESULT_FAILURE);
            }
        });

    }

    /**
     * todo erklären
     * The function works like this:
     * 1) Add the foreignUserId to the own FOLLOWED userIds
     * 2) Add the own userId to the foreign user's FOLLOWERS userIds
     * 3) Subscribe to the foreign user's topic to receive FCM's about playback started
     * @param foreignUserId
     * @param callback
     */
    private void notifyFirebaseAboutUnsubscription(String foreignUserId, final ValueCallback callback) {

        List<Boolean> promises = Collections.synchronizedList(new ArrayList<>());

        /*
        Update FOLLOWED entry
         */
        Map<String, Object> unsubscribeData = new HashMap<>();
        unsubscribeData.put(foreignUserId, FieldValue.delete());

        firestoreDatabase.collection(GlobalVariables.DB_FOLLOWED)
                .document(userId)
                .update(unsubscribeData)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d(getClass().getName(), "Successfully deleted FOLLOWED entry with id " + userId);
                        promises.add(true);
                        callback.onSuccess(promises.size());
                    }
                }).addOnCanceledListener(new OnCanceledListener() {
            @Override
            public void onCanceled() {
                Log.d(getClass().getName(), "Deleting FOLLOWED entry with id " + userId + " was canceled");
                callback.onFailure(GlobalVariables.RESULT_CANCELED);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(getClass().getName(), "Deleting FOLLOWED entry with id " + userId + " failed: " + e.getMessage());
                callback.onFailure(GlobalVariables.RESULT_FAILURE);
            }
        });

        /*
        Delete FOLLOWERS entry
         */
        unsubscribeData.clear();
        unsubscribeData.put(userId, FieldValue.delete());

        firestoreDatabase.collection(GlobalVariables.DB_FOLLOWERS)
                .document(foreignUserId)
                .update(unsubscribeData)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d(getClass().getName(), "Successfully deleted FOLLOWERS entry with id " + foreignUserId);
                        promises.add(true);
                        callback.onSuccess(promises.size());
                    }
                }).addOnCanceledListener(new OnCanceledListener() {
            @Override
            public void onCanceled() {
                Log.d(getClass().getName(), "Deleting FOLLOWERS entry with id " + foreignUserId + " was canceled");
                callback.onFailure(GlobalVariables.RESULT_CANCELED);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(getClass().getName(), "Deleting FOLLOWERS entry with id " + foreignUserId + " failed: " + e.getMessage());
                callback.onFailure(GlobalVariables.RESULT_FAILURE);
            }
        });

        /*
        Unsubscribe from foreign users TOPIC
         */
        firebaseMessaging.unsubscribeFromTopic(foreignUserId)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d(getClass().getName(), "Successfully unsubscribed from user with id " + foreignUserId);
                        promises.add(true);
                        callback.onSuccess(promises.size());
                    }
                }).addOnCanceledListener(new OnCanceledListener() {
            @Override
            public void onCanceled() {
                Log.d(getClass().getName(), "Unsubscribing from user with id " + foreignUserId + " was canceled");
                callback.onFailure(GlobalVariables.RESULT_FAILURE);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(getClass().getName(), "Unsubscribing from user with id " + foreignUserId + " failed: " + e.getMessage());
                callback.onFailure(GlobalVariables.RESULT_FAILURE);
            }
        });

        /*
        Notify the foreign user about the unsubscription by FCM
         */
        firebaseRepository.sendMessageToDevice(
                foreignUserId,
                GlobalVariables.FCM_CATEGORY_USERRELATIONSHIP,
                GlobalVariables.FCM_ACTIONCODE_UNSUBSCRIBED,
                new ValueCallback() {
                    @Override
                    public void onSuccess(Object value) {
                        Log.d(getClass().getName(), "Successfully sent a message to user with id " + foreignUserId);
                        promises.add(true);
                        callback.onSuccess(promises.size());
                    }

                    @Override
                    public void onFailure(Object value) {
                        Log.d(getClass().getName(), "Send message to user with id " + foreignUserId + " failed");
                        callback.onFailure(GlobalVariables.RESULT_FAILURE);
                    }
                });

    }



    /**
     * Notifies all neccessary app components about the subscription the current logged-in user just did
     * @param foreignUserId
     */
    private void notifyComponentsAboutSubscription(String foreignUserId) {
        SubscriptionEvent subscriptionEvent = new SubscriptionEvent();
        subscriptionEvent.setActionCode(GlobalVariables.FCM_ACTIONCODE_SUBSCRIBED);
        subscriptionEvent.setActiveParticipant(userId);
        subscriptionEvent.setPassiveParticipant(foreignUserId);

        // Use GreenRobot EventBus to notify the app components about subscription
        EventBus.getDefault().post(subscriptionEvent);
        Log.d(getClass().getName(),
                "A SubscriptionEvent with the actionCode "
                        + subscriptionEvent.getActionCode()
                        + " was broadcasted by Greenrobot. Active: "
                        + subscriptionEvent.getActiveParticipant()
                        + " | Passive: "
                        + subscriptionEvent.getPassiveParticipant());
    }

    /**
     * Notifies all neccessary app components about the unsubscription the current logged-in user just did
     * @param foreignUserId
     */
    private void notifyComponentsAboutUnsubscription(String foreignUserId) {
        SubscriptionEvent subscriptionEvent = new SubscriptionEvent();
        subscriptionEvent.setActionCode(GlobalVariables.FCM_ACTIONCODE_UNSUBSCRIBED);
        subscriptionEvent.setActiveParticipant(userId);
        subscriptionEvent.setPassiveParticipant(foreignUserId);

        // Use GreenRobot EventBus to notify the app components about subscription
        EventBus.getDefault().post(subscriptionEvent);
        Log.d(getClass().getName(),
                "A SubscriptionEvent with the actionCode "
                        + subscriptionEvent.getActionCode()
                        + " was broadcasted by Greenrobot. Active: "
                        + subscriptionEvent.getActiveParticipant()
                        + " | Passive: "
                        + subscriptionEvent.getPassiveParticipant());
    }


    /**
     * Resubscribes to the topics of all the followed users
     * Reason: The information about the topic subscription of a user may be lost. This information is very
     * important, without, the app won't get any notifications about a playbackstate event
     * @param followedUIds a list of all followed userIds of the current logged-in user
     * @param callback returns the size of successful topic subscriptions. This value should be equals the size of the list followedUids
     */
    public void resubscribeToAllFollowedUsers(final List<String> followedUIds, final ValueCallback callback) {

        List<Boolean> promises = Collections.synchronizedList(new ArrayList<>());

        for (String followedId : followedUIds) {
            firebaseMessaging.subscribeToTopic(followedId)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Log.d(getClass().getName(), "Successfully subscribed to user with id " + followedId);
                            promises.add(true);
                            callback.onSuccess(promises.size());
                        }
                    }).addOnCanceledListener(new OnCanceledListener() {
                @Override
                public void onCanceled() {
                    Log.d(getClass().getName(), "Subscribing to user with id " + followedId + " was canceled");
                    callback.onFailure(GlobalVariables.RESULT_FAILURE);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d(getClass().getName(), "Subscribing to user with id " + followedId + " failed: " + e.getMessage());
                    callback.onFailure(GlobalVariables.RESULT_FAILURE);
                }
            });
        }
    }




}
