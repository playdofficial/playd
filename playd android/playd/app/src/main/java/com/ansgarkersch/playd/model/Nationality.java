package com.ansgarkersch.playd.model;

import android.content.Context;

import com.ansgarkersch.playd.R;

public enum Nationality {
    NOT_YOUR_BUSINESS, DE, GB, US, TR, IT;

    public static Nationality getNationalityBySpinnerPosition(int position) {
        return Nationality.values()[position];
    }

    public static String getNationalityNameInLanguage(Context context, Nationality nationality) {
        switch (nationality) {
            case NOT_YOUR_BUSINESS:
                return context.getResources().getString(R.string.notYourBusiness);
            case DE:
                return context.getResources().getString(R.string.countryshort_de);
            case GB:
                return context.getResources().getString(R.string.countryshort_gb);
            case US:
                return context.getResources().getString(R.string.countryshort_us);
            case TR:
                return context.getResources().getString(R.string.countryshort_tr);
            case IT:
                return context.getResources().getString(R.string.countryshort_it);
        }
        return "Heaven";
    }
}
