package com.ansgarkersch.playd.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.IScrollable;
import com.ansgarkersch.playd.model.RowSearchHeader;
import com.ansgarkersch.playd.model.User;
import com.ansgarkersch.playd.model.UserOverview;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterSubscriptions extends RecyclerView.Adapter<AdapterSubscriptions.ViewHolder> {

    private List<String> followedList;
    private List<String> newFollowersList = new ArrayList<>();;

    private List<IScrollable> userData;
    private User currentLoggedIn;
    private LayoutInflater mInflater;
    private Context context;
    private AdapterSubscriptions.ItemClickListener mClickListener;

    private String subscriptionType = "";

    // data is passed into the constructor
    public AdapterSubscriptions(Context context, User currentLoggedIn) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.currentLoggedIn = currentLoggedIn;
        this.followedList = new ArrayList<>(currentLoggedIn.getFollowedIds().keySet());
        this.userData = new ArrayList<>();
    }

    // Inflates the row layout from xml when needed
    @NonNull
    @Override
    public AdapterSubscriptions.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case GlobalVariables.VIEWTYPE_USEROVERVIEW:
                view = mInflater.inflate(R.layout.row_useroverview, parent, false);
                break;
            case GlobalVariables.VIEWTYPE_SHOWALLRESULTS:
                view = mInflater.inflate(R.layout.row_search_seeallresults, parent, false);
                break;
            default:
                view = mInflater.inflate(R.layout.row_search_title, parent, false);
                break;
        }
        return new AdapterSubscriptions.ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return userData.get(position).getType();
    }

    public void addItem(IScrollable item) {
        userData.add(item);
        notifyDataSetChanged();
    }

    public void setIScrollableData(List<IScrollable> data) {
        Iterator<IScrollable> iterator = data.iterator();
        while (iterator.hasNext()) {
            IScrollable iScrollable = iterator.next();
            if (iScrollable == null) {
                iterator.remove();
            }
        }
        this.userData = data;
        notifyDataSetChanged();
    }

    public void setUserData(List<UserOverview> userData) {
        // Remove all null-values
        List<IScrollable> items = new ArrayList<>();
        Iterator<UserOverview> iterator = userData.iterator();
        while (iterator.hasNext()) {
            IScrollable iScrollable = iterator.next();
            if (iScrollable == null) {
                iterator.remove();
            } else {
                items.add(iScrollable);
            }
        }
        this.userData = items;
        notifyDataSetChanged();
    }

    public void setUserData(List<UserOverview> userData, String subscriptionType) {
        this.subscriptionType = subscriptionType;
        setUserData(userData);
    }

    public void setNewFollowers(List<String> newFollowersList) {
        this.newFollowersList = newFollowersList;
    }

    public void setFollowedList(List<String> followedList) {
        this.followedList = followedList;
        notifyDataSetChanged();
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(@NonNull final AdapterSubscriptions.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (userData == null) {
            return;
        }
        IScrollable listItem = userData.get(position);

        switch (listItem.getType()) {
            case GlobalVariables.VIEWTYPE_USEROVERVIEW:
                UserOverview currentUser = (UserOverview) userData.get(position);
                if (currentUser == null || currentLoggedIn == null) {
                    return;
                }

                // Profile Image
                Glide.with(context).load(
                        GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE.equals(currentUser.getProfilePicUrl())
                                ?
                                R.mipmap.no_profile_image
                                :
                                currentUser.getProfilePicUrl()).into(holder.profilePic);

                /*
                 * check if the user is a friend of myself or a famous person
                 */
        /*
        if (userData.get(position).isFollowed()) { // user is a friend
            holder.badgeAdditionalUserInformation.setVisibility(View.VISIBLE);
            holder.badgeAdditionalUserInformation.setImageDrawable(context.getResources().getDrawable(R.mipmap.ic_heart_white_24dp));
            holder.badgeAdditionalUserInformation.setBackground(context.getResources().getDrawable(R.drawable.circle_gradient_bordered_profilepic));
        } else if (userData.get(position).isIsFamous()) { // user is a famous person
            holder.badgeAdditionalUserInformation.setVisibility(View.VISIBLE);
            holder.badgeAdditionalUserInformation.setPadding(2,2,2,2);
            holder.badgeAdditionalUserInformation.setImageDrawable(context.getResources().getDrawable(R.mipmap.ic_star_white_24dp));
            holder.badgeAdditionalUserInformation.setBackground(context.getResources().getDrawable(R.drawable.circle_gradient_famous_profile));
        } else {
            holder.badgeAdditionalUserInformation.setVisibility(View.GONE);
        }
        */

                // remove divider after last entry
                if (position == (getItemCount() - 1)) {
                    holder.divider.setVisibility(View.GONE);
                } else {
                    holder.divider.setVisibility(View.VISIBLE);
                }

                // check subscriptionType, maybe there are new followers to highlight
                if (newFollowersList != null && subscriptionType.equals(GlobalVariables.SUBSCRIPTION_TYPE_FOLLOWERS)) {
                    if (newFollowersList.contains(currentUser.getId())) {
                        holder.flagNew.setVisibility(View.VISIBLE);
                    } else {
                        holder.flagNew.setVisibility(View.GONE);
                    }
                }

                if (currentUser.getId().equals(currentLoggedIn.getId())) {
                    holder.name.setText(context.getResources().getString(R.string.you));
                    holder.subscriptionOptionsSubscribeNow.setVisibility(View.GONE);
                    holder.subscriptionOptionsSubscribed.setVisibility(View.GONE);
                    return;
                } else {
                    holder.name.setText(currentUser.getU());
                }

                // 'SubscribeButton' Or 'AlreadySubscribedButton'
                if (followedList != null
                        && followedList.size() != 0
                        && followedList.contains(currentUser.getId())) {
                    // user is already subscribed
                    holder.subscriptionOptionsSubscribeNow.setVisibility(View.GONE);
                    holder.subscriptionOptionsSubscribed.setVisibility(View.VISIBLE);
                } else {
                    holder.subscriptionOptionsSubscribed.setVisibility(View.GONE);
                    holder.subscriptionOptionsSubscribeNow.setVisibility(View.VISIBLE);
                    holder.subscriptionOptionsSubscribeNow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            holder.subscriptionOptionsSubscribeNow.setVisibility(View.GONE);
                            holder.subscriptionOptionsSubscribed.setVisibility(View.VISIBLE);
                            if (mClickListener != null) {
                                mClickListener.onSubscribeClick(v, currentUser.getId());
                            }
                        }
                    });
                }

                // Check if user is currently playing music
                if (currentUser.isT()) {
                    // User is a spot at this moment
                    holder.relativeLayoutPlayingIndicator
                            .setBackground(context.getResources().getDrawable(R.mipmap.playd_circle_only_orange));
                    holder.imageViewPlayingIndicator.setVisibility(View.VISIBLE);
                } else {
                    // User is not a spot at this moment
                    holder.relativeLayoutPlayingIndicator.setBackground(null);
                    holder.imageViewPlayingIndicator.setVisibility(View.GONE);

                }


                break;
            case GlobalVariables.VIEWTYPE_SHOWALLRESULTS:
                holder.seeAllResultsContent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mClickListener != null) {
                            mClickListener.onLoadMore(view);
                        }
                    }
                });
                break;
            case GlobalVariables.VIEWTYPE_HEADERTITLE:
                RowSearchHeader rowSearchHeader = (RowSearchHeader) listItem;
                holder.headerTitle.setText(rowSearchHeader.getTitle());
                break;
        }

    }

    // total number of rows
    @Override
    public int getItemCount() {
        if (userData == null) {
            return 0;
        } else {
            return userData.size();
        }
    }

    public IScrollable getItemByPosition(int position) {
        return userData.get(position);
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        /*
        User Overview
         */
        CircleImageView profilePic;
        CircleImageView badgeAdditionalUserInformation;
        TextView name, flagNew;
        View divider;
        LinearLayout subscriptionOptionsSubscribeNow, subscriptionOptionsSubscribed;
        RelativeLayout relativeLayoutPlayingIndicator;
        ImageView imageViewPlayingIndicator;
        /*
        See all results
         */
        LinearLayout seeAllResultsContent;

        /*
        Title
         */
        TextView headerTitle;

        ViewHolder(View itemView) {
            super(itemView);
            /*
            User Overview
             */
            profilePic = itemView.findViewById(R.id.row_useroverview_profilePic);
            //badgeAdditionalUserInformation = itemView.findViewById(R.id.spots_small_badge_additionalUserInformation);
            name = itemView.findViewById(R.id.spots_small_name);
            flagNew = itemView.findViewById(R.id.spots_small_flag_new);
            divider = itemView.findViewById(R.id.spots_small_divider);
            subscriptionOptionsSubscribeNow = itemView.findViewById(R.id.spots_small_subscribe_now);
            subscriptionOptionsSubscribed = itemView.findViewById(R.id.spots_small_subscribed);
            relativeLayoutPlayingIndicator = itemView.findViewById(R.id.row_useroverview_relativeLayout_playingIndicator);
            imageViewPlayingIndicator = itemView.findViewById(R.id.row_useroverview_image_playingIndicator);
            itemView.setOnClickListener(this);

            /*
            See all results
             */
            seeAllResultsContent = itemView.findViewById(R.id.row_search_seeallresults_mainContent);

            /*
            Title
             */
            headerTitle = itemView.findViewById(R.id.row_search_title_text);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null && userData.get(getAdapterPosition()).getType() == GlobalVariables.VIEWTYPE_USEROVERVIEW) {
                UserOverview user = (UserOverview) userData.get(getAdapterPosition());
                if (!user.getId().equals(currentLoggedIn.getId())) {
                    mClickListener.onItemClick(view, getAdapterPosition());
                }
            }

        }
    }

    // allows clicks events to be caught
    public void setClickListener(AdapterSubscriptions.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
        void onSubscribeClick(View view, String userId);
        void onLoadMore(View view);
    }

}
