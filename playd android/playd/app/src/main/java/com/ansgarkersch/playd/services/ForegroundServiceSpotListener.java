package com.ansgarkersch.playd.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.os.Binder;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd._spotify.SpotifyGlobalVariables;
import com.ansgarkersch.playd._spotify.repository.PlayTrackRepository;
import com.ansgarkersch.playd._spotify.repository.SpotifyRepository;
import com.ansgarkersch.playd._spotify.util.SpotifyUtilityMethods;
import com.ansgarkersch.playd.activities.ActivityMain;
import com.ansgarkersch.playd.exceptions.SpotifyInvalidTokenException;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.Playback;
import com.ansgarkersch.playd.model.Spot;
import com.ansgarkersch.playd.repository.ValueCallback;
import com.ansgarkersch.playd.util.UtilityMethods;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.spotify.android.appremote.api.ConnectionParams;
import com.spotify.android.appremote.api.Connector;
import com.spotify.android.appremote.api.SpotifyAppRemote;
import com.spotify.android.appremote.api.error.AuthenticationFailedException;
import com.spotify.android.appremote.api.error.SpotifyConnectionTerminatedException;
import com.spotify.android.appremote.api.error.SpotifyDisconnectedException;
import com.spotify.protocol.client.CallResult;
import com.spotify.protocol.client.ErrorCallback;
import com.spotify.protocol.client.Subscription;
import com.spotify.protocol.error.SpotifyAppRemoteException;
import com.spotify.protocol.types.Empty;
import com.spotify.protocol.types.PlayerState;

/**
 * TODO comment
 */
public class ForegroundServiceSpotListener extends Service implements ServiceConnectionListenerCallbacks {

    private String TAG = getClass().getName();

    private Subscription<PlayerState> playbackDetailSubscription; // to detect the end of the track
    private boolean forcePaused = false;

    private CountDownTimer playbackDetailTimer = null; // CountDownTimer to avoid multiple BroadcastReciever Messages
    private CountDownTimer playbackCurrentMillisTimer = null; // todo erklären
    private CountDownTimer playbackWaitingForSpotTimer = null; // todo erklären
    private CountDownTimer playbackConnectingToSpotTimer = null; // todo erklären

    // Current-track-specific informations
    private long currentTrackMillisUntilFinished = 0L;
    private long currentTrackDuration = 0L;

    // Firebase Services
    private DatabaseReference firebaseRealtimeDatabase;
    private DatabaseReference playbackOverviewReference;

    // Child Event Listener
    private ChildEventListener childEventListener;

    // Service Connection Listener
    ServiceConnectionListener mServiceConnectionListener;
    boolean mBoundConnectionListener = false;

    // AudioManager for seeking playbackPosition
    private AudioManager mAudioManager;
    private int volume;

    /*
    Notification
     */
    private NotificationManager mNotificationManager;
    // Notification Channel Ids
    private final int spotListeningNotificationId = 2908; // The unique spot listening notification id
    private final int spotFinishedNotificationId = 2403; // The unique spot finished notification id
    // Notification Codes
    private final int NOTIFICATIONCODE_SPOT_FINISHED_ITSELF = 0;
    private final int NOTIFICATIONCODE_SPOT_WAS_FINISHED_BY_USER = 1;

    // Spotify SDK
    private SpotifyAppRemote mSpotifyAppRemote;
    private PlayerState mPlayerState;

    // Firebase Firestore
    //private FirebaseFirestore firestoreDatabase = FirebaseFirestore.getInstance();



    //private ListenerRegistration playbackListener;

    // Util
    private final int millisToWait = 800; // After X milliseconds, the user's playback gets updated
    private boolean isPlaying = false;
    // Binding to Activities
    private boolean isBound = false;
    private boolean isCanceled = false;
    private IBinder mBinder = new ForegroundServiceSpotListener.LocalBinder();
    // Callbacks
    private ForegroundServiceSpotListenerCallbacks foregroundServiceSpotListenerCallbacks;
    // Current playback information
    private Spot currentSpot;
    private String currentlyPlayingTrackId;
    // Repositories
    private SpotifyRepository spotifyRepository;
    private PlayTrackRepository playTrackRepository;

    private boolean nextTrackInQueue = false; // TODO - Brainstorming

    public void setCallbacks(ForegroundServiceSpotListenerCallbacks foregroundServiceSpotListenerCallbacks) {
        this.foregroundServiceSpotListenerCallbacks = foregroundServiceSpotListenerCallbacks;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            if (intent.hasCategory(GlobalVariables.SPOTIFY_PLAY)) {
                Log.e(getClass().getName(), "Recieved Start Command 'Play Track'");
                Spot recievedSpot = (Spot) intent.getSerializableExtra(GlobalVariables.INTENT_SPOT);
                play(recievedSpot);
            } else if (intent.hasCategory(GlobalVariables.SPOTIFY_PAUSE)) {
                Log.e(getClass().getName(), "Recieved Start Command 'Pause Track'");
                pause();
            } else if (intent.hasCategory(GlobalVariables.SPOTIFY_ABORT)) {
                // todo
            }
        }
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Firebase Services
        firebaseRealtimeDatabase = FirebaseDatabase.getInstance().getReference();

        // Repositories
        playTrackRepository = new PlayTrackRepository(this, mSpotifyAppRemote);
        spotifyRepository = new SpotifyRepository(this);

        Notification notification = buildListeningNotification();
        startForeground(spotListeningNotificationId, notification);

        mNotificationManager =
                (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(spotFinishedNotificationId);

        // AudioManager for seeking playbackPosition
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        setCurrentVolume();

        // Bind to ServiceConnectionListener
        Intent intentConnectionListener = new Intent(this, ServiceConnectionListener.class);
        bindService(intentConnectionListener, mConnectionConnectionListener, Context.BIND_IMPORTANT);
        Log.e(getClass().getName(), "ServiceConnectionListener is binded to ActivityMain");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disconnect();
        unfollowCurrentUser();

        // Unbind ServiceConnectionListener
        unbindService(mConnectionConnectionListener);

        if (mServiceConnectionListener != null) {
            mServiceConnectionListener.setCallbacks(null); // unregister
        }
    }

    @Override
    public void isInternetAccessible(boolean isAccessible) {
        if (isAccessible) {
            if (currentSpot != null && currentSpot.getPlayback() != null) {
                play(currentSpot);
            }
        } else {
            stopForeground(true);
            stopSelf();
            if (mSpotifyAppRemote != null) {
                SpotifyAppRemote.disconnect(mSpotifyAppRemote);
            }
            isCanceled = true;

            // TODO Call notifyPaused in UI Thread

        }
    }

    @Override
    public void isGPSAccessible(boolean isAccessible) {

    }

    /**
     * Init Binding to ServiceConnectionListener
     */
    private ServiceConnection mConnectionConnectionListener = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ServiceConnectionListener.LocalBinder binder = (ServiceConnectionListener.LocalBinder) service;
            mServiceConnectionListener = binder.getService();
            mBoundConnectionListener = true;

            if (mServiceConnectionListener == null) {
                return;
            }

            mServiceConnectionListener.setCallbacks(ForegroundServiceSpotListener.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBoundConnectionListener = false;
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        isBound = true;
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        isBound = false;
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        isBound = true;
        super.onRebind(intent);
    }

    /**
     * Returns an instance of LocalService so clients can call public methods
     */
    public class LocalBinder extends Binder {
        public ForegroundServiceSpotListener getService() {
            return ForegroundServiceSpotListener.this;
        }
    }

    /*
    TODO COMMENT
    */
    private Notification buildListeningNotification() {

        if (playbackConnectingToSpotTimer != null) {
            playbackConnectingToSpotTimer.cancel();
        }

        // Build Notification
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext(), "notify_2908");
        Intent ii = new Intent(getApplicationContext(), ActivityMain.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, ii, 0);

        mBuilder.setOnlyAlertOnce(true);
        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.drawable.ic_playd_actionbar);
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setSound(null);

        if (currentSpot == null) {
            mBuilder.setContentTitle("Verbinde mit Spotify");
            playbackConnectingToSpotTimer = new CountDownTimer(10000, 1000) {
                @Override
                public void onTick(long l) { }

                @Override
                public void onFinish() {
                    notifyPaused();
                }
            }.start();
        } else {
            mBuilder.setContentTitle("Du hörst gerade bei " + currentSpot.getUser().getU() + " mit!");
            if (currentSpot.getPlayback().getSpotName() != null && !currentSpot.getPlayback().getSpotName().equals("")) {
                mBuilder.setContentText(currentSpot.getPlayback().getSpotName());
            }
        }

        mNotificationManager =
                (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        // Handle older Android Versions
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("notify_2908",
                    getClass().getName(),
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setSound(null, null);
            mNotificationManager.createNotificationChannel(channel);
        }

        return mBuilder.build();
    }

    /**
     * TODO erklären
     * @return
     */
    private Notification buildSpotFinishedNotification(int notificationCode) {

        // Build Notification
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext(), "notify_2403");
        Intent ii = new Intent(getApplicationContext(), ActivityMain.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, ii, 0);

        mBuilder.setOnlyAlertOnce(true);
        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.drawable.ic_playd_actionbar);
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setSound(null);

        if (currentSpot != null) {
            switch (notificationCode) {
                case NOTIFICATIONCODE_SPOT_WAS_FINISHED_BY_USER:
                    mBuilder.setContentTitle("Du hast aufgehört, bei " + currentSpot.getUser().getU() + " mitzuhören");
                    break;
                case NOTIFICATIONCODE_SPOT_FINISHED_ITSELF:
                    mBuilder.setContentTitle(currentSpot.getUser().getU() + " hat aufgehört Musik zu hören");
                    break;
            }
        }

        // Gets sometimes called when it not supposed to be
        /*
        if (currentSpot == null) {
            mBuilder.setContentTitle("Verbinde mit Spot");
        }
        */

        // Handle older Android Versions
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("notify_2403",
                    "Playd Foreground Service Spot Listener",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setSound(null, null);
            mNotificationManager.createNotificationChannel(channel);
        }

        return mBuilder.build();
    }


    private void editNotification() {
        Notification notification = buildListeningNotification();
        mNotificationManager.notify(spotListeningNotificationId, notification);
    }

    /**
     * Connects to the Spotify Service
     */
    private void connect(final ValueCallback callback) {
        if (mSpotifyAppRemote != null) {
            return;
        }

        ConnectionParams connectionParams = new ConnectionParams.Builder(SpotifyGlobalVariables.CLIENT_ID)
                .setRedirectUri(SpotifyGlobalVariables.REDIRECT_URI)
                .showAuthView(false)
                .build();

        SpotifyAppRemote.setDebugMode(true); // TODO - disable outside of developement
        SpotifyAppRemote.connect(this, connectionParams, new Connector.ConnectionListener() {
            @Override
            public void onConnected(SpotifyAppRemote spotifyAppRemote) {
                Log.e(getClass().getName(), "SpotifyAppRemote - " + "Successfully initialized");

                if (isCanceled) {
                    SpotifyAppRemote.disconnect(spotifyAppRemote);
                    Log.e(getClass().getName(), "SpotifyAppRemote - " + "Service was canceled: Manually disconnected");
                }
                mSpotifyAppRemote = spotifyAppRemote;

                playbackDetailSubscription = mSpotifyAppRemote
                        .getPlayerApi()
                        .subscribeToPlayerState()
                        .setEventCallback(new Subscription.EventCallback<PlayerState>() {
                            @Override
                            public void onEvent(PlayerState playerState) {

                                // Playback Detail
                                if (playbackDetailTimer != null) {
                                    playbackDetailTimer.cancel();
                                }
                                // Playback Progress
                                if (playbackCurrentMillisTimer != null) {
                                    playbackCurrentMillisTimer.cancel();
                                }

                                if (playerState == null) {
                                    Log.e(getClass().getName(), "playerState = null");
                                    return;
                                }

                                if (playerState.track == null) {
                                    Log.e(getClass().getName(), "playerState.track = null");
                                    return;
                                }

                                /*
                                Playback Progress
                                 */
                                long timeToWait = playerState.track.duration - playerState.playbackPosition;
                                if (!forcePaused) { // Prevent the reaction to the force-pause-action
                                    playbackCurrentMillisTimer = new CountDownTimer(
                                            timeToWait,
                                            1000) {
                                        public void onTick(long millisUntilFinished) {
                                            currentTrackMillisUntilFinished = millisUntilFinished;
                                            if (millisUntilFinished < 2500) {
                                                Log.d(getClass().getName(),
                                                        "Playback is nearly finished!");
                                                // Pause track
                                                forcePaused = true;
                                                if (mSpotifyAppRemote == null) {
                                                    return;
                                                }
                                                mSpotifyAppRemote.getPlayerApi().pause();
                                                Log.d(getClass().getName(),
                                                        "Playback is nearly finished. Force-Pause it");
                                                playbackCurrentMillisTimer.cancel();
                                                playbackCurrentMillisTimer = null;
                                            }
                                            //Log.d("____________", "TimeToWait: " + timeToWait);
                                            //Log.d("____________", "MillisUntilFinished: " + millisUntilFinished);
                                            //Log.d("____________", "Duration: " + playerState.track.duration);
                                            //Log.d("____________", "PlaybackPosition: " + playerState.playbackPosition);
                                        }

                                        @Override
                                        public void onFinish() { }
                                    }.start();
                                }

                                /*
                                Playback Detail
                                 */
                                playbackDetailTimer = new CountDownTimer(millisToWait, 1000) {
                                    public void onTick(long millisUntilFinished) { }

                                    public void onFinish() {

                                        // Current track duration
                                        currentTrackDuration = playerState.track.duration;

                                        currentlyPlayingTrackId = SpotifyUtilityMethods.trimTrackId(playerState.track.uri);
                                        Log.e(getClass().getName(), "CurrentlyPlayingTrackId: " + currentlyPlayingTrackId);

                                        mPlayerState = playerState;

                                        if (playbackWaitingForSpotTimer != null) {
                                            playbackWaitingForSpotTimer.cancel();
                                        }

                                        if (playerState.isPaused) {
                                            Log.e(getClass().getName(), "playerState.isPaused = " + playerState.isPaused);
                                            if (forcePaused) {
                                                // Playack was automatically paused by playd because the song is finished
                                                // and the app is waiting fot the foreign spot to answer
                                                Log.e(getClass().getName(), "playerState.isPaused = " + forcePaused);
                                                // Wait for the foreign spot to answer
                                                playbackWaitingForSpotTimer =
                                                        new CountDownTimer(10000, 1000) {
                                                    // After 10 seconds without a new recieved playback, stop listening to the past spot
                                                    @Override
                                                    public void onTick(long l) { }

                                                    @Override
                                                    public void onFinish() {
                                                        // The time is over, disconnect from the spot and continue playing some other songs
                                                        mNotificationManager.notify(
                                                                spotFinishedNotificationId,
                                                                buildSpotFinishedNotification(NOTIFICATIONCODE_SPOT_FINISHED_ITSELF));
                                                        notifyPaused();
                                                        mSpotifyAppRemote.getPlayerApi().skipNext();
                                                        forcePaused = false;
                                                    }
                                                }.start();
                                                Log.e(getClass().getName(), "playerState.isPaused = " + forcePaused);
                                            } else {
                                                // Playback was manually paused by the user
                                                // Check if the user was connected to a spot
                                                if (currentSpot != null) {
                                                    // If the user was connected to a spot until now, show a notification
                                                    mNotificationManager.notify(
                                                            spotFinishedNotificationId,
                                                            buildSpotFinishedNotification(NOTIFICATIONCODE_SPOT_WAS_FINISHED_BY_USER));
                                                }
                                                notifyPaused();
                                                Log.d("__________________", "notifyPaused() #1");
                                            }
                                            return;
                                        }

                                        if (currentSpot == null) {
                                            // There is no playback saved -> the user started the playback through the spotify app, not playd
                                            currentSpot = new Spot();
                                            currentSpot.setId(GlobalVariables.LOCAL_PLAYBACK);
                                            unfollowCurrentUser();
                                            return;
                                        } else if (!currentlyPlayingTrackId
                                                .equals(currentSpot.getPlayback().getTrackId())) {
                                            // The current tracked playback is not the same as in the saved playback
                                            // -> The playback was skipped by the spotify app
                                            mNotificationManager.notify(
                                                    spotFinishedNotificationId,
                                                    buildSpotFinishedNotification(NOTIFICATIONCODE_SPOT_WAS_FINISHED_BY_USER));
                                            notifyPaused();
                                            Log.d("__________________", "notifyPaused() #2");
                                            return;
                                        } else {
                                            // The current tracked playback is the same as in the saved playback
                                            // -> The playback state changed
                                            Log.d(TAG, "The playback state changed");
                                        }

                                        // Check if the user is following a spot
                                        if (childEventListener == null) {
                                            // This playback is freshly triggered by the user because this value
                                            // is set to null in play()
                                            followUser(currentSpot.getId());
                                        } else {
                                            // This playback is triggered from outside, from the firestore listener
                                        }

                                        // There is a playback saved -> the user started the playback through playd, not the spotify app
                                        notifyPlaying();
                                    }
                                }.start();

                            }
                        });


                if (callback != null) {
                    callback.onSuccess("");
                }

            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.e(getClass().getName(), "Failure - SpotifyAppRemote not manually disconnected");
                callback.onFailure(throwable);

                if (throwable instanceof SpotifyConnectionTerminatedException
                        || throwable instanceof SpotifyDisconnectedException) {
                    Log.e(getClass().getName(), "Spotify is disconnected.");
                } else if (throwable instanceof AuthenticationFailedException) {
                    Log.e(getClass().getName(), "AuthenticationFailedException - " + throwable.getMessage());
                    // TODO - Happens from time to time
                } else if (throwable instanceof SpotifyInvalidTokenException) {
                    Log.e(getClass().getName(), "Spotify Token is invalid - " + throwable.getMessage());
                } else if (throwable instanceof SpotifyAppRemoteException) {
                    Log.e(getClass().getName(), "SpotifyAppRemoteException - " + throwable.getMessage());

                    // wenn user nicht eingeloggt:
                    // SpotifyAppRemoteException - {"message":"User is not logged in"}


                    // TODO Meldung an Benutzer?
                    // throwed after catching TimeoutException

                }
                else {
                    Log.e(getClass().getName(), "SpotifyAppRemote - UnknownException - " + throwable.getMessage());
                }


                // TODO mehr Exceptions (zB Spotify not installed etc) und besseres Handling
            }
        });

    }

    /**
     * Disconnects from the Spotify Service if necessary e.g. mSpotifyRemote is null and the status is 'disconnected'
     */
    private void disconnect() {
        Log.e(getClass().getName(), "SpotifyAppRemote - Disconnecting...");

        if (mSpotifyAppRemote != null && mSpotifyAppRemote.isConnected()) {
            SpotifyAppRemote.disconnect(mSpotifyAppRemote);
            Log.e(getClass().getName(), "SpotifyAppRemote - Disconnected");
        }

        mSpotifyAppRemote = null;
    }

    /**
     * TODO comment
     */
    private void notifyPaused() {
        Log.e(getClass().getName(), "notifyPaused() called");

        // Cancel TrackProgress Timer
        if (playbackCurrentMillisTimer != null) {
            playbackCurrentMillisTimer.cancel();

        }

        isPlaying = false;
        unfollowCurrentUser();
        setCurrentVolume();
        if (foregroundServiceSpotListenerCallbacks == null) {
            stopService(new Intent(this, ForegroundServiceSpotListener.class));
            if (playbackDetailSubscription != null) {
                playbackDetailSubscription.cancel();
            }
            return;
        }
        foregroundServiceSpotListenerCallbacks.notifyPaused();
        stopForeground(true);
        stopSelf();
        //stopService(new Intent(this, ForegroundServiceSpotListener.class)); TODO MAL SEHEN OB DIE OBEREN BEIDEN ZEILEN GUT SIND
        if (playbackDetailSubscription != null) {
            playbackDetailSubscription.cancel();
        }
    }

    /**
     * TODO comment
     */
    private void notifyPlaying() {
        Log.e(getClass().getName(), "notifyPlaying() is triggered ");
        isPlaying = true;
        if (foregroundServiceSpotListenerCallbacks == null) {
            return;
        }
        foregroundServiceSpotListenerCallbacks.notifyPlaying(currentSpot);
    }

    /**
     * todo comment
     * @param spot
     */
    private void play(Spot spot) {
        unfollowCurrentUser();
        currentSpot = spot;
        play(spot.getPlayback());
    }

    /**
     * todo comment
     * @param playback
     */
    private void play(Playback playback) {
        if (isCanceled) {
            return;
        }
        forcePaused = false;

        if (mSpotifyAppRemote == null) {
            connect(new ValueCallback() {
                @Override
                public void onSuccess(Object value) {
                    // Subscribe to Playback-Timer
                    play(playback);
                }

                @Override
                public void onFailure(Object value) {
                    // Disconnect
                    disconnect();
                    // Notify activity that the playback was terminated
                    notifyPaused();
                }
            });
            return;
        }

        playTrackRepository.setCurrentVolume();
        long now = UtilityMethods.getCurrentTime();
        long position = now + playback.getCurrentMS() - playback.getTrackStartedTimestamp() - 2000;
        long playbackPosition = position > playback.getTrackDuration() ? 0L : position;
        String playbackId = SpotifyUtilityMethods.completeTrackId(playback.getTrackId());

        if (playbackPosition <= 0L) {
            Log.e(getClass().getName(), "Play track from the beginning");
            mSpotifyAppRemote.getPlayerApi().play(playbackId)
                    .setResultCallback(new CallResult.ResultCallback<Empty>() {
                        @Override
                        public void onResult(Empty empty) {
                            editNotification();

                            // IF forcePaused = true, resume the playback and set forcePaused = false
                            if (forcePaused) {
                                forcePaused = false;
                                mSpotifyAppRemote.getPlayerApi().resume();
                            }

                        }
                    })
                    .setErrorCallback(new ErrorCallback() {
                        @Override
                        public void onError(Throwable throwable) {
                            Log.e(getClass().getName(), "SpotifyAppRemote - Error starting playback - ");
                        }
                    });
        } else {
            disableSound();
            Log.e(getClass().getName(), "Play track from point " + playbackPosition);
            mSpotifyAppRemote.getPlayerApi().play(playbackId)
                    .setResultCallback(new CallResult.ResultCallback<Empty>() {
                        @Override
                        public void onResult(Empty empty) {
                            mSpotifyAppRemote.getPlayerApi().seekTo(playbackPosition)
                                    .setResultCallback(new CallResult.ResultCallback<Empty>() {
                                @Override
                                public void onResult(Empty empty) {
                                    new CountDownTimer(400, 1000) {
                                        public void onTick(long millisUntilFinished) { }
                                        public void onFinish() {
                                            editNotification();
                                            enableSound();

                                            // IF forcePaused = true, resume the playback and set forcePaused = false
                                            if (forcePaused) {
                                                forcePaused = false;
                                                mSpotifyAppRemote.getPlayerApi().resume();
                                            }

                                        }
                                    }.start();

                                }
                            });
                        }
                    })
                    .setErrorCallback(new ErrorCallback() {
                        @Override
                        public void onError(Throwable throwable) {
                            Log.e(getClass().getName(), "SpotifyAppRemote - Error starting playback - ");
                        }
                    });
        }

    }

    /**
     * todo erklären
     */
    private void pause() {
        if (mPlayerState != null) {
            mSpotifyAppRemote.getPlayerApi().pause()
                    .setResultCallback(new CallResult.ResultCallback<Empty>() {
                        @Override
                        public void onResult(Empty empty) {
                            Log.e(getClass().getName(), "SpotifyAppRemote - Playback paused");
                            currentSpot = null;
                        }
                    })
                    .setErrorCallback(new ErrorCallback() {
                        @Override
                        public void onError(Throwable throwable) {
                            Log.e(getClass().getName(), "SpotifyAppRemote - Error pausing playback");
                        }
                    });
        }
    }


    /**
     * todo comment
     *
     * @param userId
     */
    private void followUser(String userId) {
        if (childEventListener != null) { // There is already a listener installed
            return;
        }

        playbackOverviewReference = firebaseRealtimeDatabase
                .child(GlobalVariables.DB_PLAYBACKOVERVIEW)
                .child(userId);

        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                // The Spot changed it's playback
                if (dataSnapshot.getValue() == null) {
                    Log.e(getClass().getName(), "Recieved null - dataSnapshot.getValue() is null");
                    return;
                }
                Log.e(getClass().getName(), "Recieved playback");
                if (currentSpot == null) {
                    Log.e(getClass().getName(), "onChildChanged() - currentSpot is null");
                    return;
                }
                String trackId = dataSnapshot.getValue().toString();
                // Check if the trackId is already playing
                if (currentSpot.getPlayback().getTrackId().equals(trackId)) {
                    Log.e(getClass().getName(), "onChildChanged() - No need to trigger a playback because it is currently playing");
                    // No need to trigger a playback because it is currently playing
                    // Probably triggered the first time by this spot
                    return;
                }

                currentSpot.getPlayback().setTrackId(trackId); // Set new recieved track Id
                currentSpot.getPlayback().setTrackStartedTimestamp(0L); // Reset playback
                spotifyRepository.updatePlaybackInformation(
                        currentSpot.getPlayback(),
                        new ValueCallback() {
                    @Override
                    public void onSuccess(Object value) {
                        Playback playback = (Playback) value;
                        currentSpot.setPlayback(playback);

                        // Force-skip
                        play(currentSpot);
                    }

                    @Override
                    public void onFailure(Object value) {

                    }
                });

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                // The Spot finished it's playback
                Log.e(getClass().getName(), "The spot finished it's playback. Finish listening");
                notifyPaused();
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        playbackOverviewReference.addChildEventListener(childEventListener);

    }

    /**
     * todo comment
     */
    private void unfollowCurrentUser() {
        currentSpot = null;
        if (childEventListener == null) {
            return;
        }
        playbackOverviewReference.removeEventListener(childEventListener);
        childEventListener = null;
        playbackOverviewReference = null;
    }

    public Spot getCurrentSpot() {
        return currentSpot;
    }

    public void setCurrentSpot(Spot currentSpot) {
        this.currentSpot = currentSpot;
    }

    public boolean isPlaying() {
        Log.e(getClass().getName(), "isPlaying() = " + isPlaying);
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

    public int getCurrentTrackProgress() {
        if (currentTrackMillisUntilFinished == 0L || currentTrackDuration == 0L) {
            return 0;
        }
        return (int) (currentTrackMillisUntilFinished * 100 / currentTrackDuration);
    }

    public void setCurrentVolume() {
        if (mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC) != 0) {
            volume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        }
    }

    private void disableSound() {
        mAudioManager.setStreamVolume(
                AudioManager.STREAM_MUSIC,
                0, // Index
                0 // Flags
        );
    }

    private void enableSound() {
        mAudioManager.setStreamVolume(
                AudioManager.STREAM_MUSIC,
                volume, // Index
                0 // Flags
        );
    }
}
