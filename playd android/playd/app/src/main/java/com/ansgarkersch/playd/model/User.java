package com.ansgarkersch.playd.model;

import com.ansgarkersch.playd.globals.GlobalVariables;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

public class User implements Serializable {

    private String id = "";
    private String spotifyUserId = "";
    private String instagramUserId = "";

    private String username = "";
    private String userSubtitle = ""; // editable; like "Musican" or "Influencer" or "Entrepreneur"
    private String name = "";  // editable
    private String birthday = "";
    private String relationshipStatus = "NOT_YOUR_BUSINESS";  // editable
    private String gender = "NOT_YOUR_BUSINESS";  // editable
    private String nationality = "NOT_YOUR_BUSINESS";  // editable
    private String description = "";  // editable

    private String spotifyPlaydlistId = "";  // maybe deprecated

    private String premiumLevel = "BASIC";
    private String purchaseToken = "";

    private long lastLoggedInTimestamp = 0L;
    private boolean isFamous = false;
    private List<String> preferredMusicGenres = new ArrayList<>();

    private HashMap<String, String> profileImages = new HashMap<>(); // editable
    private HashMap<String, Long> followedIds = new HashMap<>();
    private HashMap<String, Long> followersIds = new HashMap<>();

    public User(String id) {
        this.id = id;
    }

    public User() {
        // necessary for firebase jackson
    }

    public UserOverview getOverview() {
        return new UserOverview(id, username, getProfilePicUrl(), false, 0L);
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj.getClass() == User.class && this.id.equals(((User) obj).getId());
    }
    // the hashCode is inspired by the unique userId
    @Override
    public int hashCode() {
        return id.hashCode();
    }
    @Override
    public String toString() {
        return "UserId: " + this.id +
                "Name: " + this.name +
                "Birthday: " + this.birthday;
    }

    // get the profilePic
    public String getProfilePicUrl() {
        if (profileImages == null || profileImages.size() == 0) {
            return GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE;
        }
        return getProfileImageList().get(0);
    }

    // get the profilePic thumbnail
    public String getProfilePicThumbnailUrl() {
        if (profileImages == null || profileImages.size() == 0) {
            return GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE;
        }
        String thumbnailUrl = profileImages.get(GlobalVariables.DB_THUMBNAIL);
        return thumbnailUrl == null ? getProfilePicUrl() : thumbnailUrl;
    }

    // get all profileImage-Urls
    public List<String> getProfileImageList() {
        if (profileImages == null || profileImages.size() == 0) {
            return new ArrayList<>();
        }
        TreeMap<String, String> sortedMap = new TreeMap<>(profileImages);
        sortedMap.remove(GlobalVariables.DB_THUMBNAIL);
        return new ArrayList<>(sortedMap.values());
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSpotifyUserId() {
        return spotifyUserId;
    }

    public void setSpotifyUserId(String spotifyUserId) {
        this.spotifyUserId = spotifyUserId;
    }

    public String getInstagramUserId() {
        return instagramUserId;
    }

    public void setInstagramUserId(String instagramUserId) {
        this.instagramUserId = instagramUserId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserSubtitle() {
        return userSubtitle;
    }

    public void setUserSubtitle(String userSubtitle) {
        this.userSubtitle = userSubtitle;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public RelationshipStatus getRelationshipStatus() {
        return RelationshipStatus.valueOf(relationshipStatus);
    }

    public void setRelationshipStatus(RelationshipStatus relationshipStatus) {
        this.relationshipStatus = relationshipStatus.name();
    }

    public Gender getGender() {
        return Gender.valueOf(gender);
    }

    public void setGender(Gender gender) {
        this.gender = gender.name();
    }

    public PremiumLevel getPremiumLevel() {
        return PremiumLevel.valueOf(premiumLevel);
    }

    public void setPremiumLevel(PremiumLevel premiumLevel) {
        this.premiumLevel = premiumLevel.name();
    }

    public Nationality getNationality() {
        return Nationality.valueOf(nationality);
    }

    public void setNationality(Nationality nationality) {
        this.nationality = nationality.name();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isIsFamous() {
        return isFamous;
    }

    public void setIsFamous(boolean isFamous) {
        this.isFamous = isFamous;
    }

    public HashMap<String, String> getProfileImages() {
        return profileImages;
    }

    public void setProfileImages(HashMap<String, String> profileImages) {
        this.profileImages = profileImages;
    }

    public String getSpotifyPlaydlistId() {
        return spotifyPlaydlistId;
    }

    public void setSpotifyPlaydlistId(String spotifyPlaydlistId) {
        this.spotifyPlaydlistId = spotifyPlaydlistId;
    }

    public HashMap<String, Long> getFollowedIds() {
        return followedIds;
    }

    public void setFollowedIds(HashMap<String, Long> followedIds) {
        this.followedIds = followedIds;
    }

    public HashMap<String, Long> getFollowersIds() {
        return followersIds;
    }

    public void setFollowersIds(HashMap<String, Long> followersIds) {
        this.followersIds = followersIds;
    }

    public long getLastLoggedInTimestamp() {
        return lastLoggedInTimestamp;
    }

    public void setLastLoggedInTimestamp(long lastLoggedInTimestamp) {
        this.lastLoggedInTimestamp = lastLoggedInTimestamp;
    }

    public List<String> getPreferredMusicGenres() {
        return preferredMusicGenres;
    }

    public void setPreferredMusicGenres(List<String> preferredMusicGenres) {
        this.preferredMusicGenres = preferredMusicGenres;
    }

    public String getPurchaseToken() {
        return purchaseToken;
    }

    public void setPurchaseToken(String purchaseToken) {
        this.purchaseToken = purchaseToken;
    }
}
