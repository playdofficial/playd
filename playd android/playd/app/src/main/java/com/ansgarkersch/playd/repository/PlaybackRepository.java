package com.ansgarkersch.playd.repository;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.ansgarkersch.playd._spotify.repository.SpotifyRepository;
import com.ansgarkersch.playd.globals.FirebaseEndpoints;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.Playback;
import com.ansgarkersch.playd.util.UtilityMethods;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PlaybackRepository extends Observable {

    // Current logged-in user's userId
    private String userId;

    // Repositories
    private UserRepository userRepository;
    private FirebaseRepository firebaseRepository;
    private SpotifyRepository spotifyRepository;

    // Firebase Services
    FirebaseFirestore firestoreDatabase;
    DatabaseReference firebaseRealtimeDatabase;

    private FirebaseRestService firebaseRestService;
    private String TAG = "Class: PlaybackRepository - ";

    public PlaybackRepository(Context mContext) {

        // Firebase Services
        firestoreDatabase = FirebaseFirestore.getInstance();
        firebaseRealtimeDatabase = FirebaseDatabase.getInstance().getReference();

        // Repositories
        userRepository = new UserRepository();
        firebaseRepository = new FirebaseRepository();
        spotifyRepository = new SpotifyRepository(mContext);

        // OkHttpClient
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .build();

        // Retrofit
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(FirebaseEndpoints.GLOBAL_ENDPOINT)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        firebaseRestService = retrofit.create(FirebaseRestService.class);

        // UserId
        userId = firebaseRepository.getCurrentUser().getUid();
    }


    /**
     * todo erklären
     * @param id
     * @param callback
     */
    public void getPlaybackById(final String id, final ValueCallback callback) {
        firestoreDatabase
                .collection(GlobalVariables.DB_PLAYBACK)
                .document(id)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                callback.onSuccess(documentSnapshot.toObject(Playback.class));
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(getClass().getName(), e.getMessage());
                callback.onFailure(GlobalVariables.RESULT_FAILURE);
            }
        });
    }


    /**
     * Updates the playbackOverview object including including the trackId for all spot-observers
     * @param trackId
     * @param callback
     */
    public void updateCurrentUsersPlaybackOverview(final String trackId, final ValueCallback callback) {
        Map<String, Object> data = new HashMap<>();
        data.put(GlobalVariables.DB_TRACKID, trackId);
        firebaseRealtimeDatabase
                .child(GlobalVariables.DB_PLAYBACKOVERVIEW)
                .child(firebaseRepository.getCurrentUser().getUid())
                .updateChildren(data)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                callback.onSuccess(GlobalVariables.RESULT_OK);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(getClass().getName(), "ERROR - " + e.getMessage());
                callback.onFailure(GlobalVariables.RESULT_FAILURE);
            }
        }).addOnCanceledListener(new OnCanceledListener() {
            @Override
            public void onCanceled() {
                callback.onFailure(GlobalVariables.RESULT_CANCELED);
            }
        });
    }

    /**
     * Deletes the playbackOverview object including including the trackId for all spot-observers
     * @param callback
     */
    public void deleteCurrentUsersPlaybackOverview(final ValueCallback callback) {
        firebaseRealtimeDatabase
                .child(GlobalVariables.DB_PLAYBACKOVERVIEW)
                .child(firebaseRepository.getCurrentUser().getUid())
                .removeValue()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        callback.onSuccess(GlobalVariables.RESULT_OK);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(getClass().getName(), "ERROR - " + e.getMessage());
                callback.onFailure(GlobalVariables.RESULT_FAILURE);
            }
        }).addOnCanceledListener(new OnCanceledListener() {
            @Override
            public void onCanceled() {
                callback.onFailure(GlobalVariables.RESULT_CANCELED);
            }
        });
    }



    /**
     * todo erklären
     * @param status
     * @param callback
     */
    public void updateCurrentUsersPlaybackStatus(final String status, final ValueCallback callback) {

        // todo

    }


    /**
     * Updates the playback object including all necessary informations about the current playback
     * @param playback
     * @param callback
     */
    public void updateCurrentUsersPlaybackData(final Playback playback, final ValueCallback callback) {

        Map<String, Object> playbackData = new HashMap<>();
        playbackData.put(GlobalVariables.DB_USERID, playback.getUserId());
        playbackData.put(GlobalVariables.DB_TRACKID, playback.getTrackId());
        playbackData.put(GlobalVariables.DB_TRACKSTARTEDTIMESTAMP,
                playback.getTrackStartedTimestamp() == 0L ? UtilityMethods.getCurrentTime() : playback.getTrackStartedTimestamp());
        playbackData.put(GlobalVariables.DB_SPOTSTARTEDTIMESTAMP,
                playback.getSpotStartedTimestamp() == 0L ? UtilityMethods.getCurrentTime() : playback.getSpotStartedTimestamp());
        playbackData.put(GlobalVariables.DB_CURRENTMS,
                playback.getCurrentMS());
        playbackData.put(GlobalVariables.DB_STATUS, playback.getSpotName());
        playbackData.put(GlobalVariables.DB_GENRES, playback.getTrackGenres());
        playbackData.put(GlobalVariables.DB_PRIORITY, playback.getPriority());

        firestoreDatabase.collection(GlobalVariables.DB_PLAYBACK).document(userId).set(playbackData)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                callback.onSuccess(GlobalVariables.RESULT_OK);
            }
        })
                .addOnCanceledListener(new OnCanceledListener() {
            @Override
            public void onCanceled() {
                callback.onSuccess(GlobalVariables.RESULT_CANCELED);
            }
        })
                .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onSuccess(GlobalVariables.RESULT_FAILURE);
            }
        });

    }

    /**
     * Deletes the playback object including all necessary informations about the current playback
     * @param callback
     */
    public void deleteCurrentUsersPlaybackData(ValueCallback callback) {

        firestoreDatabase.collection(GlobalVariables.DB_PLAYBACK).document(userId).delete()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                callback.onSuccess(GlobalVariables.RESULT_OK);
            }
        })
                .addOnCanceledListener(new OnCanceledListener() {
            @Override
            public void onCanceled() {
                callback.onSuccess(GlobalVariables.RESULT_CANCELED);
            }
        })
                .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onSuccess(GlobalVariables.RESULT_FAILURE);
            }
        });

    }

    /**
     * todo erklären
     * @param genre
     * @param callback
     */
    public void getAllCurrentPlaybacksByGenre(String genre, final ValueCallback callback) {
        firebaseRepository.getUserToken(new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                String userToken = value.toString();
                firebaseRestService.getAllCurrentPlaybacksByGenre(userToken, genre)
                        .enqueue(new Callback<List<Playback>>() {
                    @Override
                    public void onResponse(@NonNull Call<List<Playback>> call, @NonNull Response<List<Playback>> response) {
                        List<Playback> playbackList = response.body();
                        if (playbackList == null) {
                            return;
                        }
                        callback.onSuccess(playbackList);
                    }

                    @Override
                    public void onFailure(@NonNull Call<List<Playback>> call, @NonNull Throwable t) {
                        if (t.getMessage() != null) {
                            Log.d(TAG, t.getMessage());
                        }
                        if (t instanceof SocketTimeoutException){
                            callback.onFailure(GlobalVariables.RESULT_TIMEOUT);
                        } else {
                            callback.onFailure(GlobalVariables.RESULT_FAILURE);
                        }
                    }
                });
            }

            @Override
            public void onFailure(Object value) {
                callback.onFailure(GlobalVariables.RESULT_FAILURE);
            }
        });
    }

    /**
     * todo erklären
     * @param callback
     */
    public void getAllCurrentlyPlayingGenres(final ValueCallback callback) {
        firebaseRepository.getUserToken(new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                String userToken = value.toString();
                firebaseRestService.getAllCurrentlyPlayingGenres(userToken)
                        .enqueue(new Callback<List<String>>() {
                    @Override
                    public void onResponse(@NonNull Call<List<String>> call, @NonNull Response<List<String>> response) {
                        List<String> result = response.body();
                        if (result == null || result.size() == 0) {
                            callback.onSuccess(new ArrayList<String>());
                        }
                        callback.onSuccess(result);
                    }

                    @Override
                    public void onFailure(@NonNull Call<List<String>> call, @NonNull Throwable t) {
                        if (t.getMessage() != null) {
                            Log.d(TAG, t.getMessage());
                        }
                        if (t instanceof SocketTimeoutException){
                            callback.onFailure(GlobalVariables.RESULT_TIMEOUT);
                        } else {
                            callback.onFailure(GlobalVariables.RESULT_FAILURE);
                        }
                    }
                });
            }

            @Override
            public void onFailure(Object value) {
                callback.onFailure(GlobalVariables.RESULT_FAILURE);
            }
        });
    }


}
