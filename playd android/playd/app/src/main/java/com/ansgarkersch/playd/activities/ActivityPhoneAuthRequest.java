package com.ansgarkersch.playd.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.model.CountryPhoneData;
import com.ansgarkersch.playd.repository.UserRepository;

import java.util.Locale;

public class ActivityPhoneAuthRequest extends AppCompatActivity {

    private Spinner spinner;
    private EditText editText;
    private TextView textView;
    private UserRepository userRepository;

    private Button confirm;
    private ProgressBar confirmProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_auth_request);
        userRepository = new UserRepository();

        editText = findViewById(R.id.activity_phone_auth_request_edittext_phoneNumber);
        textView = findViewById(R.id.activity_phone_auth_request_textview_areaCode);
        spinner = findViewById(R.id.activity_phone_auth_request_spinner_countryCode);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,
                CountryPhoneData.countryNames);
        spinner.setAdapter(arrayAdapter);
        int spinnerPosition = arrayAdapter.getPosition(getResources().getConfiguration().locale.getDisplayCountry(Locale.ENGLISH));
        if (spinnerPosition != -1) {
            spinner.setSelection(spinnerPosition);
        }
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String areaCode = "+" + CountryPhoneData.countryPhoneCodes[spinner.getSelectedItemPosition()];
                textView.setText(areaCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        confirm = findViewById(R.id.activity_phone_auth_request_button_confirm);
        confirmProgressBar = findViewById(R.id.activity_phone_auth_request_button_confirm_progressBar);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String countryCode = CountryPhoneData.countryPhoneCodes[spinner.getSelectedItemPosition()];
                String number = editText.getText().toString().trim();

                if (number.isEmpty() || number.length() < 10) {
                    editText.setError(getResources().getString(R.string.error_enterValidPhoneNumber));
                    editText.requestFocus();
                    return;
                }
                final String phoneNumber = "+" + countryCode + number;
                confirm.setVisibility(View.GONE);
                confirmProgressBar.setVisibility(View.VISIBLE);

                /*
                userRepository.isUserAlreadyExisting(phoneNumber, new ValueCallback() {
                    @Override
                    public void onSuccess(Object value) {
                        boolean result = (boolean) value;
                        if (result) {
                            editText.setError(getResources().getString(R.string.error_telephoneNumberAlreadyExists));
                            confirmProgressBar.setVisibility(View.GONE);
                            confirm.setVisibility(View.VISIBLE);
                        } else {
                            Intent intent = new Intent(ActivityPhoneAuthRequest.this, ActivityPhoneAuthVerify.class);
                            intent.putExtra(GlobalVariables.TELEPHONE_NUMBER, phoneNumber);
                            startActivity(intent);
                            finish();
                        }
                    }


                    @Override
                    public void onFailure(Object value) {

                    }

                });*/

            }
        });

    }

    private void closeWindow() {
        //startActivity(new Intent(ActivityPhoneAuthRequest.this, _ActivityWelcomeScreen.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        closeWindow();
    }

    public void onClick(View view) {
        closeWindow();
    }

}
