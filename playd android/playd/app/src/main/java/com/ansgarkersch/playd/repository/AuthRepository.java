package com.ansgarkersch.playd.repository;

import android.support.annotation.NonNull;
import android.util.Log;

import com.ansgarkersch.playd.globals.GlobalVariables;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class AuthRepository {

    private FirebaseAuth firebaseAuth;
    private String verificationId;

    public AuthRepository() {
        firebaseAuth = FirebaseAuth.getInstance();
    }


    /**
     * Register a user with the given email adress and password
     * @param email
     * @param password
     * @param callback
     */
    public void registerUserWithEmail(final String email, final String password, final ValueCallback callback) {

        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                Log.e(GlobalVariables.TAG_FIREBASE_AUTH, authResult.toString());
                callback.onSuccess(authResult.getUser().getUid());
            }
        })
                .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(GlobalVariables.EXCEPTIONTAG_FIREBASE_AUTH, e.getMessage());
                if (e instanceof FirebaseAuthWeakPasswordException) {
                    callback.onFailure(GlobalVariables.RESULT_CREDENTIALS_FAILED_WEAK_PASSWORD);
                } else if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    callback.onFailure(GlobalVariables.RESULT_CREDENTIALS_FAILED_INVALID_CREDENTIALS);
                } else if (e instanceof FirebaseAuthUserCollisionException) {
                    callback.onFailure(GlobalVariables.RESULT_CREDENTIALS_FAILED_USER_COLLISION);
                } else {
                    callback.onFailure(GlobalVariables.RESULT_CREDENTIALS_FAILED);
                }
            }
        })
                .addOnCanceledListener(new OnCanceledListener() {
            @Override
            public void onCanceled() {
                Log.e(GlobalVariables.TAG_FIREBASE_AUTH, "Canceled");
                callback.onFailure(GlobalVariables.RESULT_CANCELED);
            }
        });
    }




    /*************************************************************************************************
     *************************************** PHONE AUTH **********************************************
     ************************** NOT IN USE IN THE FIRST PLAYD VERSION ********************************
     *************************************************************************************************/
    /**
     * Sign in
     */
    public void signIn(String code, final AuthCallback callback) {
        PhoneAuthCredential phoneAuthCredential = null;
        try {
            phoneAuthCredential = PhoneAuthProvider.getCredential(verificationId, code);
        } catch (IllegalArgumentException ex) {
            callback.onCallback(GlobalVariables.RESULT_FAILURE);
            return;
        }
        firebaseAuth.signInWithCredential(phoneAuthCredential).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                Log.d(GlobalVariables.TAG_FIREBASE_AUTH, "successfully logged in");
                callback.onCallback(GlobalVariables.RESULT_OK);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onCallback(GlobalVariables.RESULT_FAILURE);
                Log.e(GlobalVariables.EXCEPTIONTAG_FIREBASE_AUTH, e.getMessage());
            }
        });
    }

    /**
     * Sends a verification Code to the user's smartphone
     */
    public void sendVerificationCode(String phoneNumber, final AuthCallback callback) {
        Log.d(GlobalVariables.TAG_FIREBASE_AUTH, "Verification code is on the way");
        PhoneAuthProvider.getInstance().verifyPhoneNumber(phoneNumber,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                    @Override
                    public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                        Log.d(GlobalVariables.TAG_FIREBASE_AUTH, "onCodeSent:" + verificationId);
                        super.onCodeSent(s, forceResendingToken);
                        verificationId = s;
                    }

                    @Override
                    public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                        Log.d(GlobalVariables.TAG_FIREBASE_AUTH, "Verification completed");
                        callback.onCallback(GlobalVariables.RESULT_VERIFICATION_COMPLETED);
                    }

                    @Override
                    public void onVerificationFailed(FirebaseException e) {
                        Log.e(GlobalVariables.EXCEPTIONTAG_FIREBASE_AUTH, "onVerificationFailed", e);

                        if (e instanceof FirebaseAuthInvalidCredentialsException) {
                            Log.e(GlobalVariables.EXCEPTIONTAG_FIREBASE_AUTH, "invalid request", e);
                            callback.onCallback(GlobalVariables.RESULT_VERIFICATION_FAILED_INVALID_REQUEST);
                        } else if (e instanceof FirebaseTooManyRequestsException) {
                            callback.onCallback(GlobalVariables.RESULT_VERIFICATION_FAILED_TOO_MANY_REQUESTS);
                        }

                    }
                });

    }

    /**
     * Links the user phone credentials with email/password credentials
     */
    public void linkCredentials(FirebaseUser currentUser, String email, String password, final AuthCallback callback) {
        currentUser.linkWithCredential(EmailAuthProvider.getCredential(email, password)).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(GlobalVariables.EXCEPTIONTAG_FIREBASE_AUTH, e.getMessage());
                if (e instanceof FirebaseAuthWeakPasswordException) {
                    callback.onCallback(GlobalVariables.RESULT_CREDENTIALS_FAILED_WEAK_PASSWORD);
                } else if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    callback.onCallback(GlobalVariables.RESULT_CREDENTIALS_FAILED_INVALID_CREDENTIALS);
                } else if (e instanceof FirebaseAuthUserCollisionException) {
                    callback.onCallback(GlobalVariables.RESULT_CREDENTIALS_FAILED_USER_COLLISION);
                } else {
                    callback.onCallback(GlobalVariables.RESULT_CREDENTIALS_FAILED);
                }
            }
        }).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                Log.e(GlobalVariables.TAG_FIREBASE_AUTH, authResult.toString());
                callback.onCallback(GlobalVariables.RESULT_OK);
            }
        });
    }

}
