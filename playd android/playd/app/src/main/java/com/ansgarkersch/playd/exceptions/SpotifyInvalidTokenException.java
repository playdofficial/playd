package com.ansgarkersch.playd.exceptions;

public class SpotifyInvalidTokenException extends Exception {

    public SpotifyInvalidTokenException(String message) {
        super(message);
    }

}
