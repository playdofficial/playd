package com.ansgarkersch.playd._spotify.repository;

import com.ansgarkersch.playd._spotify.model.Artist;
import com.ansgarkersch.playd._spotify.model.Playlist;
import com.ansgarkersch.playd._spotify.model.Token;
import com.ansgarkersch.playd._spotify.model.Track;
import com.ansgarkersch.playd._spotify.model.User;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SpotifyRestService {

    @FormUrlEncoded
    @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST("api/token")
    Call<Token> getRefreshedAccessToken(@Field("client_id") String clientId,
                                        @Field("client_secret") String clientSecret,
                                        @Field("grant_type") String grantType,
                                        @Field("refresh_token") String refreshToken);


    @FormUrlEncoded
    @POST("api/token")
    Call<Token> getAllTokens(@Field("client_id") String clientId,
                             @Field("client_secret") String clientSecret,
                             @Field("code") String code,
                             @Field("grant_type") String grantType,
                             @Field("redirect_uri") String redirectUri);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("v1/tracks/{trackId}")
    Call<Track> getTrackById(@Header("Authorization") String authorization, @Path("trackId") String trackId);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("v1/artists/{artistId}")
    Call<Artist> getArtistById(@Header("Authorization") String authorization, @Path("artistId") String artistId);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("v1/me")
    Call<User> getCurrentUser(@Header("Authorization") String authorization);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("v1/users/{userId}")
    Call<User> getUser(@Header("Authorization") String authorization, @Path("userId") String userId);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("v1/me/top/artists")
    Call<List<Artist>> getUsersTopArtists(@Header("Authorization") String authorization);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @POST("v1/me/playlists")
    Call<Void> createPlaylist(@Header("Authorization") String authorization, @Body Map<String, Object> data);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("v1/playlists/{playlist_id}/tracks")
    Call<Playlist> getPlaylist(@Header("Authorization") String authorization,
                               @Path("playlist_id") String playlistId,
                               //@Query("fields") String fieldParameter,
                               @Query("limit") int limit,
                               @Query("offset") int offset);


    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @PUT("v1/me/player/play")
    Call<Void> playTrack(@Header("Authorization") String authorization, @Body Map<String, Object> data);

}