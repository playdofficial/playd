package com.ansgarkersch.playd.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.model.ChatMessage;

import java.util.ArrayList;
import java.util.List;

public class AdapterRecyclerChat extends RecyclerView.Adapter<AdapterRecyclerChat.ViewHolder> {

    // View Types
    private final int VIEWTYPE_CURRENT_LOGGED_IN_USER_MESSAGE = 0;
    private final int VIEWTYPE_FOREIGN_USER_MESSAGE = 1;

    // Adapter elements
    private LayoutInflater mInflater;
    private Context mContext;

    // Data
    private List<ChatMessage> messageData = new ArrayList<>();
    private String currentLoggedInUserId;

    // TODO constructor


    public AdapterRecyclerChat(Context context, List<ChatMessage> messageData, String currentLoggedInUserId) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.messageData = messageData;
        this.currentLoggedInUserId = currentLoggedInUserId;
    }

    @NonNull
    @Override
    public AdapterRecyclerChat.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == VIEWTYPE_CURRENT_LOGGED_IN_USER_MESSAGE) {
            // The message is from the current logged-in user himself
            view = mInflater.inflate(R.layout.row_chat_currentloggedinuser, parent, false);
        } else {
            // The message is from the foreign user
            view = mInflater.inflate(R.layout.row_chat_foreignuser, parent, false);
        }
        return new AdapterRecyclerChat.ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if (messageData == null) {
            return 0;
        }
        return messageData.size();
    }

    @Override
    public int getItemViewType(int position) {
        ChatMessage message = messageData.get(position);
        if (message.getUserId().equals(currentLoggedInUserId)) {
            // The message is from the current logged-in user himself
            return VIEWTYPE_CURRENT_LOGGED_IN_USER_MESSAGE;
        } else {
            return VIEWTYPE_FOREIGN_USER_MESSAGE;
        }
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name;
        LinearLayout content;

        ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.row_subscriptionGallery_name);
            content = itemView.findViewById(R.id.row_subscriptionGallery_content);
            name.setSelected(true);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

        }
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRecyclerChat.ViewHolder viewHolder, int i) {



    }

}
