package com.ansgarkersch.playd.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd._spotify.model.User;
import com.ansgarkersch.playd._spotify.repository.SpotifyRepository;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.repository.FirebaseRepository;
import com.ansgarkersch.playd.repository.ValueCallback;

public class ActivityWelcomeScreen extends AppCompatActivity {

    // UI elements
    private LinearLayout loginWithSpotify, loginWithPlayd;
    private TextView register;

    // Request Codes
    private final int LOGIN_WITH_SPOTIFY = 1;
    private final int LOGIN_WITH_PLAYD = 2;
    private final int REGISTER = 0;

    // Repositories
    FirebaseRepository firebaseRepository;
    private SpotifyRepository spotifyRepository;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcomescreen);

        // Repositories
        firebaseRepository = new FirebaseRepository();
        spotifyRepository = new SpotifyRepository(this);

        // UI elements
        loginWithSpotify = findViewById(R.id.activity_welcomeScreen_linearLayoutButton_loginWithSpotify);
        loginWithPlayd = findViewById(R.id.activity_welcomeScreen_linearLayoutButton_loginWithPlayd);
        register = findViewById(R.id.activity_welcomeScreen_textViewButton_register);

        initButtons();

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is already signed in but not anonymously
        if (firebaseRepository.getCurrentUser() != null && !firebaseRepository.getCurrentUser().isAnonymous()) {
            // Login
            Intent intentRegister = new Intent(ActivityWelcomeScreen.this, ActivityMain.class);
            startActivity(intentRegister);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            finish();
        }

        /*
        // At first: Check if Spotify user is accessible & premium
        spotifyRepository.getCurrentUser(new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                com.ansgarkersch.playd._spotify.model.User spotfifyUser = (com.ansgarkersch.playd._spotify.model.User) value;
            }

            @Override
            public void onFailure(Object value) {
                // Reconnect with Spotify
                Intent intentConnectWithSpotify = new Intent(ActivityWelcomeScreen.this, ActivityConnectWithSpotify.class);
                startActivity(intentConnectWithSpotify);
                finish();
            }
        });
        */

    }


    private void initButtons() {
        loginWithSpotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initSpotifyAuth(LOGIN_WITH_SPOTIFY);
            }
        });
        loginWithPlayd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initSpotifyAuth(LOGIN_WITH_PLAYD);
            }
        });
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initSpotifyAuth(REGISTER);
            }
        });
    }

    private void initSpotifyAuth(int requestCode) {
        startActivityForResult(
                new Intent(ActivityWelcomeScreen.this, ActivityConnectWithSpotify.class),
                requestCode);
        overridePendingTransition(R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == GlobalVariables.SPOTIFY_RESULT_NOT_INSTALLED) {
            // do sth
            return;
        }

        if (resultCode == GlobalVariables.SPOTIFY_RESULT_NOT_PREMIUM) {
            // do sth
            return;
        }

        if (data == null) {
            // Something went wrong!
            return;
        }

        String accessToken = data.getStringExtra(GlobalVariables.INTENT_SPOTIFY_ACCESS_TOKEN);
        User user = (User) data.getSerializableExtra(GlobalVariables.INTENT_SPOTIFY_USER);

        switch (requestCode) {
            case LOGIN_WITH_SPOTIFY:
                Intent intentLoginWithSpotify = new Intent(ActivityWelcomeScreen.this, ActivityLoginWithSpotify.class);
                intentLoginWithSpotify.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                intentLoginWithSpotify.putExtra(GlobalVariables.INTENT_SPOTIFY_USER_ID, user.getId());
                intentLoginWithSpotify.putExtra(GlobalVariables.INTENT_SPOTIFY_ACCESS_TOKEN, accessToken);
                startActivity(intentLoginWithSpotify);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            case LOGIN_WITH_PLAYD:
                Intent intentLoginWithPlayd = new Intent(ActivityWelcomeScreen.this, ActivityLoginWithPlayd.class);
                startActivity(intentLoginWithPlayd);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                finish();
                break;
            case REGISTER:
                Intent intentRegister = new Intent(ActivityWelcomeScreen.this, ActivityRegisterUsername.class);
                intentRegister.putExtra(GlobalVariables.INTENT_SPOTIFY_USER_ID, user.getId());
                startActivity(intentRegister);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                finish();
                break;
        }

    }

}
