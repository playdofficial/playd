package com.ansgarkersch.playd.model;

import android.content.Context;

import com.ansgarkersch.playd.R;

public enum Gender {
    NOT_YOUR_BUSINESS, MALE, FEMALE, TRANSGENDER;

    public static Gender getGenderBySpinnerPosition(int position) {
        return Gender.values()[position];
    }

    public static String getGenderInLanguage(Context context, Gender gender) {
        switch (gender) {
            case NOT_YOUR_BUSINESS:
                return context.getResources().getString(R.string.notYourBusiness);
            case MALE:
                return context.getResources().getString(R.string.gender_male);
            case FEMALE:
                return context.getResources().getString(R.string.gender_female);
            case TRANSGENDER:
                return context.getResources().getString(R.string.gender_transgender);
        }
        return "";
    }
}
