package com.ansgarkersch.playd.viewModel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import com.ansgarkersch.playd._spotify.repository.SpotifyRepository;
import com.ansgarkersch.playd.events.PlaybackStateChangedEvent;
import com.ansgarkersch.playd.events.SubscriptionEvent;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.Playback;
import com.ansgarkersch.playd.model.Spot;
import com.ansgarkersch.playd.model.User;
import com.ansgarkersch.playd.model.UserOverview;
import com.ansgarkersch.playd.repository.PlaybackRepository;
import com.ansgarkersch.playd.repository.SpotRepository;
import com.ansgarkersch.playd.repository.UserRepository;
import com.ansgarkersch.playd.repository.ValueCallback;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class MySpotsViewModel extends ViewModel {

    // LiveData
    private MutableLiveData<Map<String, Spot>> followedSpots;

    // Repositories
    private UserRepository userRepository;
    private PlaybackRepository playbackRepository;
    private SpotifyRepository spotifyRepository;
    private SpotRepository spotRepository;

    // Firestore Database
    private FirebaseFirestore firestoreDatabase;

    // Firestore Listener
    private Map<String, ListenerRegistration> listenerRegistrationMap;

    // User
    private User currentLoggedInUser;

    // Util
    private String TAG = getClass().getName();

    /**
     * Initialize
     * Fetches all followed current user overviews
     */
    public void init(
            User currentLoggedInUser,
            UserRepository userRepository,
            SpotifyRepository spotifyRepository,
            SpotRepository spotRepository,
            PlaybackRepository playbackRepository) {

        // User
        this.currentLoggedInUser = currentLoggedInUser;

        // Repositories
        this.userRepository = userRepository;
        this.spotifyRepository = spotifyRepository;
        this.playbackRepository = playbackRepository;
        this.spotRepository = spotRepository;

        // Firestore Database
        firestoreDatabase = FirebaseFirestore.getInstance();

        // Firestore Listener
        listenerRegistrationMap = new HashMap<>();

        // Fetch all followed current spots
        spotRepository.getAllFollowedCurrentUserOverviews(
                currentLoggedInUser.getFollowedIds().keySet(),
                null,
                new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                List<UserOverview> userOverviews = (List<UserOverview>) value;
                registerPlaybackListeners(userOverviews);
            }

            @Override
            public void onFailure(Object value) {

            }
        });

    }

    /**
     * Like Pull-to-refresh
     */
    public void fetchLatestSpots(final ValueCallback callback) {
        if (getFollowedSpots().getValue() == null
                ||
                getFollowedSpots().getValue().size() == 0) {
            callback.onSuccess(0);
            return;
        }

        // Fetch the latest current spots
        spotRepository.getAllFollowedCurrentUserOverviews(
                currentLoggedInUser.getFollowedIds().keySet(),
                getFollowedSpots().getValue().keySet(),
                new ValueCallback() {
                    @Override
                    public void onSuccess(Object value) {
                        List<UserOverview> userOverviews = (List<UserOverview>) value;
                        registerPlaybackListeners(userOverviews);
                        callback.onSuccess(userOverviews.size());
                    }

                    @Override
                    public void onFailure(Object value) {
                        callback.onFailure(value.toString());
                    }
                });

        registerEventBus();

    }

    /**
     * This listener recieves SubscriptionEvents, triggered by an FCM from the Server
     * @param playbackStateChangedEvent the PlaybackStateChangedEvent, containing all the necessary information (Who & ActionCode)
     */
    @Subscribe
    public void onPlaybackStateChangedEvent(PlaybackStateChangedEvent playbackStateChangedEvent) {
        switch (playbackStateChangedEvent.getActionCode()) {
            case GlobalVariables.FCM_ACTIONCODE_PLAYBACK_STARTED:
                Log.d(TAG, "onPlaybackStateChangedEvent: Recieved FCM_ACTIONCODE_PLAYBACK_STARTED with id " + playbackStateChangedEvent.getUserId());
                // Add the new started playback to the myPlayd-feed
                addSingleSpotToFeedIfAvailable(playbackStateChangedEvent.getUserId());
                break;
            case GlobalVariables.FCM_ACTIONCODE_PLAYBACK_TERMINATED:
                Log.d(TAG, "onPlaybackStateChangedEvent: Recieved FCM_ACTIONCODE_PLAYBACK_TERMINATED with id " + playbackStateChangedEvent.getUserId());
                unregisterSingleListener(playbackStateChangedEvent.getUserId());
                // Remove the user from the followedSpots
                removeSpotFromFollowedSpots(playbackStateChangedEvent.getUserId());
                break;
        }
    }


    /**
     * This listener recieves SubscriptionEvents, usually triggered by an FCM from the Server or from the app itself,
     * f.ex. when the current logged-in user subscribes to or unsubscribes from another user inside the app
     * @param subscriptionEvent the SubscriptionEvent, containing all the necessary information (Active/Passive Participant & ActionCode)
     */
    @Subscribe
    public void onSubscriptionEvent(SubscriptionEvent subscriptionEvent) {
        /*
        In this scenario, the current logged-in user is always the active participant
        Note: There is currently no another-user-blocked-the-current-logged-in-user-scenario implemented
              If this scenario is important to handle, just fill in the logic into both "else" brackets
              (with the placeholder 'Do nothing')
         */
        if (subscriptionEvent == null) {
            return;
        }
        String actionCode = subscriptionEvent.getActionCode();
        String activeParticipant = subscriptionEvent.getActiveParticipant();
        String passiveParticipant = subscriptionEvent.getPassiveParticipant();
        String myUserId = currentLoggedInUser.getId();

        switch (actionCode) {
            case GlobalVariables.FCM_ACTIONCODE_SUBSCRIBED:
                if (myUserId.equals(activeParticipant)) { // current logged-in user is the active Participant
                    // If the new followed user is currently a spot, add the spot to the myPlayd-feed
                    Log.d(TAG, "onSubscriptionEvent: Recieved " + subscriptionEvent.getActionCode() + " with id " + subscriptionEvent.getPassiveParticipant());
                    // If the new user is currently a spot, add the spot to the myPlayd-feed
                    addSingleSpotToFeedIfAvailable(subscriptionEvent.getPassiveParticipant());
                    return;
                } else { // current logged-in user is the passive Participant
                    // Do nothing
                }
                break;
            case GlobalVariables.FCM_ACTIONCODE_UNSUBSCRIBED:
                if (myUserId.equals(activeParticipant)) { // current logged-in user is the active Participant
                    Log.d(TAG, "onSubscriptionEvent: Recieved " + subscriptionEvent.getActionCode() + " with id " + subscriptionEvent.getPassiveParticipant());
                    // Remove the user from the followedSpots
                    removeSpotFromFollowedSpots(subscriptionEvent.getPassiveParticipant());
                    return;
                } else { // current logged-in user is the passive Participant
                    // Do nothing
                }
                break;
        }

    }

    /*
    Usually called in the onStart()-Method of the Parent - Activity/Fragment
     */
    public void registerEventBus() {
        EventBus.getDefault().register(this);
        Log.d(TAG, "registerEventBus");
    }

    /*
    Usually called in the onStop()-Method of the Parent - Activity/Fragment
     */
    public void unregisterEventBus() {
        EventBus.getDefault().unregister(this);
        Log.d(TAG, "unregisterEventBus");
    }

    /**
     * Adds a single spot to the myPlayd-feed
     * This operation includes:
     * - Fetching the userOverview from DB
     * - Register the Listener and build the spot object afterwards
     * Note: If the user is not currently a spot, nothing will be added to the myPlayd-feed
     * @param userId the userId to fetch
     */
    private void addSingleSpotToFeedIfAvailable(String userId) {
        userRepository.getCurrentlyPlayingUserOverViewById(userId, new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                if (value != null) {
                    List<UserOverview> userOverviews = new ArrayList<>();
                    userOverviews.add((UserOverview) value);
                    registerPlaybackListeners(userOverviews);
                }
            }

            @Override
            public void onFailure(Object value) {
                Log.d(TAG, "addSingleSpotToFeedIfAvailable; onFailure: " + value.toString());
            }
        });
    }

    /**
     *
     * @param users
     */
    private void registerPlaybackListeners(List<UserOverview> users) {

        // If there aren't any followed users, the list size is set to zero to trigger
        // the observer in the fragment with a non-null but zero-size spot-hashmap
        if (users.size() == 0) {
            setFollowedSpots(new HashMap<>());
            return;
        }

        // Remove all already registered playbackIds
        // Because of this the method can be called as many times as you want,
        // especially if there is a new playback started and you want to add the spot to the feed
        Iterator<UserOverview> iterator = users.iterator();
        while (iterator.hasNext()) {
            UserOverview userOverview = iterator.next();
            if (userOverview == null || listenerRegistrationMap.containsKey(userOverview.getId())) {
                iterator.remove();
            }
        }

        // TODO list
        //users.stream().filter(Objects::nonNull).collect(Collectors.toList());

        for (UserOverview userOverview : users) {
            String spotId = userOverview.getId();

            Log.d(TAG, "registerPlaybackListeners() - userId: " + spotId);

            listenerRegistrationMap.put(
                    spotId,
                    firestoreDatabase
                            .collection(GlobalVariables.DB_PLAYBACK)
                            .document(userOverview.getId())
                            .addSnapshotListener(new EventListener<DocumentSnapshot>() {

                        @Override
                        public void onEvent(@javax.annotation.Nullable DocumentSnapshot documentSnapshot,
                                            @javax.annotation.Nullable FirebaseFirestoreException e) {

                            if (e == null) {
                                // Everthing worked, no error

                                if (documentSnapshot != null && documentSnapshot.exists()) {
                                    Log.d(getClass().getName(),
                                            "DataSnapshot with id " +
                                                    spotId +
                                                    " exists. Check if the snapshot is fresh or from cache");


                                    if (documentSnapshot.getMetadata().isFromCache()) {
                                        // DataSnapshot is from cache - Fetch again
                                        Log.d(getClass().getName(), "DataSnapshot is from cache - Fetch again");
                                        playbackRepository.getPlaybackById(
                                                spotId,
                                                new ValueCallback() {
                                                    @Override
                                                    public void onSuccess(Object value) {
                                                        Playback playback = (Playback) value;
                                                        // Add Meta-Information to playback
                                                        spotifyRepository.updatePlaybackInformation(playback, new ValueCallback() {
                                                            @Override
                                                            public void onSuccess(Object value) {
                                                                Playback updatedPlayback = (Playback) value;
                                                                // Finally Build the spot object and add it to the map of currentSpots
                                                                addToFollowedSpots(new Spot(userOverview, updatedPlayback));
                                                            }

                                                            @Override
                                                            public void onFailure(Object value) {
                                                                Log.d(getClass().getName(),
                                                                        "An error occured while fetching the playback meta-information " +
                                                                                "of the playback with id " + spotId + ". Errorcode: " + value.toString());
                                                            }
                                                        });

                                                    }

                                                    @Override
                                                    public void onFailure(Object value) {
                                                        Log.d(getClass().getName(),
                                                                "An error occured while fetching the playback with id "
                                                                        + spotId + ". Errorcode: " + value.toString());
                                                    }
                                                });

                                    } else {
                                        // DataSnapshot is from server
                                        Playback playback = documentSnapshot.toObject(Playback.class);
                                        Log.d("SnapshotListener - ", "Recieved playbackId: " + spotId);
                                        // Add Meta-Information to playback
                                        spotifyRepository.updatePlaybackInformation(playback, new ValueCallback() {
                                            @Override
                                            public void onSuccess(Object value) {
                                                Playback updatedPlayback = (Playback) value;
                                                // Finally Build the spot object and add it to the map of currentSpots
                                                addToFollowedSpots(new Spot(userOverview, updatedPlayback));
                                            }

                                            @Override
                                            public void onFailure(Object value) {
                                                Log.d(getClass().getName(),
                                                        "An error occured while fetching the playback meta-information " +
                                                                "of the playback with id " + spotId + ". Errorcode: " + value.toString());
                                            }
                                        });


                                    }

                                } else {
                                    // Playback was deleted
                                    // Unregister this listener and remove it from the list
                                    if (listenerRegistrationMap.get(spotId) != null) {
                                        listenerRegistrationMap.get(spotId).remove();
                                        listenerRegistrationMap.remove(spotId);
                                    }
                                    removeSpotFromFollowedSpots(spotId);
                                }

                            } else {
                                // An error occured
                                Log.w(getClass().getName(), "Listen failed.", e);
                            }

                        }

                    }));

        }

    }

    public void unregisterAllListeners() {
        if (listenerRegistrationMap == null) {
            return;
        }
        for (ListenerRegistration listener : listenerRegistrationMap.values()) {
            listener.remove();
        }
        listenerRegistrationMap.clear();
    }

    private void unregisterSingleListener(String spotId) {
        // Unregister this listener and remove it from the list
        if (listenerRegistrationMap.get(spotId) != null) {
            listenerRegistrationMap.get(spotId).remove();
            listenerRegistrationMap.remove(spotId);
        }
    }

    public MutableLiveData<Map<String, Spot>> getFollowedSpots() {
        if (followedSpots == null) {
            followedSpots = new MutableLiveData<>();
        }
        return followedSpots;
    }

    public void setFollowedSpots(Map<String, Spot> followedSpots) {
        if (followedSpots == null) {
            this.followedSpots = new MutableLiveData<>();
        }
        getFollowedSpots().postValue(followedSpots);
    }

    public void removeSpotFromFollowedSpots(String spotId) {
        Map<String, Spot> currentSpots = this.followedSpots.getValue();
        if (currentSpots == null) {
            currentSpots = new HashMap<>();
        }
        if (!currentSpots.containsKey(spotId)) {
            return;
        }
        currentSpots.remove(spotId);
        setFollowedSpots(currentSpots);
    }

    public void addToFollowedSpots(Spot spot) {
        Map<String, Spot> data = getFollowedSpots().getValue();
        if (data == null) {
            data = new HashMap<>();
        }
        data.put(spot.getId(), spot);
        setFollowedSpots(data);
    }



}
