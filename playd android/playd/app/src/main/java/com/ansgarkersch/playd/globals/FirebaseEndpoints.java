package com.ansgarkersch.playd.globals;

public class FirebaseEndpoints {


    public static final String GLOBAL_ENDPOINT =
            "https://us-central1-well-playd.cloudfunctions.net/";


    public static final String DEBUG_ENDPOINT =
            "http://localhost:5000/well-playd/us-central1/";

}
