package com.ansgarkersch.playd.model;

public enum PremiumLevel {
    BASIC,
    PREMIUM;
}
