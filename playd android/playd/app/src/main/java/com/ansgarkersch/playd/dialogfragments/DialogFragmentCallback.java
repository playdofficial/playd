package com.ansgarkersch.playd.dialogfragments;

public interface DialogFragmentCallback {
    void onDialogCallback(Object value);
}
