package com.ansgarkersch.playd._spotify.model;

public class PlaylistTrackObject {

    Track track;

    public PlaylistTrackObject() { }

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }
}
