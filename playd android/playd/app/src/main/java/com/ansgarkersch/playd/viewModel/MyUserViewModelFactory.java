package com.ansgarkersch.playd.viewModel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;

public class MyUserViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private Context mContext;

    public MyUserViewModelFactory(Context context) {
        mContext = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new MyUserViewModel(mContext);
    }
}
