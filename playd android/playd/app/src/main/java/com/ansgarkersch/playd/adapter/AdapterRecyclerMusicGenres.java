package com.ansgarkersch.playd.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AdapterRecyclerMusicGenres extends RecyclerView.Adapter<AdapterRecyclerMusicGenres.ViewHolder> {

    private List<String> genreData;
    private LayoutInflater mInflater;
    private AdapterRecyclerMusicGenres.ItemClickListener mClickListener;
    private int[] backgroundGradients;

    // data is passed into the constructor
    public AdapterRecyclerMusicGenres(Context context) {
        this.mInflater = LayoutInflater.from(context);
        genreData = new ArrayList<>();
        backgroundGradients = new int[] {
                R.drawable.genre_gradient_red,
                R.drawable.genre_gradient_yellow,
                R.drawable.genre_gradient_blue,
                R.drawable.genre_gradient_green,
                R.drawable.genre_gradient_grey,
                R.drawable.genre_gradient_orange};
    }

    public void setMusicGenres(List<String> genreData) {
        this.genreData = genreData;
        notifyDataSetChanged();
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public AdapterRecyclerMusicGenres.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row_genre_suggestions, parent, false);
        return new AdapterRecyclerMusicGenres.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(@NonNull AdapterRecyclerMusicGenres.ViewHolder holder, int position) {
        // Title and Background
        int color;
        if (position < 4) {
            color = backgroundGradients[position];
        } else {
            Random r = new Random();
            color = backgroundGradients[r.nextInt(backgroundGradients.length)];
        }

        holder.content.setBackgroundResource(color);
        holder.title.setText(genreData.get(position));

        // Handle onClick
        holder.content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) {
                    // start SpotExplorer with specific genre
                }
            }
        });

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return genreData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout content;
        TextView title;
        ImageView genreImage;

        ViewHolder(View itemView) {
            super(itemView);
            content = itemView.findViewById(R.id.genre_suggestions_rowItem_content);
            title = itemView.findViewById(R.id.genre_suggestions_rowItem_title);
            genreImage = itemView.findViewById(R.id.genre_suggestions_rowItem_alphaImage);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            //if (mClickListener != null) mClickListener.onClickPlayback(view, getAdapterPosition());
        }
    }

    // allows clicks events to be caught
    public void setClickListener(AdapterRecyclerMusicGenres.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, User user);
    }

}
