package com.ansgarkersch.playd.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.ConnectivityManager.NetworkCallback;
import android.net.Network;
import android.net.NetworkRequest;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.text.TextUtils;

/**
 * Service for listening to networkConnection and GPSConnection state changes.
 * IMPORTANT: Only supposed to be used in a Binding Context i.e. in Combination with an Activity
 * USE IT LIKE FOLLOWING:
 * Bind service to all activities which need network or gps
 * If network state changes, notify the binded activity by callback and notify the user
 */
public class ServiceConnectionListener extends Service {

    private IBinder mBinder = new ServiceConnectionListener.LocalBinder();

    private ServiceConnectionListenerCallbacks serviceConnectionListenerCallbacks;

    private BroadcastReceiver networkReciever;
    private BroadcastReceiver locationReciever;

    private ConnectivityManager connectivityManager;
    private NetworkCallback networkCallback;

    public class LocalBinder extends Binder {
        public ServiceConnectionListener getService() {
            // Return this instance of LocalService so clients can call public methods
            return ServiceConnectionListener.this;
        }
    }

    public void setCallbacks(ServiceConnectionListenerCallbacks serviceConnectionListenerCallbacks) {
        this.serviceConnectionListenerCallbacks = serviceConnectionListenerCallbacks;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        registerListeners();
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        unregisterListeners();
        return super.onUnbind(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    private void unregisterListeners() {
        // Unregister LocationReciever
        unregisterReceiver(locationReciever);
        locationReciever = null;

        // Check if Android 5.0 or higher
        // Unregister NetworkCallback
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            connectivityManager.unregisterNetworkCallback(networkCallback);
            connectivityManager = null;
        } else {
            // Unregister NetworkReciever
            unregisterReceiver(networkReciever);
            networkReciever = null;
        }
    }

    private void registerListeners() {

        // Check if Android 5.0 or higher
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            /*
            Network Listener API 21 or higher
             */
            connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkRequest.Builder builder = new NetworkRequest.Builder();
            networkCallback = new ConnectivityManager.NetworkCallback() {
                @Override
                public void onAvailable(Network network) {
                    // There is internet access
                    if (serviceConnectionListenerCallbacks != null) {
                        serviceConnectionListenerCallbacks.isInternetAccessible(true);
                    }
                }

                @Override
                public void onLost(Network network) {
                    // There is NO internet access
                    if (serviceConnectionListenerCallbacks != null) {
                        serviceConnectionListenerCallbacks.isInternetAccessible(false);
                    }
                }
            };

            // Register NetworkCallback
            connectivityManager.registerNetworkCallback(builder.build(), networkCallback);
        } else {
            IntentFilter networkRecieverIntentFilter = new IntentFilter();
            networkRecieverIntentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            networkReciever = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    /*
                    Network Listener API < 21 or lower
                     */
                    if (intent.getAction().matches(ConnectivityManager.EXTRA_NO_CONNECTIVITY)) {
                        // There is NO internet access
                        if (serviceConnectionListenerCallbacks != null) {
                            serviceConnectionListenerCallbacks.isInternetAccessible(false);
                        }
                    }
                    if (intent.getAction().matches(ConnectivityManager.EXTRA_NETWORK)) {
                        // There is internet access
                        if (serviceConnectionListenerCallbacks != null) {
                            serviceConnectionListenerCallbacks.isInternetAccessible(true);
                        }
                    }
                }
            };

            // Register networkReciever
            registerReceiver(networkReciever, networkRecieverIntentFilter);
        }

        IntentFilter locationRecieverIntentFilter = new IntentFilter();
        locationRecieverIntentFilter.addAction(LocationManager.PROVIDERS_CHANGED_ACTION);
        locationReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                /*
                GPS Listener
                 */
                if (isGPSEnabled()) {
                    // GPS was turned on
                    if (serviceConnectionListenerCallbacks != null) {
                        serviceConnectionListenerCallbacks.isGPSAccessible(true);
                    }
                } else {
                    // GPS was turned off
                    if (serviceConnectionListenerCallbacks != null) {
                        serviceConnectionListenerCallbacks.isGPSAccessible(false);
                    }
                }
            }
        };

        // Register locationReciever
        registerReceiver(locationReciever, locationRecieverIntentFilter);

    }

    private boolean isGPSEnabled() {
        int locationMode;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException ex) {
                ex.printStackTrace();
                return false;
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

}
