package com.ansgarkersch.playd.dialogfragments;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.activities.ActivityCreateAccount;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.globals.UserRolesDialogFragment;

public class DialogFragmentDecisionDialog extends DialogFragment {

    private View view;
    private UserRolesDialogFragment USERROLE;

    @SuppressLint("InflateParams")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialogfragment_decisiondialog, null);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);

        // get AlertBox USERROLE
        Bundle bundle = getArguments();
        if (bundle != null) {
            USERROLE = UserRolesDialogFragment.valueOf(bundle.getString(GlobalVariables.ALERTBOX_USERROLE));
            initDialog();
        }

        return view;
    }

    private void initDialog() {
        TextView title = view.findViewById(R.id.alert_title);
        TextView subtitle = view.findViewById(R.id.alert_subtitle);
        Button cancel = view.findViewById(R.id.alert_cancel);
        Button confirm = view.findViewById(R.id.alert_confirm);

        cancel.setText(getResources().getString(R.string.button_cancel));
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        switch (USERROLE) {
            case DECISION_UNFOLLOW_SPOT:
                title.setText(getResources().getString(R.string.alertText_unfollowSpot));
                confirm.setText(getResources().getString(R.string.alertText_remove));
                confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO
                    }
                });
                break;
            case DECISION_ABORT_CREATING_ACCOUNT:
                title.setText(getResources().getString(R.string.alertText_abortCreatingAccount));
                subtitle.setText(getResources().getString(R.string.alertText_abortCreatingAccount_subtitle));
                confirm.setText(getResources().getString(R.string.alertText_end));
                confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((ActivityCreateAccount)getActivity()).closeWindow();
                    }
                });
                break;
        }

    }
}
