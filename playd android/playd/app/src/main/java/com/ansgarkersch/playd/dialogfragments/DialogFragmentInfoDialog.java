package com.ansgarkersch.playd.dialogfragments;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.globals.UserRolesDialogFragment;

public class DialogFragmentInfoDialog extends DialogFragment {

    private View view;
    private UserRolesDialogFragment USERROLE;

    @SuppressLint("InflateParams")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialogfragment_infodialog, null);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);

        // get AlertBox USERROLE
        Bundle bundle = getArguments();
        if (bundle != null) {
            USERROLE = UserRolesDialogFragment.valueOf(bundle.getString(GlobalVariables.ALERTBOX_USERROLE));
            //initDialog();
        }

        return view;
    }

}
