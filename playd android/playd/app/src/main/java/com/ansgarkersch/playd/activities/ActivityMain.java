package com.ansgarkersch.playd.activities;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.fragments.FragmentAccount;
import com.ansgarkersch.playd.fragments.FragmentExplorePlayd;
import com.ansgarkersch.playd.fragments.FragmentMyPlayd;
import com.ansgarkersch.playd.fragments.FragmentMyPlaydlist;
import com.ansgarkersch.playd.fragments.FragmentSearchCallback;
import com.ansgarkersch.playd.fragments.FragmentToolbar;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.Notification;
import com.ansgarkersch.playd.model.Spot;
import com.ansgarkersch.playd.model.User;
import com.ansgarkersch.playd.repository.FirebaseRepository;
import com.ansgarkersch.playd.repository.SpotRepository;
import com.ansgarkersch.playd.repository.UserRepository;
import com.ansgarkersch.playd.repository.ValueCallback;
import com.ansgarkersch.playd.services.ForegroundServiceSpotListener;
import com.ansgarkersch.playd.services.ForegroundServiceSpotListenerCallbacks;
import com.ansgarkersch.playd.services.ForegroundServiceTrackObserver;
import com.ansgarkersch.playd.services.ServiceConnectionListener;
import com.ansgarkersch.playd.services.ServiceConnectionListenerCallbacks;
import com.ansgarkersch.playd.util.CountDownAnimation;
import com.ansgarkersch.playd.util.OnSlidingTouchListener;
import com.ansgarkersch.playd.util.UtilityMethods;
import com.ansgarkersch.playd.viewModel.MySpotsViewModel;
import com.ansgarkersch.playd.viewModel.MyUserViewModel;
import com.ansgarkersch.playd.viewModel.MyUserViewModelFactory;
import com.bumptech.glide.Glide;
import com.dinuscxj.progressbar.CircleProgressBar;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class ActivityMain extends AppCompatActivity implements
        ForegroundServiceSpotListenerCallbacks,
        ServiceConnectionListenerCallbacks,
        FragmentLoadingCallback,
        FragmentSearchCallback {


    // UI elements
    private ImageView home, search, account; // TODO deprecated
    private LinearLayout noNetworkIndicator;

    // UI elements
    // Toolbar
    private CoordinatorLayout toolbarMessages, toolbarAccount;
    private TextView toolbarNotificationBadgeMessages, toolbarNotificationBadgeAccount;
    // Bottombar
    private LinearLayout bottombarMyPlayd, bottombarExplorePlayd;
    private CircleImageView bottombarSpotcover;
    private CircleProgressBar bottombarSpotProgress;
    private CountDownTimer spotProgressTimer = null;

    // Services
    ForegroundServiceSpotListener mForegroundServiceSpotListener;
    boolean mBoundSpotListener = false;
    ServiceConnectionListener mServiceConnectionListener;
    boolean mBoundConnectionListener = false;

    // Repositories
    private FirebaseRepository firebaseRepository;

    // View Models
    private MySpotsViewModel mySpotsViewModel;
    private MyUserViewModel myUserViewModel;

    // BottomBar
    private int currentBottombarScreen = R.id.customBottomBar_Home; // focused by default

    // Fragments
    private Fragment activeFragment; // Placeholder for the active fragment
    private FragmentMyPlayd fragmentMyPlayd;
    private FragmentExplorePlayd fragmentExplorePlayd;

    private FragmentToolbar fragmentToolbar; // TODO deprecated
    private FragmentAccount fragmentAccount; // TODO deprecated

    // Recievers
    //private NotificationReciever notificationReciever;

    private boolean contentInitialized = false;
    private boolean actionBarInitialized = false;
    private boolean mainViewVisible = false;

    /**
     * Callback Method, triggered by ForegroundServiceSpotListener
     * @param spot-Id can be equal to the flag LOCAL_PLAYBACK which means the currently running track can not be linked to an real spot
     */
    @Override
    public void notifyPlaying(Spot spot) {
        Log.e(getClass().getName(), "notifyPlaying() is triggered: "
                + spot.getId()
                + " and trackName "
                + spot.getPlayback().getTrackName());
        if (!mForegroundServiceSpotListener.isPlaying()) {
            return;
        }

        if (spot.getId().equals(GlobalVariables.LOCAL_PLAYBACK)) {
            // the current track is not linked to a real spot
            myUserViewModel.setCurrentlyPlayingSpot(null);
            return;
        }

        mySpotsViewModel.getFollowedSpots().observe(this, new Observer<Map<String, Spot>>() {
            @Override
            public void onChanged(@Nullable Map<String, Spot> currentFollowedSpots) {
                if (currentFollowedSpots == null) {
                    myUserViewModel.setCurrentlyPlayingSpot(null);
                    return;
                }

                myUserViewModel.setCurrentlyPlayingSpot(spot);

            }
        });

    }

    /**
     * Callback Method, triggered by ForegroundServiceSpotListener
     */
    @Override
    public void notifyPaused() {
        myUserViewModel.setCurrentlyPlayingSpot(null);
        unbindForegroundServiceSpotListener();
        mySpotsViewModel.getFollowedSpots().removeObservers(this);
    }

    /**
     * Callback Method, triggered by ServiceConnectionListener
     * @param isAccessible
     */
    @Override
    public void isInternetAccessible(boolean isAccessible) {
        if (isAccessible) {
            hideNoNetworkIndicator();
        } else {
            showNoNetworkIndicator();
            // Set currently playing spot to null
            myUserViewModel.setCurrentlyPlayingSpot(null);
        }
    }

    /**
     * Callback Method, triggered by ServiceConnectionListener
     * @param isAccessible
     */
    @Override
    public void isGPSAccessible(boolean isAccessible) {
        if (isAccessible) {
            hideNoNetworkIndicator();
        } else {
            showNoNetworkIndicator();
        }
    }







    /**
     * Helper Method, triggered by isGPSAccessible()
     */
    private void showNoNetworkIndicator() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                noNetworkIndicator.setVisibility(View.VISIBLE);
            }
        });
    }

    /**
     * Helper Method, triggered by hideNoNetworkIndicator()
     */
    private void hideNoNetworkIndicator() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                noNetworkIndicator.setVisibility(View.INVISIBLE);
            }
        });
    }








    /**
     * Callback Method, triggered by FragmentMyPlayd
     */
    @Override
    public void notifyFinishedLoading(int limitCount) {
        if (mainViewVisible) {
            return;
        }

        if (limitCount == 0) {
            // Todo
            // There are no spots, show a screen with explore-playd-options instead of the feed
        } else if (limitCount == -1) {
            // Todo
            // No Internet!
        }

        initActionBar();
        initContent(); // called twice after onResume?
        findViewById(R.id.activity_main_linearLayout).setVisibility(View.VISIBLE);
        findViewById(R.id.placeholder_activity_main_splashscreen).setVisibility(View.GONE);
        mainViewVisible = true;

    }








    /**
     * Callback Method, triggered by FragmentMainToolbar
     */
    @Override
    public void onTextChanged(String s) {
        fragmentExplorePlayd.onTextChanged(s);
    }

    /**
     * Callback Method, triggered by FragmentMainToolbar
     */
    @Override
    public void onOpenSearchView() {
        fragmentExplorePlayd.onOpenSearchView();
    }

    /**
     * Callback Method, triggered by FragmentMainToolbar
     */
    @Override
    public void onCloseSearchView() {
        fragmentExplorePlayd.onCloseSearchView();
    }









    /**
     * TODO ERKLÄREN
     * @return
     */
    public int getCurrentTrackProgress() {
        return mForegroundServiceSpotListener.getCurrentTrackProgress();
    }






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout._activity_main);

        // Repositories
        firebaseRepository = new FirebaseRepository();

        // Bind Service Connection Listener
        startService(new Intent(ActivityMain.this, ServiceConnectionListener.class));

        // Initialize Fragments now to preload them
        fragmentToolbar = new FragmentToolbar();
        fragmentMyPlayd = new FragmentMyPlayd();
        fragmentExplorePlayd = new FragmentExplorePlayd();
        fragmentAccount = new FragmentAccount();

        initUIElements();

        // View Models
        mySpotsViewModel = ViewModelProviders.of(ActivityMain.this).get(MySpotsViewModel.class);
        myUserViewModel = ViewModelProviders
                .of(ActivityMain.this, new MyUserViewModelFactory(ActivityMain.this))
                .get(MyUserViewModel.class);
        // Init MyUserViewModel
        myUserViewModel.init();

        // Recievers
        //notificationReciever = new NotificationReciever();

        // Init Toolbar
        initToolbar();
        // Init Bottombar
        initBottomBar();


        // TODO TEST
        //initStartMySpotCounter();
        // TODO TEST

    }

    @Override
    protected void onResume() {
        super.onResume();
        // Init Content
        if (!contentInitialized) {
            initContent();
        }
        // Bind to ServiceConnectionListener
        Intent intentConnectionListener = new Intent(this, ServiceConnectionListener.class);
        bindService(intentConnectionListener, mConnectionConnectionListener, Context.BIND_IMPORTANT);
        Log.e(getClass().getName(), "ServiceConnectionListener is binded to ActivityMain");

        // Check if ForegroundServiceSpotListener is already running
        if (UtilityMethods.isMyServiceRunning(this, ForegroundServiceSpotListener.class)) {
            if (!mBoundSpotListener) {
                bindForegroundServiceSpotListener();
            }
        }

        // Enable all Buttons
        enableAllClickableElements();

        /*
        Register Notification Reciever and add Observer
         */
        //EventBus.getDefault().register(this); TODO
        //notificationReciever.registerReciever(this);
        //notificationReciever.addObserver(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        /*
        Unregister Notification Reciever and delete Observer
         */
        //EventBus.getDefault().unregister(this); TODO
        //notificationReciever.unregisterReciever(this);
        //notificationReciever.deleteObserver(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Register ViewModel Reciever
        if (myUserViewModel != null) {
            //myUserViewModel.registerEventBus();
        }
        // Register EventBus for MySpotsViewModel
        //mySpotsViewModel.registerEventBus();

        // Check if ForegroundServiceSpotListener is already running
        if (UtilityMethods.isMyServiceRunning(ActivityMain.this, ForegroundServiceSpotListener.class)) {
            bindForegroundServiceSpotListener();
        } else {
            // Set currentPlayingSpot null
            myUserViewModel.setCurrentlyPlayingSpot(null);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unrgister EventBus for MySpotsViewModel
        //mySpotsViewModel.unregisterEventBus();
    }

    @Override
    protected void onDestroy() {
        // Unregister ViewModel Reciever
        myUserViewModel.unregisterEventBus();
        // Unbind ServiceConnectionListener
        unbindService(mConnectionConnectionListener);
        mServiceConnectionListener.setCallbacks(null); // unregister
        mBoundConnectionListener = false;
        Log.e(getClass().getName(), "ServiceConnectionListener is unbinded by ActivityMain");
        // Kill all activities below
        // Unbind ForegroundServiceSpotListener
        unbindForegroundServiceSpotListener();
        finishAffinity();
        super.onDestroy();
    }


    /**
     * todo comment
     */
    public void bindForegroundServiceSpotListener() {
        if (!mBoundSpotListener) {
            Intent intentForegroundServiceSpotListener = new Intent(this, ForegroundServiceSpotListener.class);
            bindService(intentForegroundServiceSpotListener, mConnectionSpotListener, Context.BIND_AUTO_CREATE);
            Log.e(getClass().getName(), "ForegroundServiceSpotListener is binded to ActivityMain");
        }
    }

    public void unbindForegroundServiceSpotListener() {
        if (mBoundSpotListener) {
            unbindService(mConnectionSpotListener);
            mBoundSpotListener = false;
            if (mForegroundServiceSpotListener != null) {
                mForegroundServiceSpotListener.setCallbacks(null);
            }
            Log.e(getClass().getName(), "ForegroundServiceSpotListener is unbinded by ActivityMain");
        }
    }

    /**
     * Init Binding to ForegroundServiceSpotListener
     */
    private ServiceConnection mConnectionSpotListener = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ForegroundServiceSpotListener.LocalBinder binder = (ForegroundServiceSpotListener.LocalBinder) service;
            mForegroundServiceSpotListener = binder.getService();
            mBoundSpotListener = true;

            if (mForegroundServiceSpotListener == null) {
                return;
            }

            mForegroundServiceSpotListener.setCallbacks(ActivityMain.this);

            // is called only once, when the service is fresh binded to this activity
            if (mForegroundServiceSpotListener.getCurrentSpot() == null) {
                return;
            }
            notifyPlaying(mForegroundServiceSpotListener.getCurrentSpot());

        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBoundSpotListener = false;
            myUserViewModel.setCurrentlyPlayingSpot(null);
            //unbindForegroundServiceSpotListener();
            mySpotsViewModel.getFollowedSpots().removeObservers(ActivityMain.this);
        }
    };


    /**
     * Init Binding to ServiceConnectionListener
     */
    private ServiceConnection mConnectionConnectionListener = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ServiceConnectionListener.LocalBinder binder = (ServiceConnectionListener.LocalBinder) service;
            mServiceConnectionListener = binder.getService();
            mBoundConnectionListener = true;

            if (mServiceConnectionListener == null) {
                return;
            }

            mServiceConnectionListener.setCallbacks(ActivityMain.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBoundConnectionListener = false;
        }
    };

    public void setAccountNotificationBadge(int count) {
        fragmentToolbar.setAccountNotificationBadge(count);
    }

    /**
     * Initializes the action bar
     */
    private void initActionBar() {
        if (actionBarInitialized) {
            return;
        }
        try {
            actionBarInitialized = true;
            Toolbar toolbar = findViewById(R.id.fragment_main_toolbar_toolbar_mainContent);
            setSupportActionBar(toolbar);
            ActionBar actionbar = getSupportActionBar();
            actionbar.setTitle("");
        } catch (NullPointerException ex) {
            actionBarInitialized = false;
            Log.e(GlobalVariables.EXCEPTIONTAG_NPE, " - ActionBar is null");
        }
    }

    /**
     * initializes the bottom bar
     */
    private void initContent() {
        if (contentInitialized) {
            return;
        }
        try {
            FragmentManager manager = getSupportFragmentManager();

            // Init the first main fragments
            manager.beginTransaction().replace(R.id.frame_activity_main_top, fragmentToolbar).commit();
            manager.beginTransaction().add(R.id.frame_activity_main_bottom, fragmentMyPlayd, "1").commit();
            activeFragment = fragmentMyPlayd;
            // Init and hide the other fragments
            manager.beginTransaction().add(R.id.frame_activity_main_bottom, fragmentExplorePlayd, "2").hide(fragmentExplorePlayd).commit();
            //manager.beginTransaction().add(R.id.frame_activity_main_bottom, fragmentMyPlaydlists, "3").hide(fragmentMyPlaydlists).commit();
            manager.beginTransaction().add(R.id.frame_activity_main_bottom, fragmentAccount, "4").hide(fragmentAccount).commit();

            contentInitialized = true;
        } catch (IllegalStateException ex) {
            contentInitialized = false;
        }
    }

    private void initUIElements() {
        noNetworkIndicator = findViewById(R.id.activity_main_noNetworkIndicator);
    }

    /**
     * handles click events on bottomBar
     */
    public void onClickCustomBottombar(View view) {
        home = findViewById(R.id.customBottomBar_Icon_Home);
        search = findViewById(R.id.customBottomBar_Icon_Search);
        //myPlaydlist = findViewById(R.id.customBottomBar_Icon_MyPlaydlist);
        account = findViewById(R.id.customBottomBar_Icon_Account);
        home.setColorFilter(getResources().getColor(android.R.color.darker_gray));
        search.setColorFilter(getResources().getColor(android.R.color.darker_gray));
        //myPlaydlist.setColorFilter(getResources().getColor(android.R.color.darker_gray));
        account.setColorFilter(getResources().getColor(android.R.color.darker_gray));

        FragmentTransaction manager = getSupportFragmentManager().beginTransaction();
        if (view.getId() != currentBottombarScreen) {
            //manager.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
        }

        UtilityMethods.hideKeyboard(this);

        switch (view.getId()) {
            case R.id.customBottomBar_Home:
                fragmentToolbar.enableMainView();
                manager.hide(activeFragment).show(fragmentMyPlayd).commit();
                activeFragment = fragmentMyPlayd;
                home.setColorFilter(getResources().getColor(R.color.colorBlack));
                break;
            case R.id.customBottomBar_Search:
                fragmentToolbar.enableSearchView();
                manager.hide(activeFragment).show(fragmentExplorePlayd).commit();
                activeFragment = fragmentExplorePlayd;
                search.setColorFilter(getResources().getColor(R.color.colorBlack));
                break;
                /*
            case R.id.customBottomBar_MyPlaydlist:
                fragmentToolbar.enableMainView();
                manager.hide(activeFragment).show(fragmentMyPlaydlists).commit();
                activeFragment = fragmentMyPlaydlists;
                myPlaydlist.setColorFilter(getResources().getColor(R.color.colorBlack));
                break;
                */
            case R.id.customBottomBar_Account:
                fragmentToolbar.enableAccountView();
                manager.hide(activeFragment).show(fragmentAccount).commit();
                activeFragment = fragmentAccount;
                account.setColorFilter(getResources().getColor(R.color.colorBlack));
                break;
        }
        currentBottombarScreen = view.getId();
    }

    public void onClickAccount(View view) {
        switch (view.getId()) {
            case R.id.fragment_account_button_editProfile:
                Intent editProfile = new Intent(ActivityMain.this, ActivityAccountEditProfile.class);
                editProfile.putExtra(GlobalVariables.INTENT_PROFILE, myUserViewModel.getCurrentUser().getValue());
                startActivityForResult(editProfile, GlobalVariables.REQUESTCODE_GET_USER);
                overridePendingTransition(R.anim.fade_in, android.R.anim.fade_out);
                break;
        }
    }

    @Override
    public void onBackPressed(){
        if (activeFragment == null) {
            finish();
            return;
        }

        if (activeFragment instanceof FragmentMyPlayd) {
            if (((FragmentMyPlayd) activeFragment).isFeedScrolled()) {
                // the feed is scrolled
                ((FragmentMyPlayd) activeFragment).scrollToTop();
            } else {
                finish();
            }
        } else if (activeFragment instanceof FragmentExplorePlayd) {
            FragmentTransaction manager = getSupportFragmentManager().beginTransaction();
            manager.hide(activeFragment).show(fragmentMyPlayd).commit();
            bottombarMyPlayd.setBackground(null);
            bottombarExplorePlayd.setBackgroundColor(getResources()
                    .getColor(R.color.colorPlaydTransparentWhite));
            activeFragment = fragmentExplorePlayd;
        }



    }

    public void onClickToolbar(View view) {
        switch (view.getId()) {
            case R.id.activity_main_toolbar_spotexplorer:

                DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                ref.child(GlobalVariables.DB_USEROVERVIEW)
                        .orderByChild("t")
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String s = "";
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        String s = "";
                    }
                });

                break;
            case R.id.activity_main_toolbar_myspot_profileImage:
                User user = myUserViewModel.getCurrentUser().getValue();
                if (user == null) {
                    return;
                }
                /*
                Intent intentMySpot = new Intent(ActivityMain.this, ActivityMySpot.class);
                intentMySpot.putExtra(GlobalVariables.INTENT_PROFILE, user);
                startActivity(intentMySpot);
                overridePendingTransition(R.anim.spotexplorer_slide_in_left_to_right, android.R.anim.fade_out);
                */

                break;
            case R.id.activity_main_toolbar_account_settings:
                firebaseRepository.logoutUser(new ValueCallback() {
                    @Override
                    public void onSuccess(Object value) {
                        Intent intentWelcomeScreen = new Intent(ActivityMain.this, ActivityWelcomeScreen.class);
                        startActivity(intentWelcomeScreen);
                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                        finish();
                    }

                    @Override
                    public void onFailure(Object value) {

                    }
                });
                break;
        }
    }

    /**
     * Called after updating user at ActivityEditProfile
     * @param requestCode
     * @param resultCode
     * @param data contains the updated user object
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case GlobalVariables.REQUESTCODE_GET_USER:
                if (resultCode == Activity.RESULT_OK){
                    // Update edited user locally
                    User updatedUser = (User) data.getSerializableExtra(GlobalVariables.INTENT_PROFILE);
                    // Remove accountBadges ("complete your account data") if possible
                    if (!updatedUser.getBirthday().isEmpty()
                            || updatedUser.getProfileImages().size() > 0) {
                        setAccountNotificationBadge(-1);
                        Log.d(getClass().getName(), "Decrement AccountNotificationBadge Count");
                    }
                    myUserViewModel.setCurrentUser(updatedUser);
                }
                break;
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Notification event) {



    }


    /**
     * Initializes the toolbar
     */
    private void initToolbar() {
        // Init Items
        toolbarMessages = findViewById(R.id.activity_main_toolbar_chat);
        toolbarAccount = findViewById(R.id.activity_main_toolbar_account);
        // Init Notifications Badges
        toolbarNotificationBadgeMessages = findViewById(R.id.activity_main_toolbar_chat_textview_notifications);
        toolbarNotificationBadgeAccount = findViewById(R.id.activity_main_toolbar_account_textview_notifications);

        myUserViewModel.getCurrentUser().observe(ActivityMain.this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                if (user == null) {
                    return;
                }
                toolbarAccount.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        /*
                        FirebaseMessaging firebaseMessaging = FirebaseMessaging.getInstance();
                        firebaseMessaging.subscribeToTopic("baba")
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        Log.d(getClass().getName(), "Successfully subscribed to user with id ");
                                    }
                                }).addOnCanceledListener(new OnCanceledListener() {
                            @Override
                            public void onCanceled() {
                                Log.d(getClass().getName(), "Subscribing to user with id was canceled");
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d(getClass().getName(), "Subscribing to user with id failed " + e.getMessage());
                            }
                        });
                        */

                        SpotRepository spotRepository = new SpotRepository(new UserRepository());
                        spotRepository.getTopSpots(4, -1, new ValueCallback() {
                            @Override
                            public void onSuccess(Object value) {
                                List<Spot> data = (List<Spot>) value;
                                String s = "";
                            }

                            @Override
                            public void onFailure(Object value) {
                                String s = "";
                            }
                        });

                        /*
                        // TODO open own account by startActivityForResult

                        Intent intent = new Intent(ActivityMain.this, ActivityWatchProfile.class);
                        intent.putExtra(GlobalVariables.INTENT_PROFILE_ID, user.getId());
                        intent.putExtra(GlobalVariables.INTENT_PROFILE_USERNAME, user.getUsername());
                        intent.putExtra(GlobalVariables.INTENT_PROFILE_CURRENT_USER, user);
                        startActivity(intent);
                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                        */
                    }
                });
            }
        });

    }

    /**
     * Initializes the bottombar
     */
    @SuppressLint("ClickableViewAccessibility")
    private void initBottomBar() {
        // Init all Items
        bottombarMyPlayd = findViewById(R.id.activity_main_linearlayout_bottombar_myPlayd);
        bottombarExplorePlayd = findViewById(R.id.activity_main_linearlayout_bottombar_explorePlayd);
        bottombarSpotcover = findViewById(R.id.activity_main_circleimageview_bottombar_spotcover);
        bottombarSpotProgress = findViewById(R.id.activity_main_circleprogressbar_bottombar_spotprogress);
        bottombarSpotProgress.setMax(100);


        // "Home": MyPlayd
        bottombarMyPlayd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction manager = getSupportFragmentManager().beginTransaction();
                manager.hide(activeFragment).show(fragmentMyPlayd).commit();
                bottombarMyPlayd.setBackground(null);
                bottombarExplorePlayd.setBackgroundColor(getResources()
                                .getColor(R.color.colorPlaydTransparentWhite));
                activeFragment = fragmentExplorePlayd;
            }
        });

        // "Magnifiy": ExplorePlayd
        bottombarExplorePlayd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction manager = getSupportFragmentManager().beginTransaction();
                manager.hide(activeFragment).show(fragmentExplorePlayd).commit();
                bottombarExplorePlayd.setBackground(null);
                bottombarMyPlayd.setBackgroundColor(getResources()
                        .getColor(R.color.colorPlaydTransparentWhite));
                activeFragment = fragmentExplorePlayd;
            }
        });

        // Update the bottombarSpotCover
        myUserViewModel.getCurrentlyPlayingSpot().observe(this, new Observer<Spot>() {
            @Override
            public void onChanged(@Nullable Spot spot) {
                if (spot == null || GlobalVariables.LOCAL_PLAYBACK.equals(spot.getId())) {
                    // The user does not listen to another spot
                    // 1) Hide the SpotProgress
                    bottombarSpotProgress.setVisibility(View.GONE);
                    bottombarSpotProgress.setProgress(0);
                    // Set the default image to the spotCover
                    bottombarSpotcover.setImageResource(R.mipmap.icon_colored_small);

                    // Change onClickListener, no spot is active
                    bottombarSpotcover.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            bottombarSpotcover.setOnClickListener(null); // Disable the click listener until the windows is opened

                            if (UtilityMethods.isMyServiceRunning(ActivityMain.this, ForegroundServiceTrackObserver.class)) {
                                // Service is already running
                                disableSpot();
                            } else {
                                enableSpot();
                            }

                        }
                    });
                    /*
                    bottombarSpotcover.setOnTouchListener(new OnSlidingTouchListener(ActivityMain.this) {

                        @Override
                        public boolean onSlideUp() {
                            bottombarSpotcover.setOnTouchListener(null); // Disable the click listener until the windows is opened

                            if (UtilityMethods.isMyServiceRunning(ActivityMain.this, ForegroundServiceTrackObserver.class)) {
                                // Service is already running
                                disableSpot();
                            } else {
                                enableSpot();
                            }

                            return true;
                        }

                    });
                    */

                    // Reset TrackProgressTimer
                    if (spotProgressTimer != null) {
                        spotProgressTimer.cancel();
                    }

                    return;
                }

                // Set the albumArt to the spotCover
                Glide.with(ActivityMain.this).load(spot.getPlayback().getAlbumArt_300()).into(bottombarSpotcover);

                // Reset TrackProgressTimer
                if (spotProgressTimer != null) {
                    spotProgressTimer.cancel();
                }

                spotProgressTimer = new CountDownTimer(Integer.MAX_VALUE, 1000) {
                    @Override
                    public void onTick(long l) {
                        /*
                        if (getActivity() == null) {
                            spotProgressTimer.cancel();
                            return;
                        }
                        */
                        bottombarSpotProgress.setProgress(getCurrentTrackProgress());
                    }

                    @Override
                    public void onFinish() { }
                }.start();

                // Show spotProgress
                bottombarSpotProgress.setVisibility(View.VISIBLE);

                // Change onClickListener, the foreign spot is active
                bottombarSpotcover.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        disableAllClickableElements();
                        bottombarSpotcover.setOnClickListener(null); // Disable the click listener until the windows is opened

                        // Unbind ForegroundServiceSpotListener
                        unbindForegroundServiceSpotListener();
                        openSpotViewWithForeignSpot(spot);

                    }
                });
                /*
                bottombarSpotcover.setOnTouchListener(new OnSlidingTouchListener(ActivityMain.this) {
                    @Override
                    public boolean onSlideUp() {
                        disableAllClickableElements();
                        // Unbind ForegroundServiceSpotListener
                        openSpotViewWithForeignSpot(spot);

                        return true;
                    }
                });
                */

            }
        });


    }

    private void enableSpot() {
        Intent i = new Intent(this, ForegroundServiceTrackObserver.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            startForegroundService(i);
        } else {
            startService(i);
        }
    }

    private void disableSpot() {
        Intent serviceIntent = new Intent(this, ForegroundServiceTrackObserver.class);
        stopService(serviceIntent);
    }

    private void openSpotViewWithForeignSpot(Spot foreignSpot) {
        Intent intentSpotView = new Intent(ActivityMain.this, ActivitySpotView.class);
        intentSpotView.putExtra(GlobalVariables.INTENT_SPOT, foreignSpot);
        intentSpotView.putExtra(GlobalVariables.INTENT_PROFILE_CURRENT_USER, myUserViewModel.getCurrentUser().getValue());
        startActivity(intentSpotView);
        overridePendingTransition(R.anim.activity_slide_in_up, android.R.anim.fade_out);
    }


    private void disableAllClickableElements() {
        bottombarSpotcover.setEnabled(false);
    }

    private void enableAllClickableElements() {
        bottombarSpotcover.setEnabled(true);
    }



    private void initStartMySpotCounter() {
        TextView mySpotCountdown = findViewById(R.id.activity_main_spotStart_textview_counter);

        CountDownAnimation countDownAnimation = new CountDownAnimation(mySpotCountdown, 4);
        countDownAnimation.setCountDownListener(new CountDownAnimation.CountDownListener() {
            @Override
            public void onCountDownEnd(CountDownAnimation animation) {
                mySpotCountdown.setVisibility(View.VISIBLE);
                mySpotCountdown.setText("Du bist jetzt Live!");
                Intent intentMySpot = new Intent(ActivityMain.this, ActivityMySpot.class);
                startActivity(intentMySpot);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });
        countDownAnimation.start();

    }























    /*
    Notification Observer
     */
    /*
    @Override
    public void update(Observable observable, Object o) {
        Notification recievedNotification = (Notification) o;

        new UserRepository().getUserOverViewById(recievedNotification.getUserId(), new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                UserOverview userOverview = (UserOverview) value;
                if (GlobalVariables.RELATIONSHIP_FRIEND.equals(recievedNotification.getNotificationType())) {
                    // Show DialogFragment
                    // TODO ?

                } else {
                    LinearLayout notification = findViewById(R.id.activity_main_notification_linearlayout);
                    TextView notificationMessage = findViewById(R.id.activity_main_notification_textview_message);
                    CircleImageView notificationImage = findViewById(R.id.activity_main_notification_circleimageview);
                    // Load profileImage
                    if (userOverview.getProfilePicUrl()!= null && !userOverview.getProfilePicUrl().equals("")) {
                        Glide.with(ActivityMain.this).load(userOverview.getProfilePicUrl()).into(notificationImage);
                    }
                    switch (recievedNotification.getNotificationType()) {
                        case GlobalVariables.ACTIONCODE_NOTIFICATION_MESSAGE:
                            notificationMessage.setText("Eine neue Nachricht von " + userOverview.getU());
                            notification.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    User user = myUserViewModel.getCurrentUser().getValue();
                                    if (user == null) {
                                        return;
                                    }
                                    Intent intentMySpot = new Intent(ActivityMain.this, ActivityMySpot.class);
                                    intentMySpot.putExtra(GlobalVariables.INTENT_PROFILE, user);
                                    startActivity(intentMySpot);
                                    overridePendingTransition(R.anim.spotexplorer_slide_in_left_to_right, android.R.anim.fade_out);
                                }
                            });
                            break;
                        case GlobalVariables.ACTIONCODE_NOTIFICATION_NEW_FOLLOWER:
                            notificationMessage.setText(userOverview.getU() + " folgt dir jetzt");
                            break;
                    }

                    notification.setVisibility(View.VISIBLE);
                    notification.animate().alpha(1.0f).setDuration(500).setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            new CountDownTimer(2000, 1000) {
                                @Override
                                public void onTick(long l) { }

                                @Override
                                public void onFinish() {
                                    notification.animate().alpha(0.0f).setDuration(500).setListener(new AnimatorListenerAdapter() {
                                        @Override
                                        public void onAnimationEnd(Animator animation) {
                                            super.onAnimationEnd(animation);
                                            notification.setVisibility(View.GONE);
                                        }
                                    });
                                }
                            }.start();
                        }
                    });


                }

            }

            @Override
            public void onFailure(Object value) {

            }
        });


    }
    */

}
