package com.ansgarkersch.playd.repository;

public interface AuthCallback {
    void onCallback(int resultCode);
}
