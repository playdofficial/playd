package com.ansgarkersch.playd.fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.activities.ActivityCreateAccount;
import com.ansgarkersch.playd.activities.ActivityCropImage;
import com.ansgarkersch.playd.globals.GlobalVariables;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class FragmentCreateAccountSecond extends Fragment {

    private CircleImageView profilePic;
    private View view;
    private Uri uri = null;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_create_account_second,
                container, false);
        profilePic = view.findViewById(R.id.fragment_create_account_second_profilePic);
        uri = Uri.parse("android.resource://com.ansgarkersch.playd/mipmap/sample_user_1"); // TODO default picture
        ((ActivityCreateAccount) Objects.requireNonNull(getActivity())).setUserProfilePicToBeCreated(uri);

        view.findViewById(R.id.fragment_create_account_second_addProfilePic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cropImageIntent = new Intent(view.getContext(), ActivityCropImage.class);
                startActivityForResult(cropImageIntent, GlobalVariables.REQUESTCODE_PICKIMAGE);
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == GlobalVariables.REQUESTCODE_PICKIMAGE) {
            if(resultCode == Activity.RESULT_OK){
                uri = Uri.parse(data.getStringExtra(GlobalVariables.URI_IMAGE));
                ((ActivityCreateAccount)getActivity()).setUserProfilePicToBeCreated(uri);
                profilePic.setImageURI(uri);
                ((ActivityCreateAccount) Objects.requireNonNull(getActivity())).setUserProfilePicToBeCreated(uri);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                // Who care's
            }
        }

        // TODO delete image

    }

}
