package com.ansgarkersch.playd.model;

import com.ansgarkersch.playd.globals.GlobalVariables;

import java.io.Serializable;

public class UserOverview implements Serializable, IScrollable {

    private String id = "";
    private String profilePicUrl = ""; // profilePic
    private String u = ""; // username
    private boolean t = false; // isPlaying
    private long s = 0L; // spotStartedTimestamp

    @Override
    public int getType() {
        return GlobalVariables.VIEWTYPE_USEROVERVIEW;
    }

    public UserOverview() { }

    /**
     *
     * @param id KEY userId
     * @param username CHILD username
     * @param profilePicUrl RUNTIME profilePicUrl
     * @param t OPTIONAL isPlaying
     * @param s OPTIONAL spotStartedTimestamp
     */
    public UserOverview(String id, String username, String profilePicUrl, boolean t, long s) {
        this.id = id;
        this.profilePicUrl = profilePicUrl;
        this.u = username; // username
        this.t = t; // isPlaying
        this.s = s; // spotStartedTimestamp
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProfilePicUrl() {
        if (profilePicUrl.equals("")) {
            return GlobalVariables.PLACEHOLDER_NO_PROFILEIMAGE;
        }
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public String getU() {
        return u;
    }

    public void setU(String username) {
        this.u = username;
    }

    public boolean isT() {
        return t;
    }

    public void setT(boolean t) {
        this.t = t;
    }

    public long getS() {
        return s;
    }

    public void setS(long s) {
        this.s = s;
    }
}
