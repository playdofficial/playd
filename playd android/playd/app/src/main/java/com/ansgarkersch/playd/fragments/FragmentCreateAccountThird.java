package com.ansgarkersch.playd.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.activities.ActivityCreateAccount;

import java.util.Objects;

public class FragmentCreateAccountThird extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_account_third,
                container, false);
        view.findViewById(R.id.fragment_create_account_third_button_spotify).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ActivityCreateAccount) Objects.requireNonNull(getActivity())).getSpotifyPermissionCode();
            }
        });

        return view;
    }

}
