package com.ansgarkersch.playd.model;

import android.support.annotation.NonNull;

import com.ansgarkersch.playd.globals.GlobalVariables;

import java.io.Serializable;

public class Spot implements Serializable, ISwipeable, IScrollable, Comparable<Spot> {

    private String id;
    private UserOverview user;
    private Playback playback;

    public Spot() { }

    public Spot(String id) {
        this.id = id;
    }

    public Spot(UserOverview user, Playback playback) {
        this.id = user.getId();
        this.user = user;
        this.playback = playback;
    }

    @Override
    public int getType() {
        return GlobalVariables.VIEWTYPE_SPOT;
    }

    @Override
    public int compareTo(@NonNull Spot o) {
        return getPlayback().compareTo(o.getPlayback());
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj.getClass() == Spot.class && this.id.equals(((Spot) obj).getId());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserOverview getUser() {
        return user;
    }

    public void setUser(UserOverview user) {
        this.user = user;
    }

    public Playback getPlayback() {
        return playback;
    }

    public void setPlayback(Playback playback) {
        this.playback = playback;
    }
}
