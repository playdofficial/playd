package com.ansgarkersch.playd.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.repository.FirebaseRepository;
import com.ansgarkersch.playd.repository.ValueCallback;

public class ActivityLoginWithSpotify extends AppCompatActivity {

    // UI elements
    private ImageButton back;

    // Repositories
    private FirebaseRepository firebaseRepository;

    // Spotify user data
    private String accessToken;
    private String spotifyUserId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connectwithspotify);

        // Get recieved Spotify userId and accessToken from Bundle
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            finish();
            Toast.makeText(this, R.string.toast_unexpected_error_occured, Toast.LENGTH_LONG).show();
            return;
        } else {
            spotifyUserId = getIntent().getStringExtra(GlobalVariables.INTENT_SPOTIFY_USER_ID);
            accessToken = getIntent().getStringExtra(GlobalVariables.INTENT_SPOTIFY_ACCESS_TOKEN);
        }

        // Repositories
        firebaseRepository = new FirebaseRepository();

        // UI elements
        back = findViewById(R.id.activity_connectwithspotify_imagebutton_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        // Login with Spotify
        firebaseRepository.loginWithSpotify(accessToken, spotifyUserId, new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                String userId = value.toString();
                if (userId == null || userId.isEmpty()) {
                    handleError();
                    return;
                }
                // The Login was successful. Open ActivityMain
                Intent intentRegister = new Intent(ActivityLoginWithSpotify.this, ActivityMain.class);
                startActivity(intentRegister);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }

            @Override
            public void onFailure(Object value) {
                handleError();
            }
        });

    }

    private void handleError() {
        // TODO Meldung: Der Login ist gerade nicht möglich. Bitte probieren Sie es ein anderes mal
    }

}
