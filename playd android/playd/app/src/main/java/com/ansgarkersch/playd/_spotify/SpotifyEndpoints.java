package com.ansgarkersch.playd._spotify;

public class SpotifyEndpoints {

    public static final String GLOBAL_ENDPOINT = "https://api.spotify.com/";
    public static final String TOKEN_URL = "https://accounts.spotify.com/";

}
