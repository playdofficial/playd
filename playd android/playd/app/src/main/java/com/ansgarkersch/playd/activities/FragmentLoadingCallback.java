package com.ansgarkersch.playd.activities;

public interface FragmentLoadingCallback {
    void notifyFinishedLoading(int itemCount);
}
