package com.ansgarkersch.playd.recievers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.CountDownTimer;
import android.support.v4.content.LocalBroadcastManager;

import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.events.SubscriptionEvent;

import java.util.Observable;

public class SubscriptionReciever extends Observable {

    private CountDownTimer broadcastRecieverTimer = null;

    public SubscriptionReciever() { }

    public void registerReciever(Context context) {
        IntentFilter customIntentFilter = new IntentFilter(GlobalVariables.FCM_ACTIONCODE_SUBSCRIBED);
        customIntentFilter.addAction(GlobalVariables.FCM_ACTIONCODE_UNSUBSCRIBED);
        customIntentFilter.addCategory(GlobalVariables.FCM_CATEGORY_USERRELATIONSHIP);
        LocalBroadcastManager.getInstance(context).registerReceiver(bReciever, customIntentFilter);
    }

    public void unregisterReciever(Context context) {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(bReciever);
    }

    private BroadcastReceiver bReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null || intent.getAction() == null) {
                return;
            }

            if (broadcastRecieverTimer != null) {
                broadcastRecieverTimer.cancel();
            }
            broadcastRecieverTimer = new CountDownTimer(1000, 1000) {
                public void onTick(long millisUntilFinished) { }

                public void onFinish() {
                    // ... activeParticipant does 'actionCode' to passiveParticipant ...
                    // example: userA(me) subscribed userB
                    // -> SubscriptionEvent = { userId: userA, user: userB{}, actionCode: SUBSCRIBED }
                    // example: userB unsubscribed userA(me)
                    // -> SubscriptionEvent = { userId: userB, user: userB{}, actionCode: UNSUBSCRIBED }

                    SubscriptionEvent subscriptionEvent = (SubscriptionEvent) intent.getSerializableExtra(GlobalVariables.INTENT_SUBSCRIPTIONEVENT);
                    setChanged();
                    notifyObservers(subscriptionEvent);

                }
            }.start();

        }
    };


}
