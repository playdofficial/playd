package com.ansgarkersch.playd.services;

import com.ansgarkersch.playd.model.Spot;

public interface ForegroundServiceSpotListenerCallbacks {
    void notifyPlaying(Spot spot);
    void notifyPaused();
}
