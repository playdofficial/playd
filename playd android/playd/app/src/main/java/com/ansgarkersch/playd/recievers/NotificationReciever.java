package com.ansgarkersch.playd.recievers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.CountDownTimer;
import android.support.v4.content.LocalBroadcastManager;

import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.Notification;

import java.util.Observable;

/**
 * An Activity can implement Oberserver Interface and subscribe to this Reciever and gets updated if:
 * - A message comes in (small Notification)
 * - A user subscibed to the current-logged-in user (small Notification)
 * - A new friendship is born (large Notification)
 * RECOMMENDATION: observe in onResume(), removeObserver in onPause()
 */
public class NotificationReciever extends Observable {

    private CountDownTimer broadcastRecieverTimer = null;

    public NotificationReciever() { }

    public void registerReciever(Context context) {
        IntentFilter customIntentFilter = new IntentFilter(GlobalVariables.ACTIONCODE_NOTIFICATION_FRIENDSHIP);
        customIntentFilter.addAction(GlobalVariables.ACTIONCODE_NOTIFICATION_MESSAGE);
        customIntentFilter.addAction(GlobalVariables.ACTIONCODE_NOTIFICATION_NEW_FOLLOWER);
        customIntentFilter.addCategory(GlobalVariables.CATEGORY_NOTIFICATION);
        LocalBroadcastManager.getInstance(context).registerReceiver(bReciever, customIntentFilter);
    }

    public void unregisterReciever(Context context) {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(bReciever);
    }

    private BroadcastReceiver bReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null || intent.getAction() == null) {
                return;
            }

            if (broadcastRecieverTimer != null) {
                broadcastRecieverTimer.cancel();
            }
            broadcastRecieverTimer = new CountDownTimer(1000, 1000) {
                public void onTick(long millisUntilFinished) { }

                public void onFinish() {
                    Notification notification = (Notification) intent.getSerializableExtra(GlobalVariables.INTENT_NOTIFICATION);
                    setChanged();
                    notifyObservers(notification);
                }
            }.start();

        }
    };

}
