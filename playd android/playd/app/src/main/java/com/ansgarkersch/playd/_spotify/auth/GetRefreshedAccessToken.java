package com.ansgarkersch.playd._spotify.auth;

import android.os.AsyncTask;

import com.ansgarkersch.playd._spotify.SpotifyEndpoints;
import com.ansgarkersch.playd._spotify.SpotifyGlobalVariables;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;

/**
 * Fetches the RefreshToken from Spotify
 * returns: RefreshToken as a String
 */
public class GetRefreshedAccessToken extends AsyncTask<String, Integer, String> {

    @Override
    protected void onCancelled(String s) {
        super.onCancelled(s);
    }

    @Deprecated
    @Override
    protected String doInBackground(String... strings) {
        String refreshToken = strings[0];

        HttpURLConnection httpURLConnection = null;
        String postParam =
                "client_id=" + SpotifyGlobalVariables.CLIENT_ID + "&" +
                "client_secret=" + SpotifyGlobalVariables.CLIENT_SECRET + "&" +
                "grant_type=" + "refresh_token" + "&" +
                "refresh_token=" + refreshToken;

        try {
            URL url = new URL(SpotifyEndpoints.TOKEN_URL);
            httpURLConnection = (HttpURLConnection)
                    url.openConnection();
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setReadTimeout(5000);
            httpURLConnection.setConnectTimeout(5000);
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();

            DataOutputStream dos = new DataOutputStream(httpURLConnection.getOutputStream());
            dos.writeBytes(postParam);
            dos.flush();
            dos.close();

        } catch (IOException e1) {
            e1.printStackTrace();
        }

        StringBuilder inputResponse = new StringBuilder();

        try {
            int response = Objects.requireNonNull(httpURLConnection).getResponseCode();
            if(response == HttpURLConnection.HTTP_OK) {
                InputStream dataInputStream = httpURLConnection.getInputStream();
                InputStreamReader isr = new InputStreamReader(Objects.requireNonNull(dataInputStream));
                BufferedReader reader = new BufferedReader(isr);

                String line;
                while ((line = reader.readLine()) != null) {
                    inputResponse.append(line);
                }
                dataInputStream.close();

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return inputResponse.toString();

    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
    }
}

