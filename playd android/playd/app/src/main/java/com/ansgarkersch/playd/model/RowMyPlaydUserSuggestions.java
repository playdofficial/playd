package com.ansgarkersch.playd.model;

import com.ansgarkersch.playd.globals.GlobalVariables;

import java.util.List;

public class RowMyPlaydUserSuggestions implements IScrollable {

    private List<User> suggestions;

    public RowMyPlaydUserSuggestions() { }

    public RowMyPlaydUserSuggestions(List<User> suggestions) {
        this.suggestions = suggestions;
    }

    @Override
    public int getType() {
        return GlobalVariables.VIEWTYPE_USERSUGGESTIONS;
    }

    public List<User> getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(List<User> suggestions) {
        this.suggestions = suggestions;
    }

}
