package com.ansgarkersch.playd.fragments;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd._spotify.repository.SpotifyRepository;
import com.ansgarkersch.playd.adapter.AdapterRecyclerPlaydlist;

import de.hdodenhof.circleimageview.CircleImageView;

public class FragmentWatchProfilePlaydlist extends Fragment {

    private View view;
    //private CircleImageView profileImage;
    private TextView name, recyclerViewEmptyText;

    //private User currentUser;
    private SpotifyRepository spotifyRepository;

    private AdapterRecyclerPlaydlist recyclerAdapterPlaydlist;
    private RecyclerView recyclerView;
    private ProgressBar recyclerViewProgressBar;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_watchprofile_playdlist,
                container, false);
        spotifyRepository = new SpotifyRepository(getActivity());
        //currentUser = ((ActivityWatchProfile)getActivity()).getCurrentUser();

        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // init UI elements
        initLayoutElements();
        initRecyclerView();
        //initPlaydlist();
    }

    private void initLayoutElements() {
        //profileImage = view.findViewById(R.id.watchProfile_toolbarProfileImage);
        name = view.findViewById(R.id.watchProfile_toolbarName);
        recyclerView = view.findViewById(R.id.fragment_watchProfile_playdlist_recycler);
        recyclerViewProgressBar = view.findViewById(R.id.fragment_watchProfile_playdlist_recycler_progressBar);
        recyclerViewEmptyText = view.findViewById(R.id.fragment_watchProfile_playdlist_recycler_emptyText);

        //Picasso.get().load(currentUser.getProfilePicThumbnailUrl()).into(profileImage);
        //name.setText(currentUser.getName());
    }

    /*

    private void initPlaydlist() {
        String spotifyPlaydlistId = currentUser.getSpotifyPlaydlistId();
        spotifyRepository.getPlaylistById(spotifyPlaydlistId, 100, 0, new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                recyclerViewProgressBar.setVisibility(View.GONE);
                Playlist p = (Playlist) value;
                if (p == null || p.getTracks().size() == 0) {
                    // show noPlaydlistItems - TextView
                    String information = view.getResources().getString(R.string.thePlaydlistOf) + " " +
                                    currentUser.getName() + " " +
                                    view.getResources().getString(R.string.isEmptyYet);
                    recyclerViewEmptyText.setText(information);
                    recyclerViewEmptyText.setVisibility(View.VISIBLE);
                    return;
                }
                List<Playback> playbackItems = new ArrayList<>();
                for (Track track : p.getTracks()) {
                    playbackItems.add(Playback.getLocalPlayback(track));
                }
                recyclerAdapterPlaydlist.setPlaydlistItems(playbackItems);
                // make progressBar GONE and the recyclerView VISIBLE
                recyclerViewProgressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Object value) {
                recyclerViewProgressBar.setVisibility(View.GONE);
                // show noPlaydlistItems - TextView
                String information = view.getResources().getString(R.string.thePlaydlistOf) + " " +
                        currentUser.getName() + " " +
                        view.getResources().getString(R.string.isEmptyYet);
                recyclerViewEmptyText.setText(information);
                recyclerViewEmptyText.setVisibility(View.VISIBLE);
            }
        });
    }

    */

    private void initRecyclerView() {
        recyclerView.setVisibility(View.GONE);
        // make progressBar VISIBLE

        // set up the RecyclerView
        recyclerAdapterPlaydlist = new AdapterRecyclerPlaydlist(
                view.getContext(), recyclerView);

        LinearLayoutManager llm = new LinearLayoutManager(view.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(recyclerAdapterPlaydlist);
        recyclerView.setNestedScrollingEnabled(true);

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new
                ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(
                            final RecyclerView recyclerView,
                            final RecyclerView.ViewHolder viewHolder,
                            final RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                        final int dragFlags = 0;
                        final int swipeFlags = ItemTouchHelper.END;
                        return makeMovementFlags(dragFlags, swipeFlags);
                    }

                    @Override
                    public float getSwipeThreshold(RecyclerView.ViewHolder viewHolder) {
                        return 0.25f;
                    }

                    @Override
                    public float getSwipeEscapeVelocity(float defaultValue) {
                        return 0.6f;
                    }

                    @Override
                    public float getSwipeVelocityThreshold(float defaultValue) {
                        return 0.95f;
                    }

                    @Override
                    public void onChildDraw (Canvas c, RecyclerView recyclerView,
                                             RecyclerView.ViewHolder viewHolder,
                                             float dX, float dY, int actionState,
                                             boolean isCurrentlyActive){
                        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

                        Drawable background = ContextCompat.getDrawable(view.getContext(), R.drawable.gradient_like_spot);

                        try {
                            background.setBounds(0, viewHolder.itemView.getTop(),
                                    (int) (viewHolder.itemView.getLeft() + dX),
                                    viewHolder.itemView.getBottom());
                            background.draw(c);
                        } catch (NullPointerException ex) {
                            Log.d("NPE", ex.getMessage());
                        }

                    }

                    @Override
                    public void onSwiped(
                            final RecyclerView.ViewHolder viewHolder,
                            final int swipeDir) {
                        recyclerAdapterPlaydlist.notifyItemChanged(viewHolder.getAdapterPosition());

                        // TODO
                        //like(viewHolder.getAdapterPosition());

                    }
                };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(
                simpleItemTouchCallback
        );
        itemTouchHelper.attachToRecyclerView(recyclerView);

    }

    /*

    private void like(int position) {
        Snackbar snackbarLike = Snackbar
                .make(coordinatorLayout, view.getResources().getString(R.string.songAddedToPlaydlist), Snackbar.LENGTH_LONG)
                .setAction(view.getResources().getString(R.string.undo), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Snackbar snackbarDismiss = Snackbar.make(coordinatorLayout, view.getResources().getString(R.string.songRemovedFromPlaydlist), Snackbar.LENGTH_SHORT);
                        snackbarDismiss.show();
                    }
                });
        snackbarLike.show();
        snackbarLike.addCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar snackbar, int event) {
                TODO push the required information to the user's playdlist (maybe observer/eventBus?)
            }

            @Override
            public void onShown(Snackbar snackbar) {
            }
        });

    }

*/

}
