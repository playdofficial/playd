package com.ansgarkersch.playd.services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.ansgarkersch.playd.R;
import com.ansgarkersch.playd._spotify.SpotifyGlobalVariables;
import com.ansgarkersch.playd._spotify.repository.SpotifyRepository;
import com.ansgarkersch.playd._spotify.util.SpotifyUtilityMethods;
import com.ansgarkersch.playd.activities.ActivityMain;
import com.ansgarkersch.playd.exceptions.SpotifyInvalidTokenException;
import com.ansgarkersch.playd.globals.GlobalVariables;
import com.ansgarkersch.playd.model.Playback;
import com.ansgarkersch.playd.repository.FirebaseRepository;
import com.ansgarkersch.playd.repository.PlaybackRepository;
import com.ansgarkersch.playd.repository.ValueCallback;
import com.ansgarkersch.playd.util.UtilityMethods;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.spotify.android.appremote.api.ConnectionParams;
import com.spotify.android.appremote.api.Connector;
import com.spotify.android.appremote.api.SpotifyAppRemote;
import com.spotify.android.appremote.api.error.SpotifyConnectionTerminatedException;
import com.spotify.android.appremote.api.error.SpotifyDisconnectedException;
import com.spotify.protocol.client.Subscription;
import com.spotify.protocol.error.SpotifyAppRemoteException;
import com.spotify.protocol.types.PlayerState;

import java.util.Map;

/**
 * TODO comment
 */
public class ForegroundServiceTrackObserver extends Service {

    private Playback currentPlayback = null;

    // Notification
    private NotificationManager mNotificationManager;
    private final int notificationId = 2403; // The unique notification id

    // Spotify SDK
    private SpotifyAppRemote mSpotifyAppRemote;

    // Util
    private String TAG = getClass().getName() + " - ";
    private CountDownTimer playbackStateTimer = null; // CountDownTimer to avoid multiple BroadcastReciever Messages
    private final int millisToWaitUntilUpdatingDatabase = 800; // After X milliseconds, the user's playback gets updated
    private long playbackTimestamp = 0L;
    private long playbackCurrentMS = 0L;

    // Repositories
    private FirebaseRepository firebaseRepository;
    private SpotifyRepository spotifyRepository;
    private PlaybackRepository playbackRepository;

    // Firebase Services


    private DatabaseReference firebaseRealtimeDatabase = FirebaseDatabase.getInstance().getReference();
    private ValueEventListener currentSpotterCountListener;
    private long currentSpotterCount = 0L;

    // GeoFire
    DatabaseReference databaseReference;

    // Location Service
    LocationManager locationManager;
    Location lastKnownLocation = null;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            if (intent.hasCategory(GlobalVariables.SPOTIFY_OBSERVE_CURRENTUSERS_PLAYBACK)) {
                Log.e(getClass().getName(), "Recieved Start Command 'Observe current users playback'");
            } else if (intent.hasCategory(GlobalVariables.SPOTIFY_REMOVE_CURRENTUSERS_PLAYBACK_OBSERVER)) {
                Log.e(getClass().getName(), "Recieved Start Command 'Remove current users playback observer'");
                stopService(new Intent(this, ForegroundServiceTrackObserver.class));
            }
        }
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Notification
        Notification notification = buildNotification();
        startForeground(notificationId, notification);
        connect();

        // Repositories
        firebaseRepository = new FirebaseRepository();
        spotifyRepository = new SpotifyRepository(this);
        playbackRepository = new PlaybackRepository(this);

        // GeoFire
        databaseReference = FirebaseDatabase.getInstance().getReference(GlobalVariables.DB_USEROVERVIEW);

        // Location Service
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        initLocationReciever();

        addCurrentSpotterCountListener();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        deletePlayback();
        disconnect();
        if (playbackStateTimer != null) {
            playbackStateTimer.cancel();
        }

        removeCurrentSpotterCountListener();
    }

    private void initLocationReciever() {
        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                lastKnownLocation = location;
                Log.d(getClass().getName(), "Recieved new Location: " + location.getLatitude() + ", " + location.getLongitude());
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

        // Register the listener with the Location Manager to receive location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    }

    /*
    TODO COMMENT
     */
    private Notification buildNotification() {

        // Build Notification
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext(), "notify_2403");
        Intent ii = new Intent(getApplicationContext(), ActivityMain.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, ii, 0);

        mBuilder.setOnlyAlertOnce(true);
        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher_round);

        if (currentPlayback == null) {
            mBuilder.setContentTitle("Bereit zur Wiedergabe");
            mBuilder.setContentText("Zeig der Welt deine Musik");
        } else {
            mBuilder.setContentTitle("Du bist gerade ein Spot");
            mBuilder.setContentText("Du hast schon " + currentSpotterCount + " Zuhörer");
        }

        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setSound(null);

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_round);
        mBuilder.setLargeIcon(bitmap);

        mNotificationManager =
                (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        // Handle older Android Versions
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("notify_2403",
                    "Playd Foreground Service Track Observer",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setSound(null, null);
            mNotificationManager.createNotificationChannel(channel);
        }

        return mBuilder.build();
    }

    private void editNotification() {
        Notification notification = buildNotification();
        mNotificationManager.notify(notificationId, notification);
    }

    /**
     * Connects to the Spotify Service
     */
    private void connect() {
        if (mSpotifyAppRemote != null) {
            return;
        }

        ConnectionParams connectionParams = new ConnectionParams.Builder(SpotifyGlobalVariables.CLIENT_ID)
                .setRedirectUri(SpotifyGlobalVariables.REDIRECT_URI)
                .showAuthView(false)
                .build();

        SpotifyAppRemote.connect(this, connectionParams, new Connector.ConnectionListener() {
            @Override
            public void onConnected(SpotifyAppRemote spotifyAppRemote) {
                Log.e(getClass().getName(), "SpotifyAppRemote - " + "Successfully initialized");
                mSpotifyAppRemote = spotifyAppRemote;

                mSpotifyAppRemote.getPlayerApi().subscribeToPlayerState().setEventCallback(new Subscription.EventCallback<PlayerState>() {
                    @Override
                    public void onEvent(PlayerState playerState) {

                        if (playbackStateTimer != null) {
                            playbackStateTimer.cancel();
                            playbackTimestamp = UtilityMethods.getCurrentTime();
                        }

                        if (playerState == null) {
                            deletePlayback();
                            return;
                        }

                        if (playerState.track == null) {
                            deletePlayback();
                            return;
                        }

                        playbackCurrentMS = playerState.playbackPosition;

                        playbackStateTimer = new CountDownTimer(millisToWaitUntilUpdatingDatabase, 1000) {
                            public void onTick(long millisUntilFinished) {
                            }

                            public void onFinish() {

                                if (playerState.isPaused) {
                                    Log.d(TAG, "Playback with id " + playerState.track.uri + " is paused");
                                    deletePlayback();
                                    return;
                                }

                                Log.d(TAG, "Playback with id " + playerState.track.uri + " is started");

                                // build playback object and upload
                                Playback playbackToUpload = new Playback();
                                playbackToUpload.setUserId(firebaseRepository.getCurrentUser().getUid());
                                playbackToUpload.setCurrentMS(playbackCurrentMS);
                                playbackToUpload.setTrackStartedTimestamp(playbackTimestamp);
                                playbackToUpload.setTrackId(SpotifyUtilityMethods.trimTrackId(playerState.track.uri));
                                //playbackToUpload.setSpotName(); TODO get status from SharedPreferences
                                spotifyRepository.getGenresByArtist(SpotifyUtilityMethods.trimTrackId(playerState.track.artist.uri)
                                        , new ValueCallback() {
                                            @Override
                                            public void onSuccess(Object value) {
                                                Map<String, Boolean> trackGenres = (Map<String, Boolean>) value;
                                                playbackToUpload.setTrackGenres(trackGenres);
                                                uploadPlayback(playbackToUpload);
                                                editNotification();
                                            }

                                            @Override
                                            public void onFailure(Object value) {

                                            }
                                        });

                            }
                        }.start();

                    }
                });

            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.e(getClass().getName(), "Failure - SpotifyAppRemote not manually disconnected");
                disconnect();
                if (throwable instanceof SpotifyConnectionTerminatedException
                        || throwable instanceof SpotifyDisconnectedException) {
                    Log.e(getClass().getName(), "Spotify is disconnected.");
                } else if (throwable instanceof SpotifyInvalidTokenException) {
                    Log.e(getClass().getName(), "Spotify Token is invalid - " + throwable.getMessage());
                } else if (throwable instanceof SpotifyAppRemoteException) {
                    Log.e(getClass().getName(), "SpotifyAppRemoteException - " + throwable.getMessage());
                    // TODO Meldung an Benutzer?
                    // throwed after catching TimeoutException
                } else {
                    Log.e(getClass().getName(), "SpotifyAppRemote - UnknownException - " + throwable.getMessage());
                }

                // TODO mehr Exceptions (zB Spotify not installed etc) und besseres Handling
            }
        });

    }

    /**
     * Disconnects from the Spotify Service if necessary e.g. mSpotifyRemote is null and the status is 'disconnected'
     */
    private void disconnect() {
        Log.e(getClass().getName(), "SpotifyAppRemote - Disconnecting...");
        if (mSpotifyAppRemote != null && mSpotifyAppRemote.isConnected()) {
            SpotifyAppRemote.disconnect(mSpotifyAppRemote);
            Log.e(getClass().getName(), "SpotifyAppRemote - Disconnected");
        }
        mSpotifyAppRemote = null;
    }

    private void addCurrentSpotterCountListener() {
        // Initialize SpotterCount Listener
        // 1) Remove Value Event Listener if it's already active
        if (currentSpotterCountListener != null) {
            firebaseRealtimeDatabase.child(GlobalVariables.DB_CURRENTSPOTTER_COUNT)
                    .removeEventListener(currentSpotterCountListener);
        }
        // 1) Create new Value Event Listener
        currentSpotterCountListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    currentSpotterCount = 0L;
                } else {
                    currentSpotterCount = (long) dataSnapshot.getValue();
                }
                // Update UI spotterCount
                editNotification();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
                currentSpotterCount = 0L;
                // Update UI spotterCount
                editNotification();
            }
        };
        firebaseRealtimeDatabase
                .child(GlobalVariables.DB_CURRENTSPOTTER_COUNT)
                .child(firebaseRepository.getCurrentUser().getUid())
                .addValueEventListener(currentSpotterCountListener);

    }

    private void removeCurrentSpotterCountListener() {
        firebaseRealtimeDatabase.removeEventListener(currentSpotterCountListener);
    }

    private void uploadPlayback(Playback playback) {
        currentPlayback = playback;
        // Update the playbackOverview object including the trackId
        playbackRepository.updateCurrentUsersPlaybackOverview(playback.getTrackId(), new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                // todo
            }

            @Override
            public void onFailure(Object value) {
                // todo
            }
        });

        // Update the playback object including all informations
        playbackRepository.updateCurrentUsersPlaybackData(playback, new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                // todo
            }

            @Override
            public void onFailure(Object value) {
                // todo
            }
        });
    }

    private void deletePlayback() {
        currentPlayback = null;
        // Delete the playbackOverview object including the trackId
        playbackRepository.deleteCurrentUsersPlaybackOverview(new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                // todo
            }

            @Override
            public void onFailure(Object value) {
                // todo
            }
        });

        // Delete the playback object including all informations
        playbackRepository.deleteCurrentUsersPlaybackData(new ValueCallback() {
            @Override
            public void onSuccess(Object value) {
                // todo
            }

            @Override
            public void onFailure(Object value) {
                // todo
            }
        });
    }

}
