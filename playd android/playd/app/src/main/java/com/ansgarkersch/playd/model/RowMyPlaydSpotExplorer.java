package com.ansgarkersch.playd.model;

import com.ansgarkersch.playd.globals.GlobalVariables;

public class RowMyPlaydSpotExplorer implements IScrollable {
    @Override
    public int getType() {
        return GlobalVariables.VIEWTYPE_SPOTEXPLORER;
    }
}
