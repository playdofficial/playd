package com.ansgarkersch.playd.model;

import com.ansgarkersch.playd.globals.GlobalVariables;

public class Advertisement implements ISwipeable, IScrollable {

    private String adId;
    private String adSourceUrl;
    private String adLinkUrl;

    public Advertisement() {}

    public Advertisement(String adId, String adSourceUrl, String adLinkUrl) {
        this.adId = adId;
        this.adSourceUrl = adSourceUrl;
        this.adLinkUrl = adLinkUrl;
    }

    @Override
    public int getType() {
        return GlobalVariables.VIEWTYPE_ADVERTISEMENT;
    }

    public String getAdId() {
        return adId;
    }

    public void setAdId(String adId) {
        this.adId = adId;
    }

    public String getAdSourceUrl() {
        return adSourceUrl;
    }

    public void setAdSourceUrl(String adSourceUrl) {
        this.adSourceUrl = adSourceUrl;
    }

    public String getAdLinkUrl() {
        return adLinkUrl;
    }

    public void setAdLinkUrl(String adLinkUrl) {
        this.adLinkUrl = adLinkUrl;
    }
}
