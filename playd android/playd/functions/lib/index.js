"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp({
    credential: admin.credential.applicationDefault(),
    databaseURL: "https://well-playd.firebaseio.com"
});
const firestore = admin.firestore();
const database = admin.database();
const https = require('https');
// FCM Categories
const actionCategoryPlayback = "FCM_CATEGORY_PLAYBACK";
// FCM Action Codes
const actionCodePlaybackStarted = "FCM_ACTIONCODE_PLAYBACK_STARTED";
const actionCodePlaybackTerminated = "FCM_ACTIONCODE_PLAYBACK_TERMINATED";
/**
 ***************************************************************************************************************************************************
 ********* ASYNC FUNCTIONS *************************************************************************************************************************
 ***************************************************************************************************************************************************
 */
/**
 * BASE FUNCTION: FETCH USER BY GIVEN ID
 */
function fetchUserById(userId) {
    return __awaiter(this, void 0, void 0, function* () {
        console.log('Get User with Id: ' + userId);
        // 3 Promises:
        // 1) Get User by Id
        // 2) Get all Followed User Ids
        // 3) Get all Followers User Ids
        // in Promise.all -> add Followed & Followers to the User Object -> send user 
        const promises = [];
        let user;
        let followedIds;
        let followersIds;
        promises.push(
        // Get user
        firestore.collection('users').doc(userId).get()
            .then(snapshotUser => {
            if (snapshotUser.exists) {
                user = snapshotUser.data();
                user.id = userId;
            }
            else {
                return null;
            }
        })
            .catch(err => {
            console.log('Error getting document', err);
            return null;
        }));
        promises.push(
        // Get followed UIds
        firestore.collection("followed").doc(userId).get()
            .then(followedIdsSnapshot => {
            if (followedIdsSnapshot.exists) {
                followedIds = followedIdsSnapshot.data();
            }
            else {
                followedIds = {};
            }
        })
            .catch((error) => {
            console.log('Error updating database:', error);
            return null;
        }));
        promises.push(
        // Get followers UIds
        firestore.collection("followers").doc(userId).get()
            .then(followersIdsSnapshot => {
            if (followersIdsSnapshot.exists) {
                followersIds = followersIdsSnapshot.data();
            }
            else {
                followersIds = {};
            }
        })
            .catch((error) => {
            console.log('Error updating database:', error);
            return null;
        }));
        // edit user object and send it as response
        return Promise.all(promises)
            .then(() => {
            user.followedIds = followedIds;
            user.followersIds = followersIds;
            return user;
        })
            .catch((e) => {
            console.log('Error:', e);
            return null;
        });
    });
}
/**
 ***************************************************************************************************************************************************
 ********* FIRESTORE TRIGGER ***********************************************************************************************************************
 ***************************************************************************************************************************************************
 */
/**
 * FIRESTORE TRIGGER: CREATE FOLLOWED/FOLLOWERS ENTRY
 */
exports.createFollowedEntry = functions.firestore
    .document('users/{userId}')
    .onCreate((snap, context) => {
    const userId = context.params.userId;
    console.log('New User created. Creating followed/followers entries');
    /*
    FOLLOWED
    */
    firestore.doc('followed/' + userId).set({})
        .then(() => {
        console.log('Successfully created followedEntry');
    }).catch(function (error) {
        console.log('Error creating followedEntry:', error);
    });
    /*
    FOLLOWERS
    */
    firestore.doc('followers/' + userId).set({})
        .then(() => {
        console.log('Successfully created followersEntry');
    }).catch(function (error) {
        console.log('Error creating followersEntry:', error);
    });
});
/**
* FIRESTORE TRIGGER: UPDATE THE CURRENT SPOTTER COUNT IN REALTIME DATABASE
*/
exports.updateCurrentSpotterCount = functions.firestore
    .document('currentSpotter/{userId}')
    .onWrite((change, context) => {
    const id = context.params.userId;
    if (!change.after.exists) {
        // Document was deleted
        // Delete currentSpotterCount in RealtimeDatabase
        database
            .ref('currentSpotterCount')
            .child(id)
            .remove()
            .then(() => {
            console.log('Successfully deleted currentSpotterCount with id '
                + id);
        })
            .catch(function (error) {
            console.log('Error deleting currentSpotterCount with id '
                + id + ': ', error);
        });
    }
    else {
        // Document was created or updated
        // Update currentSpotterCount in RealtimeDatabase
        let countAfter = 1, countBefore = 0;
        if (change.after.data() !== undefined) {
            countAfter = Object.keys(change.after.data()).length;
        }
        if (change.before.data() !== undefined) {
            countBefore = Object.keys(change.before.data()).length;
        }
        // Build map entry
        const data = {};
        data[id] = countAfter;
        database
            .ref('currentSpotterCount')
            .update(data)
            .then(() => {
            console.log('Successfully updated currentSpotterCount with id '
                + id
                + ' with old value: '
                + countBefore
                + ' and new value: '
                + countAfter);
        })
            .catch(function (error) {
            console.log('Error updating currentSpotterCount with id '
                + id + ': ', error);
        });
    }
});
/**
* FIRESTORE TRIGGER: PLAYBACK CREATED
*/
exports.playbackCreated = functions.firestore
    .document('playback/{userId}')
    .onCreate((snap, context) => {
    const userId = context.params.userId;
    console.log('A new playback with ID ' + userId + ' was created');
    // Notify all topic subscribers
    const message = {
        data: {
            "category": actionCategoryPlayback,
            "actionCode": actionCodePlaybackStarted,
            "userId": userId
        },
        topic: '/topics/' + userId
    };
    admin.messaging().send(message)
        .then(() => {
        // Response is a message ID string.
        console.log('Successfully sent FCM:');
    })
        .catch((error) => {
        console.log('Error sending message:', error);
    });
});
/**
* FIRESTORE TRIGGER: PLAYBACK TERMINATED
*/
exports.playbackTerminated = functions.firestore
    .document('playback/{userId}')
    .onDelete((snap, context) => {
    const userId = context.params.userId;
    console.log('The playback with ID ' + userId + ' was deleted');
    // Notify all topic subscribers
    const message = {
        data: {
            "category": actionCategoryPlayback,
            "actionCode": actionCodePlaybackTerminated,
            "userId": userId
        },
        topic: '/topics/' + userId
    };
    admin.messaging().send(message)
        .then(() => {
        // Response is a message ID string.
        console.log('Successfully sent FCM:');
    })
        .catch((error) => {
        console.log('Error sending message:', error);
    });
});
/**
 ***************************************************************************************************************************************************
 ********* HTTP FUNCTIONS **************************************************************************************************************************
 ***************************************************************************************************************************************************
 */
exports.loginWithSpotify = functions.https.onRequest((req, res) => {
    console.log('Trying to login with Spotify');
    const accessToken = req.header('AccessToken');
    const spotifyUserId = req.header('SpotifyUserId');
    https.get({
        host: 'api.spotify.com',
        path: '/v1/me',
        headers: {
            Accept: 'application/json',
            Authorization: 'Bearer ' + accessToken
        }
    }, function (spotifyRes) {
        if (spotifyRes.statusCode !== 200) {
            // Access denied
            res.send('Not authorized');
            return;
        }
        else {
            // Continuously update stream with data
            let body = '';
            let user;
            spotifyRes.on('data', function (d) {
                body += d;
            });
            spotifyRes.on('end', function () {
                user = JSON.parse(body);
                // Verify that the recieved spotifyUserId is equal the requested spotifyUserId
                if (user.id !== spotifyUserId) {
                    console.log('The requesting user is not the owner of the account identified by the accessToken. Consider blocking this IP');
                    res.send('Not authorized');
                    return;
                }
                // Permission granted
                // Check if a user with the recieved spotifyUserId exists in the playd firestore database
                firestore.collection('users').where('spotifyUserId', '==', spotifyUserId)
                    .get().then(result => {
                    if (result.empty) {
                        // The user is not registered yet
                        // Create a new account with the spotify user information and create a custom token afterwards
                        let newUser = {
                            spotifyUserId: spotifyUserId,
                            username: spotifyUserId,
                            name: spotifyUserId,
                            birthday: "",
                            relationshipStatus: "NOT_YOUR_BUSINESS",
                            gender: "NOT_YOUR_BUSINESS",
                            nationality: "NOT_YOUR_BUSINESS",
                            description: "",
                            spotifyPlaydlistId: "",
                            premiumLevel: "BASIC",
                            purchaseToken: "",
                            lastLoggedInTimestamp: 0,
                            isFamous: false,
                            preferredMusicGenres: []
                        };
                        let userOverview = {
                            u: newUser.username
                        };
                        // ------ Skipped until it's obvious how to upload storage images with admin SDK
                        /*
                        // Check for profileImage
                        if (user.images.length > 0) {
                          if (user.images[0].url === undefined) {
                            // The user has to image set in his spotify account
                            userOverview['p'] = "";
                          } else {
                            // The user has a image set in his spotify account, use this image as profile image
                            newUser['profileImages'] = {
                              '0': user.images[0].url
                            };
                            userOverview['p'] = user.images[0].url;
                          }
                        } else {
                          userOverview['p'] = "";
                        }
                        */
                        firestore.collection('users')
                            .doc(spotifyUserId)
                            .set(newUser)
                            .then(function () {
                            // Send token back to client
                            console.log('Successfully created user with userId ' + spotifyUserId + ' in the firestore database');
                            // Create UserOverview
                            database
                                .ref('userOverview')
                                .child(spotifyUserId)
                                .set({
                                u: newUser.username
                            })
                                .then(() => {
                                console.log('Successfully created UserOverview with userId ' + spotifyUserId + ' and username ' + newUser.username);
                                // Send token back to client
                                admin.auth().createCustomToken(spotifyUserId)
                                    .then(function (customToken) {
                                    // Send token back to client
                                    console.log('Successfully created CustomToken with userId ' + spotifyUserId);
                                    res.send(JSON.stringify(customToken));
                                })
                                    .catch(function (error) {
                                    console.log("Error creating custom token:", error);
                                    res.send('Not authorized');
                                });
                            })
                                .catch(function (error) {
                                console.log("Error creating custom token:", error);
                            });
                        })
                            .catch(function (error) {
                            console.log('Error creating user with userId ' + spotifyUserId + ' in firestore database:', error);
                            res.send('Error creating user');
                        });
                    }
                    else {
                        // This user is already registered
                        // Create a custom token with his playdId and send back to client
                        admin.auth().createCustomToken(result.docs[0].id)
                            .then(function (customToken) {
                            // Send token back to client
                            console.log('Successfully created CustomToken with userId ' + result.docs[0].id);
                            res.send(JSON.stringify(customToken));
                        })
                            .catch(function (error) {
                            console.log("Error creating custom token:", error);
                            res.send('Not authorized');
                        });
                    }
                }).catch(err => {
                    console.log('Error getting document', err);
                    res.send('Not authorized');
                });
            });
        }
    }).on('error', function (e) {
        console.error(e);
        res.send('Not authorized');
    });
});
/**
 * SEND MESSAGE TO DEVICE
 * Send a message to an inidvidual device, e.g. for notifying the user about a subscription or an unsubscription
 */
exports.sendMessageToDevice = functions.https.onRequest((req, res) => {
    console.log('Trying to access with token: ' + req.header('Authorization'));
    admin.auth().verifyIdToken(req.header('Authorization'))
        .then(function (decodedToken) {
        const originUserId = decodedToken.uid.toString();
        const foreignUserId = req.body.foreignUserId;
        const category = req.body.category;
        const actionCode = req.body.actionCode;
        database.ref("registrationTokens").child(foreignUserId).once("value")
            .then((passiveRegistrationToken) => {
            const message = {
                data: {
                    "category": category,
                    "actionCode": actionCode,
                    "activeParticipant": originUserId,
                    "passiveParticipant": foreignUserId
                },
                android: {
                    ttl: 3600 * 1000
                },
                apns: {
                    headers: {
                        priority: "high"
                    },
                },
                topic: passiveRegistrationToken.val()
            };
            admin.messaging().send(message)
                .then(() => {
                console.log('Successfully sent FCM ' + '->', message);
            })
                .catch((error) => {
                console.log('Error sending message:', error);
            });
        })
            .catch((error) => {
            console.log('Error getting registrationToken:', error);
        });
    }).catch(function (error) {
        res.sendStatus(401).send('Error - Not authorized');
    });
});
/**
 * UPDATE USERS REGISTRATION TOKEN
 */
exports.updateRegistrationToken = functions.https.onRequest((req, res) => {
    console.log('Trying to access with token: ' + req.header('Authorization'));
    admin.auth().verifyIdToken(req.header('Authorization'))
        .then(function (decodedToken) {
        // request is o.k.
        const userId = decodedToken.uid.toString();
        const registrationToken = req.header('RegistrationToken');
        const data = { [userId]: registrationToken };
        database.ref("registrationTokens").update(data)
            .then(() => {
            res.sendStatus(200).send('RegistrationToken successfully updated');
        }).catch(function (error) {
            res.sendStatus(401).send('Updating registrationToken failed');
        });
    }).catch(function (error) {
        res.send('Error - Not authorized');
    });
});
/**
 * GET ALL CURRENTLY PLAYING GENRES
 */
exports.getAllCurrentlyPlayingGenres = functions.https.onRequest((req, res) => {
    console.log('Trying to access with token: ' + req.header('Authorization'));
    admin.auth().verifyIdToken(req.header('Authorization'))
        .then(function (decodedToken) {
        // request is o.k.
        const result = [];
        const promises = [];
        const userId = decodedToken.uid.toString();
        promises.push(firestore.collection('playback')
            .orderBy("trackStartedTimestamp", "desc")
            .limit(30)
            .get()
            .then(snapshot => {
            snapshot.forEach(doc => {
                if (doc.id !== userId) {
                    const genreField = doc.data()["trackGenres"];
                    if (genreField !== undefined) {
                        Object.keys(genreField).forEach(function (genreKey) {
                            result.push(genreKey);
                        });
                    }
                }
            });
        })
            .catch(err => {
            console.log('Error getting documents', err);
        }));
        Promise.all(promises)
            .then(() => {
            res.send(JSON.stringify(result));
        })
            .catch((e) => {
            res.sendStatus(400).send('Error');
        });
    }).catch(function (error) {
        res.sendStatus(401).send('Not authorized');
    });
});
/**
 * GET ALL CURRENT PLAYBACKS BY GENRE
 */
exports.getAllCurrentPlaybacksByGenre = functions.https.onRequest((req, res) => {
    console.log('Trying to access with token: ' + req.header('Authorization'));
    admin.auth().verifyIdToken(req.header('Authorization'))
        .then(function (decodedToken) {
        // request is o.k.
        const genre = req.query.genre;
        firestore.collection('playback')
            .limit(30)
            .orderBy('trackStartedTimestamp', 'desc')
            .where('trackGenres', 'array-contains', genre)
            .get()
            .then(listOfPlaybacks => {
            res.sendStatus(200).send(JSON.stringify(listOfPlaybacks));
        })
            .catch(err => {
            console.log('Error getting document', err);
            res.sendStatus(404).send('Error getting document');
        });
    }).catch(function (error) {
        res.sendStatus(401).send('Not authorized');
    });
});
/**
 * GET USER BY ID
 */
exports.getUserById = functions.https.onRequest((req, res) => {
    console.log('Trying to access with token: ' + req.header('Authorization'));
    admin.auth().verifyIdToken(req.header('Authorization'))
        .then(function (decodedToken) {
        const userId = req.query.userId;
        fetchUserById(userId)
            .then(user => {
            if (user === null) {
                res.sendStatus(404).send('User does not exist');
            }
            else {
                // check if the requesting user is the owner of the account he's looking for
                if (userId !== decodedToken.uid.toString()) {
                    // it's not own account
                    user.lastLoggedInTimestamp = undefined;
                }
                res.sendStatus(200).send(JSON.stringify(user));
            }
        })
            .catch((error) => {
            console.log('Error fetching User', error);
            res.sendStatus(404).send('User does not exist');
        });
    }).catch(function (error) {
        res.sendStatus(401).send('Error - Not authorized');
    });
});
//# sourceMappingURL=index.js.map