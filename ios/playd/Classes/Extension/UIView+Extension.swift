//
//  UIView.swift
//  playd
//
//  Created by Ronald Smith Djomkam Yotedje on 22.01.19.
//  Copyright © 2019 Ronald Smith Djomkam Yotedje. All rights reserved.
//

import Foundation

import UIKit

extension UINib {
    func instantiate() -> Any? {
        return self.instantiate(withOwner: nil, options: nil).first
    }
}

extension UIView {
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    static func instantiate(autolayout: Bool = false) -> Self {
        // generic helper function
        func instantiateUsingNib<T: UIView>(autolayout: Bool) -> T {
            let view = self.nib.instantiate() as! T
            view.translatesAutoresizingMaskIntoConstraints = !autolayout
            return view
        }
        
        return instantiateUsingNib(autolayout: autolayout)
    }
}
