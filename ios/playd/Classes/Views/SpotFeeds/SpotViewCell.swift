//
//  SpotViewCell.swift
//  playd
//
//  Created by Ronald Smith Djomkam Yotedje on 16.01.19.
//  Copyright © 2019 Ronald Smith Djomkam Yotedje. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class SpotViewCell : UITableViewCell {
    
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var liveSince: UILabel!
    
    @IBOutlet weak var spotTitle: UILabel!
    @IBOutlet weak var albumName: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var albumIcon: UIImageView!
    @IBOutlet weak var views: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        userIcon.layer.borderWidth = 1
        userIcon.layer.borderColor = UIColor.white.cgColor
    }
    
}

extension SpotViewCell {
    
    func bundle() -> Bundle {
        return Bundle(for: type(of: self))
    }
    
}
