//
//  OverviewController.swift
//  playd
//
//  Created by Ronald Smith Djomkam Yotedje on 08.01.19.
//  Copyright © 2019 Ronald Smith Djomkam Yotedje. All rights reserved.
//

import Foundation
import UIKit
import Foundation

extension OverviewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.spots.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        let spot = tableView.dequeueReusableCell(withIdentifier: self.tableCellName) as! SpotViewCell
        
        var spotVal = self.spots[index]
        spot.userIcon.image = UIImage(named: spotVal[0])
        spot.userName.text = spotVal[1]
        spot.liveSince.text = spotVal[2]
        spot.spotTitle.text = spotVal[3]
        spot.albumName.text = spotVal[4]
        spot.artistName.text = spotVal[5]
        spot.albumIcon.image = UIImage(named: spotVal[6])
        spot.views.text = spotVal[7]
        
        return spot
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 210
    }
    
}

extension OverviewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

class OverviewController : AbstractViewController {
    
    @IBOutlet weak var header: UIView!
    @IBOutlet weak var locationView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var profil: UIButton!
    @IBOutlet weak var chat: UIButton!
    
    
    @IBOutlet weak var bottomBar: UIView!
    @IBOutlet weak var home: UIButton!
    @IBOutlet weak var search: UIButton!
    
    var homeIsActive = true
    
    @IBOutlet weak var playButton: UIButton!
    
    var tableCellName = String(describing: SpotViewCell.self)
    
    var spots: [[String]] = []
    
    var height: CGFloat = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSpotViews()
        
        registerTableViewCells()
        
        playButton.layer.borderWidth = 1
        playButton.layer.borderColor = UIColor.white.cgColor
        
        playButton.setImage(UIImage(named: spots[4][6]), for: .normal)
        
        search.alpha = 0.6
        
        view.bringSubviewToFront(playButton)
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    // MARK - Helper methods
    
    func registerTableViewCells() {
        tableView.register(SpotViewCell.nib, forCellReuseIdentifier: tableCellName)
    }
    
    func setupSpotViews() {
        spots = [
            ["rkellyuser", "user1", "seit 55 minuten | 12 km entfernt", "First Spot", "Double Up", "R.Kelly", "rkelly", "👁 markussch1 und 1K andere"],
            ["50centsuser", "user2", "seit 55 minuten | 12 km entfernt", "Second Spot", "Curtis", "50 Cents", "50cents", "👁 markussch2 und 1K andere"],
            ["xavieruser", "user3", "seit 55 minuten | 12 km entfernt", "Third Spot", "Nicht von dieser Welt", "Xavier Naidoo", "xavier", "👁 markussch3 und 1K andere"],
            ["celineuser", "user4", "seit 55 minuten | 12 km entfernt", "Fourth Spot", "Taking chances", "Celine Dion", "celine", "👁 markussch4 und 1K andere"],
            ["akonuser", "user5", "seit 55 minuten | 12 km entfernt", "Fifth Spot", "Konvicted", "Akon", "akon", "👁 markussch5 und 1K andere"]
        ]
    }
    
    @IBAction func searchHandler(_ sender: Any) {
        if homeIsActive {
            search.alpha = 1.0
            
            home.alpha = 0.6
            homeIsActive = false
        }
    }
    
    @IBAction func homeHandler(_ sender: Any) {
        if !homeIsActive {
            home.alpha = 1.0
            
            search.alpha = 0.6
            homeIsActive = true
        }
    }
    
}
