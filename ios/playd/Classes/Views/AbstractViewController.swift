//
//  AbstractViewController.swift
//  playd
//
//  Created by Ronald Smith Djomkam Yotedje on 14.01.19.
//  Copyright © 2019 Ronald Smith Djomkam Yotedje. All rights reserved.
//

import Foundation
import UIKit

class AbstractViewController : UIViewController {
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
}
